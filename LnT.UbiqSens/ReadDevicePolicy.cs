﻿////-----------------------------------------------------------------------
//// <copyright file="ReadDevicePolicy.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

namespace LnT.UbiqSens
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml;
    using LnT.UbiqSens.Helpers;
    using LnT.UbiqSens.PluginInterfaces;

    /// <summary>
    /// Class Read Device Policy
    /// </summary>
    public class ReadDevicePolicy
    {
        /// <summary>
        /// Passive Device Dictionary
        /// </summary>
        public static ConcurrentDictionary<string, Dictionary<string, string>> PassiveDeviceIdDictionary = new ConcurrentDictionary<string, Dictionary<string, string>>();

        /// <summary>
        /// Listening Ports List
        /// </summary>
        public List<string> ListeningPortsList = new List<string>();

        /// <summary>
        /// Initiate Read Device Policy
        /// </summary>
        public void Initiate()
        {
            if (Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + Constants.DevicePolicyPath))
            {
                var devicePolicyFiles = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory + Constants.DevicePolicyPath);
                foreach (var devicePolicyFile in devicePolicyFiles)
                {
                    var str = File.ReadAllText(devicePolicyFile);

                    if (UbiqsensBooter.IsSecure)
                    {
                        //Read Encrypted Deviec type XML
                        str = AES128.GetInstance().Decrypt(str, UbiqsensBooter.SecurityKey);
                    }

                    try
                    {
                        this.InitiatePassiveDevice(str, true);
                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine("Error in Reading device policy. Ex" + ex.Message);
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "ReadDevicePolicy: Error in Reading device policy. Initiate() Method. @Ex: ", ex);
                    }
                }
            }
        }

        ///// <summary>
        ///// Initiate Active Device
        ///// </summary>
        ///// <param name="objReqRes">object Request Response</param>
        ///// <param name="objReqResHlr">Object of RequestContinuousResponseHandler</param>
        ///// <param name="objReqRes">Request Continuous Response Type</param>
        ///// <param name="objRrch">Request Continuous Response Handler</param>
        //public void InitiateActiveDevice(RequestContinuousResponseType objReqRes, RequestContinuousResponseHandler objReqResHlr)
        //{
        //    var task = Task.Factory.StartNew(() => objReqResHlr.Handle(objReqRes));
        //    QueueManager.Tasks.Add(objReqRes.DeviceId, task);
        //}

        /// <summary>
        /// Read policy
        /// </summary>
        /// <param name="devicePolicy">device Policy</param>
        internal void InitiatePassiveDevice(string devicePolicy, bool isInit)
        {
            try
            {
                var subDoc = new XmlDocument();
                var doc = new XmlDocument();
                doc.LoadXml(devicePolicy);
                var nodeList = doc.GetElementsByTagName(Constants.Device);

                foreach (XmlNode deviceNode in nodeList)
                {
                    try
                    {
                        subDoc.LoadXml(deviceNode.OuterXml);
                        var deviceType = subDoc.GetElementsByTagName(Constants.DeviceType)[0].InnerText;
                        var isVirtual = DeviceTypeLookUp.IsVirtualDeviceType(deviceType);
                        if (isVirtual)
                        {
                            Dictionary<string, string> tempDictionary = null;
                            var objDeviceTypeLookUp = new DeviceTypeLookUp();
                            var obj = objDeviceTypeLookUp.GetAcceptConnectionEthernetParameters(devicePolicy, out tempDictionary);
                            PassiveDeviceIdDictionary.TryAdd(subDoc.GetElementsByTagName(Constants.Deviceidd)[0].InnerText, tempDictionary);

                            foreach (var keys in tempDictionary.ToList())
                            {
                                tempDictionary[keys.Key] = subDoc.GetElementsByTagName(keys.Key)[0].InnerText;
                            }
                            if (isInit)
                            {
                                if (!this.ListeningPortsList.Contains(obj.ListenerPort))
                                {
                                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Init Listener Port: " + obj.ListenerPort);
                                    this.ListeningPortsList.Add(obj.ListenerPort);
                                    var dtg = new DeviceToGateway();
                                    var deviceTypeThread = new Thread(new ThreadStart(() => dtg.EthernetGatewayConnectionInitiator(obj)));
                                    deviceTypeThread.IsBackground = false;
                                    deviceTypeThread.Priority = ThreadPriority.Highest;
                                    deviceTypeThread.Start();
                                    UbiqsensBooter.RunningThreads.Add(deviceTypeThread);
                                    //var locationserviceThread = new Thread(new ThreadStart(() => dtg.LocationServiceConnectionInitiator(obj)));
                                    //locationserviceThread.IsBackground = false;
                                    //locationserviceThread.Priority = ThreadPriority.Normal;
                                    //locationserviceThread.Start();
                                    //UbiqsensBooter.RunningThreads.Add(locationserviceThread);
                                }
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "ReadDevicePolicy: Error in device policy with Virtual have invalid information about plugins, InitiatePassiveDevice() Method. @Ex: ", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "ReadDevicePolicy: Error Occurred at InitiatePassiveDevice() Method with @DevicePolicy: " + devicePolicy, ex);
            }
        }
    }
}
