﻿////-----------------------------------------------------------------------
//// <copyright file="DataDispatcher.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

using System.Collections.Concurrent;
using System.Text.RegularExpressions;
using System.Xml;

namespace LnT.UbiqSens
{
    using System;
    using System.Collections.Generic;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading;
    using System.Linq;
    using System.Threading.Tasks;
    using LnT.UbiqSens.Helpers;
    using LnT.UbiqSens.PluginInterfaces;

    /// <summary>
    /// Class data dispatcher
    /// </summary>
    public class DataDispatcher
    {
        /// <summary>
        /// Message to receive
        /// </summary>
        private const int MsgToRetrieve = 20;

        /// <summary>
        /// total message pushed
        /// </summary>
        private static int totalMsgPushed = 0;

        /// <summary>
        /// total message saved
        /// </summary>
        //private static int totalMsgSaved = 0;

        /// <summary>
        /// object SHA256 Crypto Service Provider
        /// </summary>
        private static SHA256CryptoServiceProvider objSha256CryptoServiceProvider;

        /// <summary>
        /// Initializes static members of the DataDispatcher class
        /// </summary>
        static DataDispatcher()
        {
            objSha256CryptoServiceProvider = new SHA256CryptoServiceProvider();
        }

        /// <summary>
        /// Push data
        /// </summary>
        //public static void PushData()
        //{
        //    // Console.WriteLine("PushData in progress");
        //    while (UbiqsensBooter.ToStore && UbiqSens.Ubiqsens.isRunning)
        //    {
        //        try
        //        {
        //            var datalist = UbiqsensBooter.ObjStorageService.RetreiveData(MsgToRetrieve);
        //            //// Console.WriteLine("Retrieved msg " + datalist.Count);

        //            if (datalist.Count == 0)
        //            {
        //                Thread.Sleep(1000);
        //                continue;
        //            }

        //            //// Console.WriteLine("Before retreive " + DateTime.Now);
        //            foreach (var data in datalist)
        //            {
        //                Dispatch(data);
        //            }

        //            // Console.WriteLine("After push" + DateTime.Now);
        //            if (datalist.Count > 0)
        //            {
        //                UbiqsensBooter.ObjStorageService.DeleteMessage(datalist.Count);
        //            }
        //            Thread.Sleep(10);
        //        }
        //        catch (Exception ex)
        //        {
        //            // Console.WriteLine("Error in Pushing data from DB. Ex:" + ex.Message);
        //            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in Pushing data directly form Queue. Ex:" + ex.Message);
        //        }
        //    }


        /// <summary>
        /// Push data
        /// </summary>
        public static void PushStatusData()
        {
            //System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            while (UbiqSens.Ubiqsens.isRunning)
            {
                try
                {
                    foreach (var record in QueueManager.DictStatusWorkItemQueue)
                    {
                        if (!QueueManager.ConnectedActiveDevice.ContainsKey(record.Key))
                        {
                            while (record.Value.Count > 0)
                            {
                                var workItemId = record.Value.Dequeue().Split(Constants.DollarSymbol)[0];
                                //UbiqsensBooter.ObjPubMqtt.Publish(Constants.WITopic + workItemId, ASCIIEncoding.ASCII.GetBytes(LnT.UbiqSens.Helpers.Constants.DeviceNotAvailable), MqttQos.QOS_LEVEL_AT_LEAST_ONCE, false);
                                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "*Processed Workitem. PushStatus " + Constants.WITopic + workItemId);
                            }
                        }
                    }
                    var noOfQueue = QueueManager.DictStatusWorkItemQueue.ToList().Sum(q => QueueManager.DictStatusWorkItemQueue[q.Key].Count);
                    //UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Pending Status Workitem. " + noOfQueue);
                    Thread.Sleep(1000);
                }
                catch (Exception ex)
                {
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in Pushing data directly form Queue. @Ex: " + ex.Message,ex);
                }
            }
            //// Console.WriteLine("Push data exit");
        }

        //private static void Upload(ConcurrentQueue<Message> newItq)
        //{
        //    while (newItq.Count > 0)
        //    {
        //        try
        //        {
        //            var data = new Message();
        //            if (newItq.TryDequeue(out data))
        //            {
        //                Dispatch(data);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            //// Console.WriteLine("Error in Pushing data directly form Queue. Ex:" + ex.Message);
        //            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in Pushing data directly form Queue. @Ex:" + ex.Message);
        //        }
        //    }
        //}
        /// <summary>
        /// Save Data
        /// </summary>
        //public static void SaveData()
        //{
        //    // Console.WriteLine("Save Data in progress");
        //    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Save Data in progress");

        //    try
        //    {
        //        while (UbiqsensBooter.ToStore && UbiqSens.Ubiqsens.isRunning)
        //        {
        //            try
        //            {
        //                while (QueueManager.Itq.Count > 0)
        //                {
        //                    // Console.WriteLine("No of msg in queue " + QueueManager.Itq.Count);
        //                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "No of msg in queue " + QueueManager.Itq.Count);

        //                    var msgData = new List<Message>();
        //                    for (var i = 0; i < MsgToRetrieve; i++)
        //                    {
        //                        Message otqobj;
        //                        if (QueueManager.Itq.TryDequeue(out otqobj))
        //                        {
        //                            msgData.Add(otqobj);
        //                            totalMsgSaved++;
        //                        }
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                //// Console.WriteLine("Error in saving data. Ex:" + ex.Message);
        //                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in saving data. Ex:" + ex.Message);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        // Console.WriteLine("Error in calling saving data. Ex:" + ex.Message);
        //        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in calling saving data. Ex:" + ex.Message);
        //    }

        //    // Console.WriteLine("Save data exit");
        //}

        /// <summary>
        /// Dispatch data to server
        /// </summary>
        /// <param name="data">message data</param>
        private static void Dispatch(Message data)
        {
            try
            {
                var dataWithCRC = Encoding.ASCII.GetBytes(data.Data);

                //if (data.Topic.Contains("d036d9a66e8341feb7e7fc3bed8d2e699aff59209a2546ec96e12e15c06eec0b") || data.Topic.Contains("14191"))
                //    UbiqsensBooter.ObjPubMqtt.Publish("Aprilaire/US", ASCIIEncoding.ASCII.GetBytes("Dispatched from US at" + DateTime.Now.ToString() + ":" + data.Data), MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);


                if (data.Topic.Contains(Constants.DeviceStatusTopic))
                {
                    //data.Topic = data.Topic.Replace(Constants.Slash.ToString() + Constants.Update, string.Empty);
                    //data.Topic = Constants.DeviceStatusTopic + data.Topic;
                    UbiqsensBooter.ObjPubMqtt.Publish(data.Topic, dataWithCRC, MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "DataDispatcher: Dispatched successfully. @Topic: " + data.Topic);
                    //// Console.WriteLine("Data pushed from Queue using Rest. Topic:" + data.Topic + "  Time:" + Constants.DateTimeFormat);
                }
                else if (data.Topic.EndsWith(Constants.NOTIFICATIONS))
                {
                    data.Topic = data.Topic.Replace(Constants.Slash.ToString() + Constants.NOTIFICATIONS, string.Empty);
                    data.Topic = UbiqsensBooter.ServerId + Constants.Slash.ToString() + Constants.NOTIFICATIONS + Constants.Slash.ToString() + data.Topic;
                    UbiqsensBooter.ObjPubMqtt.Publish(data.Topic, dataWithCRC, MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "DataDispatcher: Notification sent successfully. @Topic: " + data.Topic);
                    //if (UbiqsensBooter.IsDiagnosticCheck)
                    //    UbiqsensBooter.ObjPubMqtt.PublisgDiagnostic("Diagnostic/US/" + data.Topic.Split('/')[2], "Notification sent successfully  " + data.Data, MqttQos.QOS_LEVEL_EXACTLY_ONCE, true);

                    //// Console.WriteLine("Notification sent successfully. Topic:" + data.Topic + "  Time:" + Constants.DateTimeFormat);
                    ///     //*****************************************Diagnostic purpose//*****************************************//
                    //if (UbiqsensBooter.IsDiagnosticCheck)
                    //{
                    //    string datatime = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss:fff tt");
                    //    datatime = datatime + "$" + ASCIIEncoding.ASCII.GetString(dataWithCRC);
                    //    XmlDocument xdoc = new XmlDocument();
                    //    xdoc.LoadXml(Encoding.ASCII.GetString(dataWithCRC));
                    //    XmlElement root = xdoc.DocumentElement;
                    //    string dtuid = Regex.Replace(root.Attributes["DateTime"].Value, "[^0-9]+", "");
                    //    data.Topic = "Diagnostic/US/Notification/" + dtuid;
                    //    UbiqsensBooter.ObjMqtt.PublisgDiagnostic(data.Topic, Encoding.ASCII.GetBytes(datatime), MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                    //}
                    //*****************************************Diagnostic purpose//*****************************************//
                }
                else if (data.Topic.EndsWith("AUTO_DETECTED_DEVICES"))
                {
                    data.Topic = data.Topic.Replace(Constants.AutoDetectedDeviceTopic, string.Empty) + UbiqsensBooter.GatewayId;
                    data.Topic = data.Topic.Replace("AUTO_DETECTED_DEVICES", string.Empty) + UbiqsensBooter.GatewayId;
                    UbiqsensBooter.ObjPubMqtt.Publish(data.Topic, dataWithCRC, MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "DataDispatcher: AUTO_DETECTED_DEVICES sent successfully. @Topic: " + data.Topic);
                    //// Console.WriteLine("AUTO_DETECTED_DEVICES sent successfully. Topic:" + data.Topic + "  Time:" + Constants.DateTimeFormat);
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "DataDispatcher: AUTO_DETECTED_DEVICES sent successfully. @Topic: " + data.Topic + " @Time: " + Constants.DateTimeFormat);
                }
                else
                {
                    data.Topic = UbiqsensBooter.ServerId + Constants.Slash + Constants.WorkItemTopic + Constants.Slash + data.Topic.Substring(data.Topic.LastIndexOf(Constants.Slash) + 1);
                    UbiqsensBooter.ObjPubMqtt.Publish(data.Topic, dataWithCRC, MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "DataDispatcher: Dispatched successfully. @Topic: " + data.Topic);

                    //if (UbiqsensBooter.IsDiagnosticCheck)
                    //    UbiqsensBooter.ObjPubMqtt.PublisgDiagnostic("Diagnostic/US/" + data.Topic.Split('/')[2], "Workitem responded  " + data.Data, MqttQos.QOS_LEVEL_EXACTLY_ONCE, true);

                    //// Console.WriteLine("Data pushed from Queue using MQTT. Topic:" + data.Topic + "  Time:" + Constants.DateTimeFormat);

                    /// *****************************************Diagnostic purpose//*****************************************//
                    //if (UbiqsensBooter.IsDiagnosticCheck)
                    //{
                    //    string checkdta = ASCIIEncoding.ASCII.GetString(dataWithCRC);
                    //    if (!checkdta.Contains("Device Not Available"))
                    //    {
                    //    data.Topic = "Diagnostic/US/" + data.Topic.Substring(data.Topic.LastIndexOf(Constants.Slash) + 1);
                    //    UbiqsensBooter.ObjMqtt.PublisgDiagnostic(data.Topic, dataWithCRC, MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                    //    }
                    //}
                    ///*****************************************Diagnostic purpose//*****************************************//
                }

                totalMsgPushed++;
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "DataDispatcher: @Totalmsgpushed: " + totalMsgPushed);
                //// Console.WriteLine("Total msg pushed:" + totalMsgPushed);
            }
            catch (Exception ex)
            {
                //// Console.WriteLine("Error in Pushing data. Ex:" + ex.Message);
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "DataDispatcher: Error in Pushing data. @Ex: " + ex.Message,ex);
            }
        }
    }
}
