﻿using System.ComponentModel;
using System.Configuration.Install;

namespace LnT.UbiqSens
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
    }
}
