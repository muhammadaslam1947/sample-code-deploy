﻿////-----------------------------------------------------------------------
//// <copyright file="MosquittoMqttClient.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

using System.Linq;

namespace LnT.UbiqSens.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using LnT.UbiqSens.PluginInterfaces;
    using uPLibrary.Networking.M2Mqtt;
    using uPLibrary.Networking.M2Mqtt.Messages;
    using utility = uPLibrary.Networking.M2Mqtt.Utility;
    using uPLibrary.Networking.M2Mqtt.Exceptions;
    /// <summary>
    /// MQTT client
    /// </summary>
    public class MosquittoMqttSubClient //: IServerConnectivity
    {
        #region IServerConnectivity Members

        /// <summary>
        /// object subscriber
        /// </summary>
        private MqttClient objMqttSub = default(MqttClient);


        /// <summary>
        /// Gets or sets Broker IP address
        /// </summary>
        public IPAddress BrokerIp { get; set; }

        /// <summary>
        /// Gets or sets Host IP address
        /// </summary>
        public IPAddress HostIp { get; set; }

        /// <summary>
        /// Gets or sets Broker Port
        /// </summary>
        public int BrokerPort { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Is secure
        /// </summary>
        public bool Secure { get; set; }

        /// <summary>
        /// Gets or sets Certificate for security
        /// </summary>
        public X509Certificate Certificate { get; set; }

        /// <summary>
        /// Gets or sets subscriber Client Id
        /// </summary>
        public string SubClientId { get; set; }

        /// <summary>
        ///  Gets or sets publisher Client Id
        /// </summary>
        public string PubClientId { get; set; }

        /// <summary>
        /// Gets or sets User name
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets Password for connecting to broker
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Will retain
        /// </summary>
        public bool WillRetain { get; set; }

        /// <summary>
        /// Gets or sets Quality of service
        /// </summary>
        public byte Quality { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Will flag
        /// </summary>
        public bool WillFlag { get; set; }

        /// <summary>
        /// Gets or sets will topic
        /// </summary>
        public string WillTopic { get; set; }

        /// <summary>
        /// Gets or sets will message
        /// </summary>
        public string WillMessage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Clean Session
        /// </summary>
        public bool CleanSession { get; set; }

        /// <summary>
        /// Gets or sets Keep Alive Period
        /// </summary>
        public ushort KeepAlivePeriod { get; set; }

        /// <summary>
        /// Gets or sets Topics to subscribe
        /// </summary>
        private Dictionary<string, MqttQos> Topics { get; set; }

        #endregion

        /// <summary>
        ///  Constructor of MqttClient
        /// </summary>
        public MosquittoMqttSubClient()
        {
            //TODO : Should be moved to configuration file
            utility.Trace.TraceLevel = utility.TraceLevel.Warning | utility.TraceLevel.Error;
            utility.Trace.TraceListener = (f, a) => WriteTrace(f, a);
            //Task.Factory.StartNew(new Action(IsSubscriberConnected));
        }

        /// <summary>
        /// Connect method
        /// </summary>
        /// <returns>true or false</returns>
        public bool Connect()
        {
            try
            {
                IPAddress ipaddress;

                if (!string.IsNullOrEmpty(this.SubHostName))
                {
                    if (IPAddress.TryParse(SubHostName, out ipaddress))
                    {
                        if (this.objMqttSub == null)
                        {
                            this.objMqttSub = new MqttClient(ipaddress, this.BrokerPort, this.Secure, this.Certificate, this.Certificate, MqttSslProtocols.None);
                        }
                    }

                    else
                    {
                        if (this.objMqttSub == null)
                        {
                            this.objMqttSub = new MqttClient(SubHostName, this.BrokerPort, this.Secure, this.Certificate, this.Certificate, MqttSslProtocols.None);
                        }
                    }
                }

                if (!this.objMqttSub.IsConnected)
                {
                    this.SubClientId = Guid.NewGuid().ToString();

                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Connecting the MQTT client for the @clientId: " + this.SubClientId);

                    var b = this.objMqttSub.Connect(this.SubClientId);//, this.Username, this.Password, true, this.Quality,
                    // this.WillFlag, this.WillTopic, this.WillMessage, this.CleanSession, this.KeepAlivePeriod);
                    this.objMqttSub.MqttMsgPublishReceived -= this.Client_MqttMsgPublishReceived;
                    this.objMqttSub.MqttMsgPublishReceived += this.Client_MqttMsgPublishReceived;
                    this.objMqttSub.ConnectionClosed -= this.objMqttSub_ConnectionClosed;
                    this.objMqttSub.ConnectionClosed += this.objMqttSub_ConnectionClosed;

                }

                return true;
            }
            catch (Exception ex)
            {

                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "MQTTSUB: Exception while connecting MQTT Subscriber,Connect() Method With @ClientId: " + this.SubClientId, ex);
                return false;
            }
        }

        private void WriteTrace(string format, params object[] args)
        {
            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "MQTTSUB: Sub MQTTTrace: " + string.Format(format, args));
        }

        /// <summary>
        /// Subscribe data from broker
        /// </summary>
        /// <param name="topicAndQos">topic and Quality of service</param>
        /// <returns>true or false</returns>
        public bool Subscribe(Dictionary<string, MqttQos> topicAndQos)
        {
            try
            {
                if (topicAndQos != null)
                    Topics = topicAndQos;

                if (Topics != null)
                {
                    var topics = new List<string>();
                    var qos = new List<byte>();
                    foreach (var keys in topicAndQos)
                    {
                        topics.Add(keys.Key);
                        qos.Add((byte)keys.Value);
                    }

                    this.objMqttSub.Subscribe(topics.ToArray(), qos.ToArray());
                    topics.Clear();
                    qos.Clear();
                }
                else
                {
                    throw new Exception("Topics cannot be null");

                }
                return true;
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "MQTTSUB: Exception in subscribing MQTT Subscribe() Method with @ClientId: " + this.SubClientId, ex);
                return false;
            }
        }

        /// <summary>
        /// Subscribe data from broker
        /// </summary>
        /// <param name="topics">topics</param>
        /// <returns>true or false</returns>
        public bool Unsubscribe(string[] topics)
        {
            try
            {
                this.objMqttSub.Unsubscribe(topics);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Topics cannot be null. @Ex: ", ex);
            }
        }

        /// <summary>
        /// Is Publisher Connected
        /// </summary>
        /// <returns>true or false</returns>
        public bool IsPublisherConnected()
        {
            return true;
        }

        public bool IsConnected()
        {
            if (this.objMqttSub != null && this.objMqttSub.IsConnected)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Is Subscriber Connected
        /// </summary>
        /// <returns>true or false</returns>
        public void IsSubscriberConnected()
        {
            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MQTTSUB: Called  IsSubscriberConnected With @ClientId: " + this.SubClientId + " @DateTime: " + DateTime.Now);
            //while (Ubiqsens.isHeathCheckReq)
            //{
            //    if (this.objMqttSub == null)
            //    {
            //        Thread.Sleep(1000 * 5);
            //        continue;
            //    }
            //    try
            //    {
            //        var connected = false;
            //        //if (this.objMqttSub.IsConnected)
            //        //{
            //        //    this.objMqttSub.Disconnect();
            //        //    Thread.Sleep(1000);
            //        //}

            //        if (!this.objMqttSub.IsConnected)
            //        {
            //            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Sub Reconnect start time " + DateTime.Now);
            //            while (!connected)
            //            {
            //                try
            //                {
            //                    this.Connect();
            //                    //this.Subscribe(Topics);
            //                    connected = true;
            //                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Sub Reconnected");
            //                }
            //                catch (Exception ex)
            //                {
            //                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Reconnecting Failed. Reconnecting after 10 seconds.  EXP" + ex.ToString());
            //                    Thread.Sleep(1000 * 10);
            //                }
            //            }

            //            this.Subscribe(UbiqsensBooter.SubscribeTopic);
            //            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Sub Reconnect end time " + DateTime.Now);
            //        }
            //        Thread.Sleep(1000 * 5);
            //    }
            //    catch (Exception ex)
            //    {

            //        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "SUB CONNECTION VALIDATOR THREAD FAILED " + ex.ToString());
            //    }
            //}
        }

        /// <summary>
        /// publish received event
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">publish received </param>
        private void Client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            try
            {
                //Task.Factory.StartNew(new Action(()=> new InstantMessageHandler().HandleIncomingMsg(e)));

                Thread newDevice = new Thread(new ThreadStart(() =>
                {
                    new InstantMessageHandler().HandleIncomingMsg(e);
                }))
                {
                    Priority = ThreadPriority.Normal,
                    IsBackground = false
                };
                newDevice.TrySetApartmentState(ApartmentState.MTA);
                newDevice.Start();
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "MQTTSUB: Exception Occurred while in  Client_MqttMsgPublishReceived() Method With @Message: " + e.Message + " @Topic: " + e.Topic, ex);
                ////Console.WriteLine("Exception Occurred while in  Client_MqttMsgPublishReceived" + ex.ToString());
            }
        }

        void objMqttSub_ConnectionClosed(object sender, EventArgs e)
        {
            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Sub on connection close is called, @ClientId: " + this.SubClientId);
            try
            {
                Task.Factory.StartNew(RetrySubConnection);
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "MQTTSUB: RetrySubConnection: Exception Occurred at objMqttSub_ConnectionClosed() Method with @ClientId: " + this.SubClientId, ex);
            }
        }

        private void RetrySubConnection()
        {
            try
            {
                Thread.Sleep(30 * 1000);
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MQTTSUB: RetrySubConnection:Re-connect Sub MQTT Object, @ClientId: " + this.SubClientId);

                if (this.objMqttSub == null || !this.objMqttSub.IsConnected)
                {
                    if (this.Connect())
                    {
                        this.Subscribe(Topics);
                        this.Subscribe(UbiqsensBooter.SubscribeTopic);
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MQTTSUB: RetrySubConnection: Re-connected to Sub broker, @ClientId: " + this.SubClientId);
                    }
                    else
                    {
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Warn, "MQTTSUB: RetrySubConnection: Re-connection Sub failed, again retrying after 1 min, @ClientId: " + this.SubClientId);
                        Thread.Sleep(30 * 1000);
                        RetrySubConnection();
                    }

                }
                else
                {
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Warn, "MQTTSUB: RetrySubConnection: Sub MQTT Object is connected, @ClientId: " + this.SubClientId);
                }

            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "MQTTSUB: RetrySubConnection: Exception Occurred at RetrySubConnection() Method with @ClientId: " + this.SubClientId, ex);
                //Console.WriteLine("Exception Occurred while in  objMqttSub_ConnectionClosed" + ex.ToString(),ex);
            }
        }

        public bool Disconnect()
        {
            bool isDone = false;
            try
            {
                this.objMqttSub.Disconnect();
                isDone = true;
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MQTTSUB: Sub Disconnected, @ClientId: " + this.SubClientId + DateTime.Now);
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "MQTTSUB: SUB Disconnection Failure, Disconnect() Method with @ClientId: " + this.SubClientId, ex);
            }
            return isDone;
        }


        public string HostName
        {
            get;
            set;
        }


        public string SubHostName
        {
            get;
            set;
        }

        public string PubHostName
        {
            get;
            set;
        }
    }


    /// <summary>
    /// MQTT client
    /// </summary>
    public class MosquittoMqttPubClient //: IServerConnectivity
    {
        #region IServerConnectivity Members

        /// <summary>
        /// object publisher
        /// </summary>
        private MqttClient objMqttPub = default(MqttClient);

        /// <summary>
        /// Gets or sets Broker IP address
        /// </summary>
        public IPAddress BrokerIp { get; set; }

        public string SubHostName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets Broker Port
        /// </summary>
        public int BrokerPort { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Is secure
        /// </summary>
        public bool Secure { get; set; }

        /// <summary>
        /// Gets or sets Certificate for security
        /// </summary>
        public X509Certificate Certificate { get; set; }

        /// <summary>
        /// Gets or sets subscriber Client Id
        /// </summary>
        public string SubClientId { get; set; }

        /// <summary>
        ///  Gets or sets publisher Client Id
        /// </summary>
        public string PubClientId { get; set; }

        /// <summary>
        /// Gets or sets User name
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets Password for connecting to broker
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Will retain
        /// </summary>
        public bool WillRetain { get; set; }

        /// <summary>
        /// Gets or sets Quality of service
        /// </summary>
        public byte Quality { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Will flag
        /// </summary>
        public bool WillFlag { get; set; }

        /// <summary>
        /// Gets or sets will topic
        /// </summary>
        public string WillTopic { get; set; }

        /// <summary>
        /// Gets or sets will message
        /// </summary>
        public string WillMessage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Clean Session
        /// </summary>
        public bool CleanSession { get; set; }

        /// <summary>
        /// Gets or sets Keep Alive Period
        /// </summary>
        public ushort KeepAlivePeriod { get; set; }

        /// <summary>
        /// Gets or sets Topics to subscribe
        /// </summary>
        private Dictionary<string, MqttQos> Topics { get; set; }

        #endregion

        /// <summary>
        ///  Constructor of MqttClient
        /// </summary>
        public MosquittoMqttPubClient()
        {
            //TODO : Should be moved to configuration file
            utility.Trace.TraceLevel = utility.TraceLevel.Warning | utility.TraceLevel.Error;
            utility.Trace.TraceListener = (f, a) => WriteTrace(f, a);
            //Task.Factory.StartNew(new Action(IsPublisherConnected));
        }

        /// <summary>
        /// Connect method
        /// </summary>
        /// <returns>true or false</returns>
        public bool Connect()
        {
            try
            {
                IPAddress ipaddress;

                if (!string.IsNullOrEmpty(this.PubHostName))
                {

                    if (IPAddress.TryParse(PubHostName, out ipaddress))
                    {
                        if (this.objMqttPub == null)
                        {
                            this.objMqttPub = new MqttClient(ipaddress, this.BrokerPort, this.Secure, this.Certificate, this.Certificate, MqttSslProtocols.None);
                        }
                    }
                    else
                    {
                        if (this.objMqttPub == null)
                        {
                            this.objMqttPub = new MqttClient(PubHostName, this.BrokerPort, this.Secure, this.Certificate, this.Certificate, MqttSslProtocols.None);
                        }
                    }
                }
                else
                {
                    throw new Exception("Pub Broker IP cannot be null");
                }


                if (!this.objMqttPub.IsConnected)
                {
                    this.PubClientId = Guid.NewGuid().ToString();
                    var b1 = this.objMqttPub.Connect(this.PubClientId);

                    //added when the frequent connection and disconnection to MQTT broker code was commented in IsPublisherConnected method
                    this.objMqttPub.ConnectionClosed -= this.objMqttPub_ConnectionClosed;
                    this.objMqttPub.ConnectionClosed += this.objMqttPub_ConnectionClosed;
                }

                return true;
            }
            catch (Exception exp)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "MQTTPUB: Exception while connecting MQTT Publisher, Connect() Method with @ClientId: " + this.PubClientId, exp);
                return false;
            }
        }

        private void WriteTrace(string format, params object[] args)
        {
            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "MQTTPUB: Pub MQTTTrace: " + string.Format(format, args));
        }

        /// <summary>
        /// Publish data to broker
        /// </summary>
        /// <param name="topic">string topic</param>
        /// <param name="data">byte array data</param>
        /// <param name="qos">Quality of service</param>
        /// <param name="retain">to retain</param>
        /// <returns>true or false</returns>
        public bool Publish(string topic, byte[] data, MqttQos qos, bool retain)
        {
            try
            {
                if (data == null)
                {
                    throw new Exception("Data cannot be null");
                }
                if (this.objMqttPub.IsConnected)
                {
                    var a = this.objMqttPub.Publish(topic, data);
                    //if (topic != "FWProfiling")
                    //    this.objMqttPub.Publish("FWProfiling", ASCIIEncoding.ASCII.GetBytes(DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss:ffff") + " Data Sent to Web Service, Data :" + BitConverter.ToString(data)));
                }
                else
                {
                    while (!this.objMqttPub.IsConnected)
                    {
                        Thread.Sleep(10);
                    }
                    var a = this.objMqttPub.Publish(topic, data);
                }
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MQTTPUB: MQTT - Failure at Publish() Method with @Topic: " + topic + " @Data: " + BitConverter.ToString(data) + " @Message: " + ex.ToString() + " @ClientId: " + this.PubClientId, ex);
            }
            return true;
        }

        public bool IsConnected()
        {
            if (this.objMqttPub != null && this.objMqttPub.IsConnected)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Is Publisher Connected
        /// </summary>
        /// <returns>true or false</returns>
        public void IsPublisherConnected()
        {
            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MQTTPUB: IsPublisherConnected Stared, @ClientId: " + this.PubClientId + DateTime.Now);
        }

        public bool Disconnect()
        {
            bool isDone = false;
            try
            {
                this.objMqttPub.Disconnect();
                isDone = true;
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MQTTPUB: Pub Disconnected, @ClientId: " + this.PubClientId + DateTime.Now);
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "MQTTPUB: PUB Disconnection Failure, Disconnect() Method with @ClientId: " + this.PubClientId, ex);
            }
            return isDone;
        }

        void objMqttPub_ConnectionClosed(object sender, EventArgs e)
        {
            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "RetryPubConnection is called, @ClientId: " + this.PubClientId);
            try
            {
                Task.Factory.StartNew(RetryPubConnection);
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "MQTTPUB: RetryPubConnection: Exception Occurred at objMqttPub_ConnectionClosed() Method with @ClientId: " + this.PubClientId, ex);
            }
        }

        private void RetryPubConnection()
        {
            try
            {
                Thread.Sleep(30 * 1000);
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MQTTPUB: RetryPubConnection:Re-connect Pub MQTT Object");

                if (this.objMqttPub == null || !this.objMqttPub.IsConnected)
                {
                    if (this.Connect())
                    {
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MQTTPUB: RetryPubConnection: Re-connected to Pub broker with @ClientId: " + this.PubClientId);
                    }
                    else
                    {
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Warn, "MQTTPUB: RetryPubConnection: Re-connection Pub failed, again retrying after 1 min with @ClientId: " + this.PubClientId);
                        Thread.Sleep(30 * 1000);
                        RetryPubConnection();
                    }
                }
                else
                {
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Warn, "MQTTPUB: RetryPubConnection: Pub MQTT Object is connected with @ClientId: " + this.PubClientId);
                }

            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "MQTTPUB: RetryPubConnection: Exception Occurred at RetryPubConnection() Method with @ClientId: " + this.PubClientId, ex);
            }
        }


        public string HostName
        {
            get;
            set;
        }


        public string PubHostName
        {
            get;
            set;
        }
    }
}
