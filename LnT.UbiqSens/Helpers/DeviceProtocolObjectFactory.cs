﻿////-----------------------------------------------------------------------
//// <copyright file="DeviceProtocolObjectFactory.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

//namespace LnT.UbiqSens.Helpers
//{
//    using System;
//    using System.Globalization;
//    using System.IO;
//    using System.Linq;
//    using System.Reflection;
//    using LnT.UbiqSens.PluginInterfaces;

//    /// <summary>
//    /// Device Protocol Object Factory
//    /// </summary>
//    public class DeviceProtocolObjectFactory
//    {
//        /// <summary>
//        /// Searches the plugin in the \DeviceProtocolPlugins, Creates the IDeviceConnection object and returns
//        /// </summary>
//        /// <param name="protocolName">protocol Name</param>
//        /// <returns>object Device Protocol</returns>
//        public IDeviceProtocol GetIDeviceProtocolObject(string protocolName)
//        {
//            var deviceConnectionPlugins = Directory.GetFiles(Constants.CurrentPath + Constants.DeviceProtocolPlugins + UbiqsensBooter.XPath, Constants.StrDll);
//            IDeviceProtocol objDeviceProtocol = null;
//            foreach (var communicationProtocolPath in deviceConnectionPlugins)
//            {
//                try
//                {
//                    var assembly = Assembly.LoadFrom(communicationProtocolPath);
//                    var lstTypes = assembly.GetTypes();
//                    var lst = lstTypes.Where(t => t.Name.ToLower(CultureInfo.InvariantCulture) == protocolName.ToLower(CultureInfo.InvariantCulture)).ToList();
//                    if (lst.Count > 0)
//                    {
//                        objDeviceProtocol = Activator.CreateInstance(lst.First()) as IDeviceProtocol;
//                    }

//                    if (objDeviceProtocol != null)
//                    {
//                        break;
//                    }
//                }
//                catch (Exception ex)
//                {
//                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in creating Protocol object from Assembly. Exp:" + ex.Message);
//                }
//            }

//            return objDeviceProtocol;
//        }
//    }
//}
