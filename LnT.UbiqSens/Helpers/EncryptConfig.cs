﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using LnT.UbiqSens.PluginInterfaces;


namespace LnT.UbiqSens.Helpers
{
    public class EncryptConfig
    {
        public string exePath = string.Empty;
        public EncryptConfig()
        {
            exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\LnT.UbiqSens.exe";
        }
   

        public void UpdateConfigFile(string con)
        {

            //updating config file
            XmlDocument XmlDoc = new XmlDocument();
            //Loading the Config file
            XmlDoc.Load(exePath + ".Config");
            foreach (XmlElement xElement in XmlDoc.DocumentElement.GetElementsByTagName("connectionStrings"))
            {
                //if (xElement.Name == "connectionStrings")
                //{
                //setting the coonection string
                xElement.FirstChild.Attributes[0].Value = con;
                //}
            }
            //writing the connection string in config file
            XmlDoc.Save(exePath + ".Config");
        }

        public void Encrypt()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(exePath);
            ConfigurationSection configSection = config.GetSection("connectionStrings");

            try
            {

                ////Check if the section is already encrypted
                if (!config.ConnectionStrings.SectionInformation.IsProtected)
                {
                    //Encrypt the web.config section.
                    config.ConnectionStrings.SectionInformation.ForceSave = true;
                    config.ConnectionStrings.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                }

                config.Save(ConfigurationSaveMode.Modified);
            }

            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error while encrypting config @Ex: " + ex.Message,ex);
            }
        }
        public void Decrypt()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(exePath);
            ConfigurationSection configSection = config.GetSection("connectionStrings");
            try
            {
                ////Check if the section is already encrypted
                if (config.ConnectionStrings.SectionInformation.IsProtected)
                {
                    //Encrypt the web.config section.
                    config.ConnectionStrings.SectionInformation.ForceSave = true;
                    config.ConnectionStrings.SectionInformation.UnprotectSection();
                }
                config.Save(ConfigurationSaveMode.Modified);
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error while decrypting config: @Ex: " + ex.Message,ex);

            }
        }


    }
}
