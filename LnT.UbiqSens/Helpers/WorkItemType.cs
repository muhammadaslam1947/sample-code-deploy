﻿//-----------------------------------------------------------------------
// <copyright file="WorkItemType.cs" company="L&T">
//     Copyright (c) L&T. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LnT.UbiqSens.Helpers
{
    /// <summary>
    /// work item type
    /// </summary>
    public enum WorkItemType
    {
        /// <summary>
        /// work item type connect
        /// </summary>
        Connect,

        /// <summary>
        /// work item type Commands
        /// </summary>
        Commands,

        /// <summary>
        /// work item type Disconnect
        /// </summary>
        Disconnect,

        /// <summary>
        /// work item type CommandError
        /// </summary>
        CommandError
    }
}
