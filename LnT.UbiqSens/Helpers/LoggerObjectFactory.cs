﻿////-----------------------------------------------------------------------
//// <copyright file="LoggerObjectFactory.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

namespace LnT.UbiqSens.Helpers
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using LnT.UbiqSens.PluginInterfaces;

    /// <summary>
    /// Class logger object factory
    /// </summary>
    public class LoggerObjectFactory
    {
        /// <summary>
        /// Searches the plugin in the \ILoggerPlugins, Creates the ILogger object and returns
        /// </summary>
        /// <param name="pluginName">plugin name</param>
        /// <returns>logger object</returns>
        public ILogger GetILoggerObject(string pluginName)
        {
            var loggerPlugins = Directory.GetFiles(Constants.CurrentPath + Constants.StrILoggerPlugins + UbiqsensBooter.XPath, Constants.StrDll);
            ILogger objLogger = null;
            foreach (var communicationProtocolPath in loggerPlugins)
            {
                var assembly = Assembly.LoadFrom(communicationProtocolPath);
                var lstTypes = assembly.GetTypes().Where(typeof(ILogger).IsAssignableFrom).ToList();
                var lst = lstTypes.Where(t => t.Name.ToLower(CultureInfo.InvariantCulture) == pluginName.ToLower(CultureInfo.InvariantCulture)).ToList();
                if (lst.Count > 0)
                {
                    objLogger = Activator.CreateInstance(lst.First()) as ILogger;
                }

                if (objLogger != null)
                {
                    break;
                }
            }

            return objLogger;
        }
    }
}
