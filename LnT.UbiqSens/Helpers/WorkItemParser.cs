﻿////-----------------------------------------------------------------------
//// <copyright file="WorkItemParser.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

namespace LnT.UbiqSens.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using LnT.UbiqSens.PluginInterfaces;

    /// <summary>
    /// Parses work item into Work Item Message
    /// </summary>
    public class WorkItemParser
    {
        /// <summary>
        /// Parses the work item
        /// </summary>
        /// <param name="workItem">work Item</param>
        /// <returns>object Work Item Message is returned</returns>
        public WorkItem ParseWorkItem(string workItem)
        {

            var objWorkItemMessage = new WorkItem();
            try
            {
                objWorkItemMessage.WorkItemId = workItem.Split(Constants.DollarSymbol)[0];
                workItem = workItem.Split(Constants.DollarSymbol)[1];
                var doc = new XmlDocument();
                doc.LoadXml(workItem);
                var xmlNodeList = doc.GetElementsByTagName(Constants.Message);

                if (xmlNodeList.Count >= 1)
                {
                    if(xmlNodeList[0].Attributes.Count >= 1)
                    {
                        if (!String.IsNullOrEmpty(xmlNodeList[0].Attributes[Constants.CorrelationId].Value))
                        {
                            objWorkItemMessage.Cid = xmlNodeList[0].Attributes[Constants.CorrelationId].Value;
                        }

                        if (xmlNodeList[0].Attributes[Constants.MessageType].Value == Constants.Status)
                        {
                            if (doc.GetElementsByTagName(Constants.Status)[0].InnerText == Constants.Connect)
                            {
                                objWorkItemMessage.WorkItemtype = WorkItemType.Connect;
                            }
                            else if (doc.GetElementsByTagName(Constants.Status)[0].InnerText == Constants.Disconnect)
                            {
                                objWorkItemMessage.WorkItemtype = WorkItemType.Disconnect;
                            }
                        }
                        else if (xmlNodeList[0].Attributes[Constants.MessageType].Value == Constants.WorkItem)
                        {
                            var commands = new List<Command>();
                            objWorkItemMessage.WorkItemtype = WorkItemType.Commands;
                            foreach (XmlNode node in doc.GetElementsByTagName(Constants.WorkItem))
                            {
                                var cmd = new Command();
                                cmd.CmdId = node.Attributes[Constants.CmdId].Value;
                                cmd.Delay = Convert.ToInt32(node.Attributes[Constants.Delay].Value);
                                cmd.Value = node.Attributes[Constants.Value].Value;
                                commands.Add(cmd);
                            }

                            objWorkItemMessage.Commands = commands;
                        }
                    }
                }

                doc.RemoveAll();
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "WorkItemParaser: Not valid xml structure. ParseWorkItem() Method with @Workitem: " + workItem,ex);
                objWorkItemMessage.WorkItemtype = WorkItemType.CommandError;
            }

            return objWorkItemMessage;
        }
    }
}
