﻿////-----------------------------------------------------------------------
//// <copyright file="IServerConnectivity.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

namespace LnT.UbiqSens.Helpers
{
    using System.Collections.Generic;
    using System.Net;
    using System.Security.Cryptography.X509Certificates;

    /// <summary>
    /// Server Connectivity
    /// </summary>
    public interface IServerConnectivity
    {
        /// <summary>
        /// Gets or sets Broker IP address
        /// </summary>
        IPAddress BrokerIp { get; set; }

        /// <summary>
        /// Gets or sets Broker IP address
        /// </summary>
        string SubHostName { get; set; }

        /// <summary>
        /// Gets or sets Broker IP address
        /// </summary>
        string PubHostName { get; set; }

        /// <summary>
        /// Gets or sets Broker Port
        /// </summary>
        int BrokerPort { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Is secure
        /// </summary>
        bool Secure { get; set; }

        /// <summary>
        /// Gets or sets Certificate for security
        /// </summary>
        X509Certificate Certificate { get; set; }

        /// <summary>
        /// Gets or sets subscriber Client Id
        /// </summary>
        string SubClientId { get; set; }

        /// <summary>
        ///  Gets or sets publisher Client Id
        /// </summary>
        string PubClientId { get; set; }

        /// <summary>
        /// Gets or sets User name
        /// </summary>
        string Username { get; set; }

        /// <summary>
        /// Gets or sets Password for connecting to broker
        /// </summary>
        string Password { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Will retain
        /// </summary>
        bool WillRetain { get; set; }

        /// <summary>
        /// Gets or sets Quality of service
        /// </summary>
        byte Quality { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Will flag
        /// </summary>
        bool WillFlag { get; set; }

        /// <summary>
        /// Gets or sets will topic
        /// </summary>
        string WillTopic { get; set; }

        /// <summary>
        /// Gets or sets will message
        /// </summary>
        string WillMessage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Clean Session
        /// </summary>
        bool CleanSession { get; set; }

        /// <summary>
        /// Gets or sets Keep Alive Period
        /// </summary>
        ushort KeepAlivePeriod { get; set; }

        /// <summary>
        /// Connect method
        /// </summary>
        /// <returns>true or false</returns>
        bool Connect();

        /// <summary>
        /// Publish data to broker
        /// </summary>
        /// <param name="topic">Topic to be published</param>
        /// <param name="data">Data to be published</param>
        /// <param name="qos">QOS level</param>
        /// <param name="retain">To retain</param>
        /// <returns>true or false</returns>
        bool Publish(string topic, byte[] data, MqttQos qos, bool retain);
        bool PublisgDiagnostic(string topic, string data, MqttQos qos, bool retain);

        
        /// <summary>
        /// Subscribe data from broker
        /// </summary>
        /// <param name="topicAndQos">topic And QOS</param>
        /// <returns>true or false</returns>
        bool Subscribe(Dictionary<string, MqttQos> topicAndQos);


        /// <summary>
        /// Unsubscribe data from broker
        /// </summary>
        /// <param name="topics">topics</param>
        /// <returns>true or false</returns>
        bool Unsubscribe(string[] topics);

        /// <summary>
        /// Unsubscribe data from broker
        /// </summary>
        /// <param name="topics">topics</param>
        /// <returns>true or false</returns>
        bool Disconnect();

        /// <summary>
        /// Is Subscriber Connected
        /// </summary>
        /// <returns>true or false</returns>
        bool IsSubscriberConnected();

        /// <summary>
        /// Is Publisher Connected
        /// </summary>
        /// <returns>true or false</returns>
        void IsPublisherConnected();

        ////event MqttClient.MqttMsgPublishEventHandler MqttMsgPublishReceived;
        ////public delegate void MqttMsgPublishedEventHandler(object sender, MqttMsgPublishedEventArgs e);
    }
}
