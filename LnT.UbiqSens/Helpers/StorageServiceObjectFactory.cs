﻿////-----------------------------------------------------------------------
//// <copyright file="StorageServiceObjectFactory.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

namespace LnT.UbiqSens.Helpers
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using LnT.UbiqSens.PluginInterfaces;

    /// <summary>
    /// Class storage object factory
    /// </summary>
    internal class StorageServiceObjectFactory
    {
        /// <summary>
        /// Searches the plugin in the StorageServicePlugins, Creates the IStorageServicePlugins object and returns
        /// </summary>
        /// <param name="pluginName">plugin name</param>
        /// <returns>object of storage service</returns>
        internal IStorageService GetIStorageServiceObject(string pluginName)
        {
            var deviceConDll = Directory.GetFiles(Constants.CurrentPath + Constants.StorageServicePlugins + UbiqsensBooter.XPath, Constants.StrDll);
            IStorageService objIStorageService = null;
            foreach (var communicationProtocolPath in deviceConDll)
            {
                var assembly = Assembly.LoadFrom(communicationProtocolPath);
                var lstTypes = assembly.GetTypes().Where(typeof(IStorageService).IsAssignableFrom).ToList();
                var lst = lstTypes.Where(t => t.Name.ToLower(CultureInfo.InvariantCulture) == pluginName.ToLower(CultureInfo.InvariantCulture)).ToList();

                if (lst.Count > 0)
                {
                    objIStorageService = Activator.CreateInstance(lst.First()) as IStorageService;
                }

                if (objIStorageService != null)
                {
                    break;
                }
            }

            return objIStorageService;
        }
    }
}
