﻿//////-----------------------------------------------------------------------
////// <copyright file="AutoDetectObjectFactory.cs" company="L&T">
//////     Copyright (c) L&T. All rights reserved.
////// </copyright>
//////-----------------------------------------------------------------------

//namespace LnT.UbiqSens.Helpers
//{
//    using System;
//    using System.Collections.Generic;
//    using System.IO;
//    using System.Linq;
//    using System.Reflection;
//    using LnT.UbiqSens.PluginInterfaces;

//    /// <summary>
//    /// Scans the folder and gets the Auto detect Plugins
//    /// </summary>
//    internal class AutoDetectObjectFactory
//    {
//        /// <summary>
//        /// Get Device Scanner Object
//        /// </summary>
//        /// <returns>IDeviceScanner object array</returns>
//        public IDeviceScanner[] GetDeviceScannerObject()
//        {
//            var deviceConnectionPlugins = Directory.GetFiles(Constants.CurrentPath + Constants.DeviceConnectionPlugins + UbiqsensBooter.XPath, Constants.StrDll);
//            var deviceScanner = new List<IDeviceScanner>();
//            foreach (var communicationProtocolPath in deviceConnectionPlugins)
//            {
//                try
//                {
//                    var assembly = Assembly.LoadFrom(communicationProtocolPath);
//                    var types = assembly.GetTypes().Where(typeof(IDeviceScanner).IsAssignableFrom).ToArray();
//                    foreach (var type in types)
//                    {
//                        try
//                        {
//                            deviceScanner.Add(Activator.CreateInstance(type) as IDeviceScanner);
//                        }
//                        catch (Exception ex)
//                        {
//                            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in Initialising Auto Detection Plugin. Exp:" + ex.Message);
//                            Console.WriteLine("Error in Initialising Auto Detection Plugin. Exp:" + ex.Message);
//                        }
//                    }
//                }
//                catch (Exception ex)
//                {
//                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in Initialising Auto Detection Plugin. Exp:" + ex.Message);
//                    Console.WriteLine("Error in Initialising Auto Detection Plugin. Exp:" + ex.Message);
//                }
//            }

//            return deviceScanner.ToArray();
//        }
//    }
//}
