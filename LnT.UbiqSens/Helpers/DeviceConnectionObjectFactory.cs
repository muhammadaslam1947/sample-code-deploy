﻿////-----------------------------------------------------------------------
//// <copyright file="DeviceConnectionObjectFactory.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

//namespace LnT.UbiqSens.Helpers
//{
//    using System;
//    using System.Globalization;
//    using System.IO;
//    using System.Linq;
//    using System.Reflection;
//    using LnT.UbiqSens.PluginInterfaces;

//    /// <summary>
//    /// Device Connection Object Factory
//    /// </summary>
//    public class DeviceConnectionObjectFactory
//    {
//        /// <summary>
//        /// Searches the plugin in the DeviceConnectionPlugins, Creates the IDeviceConnection object and returns
//        /// </summary>
//        /// <param name="deviceConnectionPlugin">device Connection Plugin</param>
//        /// <returns>object Device Connection</returns>
//        public IDeviceConnection GetIDeviceConnectionObject(string deviceConnectionPlugin)
//        {
//            var deviceConnectionPlugins = Directory.GetFiles(Constants.CurrentPath + Constants.DeviceConnectionPlugins + UbiqsensBooter.XPath, Constants.StrDll);
//            IDeviceConnection objDeviceConnection = null;
//            foreach (var communicationProtocolPath in deviceConnectionPlugins)
//            {
//                try
//                {
//                    var assembly = Assembly.LoadFrom(communicationProtocolPath);
//                    var lstTypes = assembly.GetTypes().Where(typeof(IDeviceConnection).IsAssignableFrom).ToList();

//                    var lst = lstTypes.Where(t => t.Name.ToLower(CultureInfo.InvariantCulture) == deviceConnectionPlugin.ToLower(CultureInfo.InvariantCulture)).ToList();
//                    if (lst.Count > 0)
//                    {
//                        objDeviceConnection = Activator.CreateInstance(lst.First()) as IDeviceConnection;
//                    }

//                    if (objDeviceConnection != null)
//                    {
//                        break;
//                    }
//                }
//                catch (Exception ex)
//                {
//                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in creating Protocol object from Assembly. Exp:" + ex.Message);
//                }
//            }

//            return objDeviceConnection;
//        }
//    }
//}
