﻿////-----------------------------------------------------------------------
//// <copyright file="GatewayConnectionObjectFactory.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

namespace LnT.UbiqSens.Helpers
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using LnT.UbiqSens.PluginInterfaces;

    /// <summary>
    /// Gateway Connection Factory
    /// </summary>
    public class GatewayConnectionFactory
    {
        /// <summary>
        /// Searches the plugin in the \IGatewayConnectionPlugins, Creates the IGatewayConnection object and returns
        /// </summary>
        /// <param name="gatewayPluginName">Device Connection Plugin</param>
        /// <returns>Gateway Connection</returns>
        public IGatewayConnection GetGatewayConnectionObject(string gatewayPluginName)
        {
            IGatewayConnection objGatewayConnection = null;

            var deviceConnectionPlugins = Directory.GetFiles(Constants.CurrentPath + Constants.GatewayConnectionPlugins + UbiqsensBooter.XPath, Constants.StrDll);
            foreach (var communicationProtocolPath in deviceConnectionPlugins)
            {
                var assembly = Assembly.LoadFrom(communicationProtocolPath);
                var lstTypes = assembly.GetTypes().Where(typeof(IGatewayConnection).IsAssignableFrom).ToList();
                var lst = lstTypes.Where(t => t.Name.ToLower(CultureInfo.InvariantCulture) == gatewayPluginName.ToLower(CultureInfo.InvariantCulture)).ToList();
                if (lst.Count > 0)
                {
                    objGatewayConnection = Activator.CreateInstance(lst.First()) as IGatewayConnection;
                }

                if (objGatewayConnection != null)
                {
                    break;
                }
            }
            return objGatewayConnection;
        }

        /// <summary>
        /// Searches the plugin in the \IGatewayConnectionPlugins, Creates the IGatewayConnection object and returns
        /// </summary>
        /// <param name="protocolName">protocol Name</param>
        /// <returns>Gateway Protocol</returns>
        public IGatewayProtocol GetGatewayProtocolObject(string protocolName)
        {
            var deviceConnectionPlugins = Directory.GetFiles(Constants.CurrentPath + Constants.GatewayProtocolPlugins + UbiqsensBooter.XPath, Constants.StrDll);
            IGatewayProtocol objGatewayProtocol = null;
            try
            {
                foreach (var communicationProtocolPath in deviceConnectionPlugins)
                {
                    var assembly = Assembly.LoadFrom(communicationProtocolPath);
                    var lstTypes = assembly.GetTypes().Where(typeof(IGatewayProtocol).IsAssignableFrom).ToList();
                    var lst = lstTypes.Where(t => t.Name.ToLower(CultureInfo.InvariantCulture) == protocolName.ToLower(CultureInfo.InvariantCulture)).ToList();
                    if (lst.Count > 0)
                    {
                        objGatewayProtocol = Activator.CreateInstance(lst.First()) as IGatewayProtocol;
                    }

                    if (objGatewayProtocol != null)
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error Occurred at GetGatewayProtocolObject() Method and @protocolName: " + protocolName + " @Ex: " + ex.Message,ex);
                //Console.WriteLine("****" + ex.Message);
            }
            return objGatewayProtocol;
        }
    }
}
