﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using LnT.UbiqSens.PluginInterfaces;

namespace LnT.UbiqSens.Helpers
{
    public class AES128
    {
        RijndaelManaged rijndaelCipher = null;

        public static AES128 obj; 

        private AES128()
        {
            rijndaelCipher = new RijndaelManaged();
        }

        public static AES128 GetInstance()
        {
            if(obj == null)
                obj=new AES128();
            return obj;

        }

        public String Encrypt(string input, string password)
        {
            try
            {
                // Assumed Mode and padding values.
                rijndaelCipher.Mode = CipherMode.ECB;
                // AssumedKeySize and BlockSize values.
                rijndaelCipher.KeySize = 0x80;
                rijndaelCipher.BlockSize = 0x80;
                // Convert Hex keys to byte Array.
                rijndaelCipher.Key = ASCIIEncoding.ASCII.GetBytes(password);
                rijndaelCipher.IV = ASCIIEncoding.ASCII.GetBytes(password);
                // Encrypt data
                byte[] plainText = rijndaelCipher.CreateEncryptor().TransformFinalBlock(ASCIIEncoding.ASCII.GetBytes(input), 0, ASCIIEncoding.ASCII.GetBytes(input).Length);
                return ByteArrayToString(plainText);  //Encoding.UTF8.GetString(plainText1);
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error while encrypting, @input: " + input + " @Password: " + password + " @Ex: " + ex.Message, ex);
                return null;
            }
        }
        public String Decrypt(string input, string password)
        {
            try
            {
                // Assumed Mode and padding values.
                rijndaelCipher.Mode = CipherMode.ECB;
                // AssumedKeySize and BlockSize values.
                rijndaelCipher.KeySize = 0x80;
                rijndaelCipher.BlockSize = 0x80;
                // Convert Hex keys to byte Array.
                byte[] encryptedData = HexToBytes(input);//encrypted data 
                rijndaelCipher.Key = ASCIIEncoding.ASCII.GetBytes(password);
                rijndaelCipher.IV = ASCIIEncoding.ASCII.GetBytes(password);
                // Decrypt data
                byte[] plainText = rijndaelCipher.CreateDecryptor().TransformFinalBlock(encryptedData, 0, encryptedData.Length);
                return Encoding.UTF8.GetString(plainText);
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error while decrypting, @input: " + input + " @Password: " + password + " @Ex: " + ex.Message, ex);
                return null;
            }
        }
        static private byte[] HexToBytes(string str)
        {
            if (str.Length == 0 || str.Length % 2 != 0)
                return new byte[0];
            byte[] buffer = new byte[str.Length / 2];
            char c;
            for (int bx = 0, sx = 0; bx < buffer.Length; ++bx, ++sx)
            {
                // Convert first half of byte   
                c = str[sx];
                buffer[bx] = (byte)((c > '9' ? (c > 'Z' ? (c - 'a' + 10) : (c - 'A' + 10)) : (c - '0')) << 4);
                // Convert second half of byte    
                c = str[++sx];
                buffer[bx] |= (byte)(c > '9' ? (c > 'Z' ? (c - 'a' + 10) : (c - 'A' + 10)) : (c - '0'));
            }
            return buffer;
        }

        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }
    }
}
