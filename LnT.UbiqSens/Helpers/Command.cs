﻿////-----------------------------------------------------------------------
//// <copyright file="Command.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

namespace LnT.UbiqSens.Helpers
{
    using System;
    using System.Collections.Concurrent;

    /// <summary>
    /// Command to be processed
    /// </summary>
    public class Command
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Command"/> class.
        /// </summary>
        public Command()
        {
            this.CmdId = string.Empty;
            this.Value = string.Empty;
        }

        /// <summary>
        /// Gets or sets Command ID
        /// </summary>
        public string CmdId { get; set; }

        /// <summary>
        /// Gets or sets Command value
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets Delay in seconds
        /// </summary>
        public int Delay { get; set; }
    }

    //[Serializable]
    //public class ActiveDevicesList
    //{
    //    private ConcurrentDictionary<string, string> activeDevices;

    //    public ConcurrentDictionary<string, string> ActiveDevices
    //    {
    //        get { return activeDevices; }
    //        set { activeDevices = value; }
    //    }
    //}


}
