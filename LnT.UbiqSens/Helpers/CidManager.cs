﻿//-----------------------------------------------------------------------
// <copyright file="CidManager.cs" company="L&T">
//     Copyright (c) L&T IES. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LnT.UbiqSens.Helpers
{
    using PluginInterfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class CidManager
    {

        private static CidManager cidInstance;

        /// <summary>
        /// Constructor 
        /// </summary>
        private CidManager() { }

        /// <summary>
        /// Creates the singleton instance of this class
        /// </summary>
        /// <returns>Returns instance of CidManager</returns>
        public static CidManager Instance
        {
            get
            {
                if (cidInstance == null)
                    cidInstance = new CidManager();
                return cidInstance;
            }
        }

        /// <summary>
        /// Generates the unique correltionid for the given action and macid
        /// </summary>
        /// <param name="action">Action Name</param>
        /// <param name="macId">MacId of the device</param>
        /// <returns>Returns Correltion Id</returns>
        public string GenerateCid(string action, string macId, string deviceAddress)
        {
            try
            {
                string actionName = String.Empty;
                if (!string.IsNullOrEmpty(action))
                {
                    actionName = Constants.ActionList.Find((x) => action.Contains(x));
                }
                if (string.IsNullOrEmpty(actionName))
                {
                    actionName = Constants.Action;
                }
                return String.Format(Constants.CidFormat, Guid.NewGuid().ToString(),
                    String.IsNullOrEmpty(macId) ? Constants.MacId : macId, actionName,
                    String.IsNullOrEmpty(deviceAddress) ? Constants.DeviceAddress : deviceAddress);
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Failed to generate Cid. @Ex: " + ex.Message);
            }
            return string.Empty;
        }
    }
}
