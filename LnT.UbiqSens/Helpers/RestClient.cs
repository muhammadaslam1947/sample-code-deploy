﻿////-----------------------------------------------------------------------
//// <copyright file="RestClient.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

namespace LnT.UbiqSens.Helpers
{
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Threading;

    /// <summary>
    /// HTTP verb supported in the REST API
    /// </summary>
    public enum HttpVerb
    {
        /// <summary>
        /// to get data from URL
        /// </summary>
        Get,

        /// <summary>
        /// To post data to URL
        /// </summary>
        Post,

        /// <summary>
        /// To Put data to URL
        /// </summary>
        Put,

        /// <summary>
        /// To Delete data using URL
        /// </summary>
        Delete
    }

    /// <summary>
    /// Defines post and get of the REST full API
    /// </summary>
    public class RestClient
    {
        /// <summary>
        /// Update Device Status Url
        /// </summary>
        private static string updateDeviceStatusUrl = "http://IP:Port/api/0.1/monitoring/devices/{0}/status";

        /// <summary>
        /// Gets or sets UpdateDeviceStatusUrl
        /// </summary>
        public static string UpdateDeviceStatusUrl
        {
            get { return RestClient.updateDeviceStatusUrl; }
            set { RestClient.updateDeviceStatusUrl = value; }
        }

        /// <summary>
        /// Work Item Request Url
        /// </summary>
        private static string workItemRequestUrl = "http://IP:Port/api/0.1/dataservice/device/{0}";

        /// <summary>
        /// Gets or sets WorkItemRequestUrl
        /// </summary>
        public static string WorkItemRequestUrl
        {
            get { return RestClient.workItemRequestUrl; }
            set { RestClient.workItemRequestUrl = value; }
        }

        /// <summary>
        /// Work Item Response Url
        /// </summary>
        private static string workItemResponseUrl = "http://IP:Port/api/0.1/dataservice/device/{0}/response";

        /// <summary>
        /// Gets or sets workItemResponseUrl
        /// </summary>
        public static string WorkItemResponseUrl
        {
            get { return RestClient.workItemResponseUrl; }
            set { RestClient.workItemResponseUrl = value; }
        }

        /// <summary>
        ///  Initializes a new instance of the RestClient class 
        /// </summary>
        public RestClient()
        {
            this.EndPoint = string.Empty;
            this.Method = HttpVerb.Get;
            this.ContentType = "text/xml";
            this.PostData = null;
        }

        ///////////// <summary>
        ///////////// Initializes a new instance of the RestClient class 
        ///////////// </summary>
        ///////////// <param name="endpoint">string endpoint</param>
        ////////public RestClient(string endpoint)
        ////////{
        ////////    this.EndPoint = endpoint;
        ////////    this.Method = HttpVerb.Get;
        ////////    this.ContentType = "text/xml";
        ////////    this.PostData = null;
        ////////}

        ///////////// <summary>
        /////////////  Initializes a new instance of the RestClient class 
        ///////////// </summary>
        ///////////// <param name="endpoint">endpoint of REST full APi</param>
        ///////////// <param name="method">Http method</param>
        ////////public RestClient(string endpoint, HttpVerb method)
        ////////{
        ////////    this.EndPoint = endpoint;
        ////////    this.Method = method;
        ////////    this.ContentType = "text/xml";
        ////////    this.PostData = null;
        ////////}

        ///////////// <summary>
        /////////////  Initializes a new instance of the RestClient class 
        ///////////// </summary>
        ///////////// <param name="endpoint">endpoint of REST full APi</param>
        ///////////// <param name="method">http verb</param>
        ///////////// <param name="postData">posting data</param>
        ////////public RestClient(string endpoint, HttpVerb method, byte[] postData)
        ////////{
        ////////    this.EndPoint = endpoint;
        ////////    this.Method = method;
        ////////    this.ContentType = "text/xml";
        ////////    this.PostData = postData;
        ////////}

        /// <summary>
        /// Gets or sets End point
        /// </summary>
        public string EndPoint
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets method
        /// </summary>
        public HttpVerb Method
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets content type
        /// </summary>
        public string ContentType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets post data
        /// </summary>
        public byte[] PostData
        {
            get;
            set;
        }

        /// <summary>
        /// Post data REST call
        /// </summary>
        /// <param name="topic">string topic</param>
        /// <param name="postData">post data</param>
        /// <returns>true or false</returns>
        ////public bool Post(string topic, byte[] postData)
        ////{
        ////    try
        ////    {
        ////        if (postData == null)
        ////        {
        ////            throw new Exception("postData cannot be Null");
        ////        }

        ////        var request = (HttpWebRequest)WebRequest.Create(string.Format(WorkItemResponseUrl, topic));

        ////        request.Method = HttpVerb.Post.ToString();
        ////        request.ContentType = "application/xml";
        ////        request.ContentLength = postData.Length;
        ////        using (var writeStream = request.GetRequestStream())
        ////        {
        ////            writeStream.Write(postData, 0, postData.Length);
        ////        }

        ////        using (var response = (HttpWebResponse)request.GetResponse())
        ////        {
        ////            if (response.StatusCode != HttpStatusCode.OK)
        ////            {
        ////                return false;
        ////            }
        ////        }

        ////        return true;
        ////    }
        ////    catch
        ////    {
        ////        return false;
        ////    }
        ////}

        //<summary>
        //posting data to RESTFull api
        //</summary>
        //<param name="apiParameters">method name in REST Full API</param>
        //<param name="haveReturnValue"></param>
        //<returns>true or false</returns>
        public string Post(string apiParameters, bool haveReturnValue, bool isJson)
        {
            var responseString = string.Empty;
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(this.EndPoint + apiParameters);
                request.Method = this.Method.ToString();
                request.ContentLength = 0;
                request.ContentType = this.ContentType;

                if (this.PostData != null && this.Method == HttpVerb.Post)
                {
                    if (haveReturnValue && isJson)
                    {
                        request.ContentType = "application/json";
                        request.Accept = "application/json";
                        request.MediaType = "application/json";
                    }
                    else
                    {
                        request.ContentType = "application/xml";
                        request.Accept = "application/xml";
                        request.MediaType = "application/xml";
                    }
                    request.ContentLength = this.PostData.Length;
                    using (var writeStream = request.GetRequestStream())
                    {
                        writeStream.Write(this.PostData, 0, this.PostData.Length);
                    }

                    using (var response = (HttpWebResponse)request.GetResponse())
                    {
                        if (response.StatusCode == HttpStatusCode.Created || response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Found || response.StatusCode == HttpStatusCode.Accepted)
                        {
                            using (var stream = response.GetResponseStream())
                            {
                                var reader = new StreamReader(stream, Encoding.UTF8);
                                return responseString = reader.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex) {
                UbiqsensBooter.ObjLogger?.WriteLog(PluginInterfaces.LogLevel.Error, "RestClient: Exception Occurred at Post() Method with @apiParameters: " + apiParameters + " @EndPoint: " + this.EndPoint, ex);
            }
            return responseString;
        }

        /// <summary>
        /// Updating data to RESTFull API
        /// </summary>
        /// <param name="topic">string topic</param>
        /// <param name="postData">Byte array post data</param>
        /// <returns>true or false</returns>
        ////public bool Put(string topic, byte[] postData)
        ////{
        ////    try
        ////    {
        ////        if (postData == null)
        ////        {
        ////            throw new Exception("postData cannot be Null");
        ////        }

        ////        if (string.IsNullOrEmpty(UpdateDeviceStatusUrl))
        ////        {
        ////            throw new Exception("EndPoint cannot be Null");
        ////        }

        ////        var request = (HttpWebRequest)WebRequest.Create(string.Format(UpdateDeviceStatusUrl, topic));

        ////        request.Method = HttpVerb.Put.ToString();
        ////        request.ContentLength = 0;
        ////        request.ContentType = this.ContentType;
        ////        request.ContentType = "application/xml";
        ////        request.ContentLength = postData.Length;
        ////        using (var writeStream = request.GetRequestStream())
        ////        {
        ////            writeStream.Write(postData, 0, postData.Length);
        ////        }

        ////        using (var response = (HttpWebResponse)request.GetResponse())
        ////        {
        ////            if (response.StatusCode != HttpStatusCode.OK)
        ////            {
        ////                return false;
        ////            }
        ////        }

        ////        return true;
        ////    }
        ////    catch (Exception)
        ////    {
        ////        return false;
        ////    }
        ////}

        /// <summary>
        /// Pulling data from REST call
        /// </summary>
        /// <param name="apiParameters">API Parameters or URI</param>
        /// <returns>response String in bytes</returns>
        internal byte[] Get(string apiParameters)
        {
            int retryCount = 0;
        TryAgain:
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(workItemRequestUrl + apiParameters);
                request.Method = HttpVerb.Get.ToString();
                request.ContentType = this.ContentType;
                request.Timeout = 60000;
                request.AllowAutoRedirect = false;
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var responseString = string.Empty;
                    if (response.StatusCode == HttpStatusCode.Accepted || response.StatusCode == HttpStatusCode.Found || response.StatusCode == HttpStatusCode.OK)
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            var reader = new StreamReader(stream, Encoding.UTF8);
                            responseString = reader.ReadToEnd();
                        }
                    }

                    return ASCIIEncoding.ASCII.GetBytes(responseString);
                }
            }
            catch (System.Exception ex)
            {
                retryCount++;
                if (retryCount == int.MaxValue)
                    retryCount = 0;
                UbiqsensBooter.ObjLogger.WriteLog(PluginInterfaces.LogLevel.Error, "RestClient: REST(UBIQRede API) Conenctivity falied \r\n ,Get() Method with @apiParameters: " + apiParameters, ex);
                UbiqsensBooter.ObjLogger.WriteLog(PluginInterfaces.LogLevel.Info, "RestClient: Retrying - REST(UBIQRede API) Conenctivity falied, Get() Method with @apiParameters: " + retryCount);
                Thread.Sleep(30000);
                goto TryAgain;
            }
        }

        ///////////// <summary>
        ///////////// To delete data using REST call
        ///////////// </summary>
        ///////////// <param name="apiParameters">apiParameters or URI</param>
        ///////////// <returns>true or false</returns>
        ////////internal bool Delete(string apiParameters)
        ////////{
        ////////    WebRequest request = WebRequest.Create(this.EndPoint + apiParameters);
        ////////    request.Method = HttpVerb.Delete.ToString();
        ////////    var response = (HttpWebResponse)request.GetResponse();
        ////////    return true;
        ////////}
    }
}
