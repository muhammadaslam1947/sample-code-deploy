﻿////-----------------------------------------------------------------------
//// <copyright file="Constants.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------
namespace LnT.UbiqSens.Helpers
{
    using System;
    using System.Collections.Generic;
    /// <summary>
    /// All constants used in this project are listed below and used
    /// </summary>
    public class Constants
    {
        #region constant string

        /// <summary>
        /// Auto detect head
        /// </summary>
        public const string AutoDetectHead = "<Message MessageType='AutoDetectedDevices' DateTime='$'>";

        /// <summary>
        /// auto detect body
        /// </summary>
        public const string AutoDetectBody = "<DeviceDetails DeviceName='@' CommunicationMode='#' />";

        /// <summary>
        /// Auto detect footer
        /// </summary>
        public const string AutoDetectFooter = "</Message>";

        /// <summary>
        /// Gets dollar symbol
        /// </summary>
        public const string AtSymbol = "@";

        /// <summary>
        /// Gets # symbol
        /// </summary>
        public const string HashSymbol = "#";

        /// <summary>
        /// New line
        /// </summary>
        public const string NewLine = "\r\n";

        /// <summary>
        /// Gets $ symbol
        /// </summary>
        public const char DollarSymbol = '$';

        /// <summary>
        /// Auto Detected Topic
        /// </summary>
        public const string AutoDetectedTopic = "Server/AUTO_DETECT/AUTO_DETECTED_DEVICES";

        /// <summary>
        /// Auto Detected Topic
        /// </summary>
        public const string WITopic = "Server/WORK_ITEM/";

        /// <summary>
        /// Device Type LookUp File
        /// </summary>
        public const string DeviceTypeLookUpFile = "\\PolicyFiles\\DeviceTypeLookUp.xml";

        /// <summary>
        /// Device Type
        /// </summary>
        public const string DeviceType = "DeviceType";

        /// <summary>
        /// name string
        /// </summary>
        public const string Name = "Name";

        /// <summary>
        /// string Request Type
        /// </summary>
        public const string RequestType = "RequestType";

        /// <summary>
        /// string Response Type
        /// </summary>
        public const string ResponseType = "ResponseType";

        /// <summary>
        /// string request Response
        /// </summary>
        public const string RequestResponse = "RequestResponse";

        /// <summary>
        /// string Request continuos Response
        /// </summary>
        public const string RequestContinuousResponse = "RequestContinuousResponse";

        /// <summary>
        /// String Single
        /// </summary>
        public const string Single = "Single";

        /// <summary>
        /// Device Policy LookUp Parser
        /// </summary>
        public const string DevicePolicyLookUpParser = "//DeviceTypePolicy/DeviceTypes/DeviceType/CommunicationPlugin";

        /// <summary>
        /// Mode
        /// </summary>
        public const string Mode = "Mode";

        /// <summary>
        /// Communication plugin string
        /// </summary>
        public const string Communicationplugin = "Communicationplugin";

        /// <summary>
        /// Server URL
        /// </summary>
        public const string StrUbiqRedeServerUrl = "UBIQRedeServerURL";

        /// <summary>
        /// Server device status URL
        /// </summary>
        public const string UbiqRedeDeviceStatusUrl = "UBIQRedeDeviceStatusURL";

        /// <summary>
        /// Server device response URL
        /// </summary>
        public const string UbiqRedeDeviceResponse = "UBIQRedeDeviceResponse";

        /// <summary>
        /// server work item URL
        /// </summary>
        public const string UbiqRedeDeviceWorkItem = "UBIQRedeDeviceWorkItem";

        /// <summary>
        /// string Schedule
        /// </summary>
        public const string StrSchedule = "schedule";

        /// <summary>
        /// Device Inactive status
        /// </summary>
        public const string StrInactive = "Inactive";

        /// <summary>
        /// Device Active status
        /// </summary>
        public const string StrActive = "Active";

        /// <summary>
        /// string Device
        /// </summary>
        public const string Device = "Device";

        /// <summary>
        /// string GatewayConnection
        /// </summary>
        public const string GatewayConnection = "GatewayConnection";

        /// <summary>
        /// Start date
        /// </summary>
        public const string StrStartDate = "StartDate";

        /// <summary>
        /// End date
        /// </summary>
        public const string StrEndDate = "EndDate";

        /// <summary>
        /// Start time
        /// </summary>
        public const string StrStartTime = "StartTime";

        /// <summary>
        /// End time
        /// </summary>
        public const string StrEndTime = "EndTime";

        /// <summary>
        /// string Days
        /// </summary>
        public const string StrDays = "Days";

        /// <summary>
        /// string Dollar
        /// </summary>
        public const string StrDollar = "$";

        /// <summary>
        /// string Delay
        /// </summary>
        public const string StrDelay = "Delay";

        /// <summary>
        /// To encrypt
        /// </summary>
        public const string StrToEncrypt = "ToEncrypt";

        /// <summary>
        /// Encryption Algorithm
        /// </summary>
        public const string StrEncryptionAlgorithm = "EncryptionAlgorithm";

        /// <summary>
        /// Encryption Password
        /// </summary>
        public const string StrEncryptionPassword = "EncryptionPassword";

        /// <summary>
        /// is Patient App
        /// </summary>
        public const string StrIsAppQueueRequired = "IsAppQueueRequired";

        /// <summary>
        /// Device protocol plugin
        /// </summary>
        public const string DeviceProtocolPlugins = "DeviceProtocolPlugins";

        /// <summary>
        /// Gateway Connection Plugin
        /// </summary>
        public const string GatewayConnectionPlugins = "GatewayConnectionPlugins";

        /// <summary>
        /// Device Connection Plugin string
        /// </summary>
        public const string DeviceConnectionPlugins = "DeviceConnectionPlugins";

        /// <summary>
        /// 32 bit machine path
        /// </summary>
        public const string Xpath86 = "\\x86";

        /// <summary>
        /// 64 bit machine path
        /// </summary>
        public const string Xpath64 = "\\x64";

        /// <summary>
        /// Gateway Protocol Plugins
        /// </summary>
        public const string GatewayProtocolPlugins = "GatewayProtocolPlugins";

        /// <summary>
        /// Storage Service Plugins
        /// </summary>
        public const string StorageServicePlugins = "StorageServicePlugins";

        /// <summary>
        /// Encrypt Decrypt Data Plugins
        /// </summary>
        public const string EncryptDecryptDataPlugins = "EncryptDecryptDataPlugins";

        /// <summary>
        /// Logger Plugins
        /// </summary>
        public const string StrILoggerPlugins = "LoggerPlugins";

        /// <summary>
        /// char One
        /// </summary>
        public const char StrOne = '1';

        /// <summary>
        /// Constant DLL
        /// </summary>
        public const string StrDll = "*.dll";

        /// <summary>
        /// Domain Specific
        /// </summary>
        public const string StrDomainSpecific = "DomainSpecific";

        /// <summary>
        /// Gets Device Parameters
        /// </summary>
        public const string DeviceParameters = "DeviceParameters";

        /// <summary>
        /// Gets Protocol Plugin
        /// </summary>
        public const string ProtocolPlugin = "ProtocolPlugin";

        /// <summary>
        /// Gets Protocol Plugin
        /// </summary>
        public const string LPort = "ListenerPort";

        /// <summary>
        /// Gets ReceiveTimeout
        /// </summary>
        public const string ReceiveTimeout = "ReceiveTimeout";

        /// <summary>
        /// Gets SendTimeout
        /// </summary>
        public const string SendTimeout = "SendTimeout";

        /// <summary>
        /// Gets ListenerPort
        /// </summary>
        public const string ListenerPort = "PortNumber";

        /// <summary>
        /// Gets ReceiveBufferSize
        /// </summary>
        public const string ReceiveBufferSize = "ReceiveBufferSize";

        /// <summary>
        /// Gets validationParameter
        /// </summary>
        public const string ValidationParameter = "ValidationParameter";

        /// <summary>
        /// Gets Work item
        /// </summary>
        public const string Workitem = "Workitem";

        /// <summary>
        /// Gets Work item
        /// </summary>
        public const string Schedule = "Schedule";

        /// <summary>
        /// Gets Work item
        /// </summary>
        public const string ScheduleWorkitem = "ScheduleWorkItem";

        /// <summary>
        /// Gets /
        /// </summary>
        public const char Slash = '/';

        /// <summary>
        /// Gets DevicePolicyPath
        /// </summary>
        public const string DevicePolicyPath = "\\PolicyFiles\\DevicePolicy\\";

        /// <summary>
        /// Gets DeviceTypeLookUpPath
        /// </summary>
        public const string DeviceTypeLookUpPath = "PolicyFiles\\";

        /// <summary>
        /// Gets DeviceTypeLookUp
        /// </summary>
        public const string DeviceTypeLookUp = "DeviceTypeLookUp.xml";

        /// <summary>
        /// Gets DeviceNotAvailableMessage
        /// </summary>
        public const string DeviceNotAvailableMessage = "<WorkItemResponse Response='Device Not Available' /></Message>";

        /// <summary>
        /// Gets xml string
        /// </summary>
        public const string Xml = ".xml";

        /// <summary>
        /// Gets SlashR
        /// </summary>
        public const string SlashR = "\r";

        /// <summary>
        /// Gets SlashN
        /// </summary>
        public const string SlashN = "\n";

        /// <summary>
        /// Gets Create
        /// </summary>
        public const string Create = "create";

        /// <summary>
        /// Gets update
        /// </summary>
        public const string UpdateUsmall = "update";

        /// <summary>
        /// Gets Update
        /// </summary>
        public const string GetAllDevices = "devices";

        /// <summary>
        /// Gets Delete
        /// </summary>
        public const string Delete = "delete";

        /// <summary>
        /// Gets Server_DEVICE_STATUS
        /// </summary>
        public const string DeviceStatusTopic = "Server/DEVICE_STATUS";

        /// <summary>
        /// Gets Message
        /// </summary>
        public const string Message = "Message";

        /// <summary>
        /// Gets MessageType
        /// </summary>
        public const string MessageType = "MessageType";

        /// <summary>
        /// Gets the Correlation Id
        /// </summary>
        public const string CorrelationId = "Cid";

        /// <summary>
        /// Gets the ThermostatLocationName
        /// </summary>
        public const string ThermostatLocationName = "ThermostatLocationName";

        /// <summary>
        /// Gets Status
        /// </summary>
        public const string Status = "Status";

        /// <summary>
        /// Gets MACId
        /// </summary>
        public const string MacId = "MACId";

        /// <summary>
        /// Gets Action
        /// </summary>
        public const string Action = "Action";

        /// <summary>
        /// Gets Cid format 
        /// {0} -Guid created by Comm
        /// {1} -MAC ID provided by protocol adapter
        /// {2} -Action decided by Comm & protocol adapter
        /// {3} - SourceIp_SourcePort info provided by protocol adapter
        /// </summary>
        public const string CidFormat = "C{0}_{1}_UserId_TstatId_{2}_{3}";

        /// <summary>
        /// connect string 
        /// </summary>
        public const string Connect = "Connect";

        /// <summary>
        /// Gets Disconnect
        /// </summary>
        public const string Disconnect = "Disconnect";

        /// <summary>
        /// Gets WorkItem
        /// </summary>
        public const string WorkItem = "WorkItem";

        /// <summary>
        /// Gets Command ID
        /// </summary>
        public const string CmdId = "CmdId";

        /// <summary>
        /// Gets Delay
        /// </summary>
        public const string Delay = "Delay";

        /// <summary>
        /// Gets Value
        /// </summary>
        public const string Value = "Value";

        /// <summary>
        /// Gets NOTIFICATIONS
        /// </summary>
        public const string NOTIFICATIONS = "NOTIFICATIONS";

        /// <summary>
        /// Gets AUTO_DETECTED_DEVICES
        /// </summary>
        public const string AutoDetectedDeviceTopic = "AUTO_DETECTED_DEVICES";

        /// <summary>
        /// Gets WORK_ITEM
        /// </summary>
        public const string WorkItemTopic = "WORK_ITEM";

        /// <summary>
        /// Gets WORK_ITEM
        /// </summary>
        public const string ScheduleWorkItemTopic = "SCHEDULE_WORKITEM";
        /// <summary>
        /// Gets device
        /// </summary>
        public const string DeviceDsmallCase = "device";

        /// <summary>
        /// Gets deviceType
        /// </summary>
        public const string DeviceTypeDsmallCase = "deviceType";

        /// <summary>
        /// Gets modeID
        /// </summary>
        public const string ModeID = "ModeID";


        /// <summary>
        /// Gets device ID
        /// </summary>
        public const string Deviceidd = "DeviceId";
        /// <summary>
        /// Gets ErrorInSendingCommand
        /// </summary>
        public const string ErrorInSendingCommand = "Error in Sending Command";

        /// <summary>
        /// Gets Command Not found
        /// </summary>
        public const string CommandNotfound = "Command Not Found";

        /// <summary>
        /// Gets PushData
        /// </summary>
        public const string PushData = "PushData";

        /// <summary>
        /// Gets SaveData
        /// </summary>
        public const string SaveData = "SaveData";

        /// <summary>
        /// Gets PolicyFiles_GatewayXml
        /// </summary>
        public const string GatewayPolicyFilePath = "\\PolicyFiles\\Gateway.xml";

        /// <summary>
        /// Gets Identity
        /// </summary>
        public const string Identity = "Identity";

        /// <summary>
        /// Gets GatewayUID
        /// </summary>
        public const string GatewayUID = "GatewayUID";

        /// <summary>
        /// Gets Server
        /// </summary>
        public const string Server = "Server";

        /// <summary>
        /// Gets Icon
        /// </summary>
        public const string UbiqsensIcon = "\\ubiqsens.ico";

        /// <summary>
        /// Gets TransportEndpointDetail
        /// </summary>
        public const string TransportEndpointDetail = "TransportEndpointDetail";

        /// <summary>
        /// Gets REST
        /// </summary>
        public const string REST = "REST";

        /// <summary>
        /// Gets HTTPServerIPAddress
        /// </summary>
        public const string HTTPServerIPAddress = "HTTPServerIPAddress";

        /// <summary>
        /// Gets HTTPServerIPAddress
        /// </summary>
        public const string HTTPServerPort = "HTTPServerPort";

        /// <summary>
        /// Gets IP
        /// </summary>
        public const string IP = "IP";

        /// <summary>
        /// Gets Port
        /// </summary>
        public const string Port = "Port";

        /// <summary>
        /// Gets RestGatewayStatusURL
        /// </summary>
        public const string RestGatewayStatusURL = "http://IP:Port/api/0.1/mgmt/gateway/{0}/status";

        /// <summary>
        /// Add device URL
        /// </summary>
        public const string RestAddDeviceURL = "http://{0}:{1}/api/0.1/mgmt/aprilaire/devices";

        /// <summary>
        /// Add device URL
        /// </summary>
        public const string DeviceFormat = "<devices><deviceInfo><device><deviceDescription>{0}</deviceDescription><deviceDisplayName>{0}</deviceDisplayName><deviceName>{0}</deviceName><deviceType><deviceTypeId>1</deviceTypeId></deviceType><gateway><gatewayId>1</gatewayId></gateway><gatewayMode><gatewayModeId>2</gatewayModeId></gatewayMode><modeID><modeID>1</modeID></modeID><status>1</status></device><deviceParameters><deviceParameter><parameter><parameterId>17</parameterId></parameter><parameterValue>{1}</parameterValue></deviceParameter><deviceParameter><parameter><parameterId>18</parameterId></parameter><parameterValue>8760</parameterValue></deviceParameter></deviceParameters></deviceInfo></devices>";

        /// <summary>
        /// Gets RestPolicyDeviceURL
        /// </summary>
        public const string RestPolicyDeviceURL = "http://{0}:{1}/api/0.1/policy/device/";

        /// <summary>
        /// Gets RestDeviceTypeLookUp
        /// </summary>
        public const string RestDeviceTypeLookUpURL = "http://{0}:{1}/api/0.1/policy/devicetype/";

        /// <summary>
        /// Gets DeviceStatusUpdateURL
        /// </summary>
        public const string DeviceStatusUpdateURL = "http://IP:Port/api/0.1/monitoring/devices/{0}/status";

        /// <summary>
        /// Gets QueueStorage
        /// </summary>
        public const string QueueStorage = "QueueStorage";

        /// <summary>
        /// Gets LoggingMechanism
        /// </summary>
        public const string LoggingMechanism = "LoggingMechanism";

        /// <summary>
        /// Gets Logger
        /// </summary>
        public const string Logger = "Logger";

        /// <summary>
        /// Gets LogFilename
        /// </summary>
        public const string LogFilename = "LogFilename";

        /// <summary>
        /// Gets LogPath
        /// </summary>
        public const string LogPath = "LogPath";

        /// <summary>
        /// Gets LogToAppend
        /// </summary>
        public const string LogToAppend = "LogToAppend";

        /// <summary>
        /// Gets LogMaxFileSizeInMB
        /// </summary>
        public const string LogMaxFileSizeInMB = "LogMaxFileSizeInMB";

        /// <summary>
        /// Gets EncryptionPassword
        /// </summary>
        public const string EncryptionPassword = "EncryptionPassword";

        /// <summary>
        /// Gets EncryptionAlgorithm
        /// </summary>
        public const string EncryptionAlgorithm = "EncryptionAlgorithm";

        /// <summary>
        /// Gets ToEncrypt
        /// </summary>
        public const string ToEncrypt = "ToEncrypt";

        /// <summary>
        /// Gets DataSecurity
        /// </summary>
        public const string DataSecurity = "DataSecurity";

        /// <summary>
        /// Gets StorageService
        /// </summary>
        public const string StorageService = "StorageService";

        /// <summary>
        /// Gets To Store
        /// </summary>
        public const string ToStore = "ToStore";

        #endregion constant string

        /// <summary>
        /// Response foot
        /// </summary>
        public const string ResponseFoot = "</Message>";

        /// <summary>
        /// Response body
        /// </summary>
        private static string responseBody = "<WorkItemResponse CmdId='{0}' Response='{1}' />";

        /// <summary>
        /// Frequency To Collect
        /// </summary>
        public const string FrequencyToCollect = "FTCD";

        /// <summary>
        /// Device status
        /// </summary>
        public const string DeviceStatus = "DeviceStatus";

        /// <summary>
        /// Active
        /// </summary>
        public const string Active = "Active";

        /// <summary>
        /// InActive
        /// </summary>
        public const string InActive = "InActive";

        /// <summary>
        /// Connect WorkItem
        /// </summary>
        public const string ConnectWorkItem = "<Message MessageType=\"Status\"> <Status>Connect</Status> </Message>";

        /// <summary>
        /// Disconnect WorkItem
        /// </summary>
        public const string DisconnectWorkItem = "<Message MessageType=\"Status\"> <Status>Disconnect</Status> </Message>";


        /// <summary>
        /// Array days
        /// </summary>
        private static string[] days = { "sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday" };

        /// <summary>
        /// Gets Action list
        /// </summary>
        public static List<string> ActionList = new List<string>() {"Connect",
                                                            "ThermostatInstallerSettings",
                                                            "ContractorInformation",
                                                            "Scale",
                                                            "DateAndTime",
                                                            "AirCleaningInstallerSettings",
                                                            "HumidityControllerInstallerSettings",
                                                            "FreshAirInstallerSettings",
                                                            "ZoningInstallerSettings",
                                                            "ThermostatSetpointModeSettings",
                                                            "WeatherUpdate",
                                                            "HumidificationSetpoint",
                                                            "DehumidificationSetpoint",
                                                            "FreshAirSetting",
                                                            "AirCleaningSettings",
                                                            "ThermostatIAQAvailable",
                                                            "ScheduleSettings",
                                                            "AwaySettings",
                                                            "ScheduleDay",
                                                            "ScheduleHold",
                                                            "HeatBlast",
                                                            "ServiceRemindersStatus",
                                                            "AlertsStatus",
                                                            "AlertsSettings",
                                                            "SensorValues",
                                                            "ControllingSensorStatusValue",
                                                            "SupportModules",
                                                            "WrittenOutdoorTempValue",
                                                            "Sync",
                                                            "ThermostatStatus",
                                                            "IAQStatus",
                                                            "ThermostatError",
                                                            "RevisionModel",
                                                            "MACAddress",
                                                            "ConnectionStatus",
                                                            "ThermostatLocationName",
                                                            "ThermostatName",
                                                            "Unregister",
                                                            "FirmwareUpdate",
                                                            "ThreeDayForecast",
                                                            "InitiateGainspanUpdate",
                                                            "FirmwareUpdatePayload",
                                                            "LCDBacklightSettings",
                                                            "LockoutSettings",
                                                            "Ping",
                                                            "NA" };

        /// <summary>
        /// Current path
        /// </summary>
        private static string currentPath = AppDomain.CurrentDomain.BaseDirectory + "\\";

        /// <summary>
        /// Gets or sets value to Days
        /// </summary>
        public static string[] Days
        {
            get { return Constants.days; }
            set { Constants.days = value; }
        }

        /// <summary>
        /// Gets or sets value to response Body
        /// </summary>
        public static string ResponseBody
        {
            get { return Constants.responseBody; }
            set
            {
                Constants.responseBody = value;
            }
        }

        /// <summary>
        /// Gets or sets value to Current Path
        /// </summary>
        public static string CurrentPath
        {
            get { return Constants.currentPath; }
            set { Constants.currentPath = value; }
        }

        /// <summary>
        /// Gets Date time format
        /// </summary>
        public static string DateTimeFormat
        {
            get
            {
                // return string.Format("{0} {1}", DateTime.Now.ToString("G"), TimeZoneInfo.Local.ToString().Substring(0, TimeZoneInfo.Local.ToString().IndexOf(')') + 1));


                //*****************************************Diagnostic purpose//*****************************************//
                return DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss:fff tt");
                //*****************************************Diagnostic purpose//*****************************************//

            }
        }

        /// <summary>
        /// Gets message format
        /// </summary>
        public static string Notification
        {
            get
            {
                return "<Message MessageType='Notification' DateTime='" + DateTimeFormat + "' Cid='{1}'><Notification value='{0}'/></Message>";
            }
        }

        /// <summary>
        /// Gets Response header
        /// </summary>
        public static string ResponseHead
        {
            get
            {
                return "<Message MessageType='WorkItemResponse' DateTime='" + DateTimeFormat + "' Cid='{0}'>";
            }
        }

        /// <summary>
        /// Gets Response header
        /// </summary>
        public static string ScheduleResponseHead
        {
            get
            {
                return "<Message MessageType='ScheduleWorkItemResponse' DateTime='" + DateTimeFormat + "'>";
            }
        }

        /// <summary>
        /// Gets Response header
        /// </summary>
        public static string DeviceNotAvailable
        {
            get
            {
                return "<Message MessageType='WorkItemResponse' DateTime='" + DateTimeFormat + "'>" + "<WorkItemResponse Response='Device Not Available' /></Message>";
            }
        }

        /// <summary>
        /// Gets Device not connected
        /// </summary>
        public static string DeviceNotConnected
        {
            get
            {
                return "<Message MessageType='WorkItemResponse' DateTime='" + DateTimeFormat + "'>" + "<WorkItemResponse Response='Device Not Connected' /></Message>";
            }
        }

        /// <summary>
        /// Gets Disconnected message
        /// </summary>
        public static string Disconnected
        {
            get
            {
                return "<Message MessageType='Status'  DateTime='" + DateTimeFormat + "' IPAddress='{1}' Cid='{2}' ><Status Reason='{0}'>Disconnected</Status></Message>";
            }
        }

        /// <summary>
        /// Gets Connected message
        /// </summary>
        public static string Connected
        {
            get
            {
                return "<Message MessageType='Status' DateTime='" + DateTimeFormat + "' IPAddress='{0}' ><Status>Connected</Status></Message>";
            }
        }

        /// <summary>
        /// Gets Connected message
        /// </summary>
        public static string FailToInitialiseProtocol
        {
            get
            {
                return "<Message MessageType='ErrorResponse' DateTime='" + DateTimeFormat + "'><ErrorResponse Error='Fail To Initialise Protocol' /></Message>";
            }
        }

        /// <summary>
        /// Gets WorkItem Ignored
        /// </summary>
        public static string WorkItemIgnored
        {
            get
            {
                return "<Message MessageType='ErrorResponse' DateTime='" + DateTimeFormat + "'><ErrorResponse Error='Requested Workitem Ignored' /></Message>";
            }
        }

        /// <summary>
        /// Gets Invalid Work item
        /// </summary>
        public static string InvalidWorkitem
        {
            get
            {
                return "<Message MessageType='ErrorResponse' DateTime='" + DateTimeFormat + "' Cid='{0}'><ErrorResponse Error='Invalid Workitem' /></Message>";
            }
        }

        /// <summary>
        /// Source IP of thermostat
        /// </summary>
        public const string DeviceAddress = "SourceIP_SourcePort";

        // This default port for location service port.
        public const int LocationServicePort = 58766;

        /// <summary>
        /// Disconnection Reason
        /// </summary>
        public const string Disconnect_Validation = "MACId Validation Failed";
        public const string Disconnect_Protocol = "Unable to initialize Protocol Plugin";
        public const string Disconnect_Health = "Health check Failed";
        public const string Disconnect_Idle = "Idle timeout(Web)";
        public const string Disconnect_Duplicate = "Duplicate Socket Disconnection";
        public const string Disconnect_Request = "Disconnect Request from Web";
     
        public static byte[] Heartbeat_Comm = new byte[] { 01, 00, 00, 04, 01, 07, 02, 02, 149 };
        public static string Automation = "Automation";
    }
}