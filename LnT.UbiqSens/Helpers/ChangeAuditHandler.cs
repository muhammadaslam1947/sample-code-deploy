﻿﻿////-----------------------------------------------------------------------
//// <copyright file="ChangeAuditHandler.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

 ////namespace LnT.UbiqSens.Helpers
////{
////    using LnT.UbiqSens.PluginInterfaces;
////    using Newtonsoft.Json;
////    using System;
////    using System.Text;

////    /// <summary>
////    /// Change Audit is for tracking the changes
////    /// </summary>
////    internal class ChangeAuditHandler
////    {
////        #region  Private Fields
////        private static ChangeAuditHandler _instance = null;
////        #endregion

////        #region Constructor
////        private ChangeAuditHandler()
////        {
////        }
////        #endregion

////        #region Methods
////        /// <summary>
////        /// Get the singleton instance
////        /// </summary>
////        /// <returns>Singleton of this object</returns>
////        internal static ChangeAuditHandler GetInstance()
////        {
////            return _instance ?? (_instance = new ChangeAuditHandler());
////        }

////        /// <summary>
////        /// To log to ChangeAudit
////        /// </summary>
////        /// <param name="componet">componet</param>
////        /// <param name="description">description</param>
////        /// <param name="subComponent">subComponent</param>
////        /// <param name="author">author</param>
////        /// <param name="deviceId">deviceId</param>
////        /// <param name="gatewayId">gatewayId</param>
////        /// <param name="applicationId">applicationId</param>
////        /// <param name="cmd">cmd</param>
////        /// <returns>string</returns>
////        public string AddChangeAudit(string componet, string description, string subComponent, string author, string deviceId, string gatewayId, string applicationId, string cmd)
////        {
////            var data = string.Empty;
////            var restClient = new RestClient();
////            restClient.Method = HttpVerb.Post;
////            restClient.EndPoint = cmd;
////            try
////            {
////                ChangeAudit changeAudit = new ChangeAudit();
////                changeAudit.Component = componet;
////                changeAudit.Description = description;
////                changeAudit.SubComponent = subComponent;
////                changeAudit.Author = author;
////                if (!string.IsNullOrEmpty(deviceId))
////                    changeAudit.DeviceId = Convert.ToInt64(deviceId);
////                if (!string.IsNullOrEmpty(gatewayId))
////                    changeAudit.GatewayId = Convert.ToInt64(gatewayId);
////                if (!string.IsNullOrEmpty(applicationId))
////                    changeAudit.AppId = Convert.ToInt64(applicationId);
////                var dataToSend = JsonConvert.SerializeObject(changeAudit);
////                restClient.PostData = ASCIIEncoding.ASCII.GetBytes(dataToSend);
////                data = restClient.Post(string.Empty, true, true);
////            }
////            catch (Exception exception)
////            {
////                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, exception.Message);
////            }
////            return data;
////        }

////        #endregion
////    }
////}
