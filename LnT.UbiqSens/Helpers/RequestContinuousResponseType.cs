﻿
//namespace LnT.UbiqSens.Helpers
//{
//    using LnT.UbiqSens.PluginInterfaces;

//    /// <summary>
//    /// Data structure for the device sending the data continuously
//    /// </summary>
//    public class RequestContinuousResponseType
//    {
//        /// <summary>
//        /// Object Device Con
//        /// </summary>
//        private IDeviceConnection objDeviceCon;

//        /// <summary>
//        /// Object Device Protocol
//        /// </summary>
//        private IDeviceProtocol objDeviceProtocol;

//        /// <summary>
//        /// frequency To Collect
//        /// </summary>
//        private int frequencyToCollect;

//        /// <summary>
//        /// device ID
//        /// </summary>
//        private string deviceId;


//        /// <summary>
//        /// Initializes the new instance of RequestContinuousResponseType
//        /// </summary>
//        public RequestContinuousResponseType()
//        {
//            this.deviceId = string.Empty;
//        }

//        /// <summary>
//        /// Gets or sets frequencyToCollect
//        /// </summary>
//        public int FrequencyToCollect
//        {
//            get { return this.frequencyToCollect; }
//            set { this.frequencyToCollect = value; }
//        }

//        /// <summary>
//        /// Gets or sets objDeviceProtocol
//        /// </summary>
//        public IDeviceProtocol ObjDeviceProtocol
//        {
//            get { return this.objDeviceProtocol; }
//            set { this.objDeviceProtocol = value; }
//        }

//        /// <summary>
//        /// Gets or sets objDeviceCon
//        /// </summary>
//        public IDeviceConnection ObjDeviceCon
//        {
//            get { return this.objDeviceCon; }
//            set { this.objDeviceCon = value; }
//        }

//        /// <summary>
//        /// Gets or sets deviceId
//        /// </summary>
//        public string DeviceId
//        {
//            get { return this.deviceId; }
//            set { this.deviceId = value; }
//        }
//    }
//}
