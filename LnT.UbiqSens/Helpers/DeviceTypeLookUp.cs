﻿////-----------------------------------------------------------------------
//// <copyright file="DeviceTypeLookUp.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

namespace LnT.UbiqSens.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using LnT.UbiqSens.Plugins.EthernetGatewayHandler;

    /// <summary>
    /// Class device type look up
    /// </summary>
    public class DeviceTypeLookUp
    {
        /// <summary>
        /// xml document device type look up
        /// </summary>
        private static XmlDocument deviceTypeLookUp = null;

        /// <summary>
        /// Is Virtual Device Type
        /// </summary>
        /// <param name="deviceType">device type</param>
        /// <returns>true or false</returns>
        public static bool IsVirtualDeviceType(string deviceType)
        {
            var isVirtual = false;
            var path = AppDomain.CurrentDomain.BaseDirectory + "\\PolicyFiles\\DeviceTypeLookUp.xml";

            if (deviceTypeLookUp == null)
            {
                if (UbiqsensBooter.IsSecure)
                {
                    //Read Encrypted Deviec type XML
                    String resultXml = AES128.GetInstance().Decrypt(File.ReadAllText(path), UbiqsensBooter.SecurityKey);
                    deviceTypeLookUp = new ConfigurationReader().GetXmlFromContent(resultXml);
                }
                else
                {
                    deviceTypeLookUp = new ConfigurationReader().GetXml(path);
                }
            }

            var subDoc = new XmlDocument();
            var nodeList = deviceTypeLookUp.GetElementsByTagName(Constants.DeviceType);
            foreach (XmlNode objDeviceType in nodeList)
            {
                subDoc.LoadXml(objDeviceType.OuterXml);
                if (subDoc.GetElementsByTagName(Constants.Name)[0].InnerText.ToLower(CultureInfo.InvariantCulture) == deviceType.ToLower(CultureInfo.InvariantCulture))
                {
                    if (subDoc.GetElementsByTagName("Virtual")[0].InnerText.ToLower(CultureInfo.InvariantCulture) == "true")
                    {
                        isVirtual = true;
                        break;
                    }
                }
            }

            return isVirtual;
        }

        /// <summary>
        /// Is Virtual Device Type
        /// </summary>
        /// <param name="deviceType">device type</param>
        /// <returns>true or false</returns>
        public static bool IsSingleResponse(string deviceType)
        {
            var path = AppDomain.CurrentDomain.BaseDirectory + Constants.DeviceTypeLookUpFile;
            if (UbiqsensBooter.IsSecure)
            {
                //Read Encrypted Deviec type XML
                String resultXml = AES128.GetInstance().Decrypt(File.ReadAllText(path), UbiqsensBooter.SecurityKey);
                deviceTypeLookUp = new ConfigurationReader().GetXmlFromContent(resultXml);
            }
            else
            {
                deviceTypeLookUp = new ConfigurationReader().GetXml(path); ;
            }

            var subDoc = new XmlDocument();
            var nodeList = deviceTypeLookUp.GetElementsByTagName(Constants.DeviceType);
            var isSingleResponseType = false;
            foreach (XmlNode objDeviceType in nodeList)
            {
                subDoc.LoadXml(objDeviceType.OuterXml);
                if (subDoc.GetElementsByTagName(Constants.Name)[0].InnerText.ToLower(CultureInfo.InvariantCulture) == deviceType.ToLower(CultureInfo.InvariantCulture))
                {
                    if (subDoc.GetElementsByTagName(Constants.ResponseType)[0].InnerText.ToLower(CultureInfo.InvariantCulture) == Constants.Single.ToLower(CultureInfo.InvariantCulture))
                    {
                        isSingleResponseType = true;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            return isSingleResponseType;
        }

        ///// <summary>
        ///// Get Request Continuous Response Type
        ///// </summary>
        ///// <param name="deviceType">device Type</param>
        ///// <param name="parameterDict">parameter Dictionary</param>
        ///// <returns>Request Continuous Response Type</returns>
        //public static RequestContinuousResponseType GetRequestContinuousResponseType(string deviceType, out Dictionary<string, string> parameterDict)
        //{
        //    try
        //    {
        //        var path = AppDomain.CurrentDomain.BaseDirectory + Constants.DeviceTypeLookUpFile;
        //        var deviceTypeLookUp = new ConfigurationReader().GetXml(path);
        //        var subDoc = new XmlDocument();
        //        var nodeList = deviceTypeLookUp.GetElementsByTagName(Constants.DeviceType);
        //        RequestContinuousResponseType objRequestContinuousResponseType = null;
        //        parameterDict = new Dictionary<string, string>();
        //        foreach (XmlNode objDeviceType in nodeList)
        //        {
        //            subDoc.LoadXml(objDeviceType.OuterXml);
        //            ////////Console.WriteLine(objDeviceType.OuterXml.ToString());
        //            if (subDoc.GetElementsByTagName(Constants.Name).Count > 0 && subDoc.GetElementsByTagName(Constants.Name)[0].InnerText.ToLower(CultureInfo.InvariantCulture) == deviceType.ToLower(CultureInfo.InvariantCulture))
        //            {
        //                objRequestContinuousResponseType = new RequestContinuousResponseType();
        //                objRequestContinuousResponseType.ObjDeviceCon = new DeviceConnectionObjectFactory().GetIDeviceConnectionObject(subDoc.GetElementsByTagName(Constants.Communicationplugin)[0].InnerText);
        //                objRequestContinuousResponseType.ObjDeviceProtocol = new DeviceProtocolObjectFactory().GetIDeviceProtocolObject(subDoc.GetElementsByTagName(Constants.ProtocolPlugin)[0].InnerText);
        //                objRequestContinuousResponseType.FrequencyToCollect = Convert.ToInt32(subDoc.GetElementsByTagName(Constants.FrequencyToCollect)[0].InnerText);
        //                var deviceInfo = new Dictionary<string, string>();

        //                foreach (XmlNode para in subDoc.GetElementsByTagName(Constants.DeviceParameters))
        //                {
        //                    parameterDict.Add(para.InnerText, string.Empty);
        //                }

        //                foreach (var kvp in objRequestContinuousResponseType.ObjDeviceCon.DeviceInfo)
        //                {
        //                    if (parameterDict.Keys.Contains(kvp.Key))
        //                    {
        //                        continue;
        //                    }

        //                    var value = subDoc.GetElementsByTagName(kvp.Key)[0].InnerText;
        //                    parameterDict.Add(kvp.Key, value);
        //                }

        //                return objRequestContinuousResponseType;
        //            }
        //        }

        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        parameterDict = null;
        //        Console.WriteLine("exp: " + ex.Message);
        //        UbiqsensBooter.ObjLogger.WriteLog(UbiqSens.PluginInterfaces.LogLevel.Error, "Error in exp: Ex" + ex.Message);
        //        return null;
        //    }
        //}

        /// <summary>
        /// Get Request Response Type
        /// </summary>
        /// <param name="deviceType">device Type</param>
        /// <param name="parameterDict">parameter Dictionary</param>
        /// <returns>Request Response Type</returns>
        ////public static RequestResponseType GetRequestResponseType(string deviceType, out Dictionary<string, string> parameterDict)
        ////{
        ////    RequestResponseType objRequestResponseType = null;
        ////    parameterDict = new Dictionary<string, string>();
        ////    try
        ////    {
        ////        var path = AppDomain.CurrentDomain.BaseDirectory + "\\PolicyFiles\\DeviceTypeLookUp.xml";
        ////        var deviceTypeLookUp = new ConfigurationReader().GetXml(path);
        ////        var subDoc = new XmlDocument();
        ////        var nodeList = deviceTypeLookUp.GetElementsByTagName("DeviceType");

        ////        foreach (XmlNode objDeviceType in nodeList)
        ////        {
        ////            subDoc.LoadXml(objDeviceType.OuterXml);
        ////            if (subDoc.GetElementsByTagName("name").Count > 0 && subDoc.GetElementsByTagName("name")[0].InnerText.ToLower(CultureInfo.InvariantCulture) == deviceType.ToLower(CultureInfo.InvariantCulture))
        ////            {
        ////                objRequestResponseType = new RequestResponseType();
        ////                objRequestResponseType.ObjDeviceCon = new DeviceConnectionObjectFactory().GetIDeviceConnectionObject(subDoc.GetElementsByTagName("Communicationplugin")[0].InnerText);
        ////                objRequestResponseType.ObjDeviceProtocol = new DeviceProtocolObjectFactory().GetIDeviceProtocolObject(subDoc.GetElementsByTagName("ProtocolPlugin")[0].InnerText);
        ////                Dictionary<string, string> deviceInfo = new Dictionary<string, string>();

        ////                foreach (XmlNode para in subDoc.GetElementsByTagName("DeviceParameters"))
        ////                {
        ////                    parameterDict.Add(para.InnerText, string.Empty);
        ////                }

        ////                foreach (KeyValuePair<string, string> kvp in objRequestResponseType.ObjDeviceCon.DeviceInfo)
        ////                {
        ////                    if (parameterDict.Keys.Contains(kvp.Key))
        ////                    {
        ////                        continue;
        ////                    }

        ////                    string value = subDoc.GetElementsByTagName(kvp.Key)[0].InnerText;
        ////                    parameterDict.Add(kvp.Key, value);
        ////                }

        ////                break;
        ////            }
        ////        }

        ////        deviceTypeLookUp = null;
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        Console.WriteLine("exp: " + ex.Message);
        ////        UbiqsensBooter.ObjLogger.WriteLog(UbiqSens.PluginInterfaces.LogLevel.Error, "Error in exp: Ex" + ex.Message);
        ////    }

        ////    return objRequestResponseType;
        ////}

        /// <summary>
        /// Get Accept Connection Ethernet Parameters
        /// </summary>
        /// <param name="deviceType">device type</param>
        /// <param name="validationDictionary">validate dictionary</param>
        /// <returns>Accept Connection Ethernet Parameters</returns>
        public AcceptConnectionEthernetParameters GetAcceptConnectionEthernetParameters(string deviceType, out Dictionary<string, string> validationDictionary)
        {
            try
            {
                var devicepolicydoc = new XmlDocument();
                devicepolicydoc.LoadXml(deviceType);
                AcceptConnectionEthernetParameters objAcceptConnectionEthernetParameters = null;
                objAcceptConnectionEthernetParameters = new AcceptConnectionEthernetParameters();
                GatewayConnectionFactory objGateConnectionFactory = new GatewayConnectionFactory();

                var path = AppDomain.CurrentDomain.BaseDirectory + Constants.DeviceTypeLookUpFile;
                var subDoc = new XmlDocument();
                if (deviceTypeLookUp == null)
                {
                    ConfigurationReader objConfigurationReader = new ConfigurationReader();
                    deviceTypeLookUp = objConfigurationReader.GetXml(path);
                }

                var nodeList = deviceTypeLookUp.GetElementsByTagName(Constants.DeviceType);

                validationDictionary = new Dictionary<string, string>();

                foreach (XmlNode objDeviceType in nodeList)
                {
                    subDoc.LoadXml(objDeviceType.OuterXml);
                    ////////Console.WriteLine(objDeviceType.OuterXml.ToString());
                    XmlNodeList deviceTypeList = deviceTypeLookUp.SelectNodes("//DeviceTypePolicy/DeviceTypes/DeviceType");
                    if (subDoc.GetElementsByTagName(Constants.Name).Count > 0 && subDoc.GetElementsByTagName(Constants.Name)[0].InnerText.ToLower(CultureInfo.InvariantCulture) == devicepolicydoc.GetElementsByTagName(Constants.DeviceType)[0].InnerText.ToLower(CultureInfo.InvariantCulture))
                    {

                        foreach (XmlNode xn in deviceTypeList[0].ChildNodes)
                        {

                            if (xn.Name == Constants.ReceiveTimeout)
                                objAcceptConnectionEthernetParameters.ReceiveTimeout = xn.InnerText;
                            if (xn.Name == Constants.LPort)
                                objAcceptConnectionEthernetParameters.ListenerPort = xn.InnerText;
                            if (xn.Name == Constants.ReceiveBufferSize)
                                objAcceptConnectionEthernetParameters.ReceiveBufferSize = xn.InnerText;
                            if (xn.Name == Constants.SendTimeout)
                                objAcceptConnectionEthernetParameters.SendTimeout = xn.InnerText;
                        }

                        XmlNode communicationPluginNode = deviceTypeLookUp.SelectSingleNode(Constants.DevicePolicyLookUpParser);

                        XmlNodeList communicationPluginNodelist = communicationPluginNode.SelectNodes(Constants.Mode);

                        foreach (XmlNode xn in communicationPluginNodelist)
                        {

                            if (xn.Attributes["id"].Value == devicepolicydoc.GetElementsByTagName(Constants.ModeID)[0].InnerText)
                            {
                                objAcceptConnectionEthernetParameters.ObjGatewayConnection = new GatewayConnectionFactory().GetGatewayConnectionObject(xn.Attributes["name"].Value);
                                objAcceptConnectionEthernetParameters.ObjGatewayProtocol = objGateConnectionFactory.GetGatewayProtocolObject(subDoc.GetElementsByTagName(Constants.ProtocolPlugin)[0].InnerText);
                                var nodeXmlList = subDoc.GetElementsByTagName(Constants.ValidationParameter);
                                foreach (XmlNode node in nodeXmlList)
                                {
                                    validationDictionary.Add(node.InnerText, string.Empty);
                                }
                            }
                        }
                        /************selecting communication based on mode id*******************************************/
                    }
                }

                return objAcceptConnectionEthernetParameters;
            }
            catch (Exception ex)
            {
                //////Console.WriteLine("exp: " + ex.Message);
                UbiqsensBooter.ObjLogger.WriteLog(UbiqSens.PluginInterfaces.LogLevel.Error, "Exception Occurred at GetAcceptConnectionEthernetParameters() Method With @deviceType: " + deviceType + " @Ex: " + ex.Message, ex);
                validationDictionary = null;
                return null;
            }
        }
    }
}
