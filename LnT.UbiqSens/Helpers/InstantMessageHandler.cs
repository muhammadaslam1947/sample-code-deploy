﻿////-----------------------------------------------------------------------
//// <copyright file="InstantMessageHandler.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

namespace LnT.UbiqSens.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using LnT.UbiqSens.PluginInterfaces;
    using uPLibrary.Networking.M2Mqtt.Messages;
    using System.Diagnostics;
    using System.ServiceProcess;
    using System.Configuration.Install;
    using System.Threading;


    /// <summary>
    /// Instant Message Handler
    /// </summary>
    public class InstantMessageHandler
    {
        /// <summary>
        /// Handle Incoming Message
        /// </summary>
        /// <param name="e">message publish event</param>
        public void HandleIncomingMsg(MqttMsgPublishEventArgs e)
        {
            try
            {
                if (e.Topic.ToLower(CultureInfo.InvariantCulture).Contains(UbiqsensBooter.DevicePolicyTopic.ToLower(CultureInfo.InvariantCulture)))
                {
                    this.DevicePolicyHandler(e);
                }
                else if (e.Topic.ToLower(CultureInfo.InvariantCulture).Contains(UbiqsensBooter.WorkItemPolicyTopic.ToLower(CultureInfo.InvariantCulture)))
                {
                    this.WorkItemMessageHandler(e);
                }
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "MessageHandler: Error in adding Workitem into queue. HandleIncomingMsg() Method With @Topic: " + e.Topic + " @Message: " + ASCIIEncoding.ASCII.GetString(e.Message), ex);
            }
        }

        /// <summary>
        /// Device Policy Handler
        /// </summary>
        /// <param name="e">message publish event</param>
        public void DevicePolicyHandler(MqttMsgPublishEventArgs e)
        {
            try
            {
                string deviceId = string.Empty;//  e.Topic.Split(Constants.Slash)[2];
                string operation = string.Empty; // e.Topic.Split(Constants.Slash)[3];

                if (!e.Topic.Contains("devices"))
                {
                    deviceId = e.Topic.Split(Constants.Slash)[2];
                    operation = e.Topic.Split(Constants.Slash)[3];
                }
                else
                    operation = "devices";


                var objReadDevicePolicy = new ReadDevicePolicy();
                String str = string.Empty;
                if (UbiqsensBooter.IsSecure)
                {
                    //Read Encrypted Deviec type XML
                    str = AES128.GetInstance().Decrypt(ASCIIEncoding.ASCII.GetString(e.Message), UbiqsensBooter.SecurityKey);
                }
                else
                    str = ASCIIEncoding.ASCII.GetString(e.Message);


                switch (operation.ToLower(CultureInfo.InvariantCulture))
                {
                    case Constants.Create:
                        File.WriteAllBytes(AppDomain.CurrentDomain.BaseDirectory + Constants.DevicePolicyPath + deviceId + Constants.Xml, ASCIIEncoding.ASCII.GetBytes(ASCIIEncoding.ASCII.GetString(e.Message).Replace(Constants.SlashR, string.Empty).Replace(Constants.SlashN, string.Empty)));
                        objReadDevicePolicy.InitiatePassiveDevice(str.Replace(Constants.SlashR, string.Empty).Replace(Constants.SlashN, string.Empty), false);
                        break;

                    case Constants.UpdateUsmall:
                        Dictionary<string, string> temp;
                        ReadDevicePolicy.PassiveDeviceIdDictionary.TryRemove(deviceId, out temp);
                        File.WriteAllBytes(AppDomain.CurrentDomain.BaseDirectory + Constants.DevicePolicyPath + deviceId + Constants.Xml, ASCIIEncoding.ASCII.GetBytes(ASCIIEncoding.ASCII.GetString(e.Message).Replace(Constants.SlashR, string.Empty).Replace(Constants.SlashN, string.Empty)));
                        objReadDevicePolicy.InitiatePassiveDevice(str.Replace(Constants.SlashR, string.Empty).Replace(Constants.SlashN, string.Empty), false);
                        break;

                    case Constants.Delete:
                        temp = new Dictionary<string, string>();
                        ReadDevicePolicy.PassiveDeviceIdDictionary.TryRemove(deviceId, out temp);
                        if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + Constants.DevicePolicyPath + deviceId + Constants.Xml))
                        {
                            File.Delete(AppDomain.CurrentDomain.BaseDirectory + Constants.DevicePolicyPath + deviceId + Constants.Xml);
                        }

                        break;
                    case Constants.GetAllDevices:
                        UbiqsensBooter.DevicePolicy = str;
                        UbiqsensBooter.areDevicePolicy.Set();
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MessageHandler: Got Device Policy from UR");
                        break;
                }
            }
            catch (Exception exception)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "MessageHandler: Error in reading Device Policy MQTT Message. DevicePolicyHandler() Method With @Topic: " + e.Topic + " @Message: " + e.Message, exception);
            }
        }

        /// <summary>
        /// Work Item MessageHandler
        /// </summary>
        /// <param name="e">message publish event</param>
        private void WorkItemMessageHandler(MqttMsgPublishEventArgs e)
        {
            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MessageHandler: *Received in the Workitem. " + e.Topic);

            var topic = e.Topic;
            var num = topic.Split('/').Count();
            if (num == 4)
            {
                topic = topic.Replace(Constants.Workitem + Constants.Slash, string.Empty);
                var workItemId = topic.Substring(0, topic.LastIndexOf(Constants.Slash));

                workItemId = workItemId.Substring(workItemId.LastIndexOf(Constants.Slash) + 1);
                var deviceId = topic.Substring(topic.LastIndexOf(Constants.Slash) + 1);

                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MessageHandler: WorkItemHandler Details, @DeviceId: " + deviceId + " @workItemId: " + workItemId + " @Message: " + ASCIIEncoding.ASCII.GetString(e.Message));

                var obj = new WorkItemParser().ParseWorkItem(workItemId + Constants.DollarSymbol +
                                                                     ASCIIEncoding.ASCII.GetString(e.Message));

                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MessageHandler: WorkItemHandler Details, @WorkItemtype: " + obj.WorkItemtype + " @DeviceId: " + deviceId + " @workItemId: " + obj.WorkItemId + " @Message: " + ASCIIEncoding.ASCII.GetString(e.Message));


                // If Device is disconnected - Sending Response as Device Not Availabale :
                if (!QueueManager.ConnectedActiveDevice.ContainsKey(deviceId))
                {
                    ////UbiqsensBooter.ObjPubMqtt.Publish(Constants.WITopic + workItemId, ASCIIEncoding.ASCII.GetBytes(LnT.UbiqSens.Helpers.Constants.DeviceNotAvailable), MqttQos.QOS_LEVEL_AT_LEAST_ONCE, false);
                    ////UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "*Processed Workitem. " + Constants.WITopic + workItemId);
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MessageHandler: WI Reached  & Ignored Device Not Connected: " + deviceId + ":" + ASCIIEncoding.ASCII.GetString(e.Message));

                    //QueueManager.Itq.Enqueue(new LnT.UbiqSens.PluginInterfaces.Message() { Topic = deviceId + Constants.Slash.ToString() + workItemId, Data = LnT.UbiqSens.Helpers.Constants.DeviceNotAvailable });
                    return;
                }

                if (obj.WorkItemtype == WorkItemType.Connect || obj.WorkItemtype == WorkItemType.Disconnect)
                {
                    if (!QueueManager.DictStatusWorkItemQueue.ContainsKey(deviceId))
                    {
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MessageHandler: DeviceId is not present in Status Queue, Add it to the queue, @DeviceId: " + deviceId + " @workItemId: " + workItemId + " @Message: " + ASCIIEncoding.ASCII.GetString(e.Message));
                        QueueManager.DictStatusWorkItemQueue.TryAdd(deviceId, new Queue<string>());
                    }

                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MessageHandler: NOTE: " + QueueManager.DictStatusWorkItemQueue[deviceId].Count);
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MessageHandler: DeviceId is present in Status Queue, Enqueue it, @DeviceId: " + deviceId + " @workItemId: " + workItemId + " @Message: " + ASCIIEncoding.ASCII.GetString(e.Message));
                    QueueManager.DictStatusWorkItemQueue[deviceId].Enqueue(workItemId + Constants.DollarSymbol +
                                                                     ASCIIEncoding.ASCII.GetString(e.Message));
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MessageHandler: *Disconnect Workitem. @DeviceId: " + deviceId + " @Topic: " + e.Topic);
                    if (QueueManager.GateMap.ContainsKey(deviceId))
                    {
                        QueueManager.GateMap[deviceId].Set();
                    }
                }
                else
                {

                    if (!QueueManager.DictWorkItemQueue.ContainsKey(deviceId))
                    {
                        QueueManager.DictWorkItemQueue.TryAdd(deviceId, new Queue<string>());
                    }
                    if (!QueueManager.LstWorkItemEntryTime.ContainsKey(deviceId))
                    {
                        QueueManager.LstWorkItemEntryTime.TryAdd(deviceId, new List<DateTime>());
                    }

                    QueueManager.DictWorkItemQueue[deviceId].Enqueue(workItemId + Constants.DollarSymbol +
                                                                     ASCIIEncoding.ASCII.GetString(e.Message));

                    QueueManager.LstWorkItemEntryTime[deviceId].Add(DateTime.Now);
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MessageHandler: *Read Request Workitem. " + e.Topic);
                    if (QueueManager.GateMap.ContainsKey(deviceId))
                    {
                        QueueManager.GateMap[deviceId].Set();
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MessageHandler: WI Data Received Event Triggered" + deviceId);
                    }
                    else
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, QueueManager.GateMap.Count + "MessageHandler: Device not available in GateMap @deviceId: " + deviceId);
                }
            }
            else
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "MessageHandler: Error in the Workitem. @topic: " + e.Topic);
            }
        }
    }
}
