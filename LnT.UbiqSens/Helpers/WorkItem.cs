﻿//-----------------------------------------------------------------------
// <copyright file="WorkItem.cs" company="L&T">
//     Copyright (c) L&T. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LnT.UbiqSens.Helpers
{
    using System.Collections.Generic;

    /// <summary>
    /// Work item
    /// </summary>
    public class WorkItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkItem"/> class.
        /// </summary>
        public WorkItem()
        {
            this.Commands = new List<Command>();
        }

        /// <summary>
        /// Gets or sets Work item ID
        /// </summary>
        public string WorkItemId { get; set; }

        /// <summary>
        /// Gets or sets Work item type
        /// </summary>
        public WorkItemType WorkItemtype { get; set; }

        /// <summary>
        /// Gets or sets list of commands
        /// </summary>
        public List<Command> Commands { get; set; }

        /// <summary>
        /// Gets or sets Cid of the Workitem
        /// </summary>
        public string Cid { get; set; }

    }

}

