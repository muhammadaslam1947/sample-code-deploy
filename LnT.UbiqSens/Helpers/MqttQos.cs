﻿//-----------------------------------------------------------------------
// <copyright file="MqttQos.cs" company="L&T">
//     Copyright (c) L&T. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LnT.UbiqSens.Helpers
{
    /// <summary>
    ///  MQTT Quality of service
    /// </summary>
    public enum MqttQos : byte
    {
        /// <summary>
        /// Quality of service 1
        /// </summary>
        QOS_LEVEL_AT_LEAST_ONCE = 1,

        /// <summary>
        /// Quality of service 0
        /// </summary>
        QOS_LEVEL_AT_MOST_ONCE = 0,

        /// <summary>
        /// Quality of service 2
        /// </summary>
        QOS_LEVEL_EXACTLY_ONCE = 2
    }
}
