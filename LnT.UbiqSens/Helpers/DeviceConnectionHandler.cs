﻿﻿﻿////-----------------------------------------------------------------------
//// <copyright file="ChangeAuditHandler.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

////using LnT.UbiqSens.PluginInterfaces;
////using System;
////using System.Collections.Generic;
////using System.Linq;
////using System.Text;
////using System.Threading;
////using System.Threading.Tasks;

////namespace LnT.UbiqSens.Helpers
////{
////    class DeviceConnectionHandler
////    {
////        /// <summary>
////        /// Disconnects the connection with the device
////        /// </summary>
////        /// <param name="activeDeviceConPlugin"></param>
////        /// <returns>disconnected is retured</returns>
////        internal bool DisconnectActiveDeviceConnection(IDeviceConnection activeDeviceConPlugin)
////        {
////            bool disconnected = false;
////            try
////            {
////                disconnected = activeDeviceConPlugin.Disconnect();
////            }
////            catch (Exception ex)
////            {
////                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in disconnecting the device. Exp:" + ex.Message);
////            }
////            return disconnected;
////        }

////        /// <summary>
////        /// Releases the protocol plugin
////        /// </summary>
////        /// <param name="protocolPlugin"></param>
////        /// <returns>Returns isReleased</returns>
////        internal bool ReleaseProtocolPlugin(IDeviceProtocol protocolPlugin)
////        {
////            bool isReleased = false;
////            try
////            {
////                isReleased = protocolPlugin.Release();
////            }
////            catch (Exception ex)
////            {
////                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in releasing the Protocol plugin. Exp:" + ex.Message);
////            }
////            return isReleased;
////        }

////        /// <summary>
////        /// Gets the Command From Protocol
////        /// </summary>
////        /// <param name="deviceConnectionPlugin"></param>
////        /// <param name="protocolPlugin"></param>
////        /// <param name="cmdId"></param>
////        /// <param name="value"></param>
////        /// <returns>Returns cmd</returns>
////        internal byte[] GetCommandFromProtocol(IDeviceConnection deviceConnectionPlugin, IDeviceProtocol protocolPlugin, string cmdId, string value)
////        {
////            byte[] cmd = null;
////            try
////            {
////                cmd = protocolPlugin.GetCommand(cmdId, value);
////            }
////            catch (Exception ex)
////            {
////                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in getting command from Protocol. Exp:" + ex.Message);
////            }
////            return cmd;
////        }

////        /// <summary>
////        /// Sends the command data to Device
////        /// </summary>
////        /// <param name="gatewayConnectionPlugin"></param>
////        /// <param name="protocolPlugin"></param>
////        /// <param name="cmd"></param>
////        /// <returns>Returns sent</returns>
////        internal bool SendCommand(IDeviceConnection deviceConnectionPlugin, IDeviceProtocol protocolPlugin, byte[] cmd)
////        {
////            bool sent = false;
////            try
////            {
////                sent = deviceConnectionPlugin.Send(cmd);
////            }
////            catch (Exception ex)
////            {
////                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in sending data. Exp:" + ex.Message);
////            }
////            return sent;
////        }

////        /// <summary>
////        /// Receives the data 
////        /// </summary>
////        /// <param name="gatewayConnectionPlugin"></param>
////        /// <returns></returns>
////        internal List<byte[]> ReceiveData(IDeviceConnection gatewayConnectionPlugin)
////        {
////            List<byte[]> recData = null;
////            try
////            {
////                recData = gatewayConnectionPlugin.Receive();
////            }
////            catch (Exception ex)
////            {
////                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in receiving data. Exp:" + ex.Message);
////            }
////            return recData;
////        }

////        /// <summary>
////        /// Parses the data received from the device using Protocol plugin
////        /// </summary>
////        /// <param name="protocolPlugin"></param>
////        /// <param name="data"></param>
////        /// <param name="cmdId"></param>
////        /// <returns>lstParsedData is retured</returns>
////        internal List<ParsedData> ParseRecData(IDeviceProtocol protocolPlugin, List<byte[]> data, string cmdId)
////        {
////            List<ParsedData> lstParsedData = null;
////            try
////            {
////                lstParsedData = protocolPlugin.Parse(data, cmdId);
////            }
////            catch (Exception ex)
////            {
////                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in Parsing data. Exp:" + ex.Message);
////            }
////            return lstParsedData;
////        }

////        /// <summary>
////        /// Connects to the device
////        /// </summary>
////        /// <param name="objDeviceCon"></param>
////        /// <param name="workItemId"></param>
////        /// <returns>isConnected</returns>
////        internal bool ConnectActiveDevice(IDeviceConnection objDeviceCon)
////        {
////            int retry = 5;
////            bool isConnected = false;
////            while (retry > 0 && !isConnected)
////            {
////                try
////                {
////                    isConnected = objDeviceCon.Connect();
////                    Console.WriteLine("Device connected");
////                }
////                catch (Exception ex)
////                {
////                    Thread.Sleep(1000);
////                    Console.WriteLine("Retrying to connect, attempt no " + retry);
////                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Retrying to connect " + retry + " Exp:" + ex.Message);
////                    retry--;
////                }
////            }
////            return isConnected;
////        }
////    }
////}
