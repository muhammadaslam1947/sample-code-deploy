﻿////-----------------------------------------------------------------------
//// <copyright file="ConfigurationReader.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------
namespace LnT.UbiqSens.Helpers
{
    using System;
    using System.Xml;
    using LnT.UbiqSens.PluginInterfaces;

    /// <summary>
    ///  Defining class for configuration reader
    /// </summary>
    public class ConfigurationReader
    {
        /// <summary>
        /// XmlDocument gateway policy
        /// </summary>
        private XmlDocument gatewayPolicy;

        /// <summary>
        /// Gets Xml data
        /// </summary>
        /// <param name="fileName">file name</param>
        /// <returns>xml document</returns>
        public XmlDocument GetXml(string fileName)
        {
            try
            {
                this.gatewayPolicy = new XmlDocument();
                var settings = new XmlReaderSettings();
                settings.IgnoreComments = true;
                var reader = XmlReader.Create(fileName, settings);
                this.gatewayPolicy.Load(reader);
                return this.gatewayPolicy;
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in reading XML.GetXml() Method With @FileName: " + fileName + " @Ex: " + ex.Message, ex);
                return null;
            }
        }

        /// <summary>
        /// Gets Xml data
        /// </summary>
        /// <param name="fileName">file name</param>
        /// <returns>xml document</returns>
        public XmlDocument GetXmlFromContent(string xmlContent)
        {
            try
            {
                this.gatewayPolicy = new XmlDocument();
                this.gatewayPolicy.LoadXml(xmlContent);
                return this.gatewayPolicy;
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in reading XML. GetXmlFromContent() Method with @xmlContent: " + xmlContent + " @Ex: " + ex.Message, ex);
                return null;
            }
        }
    }
}
