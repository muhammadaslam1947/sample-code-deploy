﻿﻿﻿////-----------------------------------------------------------------------
//// <copyright file="ChangeAuditHandler.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------


////////using LnT.UbiqSens.PluginInterfaces;
////////using System;
////////using System.Globalization;
////////using System.IO;
////////using System.Linq;
////////using System.Reflection;

////////namespace LnT.UbiqSens.Helpers
////////{
////////    class IEncryptDecryptDataObjectFactory
////////    {
////////        ///// <summary>
////////        ///// Searches the plugin in the \IEncryptDecryptDataPlugins, Creates the IEncryptDecryptData object and returns
////////        ///// </summary>
////////        ///// <param name="DeviceConnectionPlugin"></param>
////////        ///// <returns></returns>
////////        internal static IEncryptDecryptData GetIEncryptDecryptDataObject(string pluginName)
////////        {
////////            var IDeviceConnectionPlugins = Directory.GetFiles(Constants.strCurrentPath + Constants.IEncryptDecryptDataPlugins, Constants.strDll);
////////            ////var IDeviceConnectionPlugins = Directory.GetFiles(Constants.strCurrentPath , Constants.strDll);
////////            foreach (var communicationProtocolPath in IDeviceConnectionPlugins)
////////            {
////////                Assembly assembly = Assembly.LoadFrom(communicationProtocolPath);
              
////////                var lstTypes = assembly.GetTypes();////.Where(typeof(IEncryptDecryptData).IsAssignableFrom).ToList();
////////                foreach (var type in lstTypes)
////////                {
////////                    if (type.Name.ToLower(CultureInfo.InvariantCulture) == pluginName.ToLower(CultureInfo.InvariantCulture))
////////                    {
////////                        Type t = assembly.GetType(pluginName, false, false);
////////                        return Activator.CreateInstance(type) as IEncryptDecryptData;
////////                    }
////////                }
////////            }
////////            throw new Exception("Plugin not found");
////////        }
////////    }
////////}
