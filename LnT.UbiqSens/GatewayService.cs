﻿using System.ServiceProcess;
using LnT.UbiqSens.Helpers;
using System.Configuration;
using System.Diagnostics;
using System;
using System.Text;
using LnT.UbiqSens.PluginInterfaces;

namespace LnT.UbiqSens
{
    partial class GatewayService : ServiceBase
    {
        public GatewayService()
        {
            InitializeComponent();
        }
        Ubiqsens ubiqsens = null;

        internal void DebugService(string[] args)
        {
            this.OnStart(args);
            Console.ReadLine();
            //this.OnStop();
        }

        protected override void OnStart(string[] args)
        {
            //EncryptConfig ec = new EncryptConfig();
            ubiqsens = new Ubiqsens();
            //try
            //{
            //    bool isSecure = Boolean.Parse(ConfigurationManager.AppSettings["IsSecure"]);
            //    if (isSecure)
            //    {
            //        if (args.Length != 0)
            //        {
            //            UbiqsensBooter.SecurityKey = args[0];
            //            //STORE AS DATA  
            //            ec.UpdateConfigFile(UbiqsensBooter.SecurityKey);
            //            ec.Encrypt();
            //        }
            //        else
            //        {
            //            ec.Decrypt();
            //            //READ FROM CONFIG FILE
            //            UbiqsensBooter.SecurityKey = ConfigurationManager.ConnectionStrings["SecurityKey"].ConnectionString;
            //            ec.Encrypt();
            //        }
            //    }
            //}
            //catch (Exception)
            //{
            //   throw;
            //}
            ubiqsens.StartService();
        }

        protected override void OnStop()
        {
            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "GateWayService: Service Stop Initiated");
            try
            {
                UbiqsensBooter.ObjPubMqtt.Publish("Server/GATEWAY_STATUS/" + UbiqsensBooter.GatewayUid, ASCIIEncoding.ASCII.GetBytes("0"), MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                UbiqsensBooter.ObjPubMqtt.Disconnect();
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "GateWayService: MQTT Disconnected");
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "GateWayService: GatewayService OnStop() @Ex: ", ex);
                //throw;
            }
            try
            {    
                ubiqsens.StopService(); // TODO: Add code here to perform any tear-down necessary to stop your service.
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "GateWayService: Service class stopped");
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "GateWayService: GatewayService OnStop() @Ex: ", ex);
                //throw;
            }
            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "GateWayService: Service Stop Completed");
        }
    }
}
