﻿////---------------------------------------------------------------------
//// <copyright file="DeviceToGateway.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

using System.Collections.Concurrent;
using System.Runtime.InteropServices;
using System.Globalization;

namespace LnT.UbiqSens
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading;
    using System.Xml;
    using LnT.UbiqSens.Helpers;
    using LnT.UbiqSens.PluginInterfaces;
    using LnT.UbiqSens.Plugins.EthernetGatewayHandler;
    using System.Threading.Tasks;
    /// <summary>
    /// Device to gateway
    /// </summary>
    public class DeviceToGateway
    {
        /// <summary>
        /// total Socket Connection
        /// </summary>
        private static int _totalSocketConnection = 0;

        private static string strIPAddress = Dns.GetHostEntry(Dns.GetHostName()).AddressList[0].ToString();
        /// <summary>
        /// String builder to concat request & response
        /// </summary>
        private StringBuilder sb = new StringBuilder();

        public Type gatewayPluginType;
        public Type protocolPluginType;
        private Dictionary<string, MqttQos> subscriptionDictionary = new Dictionary<string, MqttQos>();
        private ConcurrentDictionary<string, BatchInfo> ReusableCollection = new ConcurrentDictionary<string, BatchInfo>();
        private ConcurrentDictionary<string, BatchInfo> ExitCollection = new ConcurrentDictionary<string, BatchInfo>();
        private ConcurrentQueue<BatchInfo> BatchQueue = new ConcurrentQueue<BatchInfo>();

        /// <summary>
        /// Ethernet Gateway Connection Handler
        /// </summary>
        /// <param name="gatewayConnectionPlugin">gateway Connection Plugin</param>
        /// <param name="protocolPlugin">protocol Plugin</param>
        /// <param name="taskId">task id</param>
        public void GatewayConnectionHandler(BatchInfo objBatchInfo, Thread newDevice, string deviceAddress)
        {
            try
            {
                var strDeviceId = string.Empty;
                var strMacId = string.Empty;
                var strType = string.Empty;
                var strState = string.Empty;
                var strRevisionModel = string.Empty;
                var Messages = new List<string>();
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "@GateMapCount: " + QueueManager.GateMap.Count + " @Cid: " + objBatchInfo.Cid);

                if (this.InitialiseProtocolPlugin(objBatchInfo.GatewayCon, objBatchInfo.GatewayPro))
                {
                    // Validation of device using MAC ID
                    if (ValidateDevice(objBatchInfo, ref deviceAddress, ref strDeviceId, ref strMacId, ref strType, ref Messages, ref strState, ref strRevisionModel))
                    {
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "@DeviceId: " + strDeviceId + " @Cid: " + objBatchInfo.Cid + "- Received Device ID from device");

                        if (!string.IsNullOrEmpty(strDeviceId))
                        {
                            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Received Macid Response from device. @Cid: " + objBatchInfo.Cid);

                            HandleDuplicateSocket(objBatchInfo, newDevice, deviceAddress, strDeviceId, strMacId);

                            QueueManager.RemoveWorkItems(strDeviceId);

                            if (!QueueManager.ConnectedActiveDevice.ContainsKey(strDeviceId))
                                if (!QueueManager.ConnectedActiveDevice.TryAdd(strDeviceId, Constants.Active))
                                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "@DeviceId: " + strDeviceId + " @deviceAddress: " + deviceAddress + " @strMacId: " + strMacId + " Failed to Add Connected Devices List");
                            //TODO: strDeviceId should be replaced with strMadId.  strDeviceId gets reset everytime a thermostat is deleted and added. (Rare scenario)
                            if (QueueManager.GateMap.ContainsKey(strDeviceId))
                                QueueManager.GateMap[strDeviceId] = objBatchInfo.RequestHandlerGate;
                            else
                                QueueManager.GateMap.TryAdd(strDeviceId, objBatchInfo.RequestHandlerGate);

                            UbiqsensBooter.ObjPubMqtt.Publish("Server/NOTIFICATIONS/" + strDeviceId, ASCIIEncoding.ASCII.GetBytes(String.Format(Constants.Notification, "Ping:" + strMacId + "," + strType + "," + strState + "," + strRevisionModel + "," + strIPAddress, objBatchInfo.Cid)), MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Sent Macid to web server for validation. @Cid: " + objBatchInfo.Cid);

                            foreach (var message in Messages)
                                UbiqsensBooter.ObjPubMqtt.Publish("Server/NOTIFICATIONS/" + strDeviceId, ASCIIEncoding.ASCII.GetBytes(String.Format(Constants.Notification, message, objBatchInfo.Cid)), MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                            HandleRequests(objBatchInfo, deviceAddress, strDeviceId, strMacId);
                            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "@DeviceId: " + strDeviceId + " @DeviceAddress: " + deviceAddress + " @MacId: " + strMacId + " @Cid: " + objBatchInfo.Cid + "- Thread gracefully closed after disconnection.");
                        }
                    }
                    else
                    {
                        //Disconnecting connected Device
                        HandleDisconnect(objBatchInfo, deviceAddress, strDeviceId, strMacId, false, Constants.Disconnect_Validation);
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "@DeviceId: " + strDeviceId + " @DeviceAddress: " + deviceAddress + " @MacId: " + strMacId + " @Cid: " + objBatchInfo.Cid + " -  Disconnected, Failed to get MacId from device.");
                    }
                }
                else
                {
                    //Disconnecting connected Device
                    HandleDisconnect(objBatchInfo, deviceAddress, strDeviceId, strMacId, false, Constants.Disconnect_Protocol);
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "@DeviceId: " + strDeviceId + " @DeviceAddress: " + deviceAddress + " @MacId: " + strMacId + " @Cid: " + objBatchInfo.Cid + " Disconnected in Validation - Unable to initialize protocol plugin");
                }
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in Gateway Connection Handler, @DeviceAddress: " + deviceAddress + " @Cid: " + objBatchInfo.Cid + " @Ex: " + ex.Message, ex);
            }
        }

        /// <summary>
        /// Releases the protocol plugin
        /// </summary>
        /// <param name="protocolPlugin">protocol Plugin</param>
        /// <returns>Returns isReleased</returns>
        public bool ReleaseProtocolPlugin(IGatewayProtocol protocolPlugin)
        {
            var isReleased = false;
            try
            {
                isReleased = protocolPlugin.Release();
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in releasing the Protocol plugin. @Ex: " + ex.Message, ex);
            }

            return isReleased;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gatewayConnectionPlugin"></param>
        /// <param name="protocolPlugin"></param>
        /// <param name="deviceAddress"></param>
        /// <param name="strDeviceId"></param>
        /// <param name="strMacId"></param>
        /// <param name="isDuplicateSocket"></param>
        private void HandleDisconnect(BatchInfo objBatchInfo, string deviceAddress, string strDeviceId, string strMacId, bool isDuplicateSocket, string reason)
        {
            try
            {
                String outString;
                AutoResetEvent objResetEvent = null;
                BatchInfo objTemp = null;
                //Disconnect Device
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "@DeviceId: " + strDeviceId + " @MacId: " + strMacId + " @Cid: " + objBatchInfo.Cid + " Disconnected in isCloseNeeded, Reason: " + reason);
                if (QueueManager.ConnectedActiveDevice.ContainsKey(strDeviceId))
                    QueueManager.ConnectedActiveDevice.TryRemove(strDeviceId, out outString);
                this.DisconnectGatewayConnection(objBatchInfo);
                if (isDuplicateSocket)
                {
                    try
                    {
                        if (ExitCollection.ContainsKey(strMacId))
                        {
                            ExitCollection[strMacId].IsThreadActive = false;
                            ExitCollection[strMacId].RequestHandlerGate.Set();
                        }
                    }
                    catch (Exception)
                    {
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "@DeviceId: " + strDeviceId + " @MacId: " + strMacId + " @Cid: " + objBatchInfo.Cid + " Failed to trigger event for duplicate socket in ReuseSocket.");
                    }
                }
                ReusableCollection.TryRemove(strMacId, out objTemp);
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, strMacId + " HanldeDisconnect - Removed MacId from Reusable collection.");
                ExitCollection.TryRemove(strMacId, out objTemp);
                QueueManager.GateMap.TryRemove(strDeviceId, out objResetEvent);

                // Release protocol plugin
                this.ReleaseProtocolPlugin(objBatchInfo.GatewayPro);
                if (_totalSocketConnection > 0)
                    _totalSocketConnection--;

                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "@DeviceId: " + strDeviceId + " @DeviceAddress: " + deviceAddress + " @MacId: " + strMacId + " @Cid: " + objBatchInfo.Cid + " Disconnected" + "Socket Count -" + _totalSocketConnection);

            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error Occurred at HanldeDisconnect() Method With @DeviceId: " + strDeviceId + " @DeviceAddress: " + deviceAddress + " @MacId: " + strMacId + " @Cid: " + objBatchInfo.Cid + " @Ex: " + ex.Message, ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gatewayConnectionPlugin"></param>
        /// <param name="protocolPlugin"></param>
        /// <param name="deviceAddress"></param>
        /// <param name="strDeviceId"></param>
        /// <param name="strMacId"></param>
        /// <param name="proceed"></param>
        /// <returns></returns>
        private bool HandleRequests(BatchInfo objBatchInfo, string deviceAddress, string strDeviceId, string strMacId)
        {
            bool isCompleted = true;
            string workItem;
            #region WorkItem & Notification handler
            TimeSpan tp = new TimeSpan(0, 0, Ubiqsens.IdleTimeout);
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            var workitemParserObj = new WorkItemParser();
            WorkItem processedItemType = null;

            sw.Start();
            while (objBatchInfo.IsThreadActive)
            {
                try
                {
                    //Handling Workitems
                    workItem = QueueManager.GetStatusWorkItem(strDeviceId);

                    if (!string.IsNullOrEmpty(workItem))
                    {
                        processedItemType = this.ManageWorkItem(workItem, objBatchInfo, strDeviceId, strMacId, deviceAddress);
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "HandleRequestsMethod: Status WI Got & Processed, @DeviceId: " + strDeviceId + " @DeviceAddress: " + deviceAddress + " @MacAddress: " + strMacId + " @WorkItem: " + workItem + " @Cid: " + processedItemType.Cid);

                        if (sw.IsRunning)
                        {
                            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, " Macid validation response received from web server(Status Workitem). @Cid: " + objBatchInfo.Cid);
                            sw.Stop();
                            sw.Reset();
                        }

                        if (processedItemType.WorkItemtype == WorkItemType.Disconnect)
                        {
                            objBatchInfo.IsThreadActive = false;
                            break;
                        }

                    }

                    workItem = QueueManager.GetWorkItem(strDeviceId);

                    if (!string.IsNullOrEmpty(workItem))
                    {
                        processedItemType = this.ManageWorkItem(workItem, objBatchInfo, strDeviceId, strMacId, deviceAddress);
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "HandleRequestsMethod: WI Got & Processed, @DeviceId: " + strDeviceId + " @deviceAddress: " + deviceAddress + " @strMacId: " + strMacId + " @workItem: " + workItem + " @Cid: " + processedItemType.Cid);
                        if (sw.IsRunning)
                        {
                            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, " Macid validation response received from web server(Workitem). @Cid: " + objBatchInfo.Cid);
                            sw.Stop();
                            sw.Reset();
                        }
                    }

                    //Handling Notification
                    var notificationMsg = this.ReceiveNotification(objBatchInfo.GatewayCon, objBatchInfo.GatewayPro, strDeviceId, strMacId, deviceAddress);
                    if (notificationMsg != null && notificationMsg.Count > 0)
                    {
                        foreach (var msg in notificationMsg)
                        {
                            if (msg.Message != null)
                            {
                                UbiqsensBooter.ObjPubMqtt.Publish("Server/NOTIFICATIONS/" + strDeviceId, ASCIIEncoding.ASCII.GetBytes(msg.Message), MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Notification sent to Web server. @Data: " + msg.Message + " @Cid: " + msg.Cid);
                            }
                        }
                    }
                    // Health Check
                    if ((!IsGatewayConnectionConnected(objBatchInfo.GatewayCon)))
                    {
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "HandleRequestsMethod: Disconnected in Health check Failure, @MacId: " + strMacId + " @DeviceId: " + strDeviceId + " @deviceAddress: " + deviceAddress + " @Cid: " + objBatchInfo.Cid);
                        HandleDisconnect(objBatchInfo, deviceAddress, strDeviceId, strMacId, true, Constants.Disconnect_Health);

                        //todo: suprassing disconnect for multiple instance temporarily
                        UbiqsensBooter.ObjPubMqtt.Publish("Server/NOTIFICATIONS/" + strDeviceId, ASCIIEncoding.ASCII.GetBytes(String.Format(Constants.Disconnected, objBatchInfo.GatewayCon.GetLastError(), strIPAddress, objBatchInfo.Cid)), MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                        //UbiqsensBooter.ObjPubMqtt.Publish(Constants.DeviceStatusTopic + Constants.Slash.ToString() + strDeviceId, ASCIIEncoding.ASCII.GetBytes(Constants.StrInactive), MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);

                        objBatchInfo.IsThreadActive = false;
                    }
                    else if (sw.Elapsed >= tp)
                    {
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "HandleRequestsMethod: Disconnected in Idle Timeout, @MacId: " + strMacId + " @DeviceId: " + strDeviceId + " @deviceAddress: " + deviceAddress + " @Cid: " + objBatchInfo.Cid + " @tp: " + tp);
                        HandleDisconnect(objBatchInfo, deviceAddress, strDeviceId, strMacId, false, Constants.Disconnect_Idle);
                        UbiqsensBooter.ObjPubMqtt.Publish("Server/NOTIFICATIONS/" + strDeviceId, ASCIIEncoding.ASCII.GetBytes(String.Format(Constants.Disconnected, "Idle Timeout (" + tp + " ) - No Request Received from server", strIPAddress, objBatchInfo.Cid)), MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "HandleRequestsMethod: @DeviceId: " + strDeviceId + " @deviceAddress: " + deviceAddress + " @strMacId: " + strMacId + " @Cid: " + objBatchInfo.Cid + " Idle Timeout (" + tp + " ) - No Request Received from server");

                        objBatchInfo.IsThreadActive = false;
                    }
                }
                catch (Exception ex)
                {
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "HandleRequestsMethod: @strDeviceId: " + strDeviceId + " @deviceAddress: " + deviceAddress + " @strMacId: " + strMacId + " @Cid: " + objBatchInfo.Cid + " - Error in Getting & Processing Workitems. @Ex: " + ex.Message, ex);
                }
                if (sw.IsRunning)
                    Thread.Sleep(1000);

                if (!sw.IsRunning)
                {
                    objBatchInfo.RequestHandlerGate.WaitOne();
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "HandleRequestsMethod: Device Thread Wakeup, @DeviceId: " + strDeviceId + " @DeviceAddress: " + deviceAddress + " @MacId: " + strMacId);
                }
            }
            #endregion
            return isCompleted;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gatewayConnectionPlugin"></param>
        /// <param name="protocolPlugin"></param>
        /// <param name="newDevice"></param>
        /// <param name="deviceAddress"></param>
        /// <param name="strDeviceId"></param>
        /// <param name="strMacId"></param>
        private void HandleDuplicateSocket(BatchInfo objBatchInfo, Thread newDevice, string deviceAddress, string strDeviceId, string strMacId)
        {
            try
            {
                #region Reuse Socket

                if (ReusableCollection.ContainsKey(strMacId))
                {
                    try
                    {
                        //Disconnecting already connected Device
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Duplicate socket connection found, Device Trying to Reconnect after disconnection due to Power/Network Failure. @Cid: " + objBatchInfo.Cid);
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, strDeviceId + "$" + strMacId + "$" + "Device Trying to Reconnect after disconnection due to Power/Network Failure.");
                        HandleDisconnect(ReusableCollection[strMacId], deviceAddress, strDeviceId, strMacId, true, Constants.Disconnect_Duplicate);
                    }
                    catch (Exception ex)
                    {
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "@DeviceId: " + strDeviceId + " @strMacId: " + strMacId + " @Cid: " + objBatchInfo.Cid + " - Failed to disconnect existing socket - @Ex: " + ex.Message, ex);
                    }
                }

                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "@DeviceId: " + strDeviceId + "$" + strMacId + " @Cid: " + objBatchInfo.Cid + " Added socket in ReuseSocket");
                ReusableCollection.TryAdd(strMacId, objBatchInfo);
                ExitCollection.TryAdd(strMacId, objBatchInfo);
                #endregion
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in Handle Duplicate Socket. @DeviceId: " + strDeviceId + " @MacId: " + strMacId + "@Cid:" + objBatchInfo.Cid + " @Ex: " + ex.Message, ex);
            }
        }

        /// <summary>
        /// Validate teh device with configured parameters
        /// </summary>
        /// <param name="gatewayConnectionPlugin"></param>
        /// <param name="protocolPlugin"></param>
        /// <param name="deviceAddress"></param>
        /// <param name="strDeviceId"></param>
        /// <param name="strMacId"></param>
        /// <returns></returns>
        private bool ValidateDevice(BatchInfo objBatchInfo, ref string deviceAddress, ref string strDeviceId, ref string strMacId, ref string strType, ref List<string> Messages, ref string strState, ref string strRevisionModel)
        {
            bool isCompleted = true;
            try
            {
                #region Validating device connecting to Gateway
                var notificationMsg = new List<string>();
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Get Device MacId Request started. @Cid: " + objBatchInfo.Cid);

                var deviceidnotifiation = this.GetDeviceId(objBatchInfo, out deviceAddress, out strMacId);

                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Received MacId reponse from device. @MacId: " + strMacId + " @Cid: " + objBatchInfo.Cid);

                if (deviceidnotifiation != null && deviceidnotifiation.Count >= 4)
                {
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, deviceidnotifiation.First());
                    strDeviceId = deviceidnotifiation.First();
                    strType = deviceidnotifiation[2];
                    strRevisionModel = deviceidnotifiation[3];

                    if (deviceidnotifiation[1].ToLower() == "true")
                    {
                        strState = "1";
                        for (int i = 4; i < deviceidnotifiation.Count; i++)
                        {
                            if (!string.IsNullOrEmpty(deviceidnotifiation[i]))
                                Messages.AddRange(deviceidnotifiation[i].Split('$'));
                        }
                    }
                    else
                        strState = "0";

                }
                else
                {
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, deviceAddress + "$" + strMacId + " @Cid: " + objBatchInfo.Cid + " : " + "Validation response is not valid ");
                    isCompleted = false;
                }
                #endregion
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "ValidateDeviceMethod: Error in Validate Device. @DeviceId: " + strDeviceId + " @deviceAddress: " + deviceAddress + " @MacId: " + strMacId + " @Cid: " + objBatchInfo.Cid + " @Ex: " + ex.Message, ex);
                isCompleted = false;
            }
            return isCompleted;
        }

        /// <summary>
        /// Checks the device connection status
        /// </summary>
        /// <param name="gatewayConnectionPlugin">gateway Connection Plugin</param>
        /// <returns>Returns isConnected</returns>
        public bool IsGatewayConnectionConnected(IGatewayConnection gatewayConnectionPlugin)
        {
            var isConnected = false;
            try
            {
                if (gatewayConnectionPlugin == null)
                    throw new Exception("GatewayPlugin  object is NULL");

                isConnected = gatewayConnectionPlugin.IsConnected();
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "IsGatewayConnectionConnected: Error in checking the device connectivity. @Ex: " + ex.Message, ex);
            }

            return isConnected;
        }

        /// <summary>
        /// Gets the Command From Protocol
        /// </summary>
        /// <param name="gatewayConnectionPlugin">gateway Connection Plugin</param>
        /// <param name="protocolPlugin">protocol Plugin</param>
        /// <param name="cmdId">command id</param>
        /// <param name="value">string value</param>
        /// <returns>Returns command</returns>
        public byte[] GetCommandFromProtocol(IGatewayConnection gatewayConnectionPlugin, IGatewayProtocol protocolPlugin, string cmdId, string value, string deviceId, string strMacId, string deviceAddress)
        {
            byte[] cmd = null;
            try
            {
                cmd = protocolPlugin.GetCommand(cmdId, value);
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, BitConverter.ToString(cmd));
                //if (UbiqsensBooter.IsDiagnosticCheck)
                //    UbiqsensBooter.ObjPubMqtt.PublisgDiagnostic("Diagnostic/US/Request/" + deviceId + "$" + deviceAddress + "$" + strMacId + "/Cmd ID : " + cmdId + " Value :" + value, BitConverter.ToString(cmd), MqttQos.QOS_LEVEL_EXACTLY_ONCE, true);
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in getting command from Protocol, GetCommandFromProtocol() Method with @DeviceId: " + deviceId + " @MacId: " + strMacId + " @CmdId: " + cmdId + " @value: " + value + " @DeviceAddress: " + deviceAddress, ex);
                //UbiqsensBooter.ObjPubMqtt.PublisgDiagnostic("Diagnostic/US/Message/" + deviceAddress + "$" + strMacId, "Error in getting command from Protocol. Exp:" + ex.Message, MqttQos.QOS_LEVEL_EXACTLY_ONCE, true);
            }

            return cmd;
        }

        /// <summary>
        /// Sends the command data to Device
        /// </summary>
        /// <param name="gatewayConnectionPlugin">gateway Connection Plugin</param>
        /// <param name="protocolPlugin">protocol Plugin</param>
        /// <param name="cmd">byte array command</param>
        /// <returns>Returns sent</returns>
        public bool SendCommand(IGatewayConnection gatewayConnectionPlugin, IGatewayProtocol protocolPlugin, byte[] cmd)
        {
            var sent = false;
            try
            {
                sent = gatewayConnectionPlugin.Send(cmd);
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in sending data. @Cmd: " + BitConverter.ToString(cmd) + " @Ex: " + ex.Message, ex);
            }

            return sent;
        }

        /// <summary>
        /// Receives the data 
        /// </summary>
        /// <param name="gatewayConnectionPlugin">gateway Connection Plugin</param>
        /// <returns>received data</returns>
        public List<byte[]> ReceiveData(IGatewayConnection gatewayConnectionPlugin, string strCid, string strDeviceId, string strMacId, string deviceAddress)
        {
            List<byte[]> recData = null;
            try
            {

                recData = gatewayConnectionPlugin.Receive();
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Received data from device.  @DeviceId: " + strDeviceId + " @MacId: " + strMacId + " @Cid: " + strCid + " @DeviceAddress: " + deviceAddress);

            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in receiving data. @Ex: " + ex.Message, ex);
            }

            return recData;
        }

        /// <summary>
        /// Parses the data received from the device using Protocol plugin
        /// </summary>
        /// <param name="protocolPlugin">protocol Plugin</param>
        /// <param name="data">byte array data</param>
        /// <param name="cmdId">command id</param>
        /// <returns>Parsed Data</returns>
        public List<ParsedData> ParseRecData(IGatewayProtocol protocolPlugin, List<byte[]> recDataBytes, string cmdId, string deviceId, string deviceAddress, string strMacId)
        {
            List<ParsedData> lstParsedData = null;
            try
            {
                lstParsedData = protocolPlugin.Parse(recDataBytes, cmdId);

                if (UbiqsensBooter.IsDiagnosticCheck)
                {
                    if (lstParsedData.Count > 0)
                    {
                        for (int i = 0; i < recDataBytes.Count && i < lstParsedData.Count; i++)
                        {
                            if (lstParsedData[i].ParsedDataType == ParsedValueType.Notification)
                            {
                                //UbiqsensBooter.ObjPubMqtt.PublisgDiagnostic("Diagnostic/US/Notification/" + deviceId + "$" + deviceAddress + "$" + strMacId + '/' + lstParsedData[i].Data, BitConverter.ToString(recDataBytes[i]), MqttQos.QOS_LEVEL_EXACTLY_ONCE, true);
                                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Diagnostic/US/Notification/" + deviceId + "$" + deviceAddress + strMacId + '/' + DateTime.Now.ToString().Replace("/", ":") + '/' + lstParsedData[i].Data);
                            }
                            else
                            {
                                //UbiqsensBooter.ObjPubMqtt.PublisgDiagnostic("Diagnostic/US/WorkItemResponse/" + deviceId + "$" + deviceAddress + "$" + strMacId + '/' + lstParsedData[i].Data, BitConverter.ToString(recDataBytes[i]), MqttQos.QOS_LEVEL_EXACTLY_ONCE, true);
                                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Diagnostic/US/WorkItemResponse/" + deviceId + '/' + DateTime.Now.ToString().Replace("/", ":") + '/' + lstParsedData[i].Data + " @MacId: " + strMacId);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in Parsing data, ParseRecData() Method With @CmdId: " + cmdId + " @DeviceId: " + deviceId + " @DeviceAddress: " + deviceAddress + " @MacId: " + strMacId + " @Ex: " + ex.Message, ex);
            }

            return lstParsedData;
        }

        /// <summary>
        /// Disconnects the connection with the device
        /// </summary>
        /// <param name="gatewayConnectionPlugin">gateway Connection Plugin</param>
        /// <returns>disconnected is returned</returns>
        public bool DisconnectGatewayConnection(BatchInfo objBatchInfo)
        {
            var disconnected = false;
            try
            {
                if (objBatchInfo.GatewayCon != null)
                {
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "@Cid: " + objBatchInfo.Cid + " DisconnectGatewayConnection: Disconnecting the device.");
                    disconnected = objBatchInfo.GatewayCon.Disconnect();
                }
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "DisconnectGatewayConnection: Error in disconnecting the device. " + "@Cid: " + objBatchInfo.Cid + " @Ex: " + ex.Message, ex);
            }

            return disconnected;
        }

        /// <summary>
        /// Gets the Device id
        /// </summary>
        /// <param name="protocolPlugin">protocol Plugin</param>
        /// <returns>Device Id is returned</returns>
        public List<string> GetDeviceId(BatchInfo objBatchInfo, out string deviceAddress, out string strMacId)
        {
            var lstValParmas = new List<string>();
            strMacId = string.Empty;
            deviceAddress = string.Empty;
         
            try
            {
                var valData = objBatchInfo.GatewayPro.ValidateDevice();

                if (valData != null && valData.Count > 0)
                {
                    strMacId = valData.Values.First<string>();
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "MacId Validation Response - Received from protocol plugin. Response: " + strMacId + " @Cid: " + objBatchInfo.Cid);
                    if (strMacId.Length == 12 || strMacId.Length == 14)
                    {
                        lstValParmas = this.ValidatePassiveDevice(valData, ref deviceAddress);
                        objBatchInfo.Cid = objBatchInfo.Cid.Replace(Constants.DeviceAddress, deviceAddress);
                        objBatchInfo.Cid = objBatchInfo.Cid.Replace(Constants.MacId, strMacId);
                        objBatchInfo.Source = deviceAddress;
                        objBatchInfo.MacId = strMacId;
                        objBatchInfo.GatewayCon.DeviceAddress = deviceAddress;
                        objBatchInfo.GatewayCon.DeviceId = lstValParmas.First();

                        if (!objBatchInfo.GatewayCon.InitializeAsync())
                        {
                            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in Initialising Async. @Cid: " + objBatchInfo.Cid);
                            return null;
                        }
                    }
                    else
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "MacId Validation Failed - Data Mismatch. @Cid: " + objBatchInfo.Cid);
                }
                else
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Empty validation response from protocol plugin @Cid: " + objBatchInfo.Cid);
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in getting Deviceid. @Ex: " + ex.Message + " @Macid: " + strMacId + " @Cid: " + objBatchInfo.Cid, ex);
            }

            return lstValParmas;
        }

        /// <summary>
        /// Receives the notification data from the Ethernet gateway plugin
        /// </summary>
        /// <param name="gatewayConnectionPlugin">gateway Connection Plugin</param>
        /// <param name="protocolPlugin">protocol Plugin</param>
        /// <returns>list of Notification</returns>
        public List<NotifcationMessage> ReceiveNotification(IGatewayConnection gatewayConnectionPlugin, IGatewayProtocol protocolPlugin, string deviceId, string strMACId, string deviceAddress)
        {
            try
            {
                List<NotifcationMessage> parsedDataList = new List<NotifcationMessage>();
                var recDataBytes = gatewayConnectionPlugin.Receive();
                if (recDataBytes != null && recDataBytes.Count != 0)
                    parsedDataList = ParseData(gatewayConnectionPlugin, protocolPlugin, recDataBytes, deviceId, strMACId, deviceAddress);
                return parsedDataList;
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in getting notification. ReceiveNotification() Method With @DeviceId: " + deviceId + " @MacId: " + strMACId + " @DeviceAddress: " + deviceAddress + " @Ex: " + ex.Message, ex);
            }

            return new List<NotifcationMessage>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="protocolPlugin"></param>
        /// <param name="recDataBytes"></param>
        /// <param name="deviceId"></param>
        /// <param name="strMacid"></param>
        /// <param name="deviceAddress"></param>
        /// <returns></returns>
        public List<NotifcationMessage> ParseData(IGatewayConnection gatewayConnectionPlugin, IGatewayProtocol protocolPlugin, List<byte[]> recDataBytes, string deviceId, string strMacid, string deviceAddress)
        {
            var lstNotification = new List<NotifcationMessage>();
            string cid = string.Empty;
            try
            {

                if (recDataBytes == null || recDataBytes.Count <= 0) return lstNotification;

                var lstParsedData = protocolPlugin.Parse(recDataBytes, null);

                lstNotification.AddRange(lstParsedData.Select(pd =>
                {
                    cid = CidManager.Instance.GenerateCid(pd.Data, strMacid, deviceAddress);
                    try
                    {
                        if (UbiqsensBooter.HeartBeatStatus)
                        {
                            // Acknowledege back to thermostat with same heartbeat packet received
                            if (pd.Data.Contains(UbiqsensBooter.HeartBeatValue))
                            {
                                var timerState = Tuple.Create<IGatewayConnection, String, String, String, String, String>(gatewayConnectionPlugin, pd.Data, cid, strMacid, deviceAddress, deviceId);
                                if (UbiqsensBooter.HeartBeatInterval == 0)
                                {
                                    SendHeartBeatAck(timerState);
                                }
                                else
                                {
                                    var heartbeatTimer = new Timer(callback: new TimerCallback(SendHeartBeatAck),
                                    state: timerState,
                                    dueTime: (UbiqsensBooter.HeartBeatInterval * 1000),
                                    period: Timeout.Infinite);
                                }
                            }
                        }
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Notification received from thermostat. @Data: " + pd.Data + " @DeviceId: " + deviceId + " @DeviceAddress: " + deviceAddress + " @MacId: " + strMacid + " @Cid: " + cid);
                        return new NotifcationMessage() { Message = string.Format(Constants.Notification, pd.Data, cid), Cid = cid };
                    }
                    catch (Exception ex)
                    {
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in formatting notification response. ParseData() Method With @DeviceId: " + deviceId + " @DeviceAddress: " + deviceAddress + " @MacId: " + strMacid + " @Cid: " + cid + " @Ex: " + ex.Message, ex);
                        return new NotifcationMessage() { Message = null, Cid = cid };
                    }
                }
                ));
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in Parsing data. ParseData() Method With @DeviceId: " + deviceId + " @DeviceAddress: " + deviceAddress + " @MacId: " + strMacid + " @Cid: " + cid + " @Ex: " + ex.Message, ex);
            }
            return lstNotification;

        }

        private void SendHeartBeatAck(object timerState)
        {
            var hbObj = (Tuple<IGatewayConnection, String, String, String, String, String>)timerState;
            try
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Heartbeat Ack sent to thermostat. @Data: " + hbObj.Item2 + " @DeviceId: " + hbObj.Item6 + " @DeviceAddress: " + hbObj.Item5 + " @MacId: " + hbObj.Item4 + " @Cid: " + hbObj.Item3);
                hbObj.Item1.Send(Constants.Heartbeat_Comm);
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in sending HeartBeat response. SendHeartBeatAck() Method With @DeviceId: " + hbObj.Item6 + " @DeviceAddress: " + hbObj.Item5 + " @MacId: " + hbObj.Item4 + " @Cid: " + hbObj.Item3 + " @Ex: " + ex.Message, ex);
            }
        }


        /// <summary>
        /// Initializes Protocol plugin
        /// </summary>
        /// <param name="gatewayConnectionPlugin">gateway Connection Plugin</param>
        /// <param name="protocolPlugin">protocol Plugin</param>
        /// <returns>is Initialized is returned</returns>
        public bool InitialiseProtocolPlugin(IGatewayConnection gatewayConnectionPlugin, IGatewayProtocol protocolPlugin)
        {
            var isInitialised = false;
            try
            {
                isInitialised = protocolPlugin.Initialize(gatewayConnectionPlugin, UbiqsensBooter.ObjLogger);
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in initialising protocol plugin " + " @Ex: " + ex.Message, ex);
            }

            return isInitialised;
        }

        /// <summary>
        /// Ethernet Gateway Connection Initiator
        /// </summary>
        /// <param name="objAccEthPara">Accept Connection Ethernet Parameters</param>
        internal void EthernetGatewayConnectionInitiator(AcceptConnectionEthernetParameters objAccEthPara)
        {
            try
            {
                if (objAccEthPara.ObjGatewayConnection == null || objAccEthPara.ObjGatewayProtocol == null)
                {
                    throw new NullReferenceException("gatewayConnectionPlugin or pluginDetails or protocolPlugin cannot be null");
                }

                var ips = Dns.GetHostAddresses(Dns.GetHostName());
                var socketAccept = new TcpListener(IPAddress.Any, Convert.ToInt32(objAccEthPara.ListenerPort));

                Task.Factory.StartNew(new Action(DoBatchOperation));
                socketAccept.Server.SendTimeout = Convert.ToInt32(objAccEthPara.SendTimeout);
                socketAccept.Server.ReceiveTimeout = int.MaxValue;//Convert.ToInt32(objAccEthPara.ReceiveTimeout);
                socketAccept.Server.ReceiveBufferSize = int.MaxValue;// Convert.ToInt32(objAccEthPara.ReceiveTimeout);
                socketAccept.Start(1000);

                gatewayPluginType = objAccEthPara.ObjGatewayConnection.GetType();
                protocolPluginType = objAccEthPara.ObjGatewayProtocol.GetType();

                socketAccept.BeginAcceptTcpClient(OnAccept, socketAccept);

            }
            catch (SocketException objSocketException)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "SocketException in GatewayConnectionHandler method. @Ex: " + objSocketException.Message + " @ErrorCode: " + objSocketException.ErrorCode, objSocketException);
            }
            catch (NullReferenceException objNullReferenceExp)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "NullReferenceException Exception in GatewayConnectionHandler method. @Ex: " + objNullReferenceExp.Message, objNullReferenceExp);
            }
            catch (Exception objException)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Exception in GatewayConnectionHandler method. @Ex: " + objException.Message, objException);
            }
        }

        private void DoBatchOperation()
        {
            while (Ubiqsens.isRunning)
            {
                try
                {
                    Thread.Sleep(5000);
                    BatchInfo batchObj = null;
                    if (!BatchQueue.IsEmpty)
                    {
                        for (int i = 0; i < 100; i++)
                        {
                            try
                            {
                                Thread newDevice = null;
                                if (!BatchQueue.TryDequeue(out batchObj))
                                    break;

                                newDevice = new Thread(new ParameterizedThreadStart((bInfo) =>
                                {
                                    BatchInfo objBatchInfo = (BatchInfo)bInfo;
                                    try
                                    {
                                        this.GatewayConnectionHandler(objBatchInfo, newDevice, string.Empty);
                                    }
                                    catch (Exception ex)
                                    {
                                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Error in Batch Processing Start Thread: @Ex: " + ex.Message + ex.StackTrace);
                                    }
                                }))
                                {
                                    Priority = ThreadPriority.Normal,
                                    IsBackground = false
                                };
                                newDevice.SetApartmentState(ApartmentState.MTA);
                                newDevice.Start(batchObj);
                                _totalSocketConnection++;
                                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Total socket connections: " + _totalSocketConnection);
                            }
                            catch (Exception ex)
                            {
                                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in Batch Processing: @Ex: " + ex.Message, ex);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Error in Batch Processing: " + ex.Message, ex);
                }
            }
        }

        /// <summary>
        /// Ethernet Gateway Connection Initiator
        /// </summary>
        /// <param name="objAccEthPara">Accept Connection Ethernet Parameters</param>
        internal void LocationServiceConnectionInitiator(AcceptConnectionEthernetParameters objAccEthPara)
        {
            try
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Location Thread Starts");

                if (objAccEthPara.ObjGatewayConnection == null || objAccEthPara.ObjGatewayProtocol == null)
                {
                    throw new NullReferenceException("gatewayConnectionPlugin or pluginDetails or protocolPlugin cannot be null");
                }

                var ips = Dns.GetHostAddresses(Dns.GetHostName());

                var socketAccept = new TcpListener(IPAddress.Any, 58766);

                socketAccept.Server.SendTimeout = Convert.ToInt32(objAccEthPara.SendTimeout);
                socketAccept.Server.ReceiveTimeout = int.MaxValue;// Convert.ToInt32(objAccEthPara.ReceiveTimeout);
                socketAccept.Server.ReceiveBufferSize = int.MaxValue;// Convert.ToInt32(objAccEthPara.ReceiveTimeout);
                socketAccept.Start(1000);
                string outVal;
                // socketAccept.BeginAcceptTcpClient(OnAccept, socketAccept);

                while (Ubiqsens.isRunning)
                {
                    try
                    {
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Location Thread Listening");

                        var receiveSocket = socketAccept.AcceptTcpClient();

                        Thread newDevice = null;

                        newDevice = new Thread(new ThreadStart(() =>
                        {
                            IGatewayConnection gatewayplugin = null;
                            BatchInfo objBatchInfo = new BatchInfo();
                            string uid = string.Empty;

                            try
                            {
                                //TODO : Write a service for location thread
                                //To disconnect socket in catch block
                                uid = Guid.NewGuid().ToString();
                                objBatchInfo.Cid = CidManager.Instance.GenerateCid(Constants.ThermostatLocationName, uid, string.Empty);

                                gatewayplugin = new EthernetGatewayHandler();
                                var devResponse = Activator.CreateInstance(protocolPluginType) as IGatewayProtocol;
                                gatewayplugin.Initialize(receiveSocket, UbiqsensBooter.ObjLogger);

                                gatewayplugin.InitializeAsync();
                                devResponse.Initialize(gatewayplugin, UbiqsensBooter.ObjLogger);
                                QueueManager.ConnectedActiveDevice.TryAdd(uid, Constants.Active);
                                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Sending ZIP Code request to device. @Cid: " + objBatchInfo.Cid);
                                gatewayplugin.Send(new byte[] { 01, 01, 00, 03, 02, 08, 04, 93 }); // Send ZIP Code request to device

                                objBatchInfo.GatewayCon = gatewayplugin;
                                objBatchInfo.GatewayPro = devResponse;

                                List<byte[]> output = new List<byte[]>();
                                Thread.Sleep(1000);
                                int count = 0;
                                while (output.Count == 0 && count <= 10)
                                {
                                    try
                                    {
                                        output = gatewayplugin.Receive();

                                        if (output.Count > 0)
                                        {
                                            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Received ZIP Code response to device. @Cid: " + objBatchInfo.Cid + " @Data: " + BitConverter.ToString(output.First()));
                                            count = 0;
                                            var parsedData = devResponse.Parse(output, Constants.Automation);
                                            //objBatchInfo.Cid = objBatchInfo.Cid.Replace(Constants.SourceIP, parsedData.First().SourceIP;
                                            //Send data to application

                                            UbiqsensBooter.ObjPubMqtt.Publish("Server/NOTIFICATIONS/WEATHER/" + uid, ASCIIEncoding.ASCII.GetBytes(String.Format(Constants.Notification, parsedData.First().Data, objBatchInfo.Cid)), MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                                            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Sent ZIP Code response to web server. @Cid: " + objBatchInfo.Cid);

                                            // TODO : Added only for debug purpose : remove
                                            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Response received from Automation. @Data: " + parsedData.First().Data);

                                            Thread.Sleep(1000);
                                            string workitem = string.Empty;
                                            while (count <= 10)
                                            {
                                                //Get data from Queue 
                                                workitem = QueueManager.GetWorkItem(uid);
                                                if (!string.IsNullOrWhiteSpace(workitem))
                                                {
                                                    //write to device socket
                                                    ManageWorkItem(workitem, objBatchInfo, uid, string.Empty, string.Empty); // Replace device address with objBatchInfo.SourceIP
                                                    break;
                                                }
                                                count++;
                                                Thread.Sleep(1000);
                                            }
                                            //Send this data
                                            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Disconnection initiated for location service device. @Cid: " + objBatchInfo.Cid);
                                            DisconnectGatewayConnection(objBatchInfo);
                                            QueueManager.ConnectedActiveDevice.TryRemove(uid, out outVal);
                                            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Disconnected the device. @Cid: " + objBatchInfo.Cid);
                                            break;
                                        }
                                        count++;
                                        Thread.Sleep(1000);
                                    }
                                    catch (Exception ex)
                                    {
                                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in LocationServiceConnectionHandler @Cid: " + objBatchInfo.Cid + " @Ex: " + ex.Message, ex);
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error inside LocationServiceConnectionInitiator in new device thread.  @Cid: " + objBatchInfo.Cid + " @Ex: " + ex.Message, ex);
                            }
                            try
                            {
                                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Disconnection initiated for location service device. @Cid: " + objBatchInfo.Cid);
                                DisconnectGatewayConnection(objBatchInfo);
                            }
                            catch (Exception ex)
                            {
                                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error inside LocationServiceConnectionInitiator in socket disconnection after new device thread.  @Cid: " + objBatchInfo.Cid + " @Ex: " + ex.Message, ex);
                            }
                        }))
                        {
                            Priority = ThreadPriority.Normal,
                            IsBackground = false
                        };
                        newDevice.SetApartmentState(ApartmentState.MTA);
                        newDevice.Start();
                    }
                    catch (Exception ex)
                    {
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in LocationServiceConnectionInitiator. @Ex: " + ex.Message, ex);
                    }
                    Thread.Sleep(1);
                }
                socketAccept.Stop();
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Exited from socket Listening.");
            }
            catch (SocketException objSocketException)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "SocketException in Location Service method. @Ex: " + objSocketException.Message + " @ErrorCode: " + objSocketException.ErrorCode, objSocketException);
            }
            catch (NullReferenceException objNullReferenceExp)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "NullReferenceException in Location Service  method. @Ex: " + objNullReferenceExp.Message, objNullReferenceExp);
            }
            catch (Exception objException)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Exception in Location Service  method. @Ex: " + objException.Message, objException);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="res"></param>
        private void OnAccept(IAsyncResult res)
        {
            TcpListener listener = (TcpListener)res.AsyncState;

            try
            {
                TcpClient receiveSocket = listener.EndAcceptTcpClient(res);
                var gatewayConnectionPlugin = new EthernetGatewayHandler();// Activator.CreateInstance(gatewayPluginType) as IGatewayConnection;
                gatewayConnectionPlugin.Initialize(receiveSocket, UbiqsensBooter.ObjLogger);
                gatewayConnectionPlugin.Send(new byte[] { 01, 00, 00, 03, 02, 08, 02, 172 });
                var protocolPlugin = Activator.CreateInstance(protocolPluginType) as IGatewayProtocol;
                var cid = CidManager.Instance.GenerateCid(Constants.Connect, string.Empty, string.Empty);

                BatchQueue.Enqueue(new BatchInfo() { RequestHandlerGate = new AutoResetEvent(true), IsThreadActive = true, ReceivedSocket = receiveSocket, GatewayCon = gatewayConnectionPlugin, GatewayPro = protocolPlugin, Cid = cid });
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Socket accepted and requested for Mac Id from thermostat. @Cid: " + cid);
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in initialising GatewayConnectionHandler.OnAccept() Method @Ex: " + ex.Message, ex);
            }
            listener.BeginAcceptTcpClient(OnAccept, listener);
        }

        /// <summary>
        /// Validate Passive Device
        /// </summary>
        /// <param name="validationDictionary">validation Dictionary</param>
        /// <returns>result string</returns>
        private List<string> ValidatePassiveDevice(Dictionary<string, string> validationDictionary, ref string deviceAddress)
        {
            var result = new List<string>();
            try
            {
                var data = ReadDevicePolicy.PassiveDeviceIdDictionary.Where(p => p.Value.Keys.First().ToLower() == validationDictionary.Keys.First().ToLower()
                    && p.Value.Values.First().ToLower() == validationDictionary.Values.First().ToLower());

                if (data.Any())  // Case : where devices registered with UR.
                {
                    result.Add(data.Last().Key);
                    // result.Add(data.First().Value.First().Value);
                    if (validationDictionary.Count > 0)
                    {
                        if (validationDictionary.ContainsKey("ForceConnection"))
                        {
                            result.Add(validationDictionary.ContainsValue("True") ? "True" : "False");
                        }
                        if (validationDictionary.ContainsKey("HVACAutoSetting"))
                        {
                            result.Add(validationDictionary["HVACAutoSetting"]);
                        }
                        if (validationDictionary.ContainsKey("RevisionModel"))
                        {
                            result.Add(validationDictionary["RevisionModel"]);
                        }
                        if (validationDictionary.ContainsKey("Message"))
                        {
                            result.Add(validationDictionary["Message"]);
                        }
                        if (validationDictionary.ContainsKey("Source"))
                        {
                            deviceAddress = validationDictionary["Source"];
                        }
                    }
                }
                else // Case : where devices not registered with UR . So registering by US
                {
                    string dataformat = string.Format(Constants.DeviceFormat, "T-" + validationDictionary.First().Value, validationDictionary.First().Value);
                    RestClient client = new RestClient();
                    client.EndPoint = string.Format(Constants.RestAddDeviceURL, UbiqsensBooter.RestIP, UbiqsensBooter.RestPort);
                    client.Method = HttpVerb.Post;
                    client.PostData = ASCIIEncoding.ASCII.GetBytes(dataformat);
                    string res = client.Post(string.Empty, false, false);
                    client = null;

                    //Split response and get device id from it
                    string _deviceId = string.Empty;
                    var doc1 = new XmlDocument();
                    var arr = new string[] { "?>" };
                    doc1.LoadXml(res.Split(arr, StringSplitOptions.None)[1]);

                    foreach (XmlNode node in doc1)
                    {
                        foreach (XmlNode child in node.ChildNodes)
                        {
                            foreach (XmlNode subchild in child.ChildNodes)
                            {
                                foreach (XmlNode subchild1 in subchild.ChildNodes)
                                {
                                    if (subchild1.Name == "deviceUid")
                                        _deviceId = subchild1.InnerText;
                                }
                            }
                        }
                    }
                    result.Add(_deviceId);

                    if (validationDictionary.ContainsKey("ForceConnection"))
                    {
                        result.Add(validationDictionary.ContainsValue("True") ? "True" : "False");
                    }
                    if (validationDictionary.ContainsKey("HVACAutoSetting"))
                    {
                        result.Add(validationDictionary["HVACAutoSetting"]);
                    }
                    if (validationDictionary.ContainsKey("RevisionModel"))
                    {
                        result.Add(validationDictionary["RevisionModel"]);
                    }
                    if (validationDictionary.ContainsKey("Message"))
                    {
                        result.Add(validationDictionary["Message"]);
                    }
                    if (validationDictionary.ContainsKey("Source"))
                    {
                        deviceAddress = validationDictionary["Source"];
                    }
                }
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in Validate Passive Device, ValidatePassiveDevice() Method. @Ex: " + ex.Message, ex);
            }
            return result;
        }

        /// <summary>
        /// Manages the incoming work items
        /// </summary>
        /// <param name="workItem">work Item</param>
        /// <param name="gatewayConnectionPlugin">gateway Connection Plugin</param>
        /// <param name="protocolPlugin">protocol Plugin</param>
        /// <param name="strDeviceId">Device Id</param>
        /// <param name="guid">unique ID</param>
        private WorkItem ManageWorkItem(string workItem, BatchInfo objBatchInfo, string strDeviceId, string strMacId, string deviceAddress)
        {
            var objWorkItemMessageNew = new WorkItem();
            string cid = string.Empty;
            try
            {
                objWorkItemMessageNew = new WorkItemParser().ParseWorkItem(workItem);
                var responseBody = string.Empty;
                cid = objWorkItemMessageNew.Cid;
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "ManageWorkItemMethod: Details WorkItemType: " + objWorkItemMessageNew.WorkItemtype + " @DeviceId: " + strDeviceId + " @MacId: " + strMacId + " @Cid: " + objWorkItemMessageNew.Cid + " @DeviceAddress: " + deviceAddress);
                switch (objWorkItemMessageNew.WorkItemtype)
                {
                    case WorkItemType.Connect:
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Connect request received  from web server & processed. @DeviceId: " + strDeviceId + " @MacId: " + strMacId + " @Cid: " + objWorkItemMessageNew.Cid + " @DeviceAddress: " + deviceAddress);
                        //UbiqsensBooter.ObjPubMqtt.Publish(Constants.WITopic + objWorkItemMessageNew.WorkItemId + Constants.Slash + strDeviceId, ASCIIEncoding.ASCII.GetBytes(Constants.Connected), MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                        //UbiqsensBooter.ObjPubMqtt.Publish(Constants.DeviceStatusTopic + Constants.Slash.ToString() + strDeviceId, ASCIIEncoding.ASCII.GetBytes(Constants.Active), MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                        break;
                    case WorkItemType.Commands:
                        for (var i = 0; i < objWorkItemMessageNew.Commands.Count(); i++)
                        {
                            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Workitem request received from web server. @DeviceId: " + strDeviceId + " @MacId: " + strMacId + " @Cid: " + objWorkItemMessageNew.Cid + " @DeviceAddress: " + deviceAddress + "@WorkItem Type: " + objWorkItemMessageNew.Commands[i].CmdId + " @Value: " + objWorkItemMessageNew.Commands[i].Value);
                            var cmd = this.GetCommandFromProtocol(objBatchInfo.GatewayCon, objBatchInfo.GatewayPro, objWorkItemMessageNew.Commands[i].CmdId, objWorkItemMessageNew.Commands[i].Value, strDeviceId, strMacId, deviceAddress);
                            var response = string.Empty;
                            if (cmd != null)
                            {
                                if (this.SendCommand(objBatchInfo.GatewayCon, objBatchInfo.GatewayPro, cmd))
                                {
                                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Request sent to device. @DeviceId: " + strDeviceId + " @MacId: " + strMacId + " @Cid: " + objWorkItemMessageNew.Cid + " @DeviceAddress: " + deviceAddress);
                                    Thread.Sleep(objWorkItemMessageNew.Commands[i].Delay);
                                    var recData = this.ReceiveData(objBatchInfo.GatewayCon, objBatchInfo.Cid, strDeviceId, strMacId, deviceAddress);
                                    if (recData != null)
                                    {
                                        var lstParsedData = this.ParseRecData(objBatchInfo.GatewayPro, recData, objWorkItemMessageNew.Commands[i].CmdId, strDeviceId, deviceAddress, strMacId);
                                        if (lstParsedData != null)
                                        {
                                            foreach (var pd in lstParsedData)
                                            {
                                                switch (pd.ParsedDataType)
                                                {
                                                    case ParsedValueType.Notification:
                                                        var notification = string.Format(Constants.Notification, pd.Data, objWorkItemMessageNew.Cid);
                                                        UbiqsensBooter.ObjPubMqtt.Publish("Server/NOTIFICATIONS/" + strDeviceId, ASCIIEncoding.ASCII.GetBytes(notification), MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                                                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Workitem Response received from device and sent to web server. @DeviceId: " + strDeviceId + " @MacId: " + strMacId + " @Cid: " + objWorkItemMessageNew.Cid + " @DeviceAddress: " + deviceAddress);
                                                        break;
                                                    case ParsedValueType.Response:
                                                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Workitem Response received from device and sent to web server. @DeviceId: " + strDeviceId + " @MacId: " + strMacId + " @Cid: " + objWorkItemMessageNew.Cid + " @DeviceAddress: " + deviceAddress);
                                                        response = pd.Data;
                                                        break;
                                                }
                                            }
                                        }
                                    }

                                    responseBody += string.Format(Constants.ResponseBody, objWorkItemMessageNew.Commands[i].CmdId, response);
                                }
                                else
                                {
                                    string error = objBatchInfo.GatewayCon.GetLastError();
                                    responseBody += string.Format(Constants.ResponseBody, objWorkItemMessageNew.Commands[i].CmdId, "Error in sending command  : " + error);
                                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "ManageWorkItemMethod: " + deviceAddress + ":" + strMacId + ":" + objWorkItemMessageNew.Commands[i].CmdId + "-Error in sending command  , @DeviceId: " + strDeviceId + " @Cid: " + objWorkItemMessageNew.Cid + " @Ex: " + error);
                                }
                            }
                            else
                            {
                                responseBody += string.Format(Constants.ResponseBody, objWorkItemMessageNew.Commands[i].CmdId, "Command not found");
                                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "ManageWorkItemMethod: " + deviceAddress + ":" + strMacId + ":" + objWorkItemMessageNew.Commands[i].CmdId + "-Error in getting command from protocol, @DeviceId: " + strDeviceId + " @Cid: " + objWorkItemMessageNew.Cid);
                            }
                        }

                        var totalMsg = String.Format(Constants.ResponseHead, objWorkItemMessageNew.Cid) + responseBody + Constants.ResponseFoot;
                        UbiqsensBooter.ObjPubMqtt.Publish(Constants.WITopic + objWorkItemMessageNew.WorkItemId + Constants.Slash + strDeviceId, ASCIIEncoding.ASCII.GetBytes(totalMsg), MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "ManageWorkItemMethod: *Processed Read/Write Workitem. " + Constants.WITopic + objWorkItemMessageNew.WorkItemId + " @Cid: " + objWorkItemMessageNew.Cid);
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Workitem request has been processed. @DeviceId: " + strDeviceId + " @MacId: " + strMacId + " @Cid: " + objWorkItemMessageNew.Cid + " @DeviceAddress: " + deviceAddress);
                        break;

                    case WorkItemType.Disconnect:
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Disconnect request received from web server. @Cid: " + objWorkItemMessageNew.Cid);
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "ManageWorkItemMethod: Received disconnect workitem, @MacId: " + strMacId + " @DeviceId: " + strDeviceId);
                        HandleDisconnect(objBatchInfo, deviceAddress, strDeviceId, strMacId, false, Constants.Disconnect_Request);
                        UbiqsensBooter.ObjPubMqtt.Publish(Constants.WITopic + objWorkItemMessageNew.WorkItemId + Constants.Slash + strDeviceId, ASCIIEncoding.ASCII.GetBytes(String.Format(Constants.Disconnected, "User Sent Disconnect Workitem", strIPAddress, objWorkItemMessageNew.Cid)), MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "ManageWorkItemMethod: *Processed Disconnect Workitem. " + Constants.WITopic + objWorkItemMessageNew.WorkItemId + " @DeviceId: " + strDeviceId + " @MacId: " + strMacId + " @Cid: " + objWorkItemMessageNew.Cid);
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Disconnect request has been processed. @DeviceId: " + strDeviceId + " @MacId: " + strMacId + " @Cid: " + objWorkItemMessageNew.Cid + " @DeviceAddress: " + deviceAddress);
                        break;
                    case WorkItemType.CommandError:
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Debug, "Invalid workitem received from web server. @DeviceId: " + strDeviceId + " @MacId: " + strMacId + " @Cid: " + objWorkItemMessageNew.Cid + " @DeviceAddress: " + deviceAddress);
                        UbiqsensBooter.ObjPubMqtt.Publish(Constants.WITopic + objWorkItemMessageNew.WorkItemId, ASCIIEncoding.ASCII.GetBytes(String.Format(Constants.InvalidWorkitem, objWorkItemMessageNew.Cid)), MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                        break;
                }
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "ManageWorkItemMethod: Manage work Item, ManageWorkItem() Method with @workItem: " + workItem + " @MacId: " + strMacId + " @DeviceId: " + strDeviceId + " @Cid: " + objWorkItemMessageNew.Cid + " @DeviceAddress: " + deviceAddress, ex);
            }
            return objWorkItemMessageNew;
        }

    }

    public class BatchInfo
    {
        public TcpClient ReceivedSocket { get; set; }
        public IGatewayConnection GatewayCon { get; set; }
        public IGatewayProtocol GatewayPro { get; set; }
        public String Cid { get; set; }

        public String Source { get; set; }
        public String MacId { get; set; }

        public bool IsThreadActive { get; set; }
        public AutoResetEvent RequestHandlerGate { get; set; }

    }

    public class NotifcationMessage
    {
        public String Cid { get; set; }
        public string Message { get; set; }
    }
}
