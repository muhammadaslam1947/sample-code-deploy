﻿////-----------------------------------------------------------------------
//// <copyright file="QueueManager.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

namespace LnT.UbiqSens
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using LnT.UbiqSens.Helpers;
    using LnT.UbiqSens.PluginInterfaces;

    /// <summary>
    /// Defining data structure 
    /// </summary>
    public class QueueManager
    {
        /// <summary>
        /// WorkItem Queue
        /// </summary>
        private static ConcurrentDictionary<string, Queue<string>> dictWorkItemQueue;

        /// <summary>
        /// WorkItem Queue
        /// </summary>
        private static ConcurrentDictionary<string, Queue<string>> dictStausWorkItemQueue;

        /// <summary>
        /// WorkItem Entry Time
        /// </summary>
        private static ConcurrentDictionary<string, DateTime> dictWorkItemEntryTime;

        /// <summary>
        /// Work item Entry time
        /// </summary>
        private static ConcurrentDictionary<string, List<DateTime>> lstWorkItemEntryTime;

        /// <summary>
        /// Stores connected device list
        /// </summary>
        private static ConcurrentDictionary<string, string> connectedActiveDevice;

        /// <summary>
        /// Stores connected device list
        /// </summary>
        private static ConcurrentDictionary<string, AutoResetEvent> gateMap = new ConcurrentDictionary<string, AutoResetEvent>();

        public static ConcurrentDictionary<string, AutoResetEvent> GateMap
        {
            get { return QueueManager.gateMap; }
            set { QueueManager.gateMap = value; }
        }

        /// <summary>
        /// Storing tasks in dictionary 
        /// </summary>
        private static Dictionary<string, Task> tasks;

        private static WorkItemParser workitemParser = new WorkItemParser();

        /// <summary>
        /// Initializes static members of the QueueManager class.
        /// </summary>
        static QueueManager()
        {
            dictWorkItemQueue = new ConcurrentDictionary<string, Queue<string>>();
            dictStausWorkItemQueue = new ConcurrentDictionary<string, Queue<string>>();
            dictWorkItemEntryTime = new ConcurrentDictionary<string, DateTime>();
            lstWorkItemEntryTime = new ConcurrentDictionary<string, List<DateTime>>();
            //dictThreadTrigger = new ConcurrentDictionary<string, ManualResetEvent>();
            connectedActiveDevice = new ConcurrentDictionary<string, string>();
            //itq = new ConcurrentQueue<Message>();
            tasks = new Dictionary<string, Task>();
        }

        /// <summary>
        /// Gets or sets value to dictionary WorkItem EntryTime
        /// </summary>
        public static ConcurrentDictionary<string, DateTime> DictWorkItemEntryTime
        {
            get { return QueueManager.dictWorkItemEntryTime; }
            set { QueueManager.dictWorkItemEntryTime = value; }
        }

        /// <summary>
        /// Gets or sets value to list Work Item Entry Time
        /// </summary>
        public static ConcurrentDictionary<string, List<DateTime>> LstWorkItemEntryTime
        {
            get { return QueueManager.lstWorkItemEntryTime; }
            set { QueueManager.lstWorkItemEntryTime = value; }
        }

        /// <summary>
        /// Gets or sets value to dictionary Work Item Queue
        /// </summary>
        public static ConcurrentDictionary<string, Queue<string>> DictWorkItemQueue
        {
            get { return QueueManager.dictWorkItemQueue; }
            set { QueueManager.dictWorkItemQueue = value; }
        }

        /// <summary>
        /// Gets or sets value to dictionary Work Item Queue
        /// </summary>
        public static ConcurrentDictionary<string, Queue<string>> DictStatusWorkItemQueue
        {
            get { return QueueManager.dictStausWorkItemQueue; }
            set { QueueManager.dictStausWorkItemQueue = value; }
        }

        /// <summary>
        /// Gets or sets value to connected Active Device
        /// </summary>
        public static ConcurrentDictionary<string, string> ConnectedActiveDevice
        {
            get { return QueueManager.connectedActiveDevice; }
            set { QueueManager.connectedActiveDevice = value; }
        }

        /// <summary>
        /// Gets or sets value to tasks
        /// </summary>
        internal static Dictionary<string, Task> Tasks
        {
            get { return QueueManager.tasks; }
            set { QueueManager.tasks = value; }
        }
        /// <summary>
        /// Get Work Item
        /// </summary>
        /// <param name="strDeviceId">Device Id</param>
        /// <returns>work item</returns>
        public static string GetWorkItem(string strDeviceId)
        {
            try
            {
                if (QueueManager.dictWorkItemQueue.ContainsKey(strDeviceId) && QueueManager.dictWorkItemQueue[strDeviceId].Count > 0)
                {
                    if (QueueManager.lstWorkItemEntryTime[strDeviceId].Count > 0)
                    {
                        QueueManager.lstWorkItemEntryTime[strDeviceId].RemoveAt(0);
                    }
                    return QueueManager.dictWorkItemQueue[strDeviceId].Dequeue();
                }
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "QueueManager: Error in getting the workitem. GetWorkItem() Method with @DeviceId: " + strDeviceId, ex);
            }

            return null;
        }

        /// <summary>
        /// Get Work Item
        /// </summary>
        /// <param name="strDeviceId">Device Id</param>
        /// <returns>work item</returns>
        public static string GetStatusWorkItem(string strDeviceId)
        {
            try
            {
                if (QueueManager.DictStatusWorkItemQueue.ContainsKey(strDeviceId) && QueueManager.DictStatusWorkItemQueue[strDeviceId].Count > 0)
                {
                    return QueueManager.DictStatusWorkItemQueue[strDeviceId].Dequeue();
                }
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "QueueManager: Error in getting the status workitem. GetStatusWorkItem() Method with @DeviceId: " + strDeviceId, ex);
            }

            return null;
        }
        /// <summary>
        /// Disposes the task with the specified task id
        /// </summary>
        /// <param name="taskId">task id</param>
        internal static void RemoveTask(string taskId)
        {
            if (tasks.ContainsKey(taskId))
            {
                var task = QueueManager.tasks[taskId];
                QueueManager.tasks.Remove(taskId);
                task.Dispose();
            }
        }

        //static Queue<string> temp = null;

        internal static void RemoveWorkItems(string strDeviceId)
        {
            int i = 0;
            if (QueueManager.dictWorkItemQueue.ContainsKey(strDeviceId))
            {
                QueueManager.dictWorkItemQueue[strDeviceId].Clear();
                //QueueManager.dictWorkItemQueue.TryRemove(strDeviceId, out temp);
            }
            if (QueueManager.DictStatusWorkItemQueue.ContainsKey(strDeviceId))
            {
                QueueManager.DictStatusWorkItemQueue[strDeviceId].Clear();
                //QueueManager.DictStatusWorkItemQueue.TryRemove(strDeviceId, out temp);
            }
            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "QueueManager: SENT IN Remove WI Count, @DeviceId: " + strDeviceId + " @i: " + i);
        }
    }
}