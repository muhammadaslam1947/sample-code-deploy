﻿////-----------------------------------------------------------------------
//// <copyright file="Ubiqsens.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

namespace LnT.UbiqSens
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using LnT.UbiqSens.Helpers;
    using LnT.UbiqSens.PluginInterfaces;
    using System.Collections.Concurrent;
    using System.Net;

    /// <summary>
    /// Device agnostic framework
    /// </summary>
    public class Ubiqsens
    {
        ///////// <summary>
        ///////// The main entry point for the application.
        ///////// </summary>
        ////////static void Main()
        ////////{
        ////////    ServiceBase[] servicesToRun;
        ////////    servicesToRun = new ServiceBase[] 
        ////////    { 
        ////////        new UbiqsensWindowsService()
        ////////    };
        ////////    ServiceBase.Run(servicesToRun);
        ////////}

        public static bool isRunning;
        public static bool isHeathCheckReq = true;
        public static int IdleTimeout = 10;
        public static int WIExpiryTime = 1;
        public static ConcurrentDictionary<string, int> HealthMap = new ConcurrentDictionary<string, int>();

        /// <summary>
        /// Main Entry point - for testing
        /// </summary>
        public static void Main1()
        {
            new Ubiqsens().StartService();
            Console.ReadLine();
        }

        /// <summary>
        /// start service
        /// </summary>
        /// <returns>returns true or false</returns>
        public bool StartService()
        {
            try
            {
                //DeviceCollection = new ArrayList();
                int.TryParse(ConfigurationManager.AppSettings["IdleTimeout"], out IdleTimeout);
                int.TryParse(ConfigurationManager.AppSettings["WorkitemExpiryTime"], out WIExpiryTime);

                var objUbiqsensBooter = new UbiqsensBooter();
                if (objUbiqsensBooter.Boot())
                {
                    isRunning = true;
                    this.Services();
                }
            }
            catch (Exception ex)
            { 
                //Console.WriteLine("Device policy Initiate failed. Exp:" + ex.Message,ex);
                //Note: Logs will not be printed if exception occurs during logger inititalization
                UbiqsensBooter.ObjLogger?.WriteLog(LogLevel.Info, "Ubiqsens: Device policy Initiate failed, StartService() Method @Ex: ", ex);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Starts the necessary service for Gateway
        /// </summary>
        /// <returns></returns>
        public bool Services()
        {
            var isStarted = false;
            try
            {
                new ReadDevicePolicy().Initiate();
                //Task.Factory.StartNew(DeleteOldWorkItem);

                //Task.Factory.StartNew(MqttSubscriberReconnect);
                Task.Factory.StartNew(DataDispatcher.PushStatusData);
                //Task.Factory.StartNew(DataDispatcher.PushWorkitemData);
                //Task.Factory.StartNew(DeleteOldWorkItem);
                //Task.Factory.StartNew(ProcessHealthCheck);

                //var pushDataTs = new ThreadStart(DataDispatcher.PushData);
                //var pushDataT = new Thread(pushDataTs);
                //pushDataT.Name = Constants.PushData;
                //pushDataT.Priority = ThreadPriority.Normal;
                //pushDataT.IsBackground = false;
                //pushDataT.Start();

                //var saveDataTs = new ThreadStart(DataDispatcher.SaveData);
                //var saveDataT = new Thread(saveDataTs);
                //saveDataT.Name = Constants.SaveData;
                //saveDataT.Priority = ThreadPriority.Normal;
                //saveDataT.IsBackground = false;
                //saveDataT.Start();
                isStarted = true;
            }
            catch (Exception ex)
            {
                UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Ubiqsens: Error in initialising services, Services() Method. @Ex: " + ex.Message, ex);
            }

            return isStarted;
        }

        private void ProcessHealthCheck()
        {
            string topic;
            int temp;
            var ipAddress = Dns.GetHostAddresses(Dns.GetHostName())[1];

            while (Ubiqsens.isRunning)
            {
                topic = "Server/HEALTHCHECK_COMM/" + ipAddress + "/" + Guid.NewGuid().ToString();

                //UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Sub MQTT obj status- " + UbiqsensBooter.ObjSubMqtt.IsConnected().ToString());
            //UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Pub MQTT obj status- " + UbiqsensBooter.ObjPubMqtt.IsConnected().ToString());


            Resend:
                // Send Health Check Packet
                UbiqsensBooter.ObjPubMqtt.Publish(topic, ASCIIEncoding.ASCII.GetBytes(string.Format(Constants.Notification, "COMM TO WEB")), MqttQos.QOS_LEVEL_AT_LEAST_ONCE, true);

                // Store that into Health Map
                HealthMap.TryAdd(topic, 0);

                Thread.Sleep(5000);

                if (HealthMap.ContainsKey(topic))
                {
                    HealthMap[topic] += 1;
                    if (HealthMap[topic] <= 2)
                    {
                        // RETRY 3 TIMES
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Ubiqsens: MQTT Healthcheck Failure Retrying " + HealthMap[topic].ToString());
                        goto Resend;
                    }
                    else
                    {
                        HealthMap.TryRemove(topic, out temp);
                        UbiqsensBooter.ObjSubMqtt.Disconnect();
                        UbiqsensBooter.ObjPubMqtt.Disconnect();
                        Thread.Sleep(1000);
                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Ubiqsens: MQTT Healthcheck Failure Reconnecting");
                    }
                }
                Thread.Sleep(1000 * 60);
            }
        }

        /// <summary>
        /// stop service
        /// </summary>
        /// <returns>returns true or false</returns>
        public bool StopService()
        {
            isRunning = false;
            isHeathCheckReq = false;
            UbiqsensBooter.ToStore = false;
            foreach (var thread in UbiqsensBooter.RunningThreads)
            {
                try
                {
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Ubiqsens: Stopped Running Threads");
                    thread.Abort();
                }
                catch (IOException ex)
                {
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Ubiqsens: Error while storing active devices in to session, StopService() Method. @Ex: ", ex);
                }
            }
            return true;
        }

        /// <summary>
        /// Deletes the work item if the work item 
        /// </summary>
        private static void DeleteOldWorkItem()
        {
            var num = 0;
            while (isRunning)
            {
                //// Checking for the old workitem every 1 minutes
                Thread.Sleep(1000);
                try
                {
                    foreach (var dt in QueueManager.LstWorkItemEntryTime.ToList())
                    {
                        var oldWi = new Queue<string>();

                        // Removing the Queue
                        for (var i = 0; i < QueueManager.LstWorkItemEntryTime[dt.Key].Count; i++)
                        {
                            if ((DateTime.Now - QueueManager.LstWorkItemEntryTime[dt.Key][i]).Minutes >= WIExpiryTime)
                            {
                                var wi = string.Empty;
                                if (QueueManager.DictWorkItemQueue[dt.Key].Count > 0)
                                {

                                    wi = QueueManager.DictWorkItemQueue[dt.Key].Dequeue();
                                    if (!string.IsNullOrEmpty(wi))
                                    {
                                        var workItemId = wi.Substring(0, wi.IndexOf(Constants.DollarSymbol));
                                        // UbiqsensBooter.ObjPubMqtt.Publish(Constants.WITopic + workItemId, ASCIIEncoding.ASCII.GetBytes(LnT.UbiqSens.Helpers.Constants.DeviceNotAvailable), MqttQos.QOS_LEVEL_AT_LEAST_ONCE, false);
                                        UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Ubiqsens: WI Processed as Expired: @workitemId: " + workItemId + " Not processed for last 1 minute");
                                    }
                                    if (num >= int.MaxValue)
                                        num = 0;
                                    num++;

                                    //Removing the DateTime associated with Queue
                                    if (QueueManager.LstWorkItemEntryTime[dt.Key].Count > 0)
                                    {
                                        QueueManager.LstWorkItemEntryTime[dt.Key].RemoveAt(0);
                                    }
                                }
                            }
                        }
                    }

                    var noOfQueue = QueueManager.DictWorkItemQueue.ToList().Sum(q => QueueManager.DictWorkItemQueue[q.Key].Count);
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Ubiqsens: No of WI deleted: " + num + " No of WI in Queue " + noOfQueue);
                }
                catch (Exception ex)
                {
                    // //Console.WriteLine("Error in deleting old WI " + ex.Message);
                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Ubiqsens: Error in deleting WI, DeleteOldWorkItem() Method. @Ex: " + ex.Message, ex);
                }
            }
            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Info, "Ubiqsens: Application Closed");
        }
    }
}
