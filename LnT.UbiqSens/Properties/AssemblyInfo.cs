﻿﻿////-----------------------------------------------------------------------
//// <copyright file="AssemblyInfo.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

using System.Reflection;
using System.Runtime.InteropServices;

//// General Information about an assembly is controlled through the following 
//// set of attributes. Change these attribute values to modify the information
//// associated with an assembly.
[assembly: AssemblyTitle("UBIQSensService")]
[assembly: AssemblyDescription("UBIQWeise")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("L&T Technology Services")]
[assembly: AssemblyProduct("UBIQSensService")]
[assembly: AssemblyCopyright("Copyright  © L&T Technology Services 2015")]
[assembly: AssemblyTrademark("Larsen & Toubro Limited")]
[assembly: AssemblyCulture("")]

//// Setting ComVisible to false makes the types in this assembly not visible 
//// to COM components.  If you need to access a type in this assembly from 
//// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

//// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("e4d90ed9-0e47-488c-acca-c1a3ea437181")]

//// Version information for an assembly consists of the following four values:
////
////      Major Version
////      Minor Version 
////      Build Number
////      Revision
////
//// You can specify all the values or you can default the Build and Revision Numbers 
//// by using the '*' as shown below:
//// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.8.2.0")]
[assembly: AssemblyFileVersion("1.8.2.0")]

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("UBIQSensUnitTest")]