﻿////-----------------------------------------------------------------------
//// <copyright file="UbiqsensBooter.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

////////**********************************************************************************************
//////// Class Name  : UbiqsensBooter
//////// Purpose     : Class used to initiate the Ubiqsens, this reads the PolicyFiles\Gateway.xml file and initiates the required components.
////////               
//////// Modification History: Initial build
////////  Ver # 1.0         Date:9 April 2014    Author/Modified By:kaliprasad M    Remarks:UBIQSens uses this class to initiates the required components.
////////----------------------------------------------------------------------------------------------
////////                   
////////***********************************************************************************************

namespace LnT.UbiqSens
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.ComponentModel;
    using System.Globalization;
    using System.IO;
    using System.Net;
    using System.Reflection;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml;
    using Helpers;
    using PluginInterfaces;
    using System.Runtime.Serialization.Formatters.Binary;

    /// <summary>
    /// Does the boot up process by reading Gateway policy file
    /// </summary>
    public class UbiqsensBooter
    {
        #region variables

        /// <summary>
        /// Server Id
        /// </summary>
        private static string serverId = string.Empty;

        /// <summary>
        /// To Store
        /// </summary>
        private static bool toStore = false;

        /// <summary>
        /// To Store
        /// </summary>
        public static string SecurityKey = "90e104180c7242fd";


        /// <summary>
        /// Storage Service
        /// </summary>
        private static string storageService = string.Empty;

        /// <summary>
        /// Device Policy Topic
        /// </summary>
        private static string devicePolicyTopic = string.Empty;

        /// <summary>
        /// Work Item Policy Topic
        /// </summary>
        private static string workItemPolicyTopic = string.Empty;

        /// <summary>
        /// Object Logger
        /// </summary>
        private static ILogger objLogger = null;

        /// <summary>
        /// Object Storage Service
        /// </summary>
        private static IStorageService objStorageService = null;

        /// <summary>
        /// x Path
        /// </summary>
        private static string xPath = string.Empty;

        /// <summary>
        /// Object MQTT
        /// </summary>
        private static MosquittoMqttSubClient objSubMqtt = null;

        /// <summary>
        /// Object MQTT
        /// </summary>
        private static MosquittoMqttPubClient objPubMqtt = null;

        /// <summary>
        /// Gateway Unique id
        /// </summary>
        internal static string gatewayUid = string.Empty;

        /// <summary>
        /// Gateway id
        /// </summary>
        private static string gatewayId = string.Empty;

        /// <summary>
        /// Application Id
        /// </summary>
        private static string applicationId = string.Empty;

        /// <summary>
        /// Gateway Name
        /// </summary>
        private static string gatewayName = string.Empty;

        /// <summary>
        /// Subscribe Topic
        /// </summary>
        private static Dictionary<string, MqttQos> subscribeTopic = null;

        /// <summary>
        /// Publish Topic
        /// </summary>
        private static string publishTopic = string.Empty;

        /// <summary>
        /// Gateway Policy Topic
        /// </summary>
        private static string gatewayPolicyTopic = string.Empty;

        /// <summary>
        /// Scheduler Policy Topic
        /// </summary>
        private static string schedulerPolicyTopic = string.Empty;

        /// <summary>
        /// Gateway Updating
        /// </summary>
        private static string gatewayUpdation = string.Empty;

        /// <summary>
        /// Object Encrypt
        /// </summary>
        private static IEncryptDecryptData objEncrypt = null;

        /// <summary>
        /// Object  Rest Client
        /// </summary>
        private static RestClient objRestClient = null;

        /// <summary>
        /// Work item Topic
        /// </summary>
        private static string workitemTopic = string.Empty;

        /// <summary>
        /// is 64 Bit Machine
        /// </summary>
        private static bool is64BitMachine = true;

        /// <summary>
        /// is packet check
        /// </summary>
        private static bool isDiagnosticCheck = false;

        /// <summary>
        /// log level enabled 
        /// </summary>
        private static string allowedLogLevel = "all";

        /// <summary>
        /// is packet check
        /// </summary>
        private static bool isSecure = false;

        /// <summary>
        /// Gateway Status
        /// </summary>
        private static string gatewayStatus = string.Empty;

        /// <summary>
        /// Running Threads
        /// </summary>
        private static List<Thread> runningThreads = new List<Thread>();

        /// <summary>
        /// Rest IP
        /// </summary>
        public static string RestIP = string.Empty;

        /// <summary>
        /// Rest IP
        /// </summary>
        public static int retryInterval = 1; // DEFAULT DEVICE POLICY WAIT TIME IS 1 MIN

        /// <summary>
        /// Rest Port
        /// </summary>
        public static string RestPort = string.Empty;

        /// <summary>
        /// Gets or sets serverId
        /// </summary>
        public static string ServerId
        {
            get { return UbiqsensBooter.serverId; }
            set { UbiqsensBooter.serverId = value; }
        }

        /// <summary>
        /// Gets or sets a value to toStore
        /// </summary>
        public static bool ToStore
        {
            get { return UbiqsensBooter.toStore; }
            set { UbiqsensBooter.toStore = value; }
        }

        /// <summary>
        /// Gets or sets storageService
        /// </summary>
        public static string StorageService
        {
            get { return UbiqsensBooter.storageService; }
            set { UbiqsensBooter.storageService = value; }
        }

        /// <summary>
        /// Gets or sets devicePolicyTopic
        /// </summary>
        public static string DevicePolicyTopic
        {
            get { return UbiqsensBooter.devicePolicyTopic; }
            set { UbiqsensBooter.devicePolicyTopic = value; }
        }

        /// <summary>
        /// Gets or sets workItemPolicyTopic
        /// </summary>
        public static string WorkItemPolicyTopic
        {
            get { return UbiqsensBooter.workItemPolicyTopic; }
            set { UbiqsensBooter.workItemPolicyTopic = value; }
        }

        /// <summary>
        /// Gets or sets object Logger
        /// </summary>
        public static ILogger ObjLogger
        {
            get { return UbiqsensBooter.objLogger; }
            set { UbiqsensBooter.objLogger = value; }
        }

        /// <summary>
        /// Gets or sets object StorageService
        /// </summary>
        public static IStorageService ObjStorageService
        {
            get { return UbiqsensBooter.objStorageService; }
            set { UbiqsensBooter.objStorageService = value; }
        }

        /// <summary>
        /// Gets or sets xPath
        /// </summary>
        public static string XPath
        {
            get { return UbiqsensBooter.xPath; }
            set { UbiqsensBooter.xPath = value; }
        }

        /// <summary>
        /// Gets or sets object MQTT
        /// </summary>
        public static MosquittoMqttSubClient ObjSubMqtt
        {
            get { return UbiqsensBooter.objSubMqtt; }
            set { UbiqsensBooter.objSubMqtt = value; }
        }

        /// <summary>
        /// Gets or sets object MQTT
        /// </summary>
        public static MosquittoMqttPubClient ObjPubMqtt
        {
            get { return UbiqsensBooter.objPubMqtt; }
            set { UbiqsensBooter.objPubMqtt = value; }
        }

        /// <summary>
        /// Gets or sets value to gatewayStatus
        /// </summary>
        internal static string GatewayStatus
        {
            get { return UbiqsensBooter.gatewayStatus; }
            set { UbiqsensBooter.gatewayStatus = value; }
        }

        /// <summary>
        /// Gets or sets value to schedulerPolicyTopic
        /// </summary>
        internal static string SchedulerPolicyTopic
        {
            get { return UbiqsensBooter.schedulerPolicyTopic; }
            set { UbiqsensBooter.schedulerPolicyTopic = value; }
        }


        /// <summary>
        /// Gets or sets gateway UID
        /// </summary>
        internal static string GatewayUid
        {
            get { return UbiqsensBooter.gatewayUid; }
            set { UbiqsensBooter.gatewayUid = value; }
        }

        /// <summary>
        /// Gets or sets gatewayId
        /// </summary>
        internal static string GatewayId
        {
            get { return UbiqsensBooter.gatewayId; }
            set { UbiqsensBooter.gatewayId = value; }
        }

        /// <summary>
        /// Gets or sets applicationId
        /// </summary>
        internal static string ApplicationId
        {
            get { return UbiqsensBooter.applicationId; }
            set { UbiqsensBooter.applicationId = value; }
        }

        /// <summary>
        /// Gets or sets gatewayName
        /// </summary>
        internal static string GatewayName
        {
            get { return UbiqsensBooter.gatewayName; }
            set { UbiqsensBooter.gatewayName = value; }
        }

        /// <summary>
        /// Gets or sets value to subscribeTopic
        /// </summary>
        internal static Dictionary<string, MqttQos> SubscribeTopic
        {
            get { return UbiqsensBooter.subscribeTopic; }
            set { UbiqsensBooter.subscribeTopic = value; }
        }

        /// <summary>
        /// Gets or sets value to publishTopic
        /// </summary>
        internal static string PublishTopic
        {
            get { return UbiqsensBooter.publishTopic; }
            set { UbiqsensBooter.publishTopic = value; }
        }

        /// <summary>
        /// Gets or sets value to gatewayPolicyTopic
        /// </summary>
        internal static string GatewayPolicyTopic
        {
            get { return UbiqsensBooter.gatewayPolicyTopic; }
            set { UbiqsensBooter.gatewayPolicyTopic = value; }
        }

        /// <summary>
        /// Gets or sets value to gateway status Update
        /// </summary>
        internal static string GatewayUpdation
        {
            get { return UbiqsensBooter.gatewayUpdation; }
            set { UbiqsensBooter.gatewayUpdation = value; }
        }

        /// <summary>
        /// Gets or sets value to object Encrypt
        /// </summary>
        internal static IEncryptDecryptData ObjEncrypt
        {
            get { return UbiqsensBooter.objEncrypt; }
            set { UbiqsensBooter.objEncrypt = value; }
        }

        /// <summary>
        /// Gets or sets value to object RestClient
        /// </summary>
        internal static RestClient ObjRestClient
        {
            get { return UbiqsensBooter.objRestClient; }
            set { UbiqsensBooter.objRestClient = value; }
        }

        /// <summary>
        /// Gets or sets value to work item Topic
        /// </summary>
        internal static string WorkitemTopic
        {
            get { return UbiqsensBooter.workitemTopic; }
            set { UbiqsensBooter.workitemTopic = value; }
        }

        /// <summary>
        /// Gets or sets value to is 64 Bit Machine
        /// </summary>
        internal static bool Is64BitMachine
        {
            get { return UbiqsensBooter.is64BitMachine; }
            set { UbiqsensBooter.is64BitMachine = value; }
        }

        /// <summary>
        /// Gets or sets value to is Packet Check 
        /// </summary>
        internal static bool IsDiagnosticCheck
        {
            get { return UbiqsensBooter.isDiagnosticCheck; }
            set { UbiqsensBooter.isDiagnosticCheck = value; }
        }

        /// <summary>
        /// Gets or sets value to is UR device policy wait time
        /// </summary>
        internal static int RetryInterval
        {
            get { return UbiqsensBooter.retryInterval; }
            set { UbiqsensBooter.retryInterval = value; }
        }


        /// <summary>
        /// Gets or sets value of the correlatio id status
        /// </summary>
        internal static string AllowedLogLevel
        {
            get { return UbiqsensBooter.allowedLogLevel; }
            set { UbiqsensBooter.allowedLogLevel = value; }
        }


        /// <summary>
        /// Gets or sets value to is Packet Check 
        /// </summary>
        internal static bool IsSecure
        {
            get { return UbiqsensBooter.isSecure; }
            set { UbiqsensBooter.isSecure = value; }
        }
        /// <summary>
        /// RetryCount
        /// </summary>
        public int RetryCount { get; private set; }

        /// <summary>
        /// Gets or sets value to is Runnign Threads
        /// </summary>
        internal static List<Thread> RunningThreads
        {
            get { return UbiqsensBooter.runningThreads; }
            set { UbiqsensBooter.runningThreads = value; }
        }

        /// <summary>
        /// Gets or sets value to is device policy content 
        /// </summary>
        internal static string DevicePolicy
        {
            get;
            set;
        }
        internal static bool HeartBeatStatus { get; private set; }
        internal static int HeartBeatInterval { get; private set; }
        internal static string HeartBeatValue { get; private set; }

        public static AutoResetEvent areDevicePolicy = new AutoResetEvent(false);
        #endregion

        /// <summary>
        /// Reads the PolicyFiles\ Gateway.xml and boots the gateway
        /// </summary>
        /// <returns>Returns bootSuccess</returns>
        public bool Boot()
        {

            #region Load Boot File




            #endregion


            var bootSuccess = false;
            is64BitMachine = Environment.Is64BitOperatingSystem;
            IsSecure = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSecure"]);
            RetryCount = Convert.ToInt32(ConfigurationManager.AppSettings["RetryCount"]);
            if (is64BitMachine)
            {
                xPath = Constants.Xpath64;
            }
            else
            {
                xPath = Constants.Xpath86;
            }

            //Console.WriteLine("Gateway bootup started.");

            var appPath = AppDomain.CurrentDomain.BaseDirectory;

            var path = AppDomain.CurrentDomain.BaseDirectory + Constants.GatewayPolicyFilePath;

            XmlDocument gatewayPolicy = null;
            if (UbiqsensBooter.IsSecure) //READ FROM CONFIG FILE
            {
                //Read Encrypted Deviec type XML
                String resultXml = AES128.GetInstance().Decrypt(File.ReadAllText(path), UbiqsensBooter.SecurityKey);
                gatewayPolicy = new ConfigurationReader().GetXmlFromContent(resultXml);
            }
            else
            {
                gatewayPolicy = new ConfigurationReader().GetXml(path);
            }


            var subDoc = new XmlDocument();

            var nodeList = gatewayPolicy.GetElementsByTagName(Constants.Identity);
            if (nodeList.Count == 0)
            {
                throw new Exception("Identity node not found");
            }

            subDoc.LoadXml(nodeList[0].OuterXml);
            gatewayUid = subDoc.GetElementsByTagName(Constants.GatewayUID)[0].InnerText;
            var ipAddress = Dns.GetHostAddresses(Dns.GetHostName())[1];
            AllowedLogLevel = ConfigurationManager.AppSettings["LogLevel"];
            serverId = Constants.Server;
            if (!this.LoggerInitializer(gatewayPolicy, AllowedLogLevel))
                throw new Exception("Logger not started");
            var bpcomponents = new Container();
            var iconPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + Constants.UbiqsensIcon;
            objSubMqtt = new MosquittoMqttSubClient();
            objPubMqtt = new MosquittoMqttPubClient();
            IsDiagnosticCheck = Convert.ToBoolean(ConfigurationManager.AppSettings["DiagnosticCheck"]);
            RetryInterval = Convert.ToInt32(ConfigurationManager.AppSettings["RetryInterval"]);
            HeartBeatStatus = Boolean.Parse(ConfigurationManager.AppSettings["HeartBeat"]);
            HeartBeatInterval = Convert.ToInt32(ConfigurationManager.AppSettings["HeartBeatDelay"]);
            HeartBeatValue = Convert.ToString(ConfigurationManager.AppSettings["HeartBeatValue"]);

            if (this.DataSecurityInitializer(gatewayPolicy) && this.MqttInitializer(gatewayPolicy)
                && this.RestClientInitilizer(gatewayPolicy) && this.StorageServiceInitializer(gatewayPolicy)
                )
            {
                ObjLogger.WriteLog(LogLevel.Info, ("Bootstrap process Successfully completed."));
                bootSuccess = true;
            }
            else
            {
                ObjLogger.WriteLog(LogLevel.Info, ("Bootstrap process failed."));
                bootSuccess = false;
            }

            return bootSuccess;
        }

        /// <summary>
        /// Reads the gateway policy and initializes MQTT client.
        /// </summary>
        /// <param name="gatewayPolicy">gatewayPolicy with MQTT broker detail</param>
        /// <returns>Returns isCreated</returns>
        public bool MqttInitializer(XmlDocument gatewayPolicy)
        {
            int retryCount = 0;
            ConnectAgain:
            retryCount++;
            var isCreated = false;
            try
            {
                //if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\ClientId.txt"))
                //{
                //    objMqtt.SubClientId = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "\\ClientId.txt");
                //}
                //else
                //{
                var clientId = Guid.NewGuid().ToString();
                //    File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + "\\ClientId.txt", clientId);
                //}

                ObjPubMqtt.WillRetain = true;
                ObjSubMqtt.WillRetain = true;
                ObjPubMqtt.Quality = (byte)MqttQos.QOS_LEVEL_EXACTLY_ONCE;
                ObjSubMqtt.Quality = (byte)MqttQos.QOS_LEVEL_EXACTLY_ONCE;
                ObjPubMqtt.WillFlag = true;
                ObjSubMqtt.WillFlag = true;
                ObjPubMqtt.WillTopic = "Test/#";// serverId + "/GATEWAY_STATUS/" + gatewayUid;
                ObjSubMqtt.WillTopic = "Test/#";// serverId + "/GATEWAY_STATUS/" + gatewayUid;
                ObjPubMqtt.WillMessage = "0";
                ObjSubMqtt.WillMessage = "0";
                ObjPubMqtt.CleanSession = false;
                ObjSubMqtt.CleanSession = false;
                ObjPubMqtt.KeepAlivePeriod = 500;
                ObjSubMqtt.KeepAlivePeriod = 500;

                var nodeList = gatewayPolicy.GetElementsByTagName("MQTT");
                var subDoc = new XmlDocument();
                subDoc.LoadXml(nodeList[0].OuterXml);
                nodeList = subDoc.GetElementsByTagName("BrokerIPAddress");
                //ObjPubMqtt.SubHostName = nodeList[0].InnerText;
                ObjSubMqtt.SubHostName = nodeList[0].InnerText;
                ObjPubMqtt.PubHostName = ConfigurationManager.AppSettings["MQTTPUBIP"];
                // ObjSubMqtt.PubHostName = ConfigurationManager.AppSettings["MQTTPUBIP"];
                nodeList = subDoc.GetElementsByTagName("BrokerPort");
                ObjPubMqtt.BrokerPort = Convert.ToInt32(nodeList[0].InnerText);
                ObjSubMqtt.BrokerPort = Convert.ToInt32(nodeList[0].InnerText);

                nodeList = subDoc.GetElementsByTagName("isSecure");
                ObjPubMqtt.Secure = Convert.ToBoolean(nodeList[0].InnerText, CultureInfo.InvariantCulture);
                ObjSubMqtt.Secure = Convert.ToBoolean(nodeList[0].InnerText, CultureInfo.InvariantCulture);

                if (ObjPubMqtt.Secure)
                {
                    nodeList = subDoc.GetElementsByTagName("X509CertificateFileName");
                    var mqttX509CertificateFileName = nodeList[0].InnerText;
                }

                nodeList = subDoc.GetElementsByTagName("UserName");
                ObjPubMqtt.Username = Convert.ToString(nodeList[0].InnerText);
                ObjSubMqtt.Username = Convert.ToString(nodeList[0].InnerText);
                nodeList = subDoc.GetElementsByTagName("password");
                ObjPubMqtt.Password = Convert.ToString(nodeList[0].InnerText);
                ObjSubMqtt.Password = Convert.ToString(nodeList[0].InnerText);
                gatewayPolicyTopic = gatewayUid + "/GATEWAY_POLICY/";
                devicePolicyTopic = gatewayUid + "/DEVICE_POLICY";
                schedulerPolicyTopic = gatewayUid + "/SchedulerPolicy";
                workitemTopic = gatewayUid + "/#";
                publishTopic = gatewayUid + "/{0}/{1}/Res";
                workItemPolicyTopic = "WORK_ITEM";
                ObjSubMqtt.Connect();
                ObjPubMqtt.Connect();
                gatewayStatus = serverId + "/GATEWAY_STATUS/" + gatewayUid;
                ObjPubMqtt.Publish(serverId + "/GATEWAY_STATUS/" + gatewayUid, ASCIIEncoding.ASCII.GetBytes("1"), MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);

                var ipAddress = Dns.GetHostAddresses(Dns.GetHostName())[1];

                subscribeTopic = new Dictionary<string, MqttQos>
                     {
                         { gatewayPolicyTopic, MqttQos.QOS_LEVEL_EXACTLY_ONCE },
                         { devicePolicyTopic, MqttQos.QOS_LEVEL_EXACTLY_ONCE },
                         { schedulerPolicyTopic, MqttQos.QOS_LEVEL_EXACTLY_ONCE }
                        ,{ workitemTopic, MqttQos.QOS_LEVEL_EXACTLY_ONCE }
                        ,{ "UBIQSens/#", MqttQos.QOS_LEVEL_EXACTLY_ONCE }
                        //,{ "Server/HEALTHCHECK_WEB/#", MqttQos.QOS_LEVEL_EXACTLY_ONCE }
                        //,{ "Server/HEALTHCHECK_COMM/"+ipAddress+"/#", MqttQos.QOS_LEVEL_EXACTLY_ONCE }
                     };
                ObjSubMqtt.Subscribe(subscribeTopic);

                //Console.WriteLine("MQTT connectivity checked.");
                objLogger.WriteLog(LogLevel.Info, "MQTT object created successfully. And checked the MQTT Broker");
                isCreated = true;
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Error in  MQTT connectivity");
                objLogger.WriteLog(LogLevel.Error, "Error in  MQTT connectivity,MqttInitializer() Method, Retry Attempt: " + retryCount + " @Ex: " + ex.Message, ex);
                if (retryCount < RetryCount)
                {
                    Thread.Sleep(new TimeSpan(0, RetryInterval, 0));
                    goto ConnectAgain;
                }
                objLogger.WriteLog(LogLevel.Error, "Failed to initialize MQTT client, MqttInitializer() Method. @Ex: " + ex.Message, ex);
            }

            return isCreated;
        }

        /// <summary>
        /// Reads the gateway policy and initializes Rest client.
        /// </summary>
        /// <param name="gatewayPolicy">gatewayPolicy with Rest Server detail</param>
        /// <returns>Returns isCreated</returns>
        public bool RestClientInitilizer(XmlDocument gatewayPolicy)
        {
            var isCreated = false;
            try
            {
                var nodeList = gatewayPolicy.GetElementsByTagName(Constants.TransportEndpointDetail);
                if (nodeList.Count == 0)
                {
                    throw new Exception("Transport Endpoint Detail not found");
                }

                var subDoc = new XmlDocument();
                subDoc.LoadXml(nodeList[0].OuterXml);
                var rest = subDoc.GetElementsByTagName(Constants.REST);

                subDoc.LoadXml(rest[0].OuterXml);

                nodeList = subDoc.GetElementsByTagName(Constants.HTTPServerIPAddress);
                var restIp = nodeList[0].InnerText;

                nodeList = subDoc.GetElementsByTagName(Constants.HTTPServerPort);
                var restPort = nodeList[0].InnerText;
                RestIP = restIp;
                RestPort = restPort;
                RestClient.WorkItemRequestUrl = RestClient.WorkItemRequestUrl.Replace(Constants.IP, restIp)
                    .Replace(Constants.Port, restPort);
                RestClient.WorkItemResponseUrl = RestClient.WorkItemResponseUrl.Replace(Constants.Port, restPort)
                    .Replace(Constants.IP, restIp);

                objRestClient = new RestClient();
                RestClient.UpdateDeviceStatusUrl = string.Format(Constants.RestGatewayStatusURL.Replace(Constants.IP, restIp).Replace(Constants.Port, restPort), gatewayUid);


                #region Getting device type look up
                //Console.WriteLine("Downloading Device type Look Up" + DateTime.Now.ToString());

                //var tempDeviceTypelookUptUrl = RestClient.WorkItemRequestUrl;
                //RestClient.WorkItemRequestUrl = string.Format(Constants.RestDeviceTypeLookUpURL, restIp, restPort) + gatewayUid;

                //if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + Constants.DeviceTypeLookUpPath))
                //{
                //    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + Constants.DeviceTypeLookUpPath);
                //}

                //var devicepolicypath = AppDomain.CurrentDomain.BaseDirectory + Constants.DeviceTypeLookUpPath;
                //if (File.Exists(devicepolicypath + Constants.DeviceTypeLookUp))
                //    File.Delete(devicepolicypath + Constants.DeviceTypeLookUp);


                //var deviceTypeLookUp = string.Empty;

                //try
                //{
                //    deviceTypeLookUp = ASCIIEncoding.ASCII.GetString(objRestClient.Get(string.Empty));

                //    deviceTypeLookUp = deviceTypeLookUp.Replace(Constants.SlashR, string.Empty).Replace(Constants.SlashN, string.Empty);
                //    Console.WriteLine("Successful Download of Device type look up");
                //}
                //catch (Exception ex)
                //{
                //    Console.WriteLine("Error in Downloading  Device type look up " + ex.Message);
                //    return isCreated;
                //}

                //this.DeviceTypeLookUpCreator(devicepolicypath, deviceTypeLookUp);


                //Console.WriteLine("Successful Download of Device type look up");
                #endregion

                return true; //TODO: only for dev remove while moving to prod/sit/uat

                #region Getting device policy
                //Console.WriteLine("Downloading Device Policy" + DateTime.Now.ToString());
                var tempWorkItemRequestUrl = RestClient.WorkItemRequestUrl;
                RestClient.WorkItemRequestUrl = string.Format(Constants.RestPolicyDeviceURL, restIp, restPort) + gatewayUid;

                if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + Constants.DevicePolicyPath))
                {
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + Constants.DevicePolicyPath);
                }

                var tempPath = AppDomain.CurrentDomain.BaseDirectory + Constants.DevicePolicyPath;
                var oldFiles = Directory.GetFiles(tempPath);

                try
                {
                    foreach (var oldFile in oldFiles)
                    {
                        File.Delete(oldFile);
                    }
                }
                catch
                {
                    //Console.WriteLine("Error in deleting old files. " + ex.Message,ex);
                }

                var devicePolicy = string.Empty;
                int retryCount = 0;

                try
                {
                    Retry:
                    ObjLogger.WriteLog(LogLevel.Info, "Downloading Device Policy");
                    string response = ASCIIEncoding.ASCII.GetString(objRestClient.Get(string.Empty));
                    areDevicePolicy.WaitOne(new TimeSpan(0, RetryInterval, 0));
                    if (!string.IsNullOrEmpty(DevicePolicy))
                        devicePolicy = DevicePolicy;// ASCIIEncoding.ASCII.GetString(objRestClient.Get(string.Empty));
                    else
                    {
                        ObjLogger.WriteLog(LogLevel.Info, "No devices Found - Retrying -Device Policy Download");
                        if (retryCount < RetryCount)
                        {
                            retryCount++;
                            goto Retry;
                        }
                    }
                    if (UbiqsensBooter.IsSecure) //READ FROM CONFIG FILES
                    {
                        //Read Encrypted Deviec type XML
                        devicePolicy = AES128.GetInstance().Decrypt(devicePolicy, UbiqsensBooter.SecurityKey);
                    }
                    devicePolicy = devicePolicy.Replace(Constants.SlashR, string.Empty).Replace(Constants.SlashN, string.Empty);
                    //Console.WriteLine("Successful Download of Device policy");
                }
                catch (Exception)
                {
                    //Console.WriteLine("Error in Downloading Device policy " + ex.Message);
                    return isCreated;
                }

                this.DevicePolicyCreator(restIp, restPort, tempWorkItemRequestUrl, tempPath, devicePolicy);
                isCreated = true;
                #endregion
            }
            catch (Exception ex)
            {
                objLogger.WriteLog(LogLevel.Error, "Error in the initialise the Rest connectivity. RestClientInitilizer() Method. @Ex: ", ex);
            }

            return isCreated;
        }

        /// <summary>
        /// Extracts individual device policy and writes into the individual xml file
        /// </summary>
        /// <param name="restIp">rest IP</param>
        /// <param name="restPort">rest Port</param>
        /// <param name="tempWorkItemRequestUrl">temp WorkItem Request Url</param>
        /// <param name="tempPath">temp Path</param>
        /// <param name="devicePolicy">device Policy</param>
        /// <returns>Returns created</returns>
        public bool DevicePolicyCreator(string restIp, string restPort, string tempWorkItemRequestUrl, string tempPath, string devicePolicy)
        {
            var created = false;
            try
            {
                var xmldata = new XmlDocument();
                if (devicePolicy != string.Empty)
                {
                    xmldata.LoadXml(devicePolicy);
                    var nodelist = xmldata.GetElementsByTagName(Constants.Device);

                    foreach (XmlNode xn in nodelist)
                    {
                        var name = xn[Constants.Deviceidd].InnerText;
                        File.WriteAllText(tempPath + name + Constants.Xml, xn.OuterXml.Replace(Constants.SlashN, string.Empty).Replace(Constants.SlashR, string.Empty));
                    }
                }

                //Console.WriteLine("Downloading Device Policy is completed");
                var ddt2 = DateTime.Now;
                RestClient.WorkItemRequestUrl = tempWorkItemRequestUrl;
                RestClient.UpdateDeviceStatusUrl = Constants.DeviceStatusUpdateURL.Replace(Constants.IP, restIp).Replace(Constants.Port, restPort);
                //Console.WriteLine("Rest connectivity checked");
                objLogger.WriteLog(LogLevel.Info, "Rest object created successfully. And checked the server");
                created = true;
            }
            catch (Exception ex)
            {
                objLogger.WriteLog(LogLevel.Error, "Exception Occurred at DevicePolicyCreator() Method with @restIp: " + restIp + " @restPort: " + restPort + " @tempWorkItemRequestUrl: " + tempWorkItemRequestUrl + " @tempPath: " + tempPath + " @devicePolicy: " + devicePolicy, ex);
                created = false;
            }

            return created;
        }

        /// <summary>
        /// Initializes storage plugin
        /// </summary>
        /// <param name="gatewayPolicy">gatewayPolicy with storage plugin detail</param>
        /// <returns>Returns isCreated</returns>
        public bool StorageServiceInitializer(XmlDocument gatewayPolicy)
        {
            var isCreated = false;
            var nodeList = gatewayPolicy.GetElementsByTagName(Constants.QueueStorage);
            var subDoc = new XmlDocument();
            subDoc.LoadXml(nodeList[0].OuterXml);
            toStore = Convert.ToBoolean(subDoc.GetElementsByTagName(Constants.ToStore)[0].InnerText, CultureInfo.InvariantCulture);
            storageService = subDoc.GetElementsByTagName(Constants.StorageService)[0].InnerText;
            //toStore = false;
            if (toStore)
            {
                try
                {
                    var objStorageServiceObjectFactory = new StorageServiceObjectFactory();
                    objStorageService = objStorageServiceObjectFactory.GetIStorageServiceObject(storageService);
                    objStorageService.Initialize(60, 5000);
                    //Console.WriteLine("Storage service object created successfully.");
                    objLogger.WriteLog(LogLevel.Info, "Storage service object created successfully.");
                    isCreated = true;
                }
                catch (Exception ex)
                {
                    objLogger.WriteLog(LogLevel.Error, "Error in the initialise the Storage service. StorageServiceInitializer() Method. @Ex: " + ex.Message, ex);
                }
            }
            else
            {
                isCreated = true;
            }

            return isCreated;
        }

        /// <summary>
        /// Data security initializer
        /// </summary>
        /// <param name="gatewayPolicy">gatewayPolicy with data security plugin detail</param>
        /// <returns>Returns isCreated</returns>
        public bool DataSecurityInitializer(XmlDocument gatewayPolicy)
        {
            var isCreated = false;
            var nodeList = gatewayPolicy.GetElementsByTagName(Constants.DataSecurity);
            var subDoc = new XmlDocument();
            subDoc.LoadXml(nodeList[0].OuterXml);
            var toEncrypt = Convert.ToBoolean(subDoc.GetElementsByTagName(Constants.ToEncrypt)[0].InnerText, CultureInfo.InvariantCulture);
            var encryptionAlgorithm = subDoc.GetElementsByTagName(Constants.EncryptionAlgorithm)[0].InnerText;
            var encryptionPassword = subDoc.GetElementsByTagName(Constants.EncryptionPassword)[0].InnerText;

            if (toEncrypt)
            {
                encryptionAlgorithm = gatewayPolicy.GetElementsByTagName(Constants.EncryptionAlgorithm)[0].InnerText;
                encryptionPassword = gatewayPolicy.GetElementsByTagName(Constants.EncryptionPassword)[0].InnerText;
                try
                {
                    //Console.WriteLine("Data security object created successfully.");
                    objLogger.WriteLog(LogLevel.Info, "Data security object created successfully.");
                    isCreated = true;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error in creating IEncryptDecryptData object. @Ex: " + ex.Message, ex);
                }
            }
            else
            {
                isCreated = true;
            }

            return isCreated;
        }

        /// <summary>
        /// DeviceTypeLookUpCreator
        /// </summary>
        /// <param name="devicepolicypath"></param>
        /// <param name="deviceTypeLookUp"></param>
        private void DeviceTypeLookUpCreator(string devicepolicypath, string deviceTypeLookUp)
        {
            try
            {
                //var xmldata = new XmlDocument();
                //if (deviceTypeLookUp != string.Empty)
                //{
                //    xmldata.LoadXml(deviceTypeLookUp);
                //    File.WriteAllText(devicepolicypath + Constants.DeviceTypeLookUp, xmldata.InnerXml.ToString());
                //}
                File.WriteAllText(devicepolicypath + Constants.DeviceTypeLookUp, deviceTypeLookUp);
            }
            catch (Exception ex)
            {
                objLogger.WriteLog(LogLevel.Error, "Error Saving device type look up, DeviceTypeLookUpCreator() Method with @devicepolicypath: " + devicepolicypath + " @deviceTypeLookUp: " + deviceTypeLookUp, ex);
            }
        }

        /// <summary>
        /// Initializes logger object
        /// </summary>
        /// <param name="gatewayPolicy">gatewayPolicy with logger plugin detail</param>
        /// <returns>Returns isCreated</returns>
        public bool LoggerInitializer(XmlDocument gatewayPolicy, string allowedLogLevel)
        {
            var isCreated = false;
            try
            {
                var nodeList = gatewayPolicy.GetElementsByTagName(Constants.LoggingMechanism);
                var subDoc = new XmlDocument();
                subDoc.LoadXml(nodeList[0].OuterXml);
                var logger = subDoc.GetElementsByTagName(Constants.Logger)[0].InnerText;
                var filename = subDoc.GetElementsByTagName(Constants.LogFilename)[0].InnerText;
                var logPath = subDoc.GetElementsByTagName(Constants.LogPath)[0].InnerText;
                var toAppend = Convert.ToBoolean(subDoc.GetElementsByTagName(Constants.LogToAppend)[0].InnerText, CultureInfo.InvariantCulture);
                var maxFileSizeInMb = Convert.ToInt32(subDoc.GetElementsByTagName(Constants.LogMaxFileSizeInMB)[0].InnerText, CultureInfo.InvariantCulture);
                var objLoggerObjectFactory = new LoggerObjectFactory();
                objLogger = objLoggerObjectFactory.GetILoggerObject(logger);
                objLogger.Initialise(logPath, filename, toAppend, maxFileSizeInMb, allowedLogLevel);
                objLogger.WriteLog(LogLevel.Info, "Logger object created successfully.");
                //Console.WriteLine("Logger object created successfully.");
                isCreated = true;
            }
            catch (Exception)
            {
                //Console.WriteLine(ex.Message);
                ////throw new Exception("Error in the initialise of the Logger. Exp:" + ex.Message);
            }

            return isCreated;
        }

    }
}
