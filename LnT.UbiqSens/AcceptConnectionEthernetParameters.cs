﻿////-----------------------------------------------------------------------
//// <copyright file="AcceptConnectionEthernetParameters.cs" company="L&T">
////     Copyright (c) L&T. All rights reserved.
//// </copyright>
////-----------------------------------------------------------------------

namespace LnT.UbiqSens
{
    using LnT.UbiqSens.PluginInterfaces;

    /// <summary>
    /// Accept Connection Ethernet Parameters
    /// </summary>
    public class AcceptConnectionEthernetParameters
    {
        /// <summary>
        /// Receive Timeout
        /// </summary>
        private string receiveTimeout = string.Empty;

        /// <summary>
        /// Send Timeout
        /// </summary>
        private string sendTimeout = string.Empty;

        /// <summary>
        /// Listener Port 
        /// </summary>
        private string listenerPort = string.Empty;
        
        /// <summary>
        /// Receive Buffer Size
        /// </summary>
        private string receiveBufferSize = string.Empty;

        /// <summary>
        /// Object Gateway Connection
        /// </summary>
        private IGatewayConnection objGatewayConnection = null;

        /// <summary>
        /// Object Gateway Protocol
        /// </summary>
        private IGatewayProtocol objGatewayProtocol = null;

        /// <summary>
        /// Initializes a new instance of AcceptConnectionEthernetParameters class
        /// </summary>
        public AcceptConnectionEthernetParameters()
        {
            this.receiveTimeout = string.Empty;
            this.sendTimeout = string.Empty;
            this.listenerPort = string.Empty;
            this.receiveBufferSize = string.Empty;
            this.objGatewayConnection = null;
            this.objGatewayProtocol = null;
        }

        /// <summary>
        /// Gets or sets object GatewayProtocol
        /// </summary>
        public IGatewayProtocol ObjGatewayProtocol
        {
            get { return this.objGatewayProtocol; }
            set { this.objGatewayProtocol = value; }
        }

        /// <summary>
        /// Gets or sets object GatewayConnection
        /// </summary>
        public IGatewayConnection ObjGatewayConnection
        {
            get { return this.objGatewayConnection; }
            set { this.objGatewayConnection = value; }
        }

        /// <summary>
        /// Gets or sets receiveBufferSize
        /// </summary>
        public string ReceiveBufferSize
        {
            get { return this.receiveBufferSize; }
            set { this.receiveBufferSize = value; }
        }

        /// <summary>
        /// Gets or sets listenerPort
        /// </summary>
        public string ListenerPort
        {
            get { return this.listenerPort; }
            set { this.listenerPort = value; }
        }

        /// <summary>
        /// Gets or sets receiveTimeout
        /// </summary>
        public string SendTimeout
        {
            get { return this.sendTimeout; }
            set { this.sendTimeout = value; }
        }

        /// <summary>
        /// Gets or sets receiveTimeout
        /// </summary>
        public string ReceiveTimeout
        {
            get { return this.receiveTimeout; }
            set { this.receiveTimeout = value; }
        }
    }
}
