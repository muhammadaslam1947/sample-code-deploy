﻿using System;
using System.ServiceProcess;
using System.Text;
using LnT.UbiqSens.Helpers;
using LnT.UbiqSens.PluginInterfaces;

namespace LnT.UbiqSens
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            if (Environment.UserInteractive) // dedug the service using visual studio
            {
                GatewayService gatewayService = new GatewayService();
                gatewayService.DebugService(args);
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                new GatewayService()
                };

                ServiceBase.Run(ServicesToRun);
            }
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in Application @Ex: " + ((Exception)e.ExceptionObject).Message + ((Exception)e.ExceptionObject).StackTrace);
            try
            {
                UbiqsensBooter.ObjPubMqtt.Publish("Server/GATEWAY_STATUS/" + UbiqsensBooter.GatewayUid, ASCIIEncoding.ASCII.GetBytes("0"), MqttQos.QOS_LEVEL_EXACTLY_ONCE, false);
                UbiqsensBooter.ObjPubMqtt.Disconnect();
            }
            catch(Exception ex) {
                UbiqsensBooter.ObjLogger?.WriteLog(LogLevel.Error, "Error Occurred at CurrentDomain_UnhandledException() method. @Ex: ", ex);
            }
        }
    }
}
