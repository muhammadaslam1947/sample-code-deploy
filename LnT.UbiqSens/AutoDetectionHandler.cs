﻿//////-----------------------------------------------------------------------
////// <copyright file="AutoDetectionHandler.cs" company="L&T">
//////     Copyright (c) L&T. All rights reserved.
////// </copyright>
//////-----------------------------------------------------------------------

//namespace LnT.UbiqSens
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Threading;
//    using LnT.UbiqSens.Helpers;
//    using LnT.UbiqSens.PluginInterfaces;

//    /// <summary>
//    /// class auto detection handler
//    /// </summary>
//    internal class AutoDetectionHandler
//    {
//        /// <summary>
//        /// Auto detection handler
//        /// </summary>
//        /// <param name="scanner">object scanner</param>
//        public void Handler(object scanner)
//        {
//            var deviceScanner = (IDeviceScanner)scanner;
//            var detectedDevices = new List<string>();
//            while (true)
//            {
//                try
//                {
//                    var scannedDevices = deviceScanner.GetDeviceName();
//                    var temps = string.Empty;
//                    var detected = false;
//                    foreach (var devices in scannedDevices)
//                    {
//                        if (!detectedDevices.Contains(devices.Key))
//                        {
//                            detected = true;
//                            detectedDevices.Add(devices.Key);
//                            var temp = Constants.AutoDetectBody;
//                            temp = temp.Replace(Constants.AtSymbol, devices.Key);
//                            temp = temp.Replace(Constants.HashSymbol, devices.Value);
//                            temps += temp + Constants.NewLine;
//                        }
//                    }

//                    if (detected)
//                    {
//                        var message = Constants.AutoDetectHead.Replace(Constants.DollarSymbol.ToString(), Constants.DateTimeFormat) + Constants.NewLine + temps + Constants.AutoDetectFooter;
//                        QueueManager.Itq.Enqueue(new Message() { Data = message, Topic = Constants.AutoDetectedTopic });
//                        Console.WriteLine(message);
//                    }
//                }
//                catch (Exception ex)
//                {
//                    UbiqsensBooter.ObjLogger.WriteLog(LogLevel.Error, "Error in getting data from Auto detected devices. Exp:" + ex.Message);
//                    Console.WriteLine("Error in getting from Auto detected devices. Exp:" + ex.Message);
//                }

//                Thread.Sleep(1000 * 15);
//            }
//        }
//    }
//}
