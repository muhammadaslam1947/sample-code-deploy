﻿//-----------------------------------------------------------------------
// <copyright file="LogLevel.cs" company="L&T">
//     Copyright (c) L&T. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LnT.UbiqSens.PluginInterfaces
{
    /// <summary>
    /// Log level
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// LogLevel debug
        /// </summary>
        Debug,

        /// <summary>
        /// LogLevel Info
        /// </summary>
        Info,

        /// <summary>
        /// LogLevel Warn
        /// </summary>
        Warn,

        /// <summary>
        /// LogLevel Error
        /// </summary>
        Error,

        /// <summary>
        /// LogLevel Fatal
        /// </summary>
        Fatal
    }
}
