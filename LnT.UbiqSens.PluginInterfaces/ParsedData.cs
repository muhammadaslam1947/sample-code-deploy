﻿//-----------------------------------------------------------------------
// <copyright file="ParsedData.cs" company="L&T">
//     Copyright (c) L&T. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LnT.UbiqSens.PluginInterfaces
{
    /// <summary>
    /// Parsed data
    /// </summary>
    public class ParsedData
    {
        /// <summary>
        /// Parsed Data Type
        /// </summary>
        private ParsedValueType parsedDataType;

        /// <summary>
        /// Parsed data
        /// </summary>
        private string data;

        /// <summary>
        /// Initializes a new instance of the ParsedData class.
        /// </summary>
        public ParsedData()
        {
            this.parsedDataType = ParsedValueType.Response;
            this.data = string.Empty;
        }

        /// <summary>
        /// Gets or sets data
        /// </summary>
        public string Data
        {
            get { return this.data; }
            set { this.data = value; }
        }

        /// <summary>
        /// Gets or sets parsedDataType
        /// </summary>
        public ParsedValueType ParsedDataType
        {
            get { return this.parsedDataType; }
            set { this.parsedDataType = value; }
        }
    }
}
