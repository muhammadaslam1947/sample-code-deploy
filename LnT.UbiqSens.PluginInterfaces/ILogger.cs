﻿//-----------------------------------------------------------------------
// <copyright file="ILogger.cs" company="L&T">
//     Copyright (c) L&T. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;

namespace LnT.UbiqSens.PluginInterfaces
{
    /// <summary>
    /// Interface logger
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Initializing the logger
        /// </summary>
        /// <param name="logPath">log path</param>
        /// <param name="filename">File name</param>
        /// <param name="append">To append</param>
        /// <param name="maxFileSizeInMb">File size in MB</param>
        /// <param name="allowedLogLevel">Log level allowed</param>
        /// <returns>true or false</returns>
        bool Initialise(string logPath, string filename, bool append, int maxFileSizeInMb, string allowedLogLevel);

        /// <summary>
        /// Write log
        /// </summary>
        /// <param name="logLevel">log level</param>
        /// <param name="log">String log message</param>
        void WriteLog(LogLevel logLevel, string log, Exception ex = null);
    }
}
