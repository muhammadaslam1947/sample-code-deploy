﻿//-----------------------------------------------------------------------
// <copyright file="IDeviceScanner.cs" company="L&T">
//     Copyright (c) L&T. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LnT.UbiqSens.PluginInterfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// Plugins returns detected devices with the Communication mode and devices name
    /// </summary>
    public interface IDeviceScanner
    {
        /// <summary>
        /// Get Device Name
        /// </summary>
        /// <returns>dictionary of string of strings</returns>
        Dictionary<string, string> GetDeviceName();
    }
}
