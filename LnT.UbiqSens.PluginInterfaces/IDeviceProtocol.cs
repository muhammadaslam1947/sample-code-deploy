﻿//-----------------------------------------------------------------------
// <copyright file="IDeviceProtocol.cs" company="L&T">
//     Copyright (c) L&T. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LnT.UbiqSens.PluginInterfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// Protocol to define parse command and raw data
    /// </summary>
    public interface IDeviceProtocol
    {
        /// <summary>
        /// Initialization required for the protocol(If any)
        /// </summary>
        /// <param name="objDeviceConnection">Object device connection</param>
        /// <returns>true or false</returns>
        bool Initialize(IDeviceConnection objDeviceConnection);

        /// <summary>
        /// Get command from protocol plugin
        /// </summary>
        /// <param name="cmdId">command id</param>
        /// <param name="value">string value,set value != null, else not null</param>
        /// <returns>byte array command</returns>
        byte[] GetCommand(string cmdId, string value);

        /// <summary>
        /// Parse the received data from device
        /// </summary>
        /// <param name="replyData">received data</param>
        /// <param name="cmdId">command id</param>
        /// <returns>parsed data</returns>
        List<ParsedData> Parse(List<byte[]> replyData, string cmdId);

        /// <summary>
        /// Release the device and used resources
        /// </summary>
        /// <returns>true or false</returns>
        bool Release();
    }
}
