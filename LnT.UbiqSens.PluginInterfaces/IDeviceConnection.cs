﻿//-----------------------------------------------------------------------
// <copyright file="IDeviceConnection.cs" company="L&T">
//     Copyright (c) L&T. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LnT.UbiqSens.PluginInterfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// Interfaces for connecting devices
    /// </summary>
    public interface IDeviceConnection
    {
        /// <summary>
        /// Gets or sets dictionary of device information
        /// </summary>
        Dictionary<string, string> DeviceInfo { get; set; }

        /// <summary>
        /// Initialization of device connection
        /// </summary>
        /// <param name="deviceInfo">device information</param>
        /// <returns>true or false</returns>
        bool Initialize(Dictionary<string, string> deviceInfo);

        /// <summary>
        /// Connecting to device
        /// </summary>
        /// <returns>true or false</returns>
        bool Connect();

        /// <summary>
        /// Sending command to the device
        /// </summary>
        /// <param name="cmd">byte array command</param>
        /// <returns>true or false</returns>
        bool Send(byte[] cmd);

        /// <summary>
        /// Receiving data from the device
        /// </summary>
        /// <returns>received data</returns>
        List<byte[]> Receive();

        /// <summary>
        /// disconnecting from device
        /// </summary>
        /// <returns>true or false</returns>
        bool Disconnect();
    }
}
