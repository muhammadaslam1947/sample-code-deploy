﻿//-----------------------------------------------------------------------
// <copyright file="IGatewayProtocol.cs" company="L&T">
//     Copyright (c) L&T. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LnT.UbiqSens.PluginInterfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// Protocol to define parse command and raw data
    /// </summary>
    public interface IGatewayProtocol
    {
        /// <summary>
        /// Initialization required for the protocol(If any)
        /// </summary>
        /// <param name="objGatewayConnection">Object Gateway connection</param>
        /// <returns>true or false</returns>
        bool Initialize(IGatewayConnection objGatewayConnection , ILogger objLogger);

        /// <summary>
        /// Get command from protocol plugin
        /// </summary>
        /// <param name="cmdId">command id</param>
        /// <param name="value">string value,set value != null, else not null</param>
        /// <returns>byte array command</returns>
        byte[] GetCommand(string cmdId, string value);

        /// <summary>
        /// Parse the received data from device
        /// </summary>
        /// <param name="replyData">received data</param>
        /// <param name="cmdId">command id</param>
        /// <returns>parsed data</returns>
        List<ParsedData> Parse(List<byte[]> replyData, string cmdId);

        /// <summary>
        /// Dictionary  to validate device
        /// </summary>
        /// <returns>Dictionary of string</returns>
        Dictionary<string, string> ValidateDevice(); //// key is expected to be parameter name, value is parameter value (eg parameter name is macId, value is 124578963121)

        /// <summary>
        /// Release the device and used resources
        /// </summary>
        /// <returns>true or false</returns>
        bool Release();  //// Release the device & used resources
    }
}
