﻿//-----------------------------------------------------------------------
// <copyright file="Message.cs" company="L&T">
//     Copyright (c) L&T. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LnT.UbiqSens.PluginInterfaces
{
    /// <summary>
    /// Class message
    /// </summary>
    public class Message
    {
        /// <summary>
        /// string Topic
        /// </summary>
        private string topic;

        /// <summary>
        /// string Data
        /// </summary>
        private string data;

        /// <summary>
        /// Initializes a new instance of the Message class
        /// </summary>
        public Message()
        {
            this.Topic = string.Empty;
            this.Data = string.Empty;
        }

        /// <summary>
        /// Gets or sets data
        /// </summary>
        public string Data
        {
            get { return this.data; }
            set { this.data = value; }
        }

        /// <summary>
        /// Gets or sets topic
        /// </summary>
        public string Topic
        {
            get { return this.topic; }
            set { this.topic = value; }
        }
    }
}
