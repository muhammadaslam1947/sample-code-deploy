﻿//-----------------------------------------------------------------------
// <copyright file="ParsedValueType.cs" company="L&T">
//     Copyright (c) L&T. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LnT.UbiqSens.PluginInterfaces
{
    /// <summary>
    /// Parsed value type
    /// </summary>
    public enum ParsedValueType
    {
        /// <summary>
        /// Parsed Value Type Notification
        /// </summary>
        Notification,

        /// <summary>
        /// Parsed Value Type Response
        /// </summary>
        Response
    }
}
