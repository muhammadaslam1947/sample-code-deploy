﻿//-----------------------------------------------------------------------
// <copyright file="IStorageService.cs" company="L&T">
//     Copyright (c) L&T. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LnT.UbiqSens.PluginInterfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface for storage service
    /// </summary>
    public interface IStorageService
    {
        /// <summary>
        /// Initializing storage service
        /// </summary>
        /// <param name="maxTopicLength">topic length</param>
        /// <param name="maxMessageLength">message length</param>
        /// <returns>true or false</returns>
        bool Initialize(int maxTopicLength, int maxMessageLength);

        /// <summary>
        /// Save data
        /// </summary>
        /// <param name="message">list of messages</param>
        /// <returns>true or false</returns>
        bool SaveData(List<Message> message);

        /// <summary>
        /// Retrieved data
        /// </summary>
        /// <param name="noOfMessage">number of messages</param>
        /// <returns>list of messages</returns>
        List<Message> RetreiveData(int noOfMessage);

        /// <summary>
        /// Delete messages
        /// </summary>
        /// <param name="count">count of messages to be deleted</param>
        /// <returns>true or false</returns>
        bool DeleteMessage(int count);
    }
}
