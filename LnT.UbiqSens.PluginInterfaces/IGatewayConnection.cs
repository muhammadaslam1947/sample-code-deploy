﻿//-----------------------------------------------------------------------
// <copyright file="IGatewayConnection.cs" company="L&T">
//     Copyright (c) L&T. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LnT.UbiqSens.PluginInterfaces
{
    using System.Collections.Generic;
    using System.Net.Sockets;

    /// <summary>
    /// Interface for Devices connecting to UBIQSENS
    /// </summary>
    public interface IGatewayConnection
    {
        /// <summary>
        ///  Initialization of gateway connection
        /// </summary>
        /// <param name="objSocket">socket object</param>
        /// <returns>true or false</returns>
        bool Initialize(TcpClient objSocket, ILogger objLogger);

        /// <summary>
        /// Initialization of a Async connection
        /// </summary>
        /// <returns>true or false</returns>
        bool InitializeAsync();

        /// <summary>
        /// Sending command to the device
        /// </summary>
        /// <param name="cmd">byte array command</param>
        /// <returns>true or false</returns>
        bool Send(byte[] cmd);

        /// <summary>
        /// Receiving data from the device
        /// </summary>
        /// <returns>received data</returns>
        List<byte[]> Receive();

        /// <summary>
        /// Receiving data from the device
        /// </summary>
        /// <returns>received data</returns>
        List<byte[]> SyncReceive();

        /// <summary>
        /// disconnecting from device
        /// </summary>
        /// <returns>true or false</returns>
        bool Disconnect();

        ////string ValidateDevice(Dictionary<string, byte[]> parameters);
        ////Dictionary<string, string> ValidateDevice();

        /// <summary>
        /// Is connected 
        /// </summary>
        /// <returns>true or false</returns>
        bool IsConnected();

        /// <summary>
        /// Any Error Happened
        /// </summary>
        /// <returns>Error Message</returns>
        string GetLastError();

         //<summary>
         //Device Id
         //</summary>
         //<returns>Error Message</returns>
        string DeviceId
        {
            get;
            set;
        }

        //<summary>
        //Device Address
        //</summary>
        //<returns>Error Message</returns>
        string DeviceAddress
        {
            get;
            set;
        }
    }

}
