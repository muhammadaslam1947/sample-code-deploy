﻿//-----------------------------------------------------------------------
// <copyright file="IEncryptDecryptData.cs" company="L&T">
//     Copyright (c) L&T. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LnT.UbiqSens.PluginInterfaces
{
    /// <summary> 
    /// To declare methods for Data encryption and decryption to achieve confidentiality 
    /// Encryption is the conversion of data into a form, called a cipher text that cannot be easily understood by unauthorized people.
    /// Decryption is the process of converting encrypted data back into its original form, so it can be understood. 
    /// </summary> 
    public interface IEncryptDecryptData
    {
        /// <summary>
        /// Encrypt Bytes
        /// </summary>
        /// <param name="plainBytes">plain bytes</param>
        /// <param name="password">string password</param>
        /// <returns>encrypted bytes</returns>
        byte[] EncryptBytes(byte[] plainBytes, string password);

        /// <summary>
        /// Decrypt Bytes
        /// </summary>
        /// <param name="encryptedBytes">encrypted bytes</param>
        /// <param name="password">string password</param>
        /// <returns>decrypted bytes</returns>
        byte[] DecryptBytes(byte[] encryptedBytes, string password);
    }
}
