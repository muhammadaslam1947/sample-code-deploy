﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using LnT.UbiqSens.PluginInterfaces;

namespace EthernetSSLPlugin
{
    public class EthernetSSLGatewayHandler : LnT.UbiqSens.PluginInterfaces.IGatewayConnection
    {
        /// <summary>
        /// socket object gateway handler
        /// </summary>
        private SslStream _socketGatewayHandler;

        private bool _isError = false;

        private readonly ConcurrentQueue<string> _errorMessage = new ConcurrentQueue<string>();

        private string _result = String.Empty;

        string deviceAddress = string.Empty;

        private X509Certificate _serverCertificate = null;

        private TcpClient _client;

        private ILogger _objLog;

        /// <summary>
        /// Array of received bytes
        /// </summary>
        private List<byte[]> _recBytes;

        public EthernetSSLGatewayHandler()
        {
            _client = new TcpClient();
        }

        /// <summary>
        /// Initializes the connection
        /// </summary>
        /// <param name="objSocket">socket object</param>
        /// <returns>true or false</returns>
        public bool Initialize(TcpClient objSocket, ILogger objLog)
        {
            if (objSocket == null)
            {

                throw new NullReferenceException();
            }

            try
            {
                _objLog = objLog;
                _client = objSocket;
                this._recBytes = new List<byte[]>();
                this._socketGatewayHandler = new SslStream(
                _client.GetStream(), false);

                _serverCertificate = X509Certificate.CreateFromCertFile(AppDomain.CurrentDomain.BaseDirectory + "\\Aprilaire_SSL.crt");

                this._socketGatewayHandler.ReadTimeout = 10000;
                this._socketGatewayHandler.WriteTimeout = 10000;

                this._socketGatewayHandler.AuthenticateAsServer(_serverCertificate,
                  false, SslProtocols.Tls, true);



                /**********************************AsynReceive**************************/
                var state = new StateObject { BufferSize = 10000 };

                this._socketGatewayHandler.BeginRead(state.Buffer, 0, state.BufferSize, this.ReadCallback, state);
                //this._client.Client.BeginReceive(state.Buffer, 0, state.BufferSize, 0, this.ReadCallback, state);
                /**********************************AsynReceive**************************/
            }
            catch (Exception ex)
            {
                _objLog.WriteLog(LogLevel.Info, "EthernetSSLGateWay:Error in SSL Plugin , Initialize() Method. @Ex: " + ex.Message, ex);
                throw;
            }
            return true;
        }

        /// <summary>
        /// Sends the command to the device
        /// </summary>
        /// <param name="cmd">byte array</param>
        /// <returns>true or false</returns>
        public bool Send(byte[] cmd)
        {
            if (this._socketGatewayHandler != null)
            {
                try
                {
                    this._socketGatewayHandler.Write(cmd);
                    _objLog.WriteLog(LogLevel.Info, "EthernetSSLGateWay:SENT TO DEVICE: " + BitConverter.ToString(cmd));
                }
                catch (Exception ex)
                {
                    _objLog.WriteLog(LogLevel.Error, "EthernetSSLGateWay:Exception Occurred at Send() @Cmd: " + BitConverter.ToString(cmd));
                    _errorMessage.Enqueue(ex.Message);
                    _isError = true;
                }
            }
            else
            {
                _errorMessage.Enqueue("Socket Object became null");
                _isError = true;
                throw new NullReferenceException("Socket Object became null");
            }

            return true;
        }

        /// <summary>
        /// Receive the data from the device
        /// </summary>
        /// <returns>byte array</returns>
        public List<byte[]> Receive()
        {
            /**********************************AsynReceive**************************/
            var buf = this._recBytes.ToList();
            this._recBytes.Clear();
            return buf;
            /**********************************AsynReceive**************************/
        }

        /// <summary>
        /// disconnect the connection from the device
        /// </summary>
        /// <returns>true or false</returns>
        public bool Disconnect()
        {
            if (this._client == null)
            {
                throw new NullReferenceException("Socket Object became null");
            }

            if (this._client.Connected)
            {
                this._client.Client.Disconnect(true);
                this._client.Client.Dispose();
                this._client = null;
            }

            return true;
        }

        /// <summary>
        /// Is connected
        /// </summary>
        /// <returns>returns true or false</returns>
        public bool IsConnected()
        {
            if (_client == null)
                throw new Exception("Socket  object is NULL");

            return _client.Connected && !_isError;
        }

        /// <summary>
        /// Asynchronous read call back
        /// </summary>
        /// <param name="ar">Asynchronous result</param>
        private void ReadCallback(IAsyncResult ar)
        {
            try
            {
                var state = (StateObject)ar.AsyncState;
                if (this._client != null)
                {
                    if (this._client.Connected)
                    {
                        int bytesRead = this._socketGatewayHandler.EndRead(ar);
                        if (bytesRead > 0)
                        {
                            byte[] rec = state.Buffer;
                            Array.Resize(ref rec, bytesRead);
                            this._recBytes.Add(rec);
                            _objLog.WriteLog(LogLevel.Info, "EthernetSSLGateWay:RECEIVED FROM DEVICE: " + BitConverter.ToString(rec));
                        }

                        var state1 = new StateObject { BufferSize = 10000 };
                        this._socketGatewayHandler.BeginRead(state1.Buffer, 0, state1.BufferSize, this.ReadCallback, state1);
                        //this._client.Client.BeginReceive(state1.Buffer, 0, state1.BufferSize, 0, this.ReadCallback, state1);
                    }
                }
            }
            catch (Exception ex)
            {
                _objLog.WriteLog(LogLevel.Info, "EthernetSSLGateWay:Error in Read Callback,ReadCallback() Method. @Ex: " + ex.Message, ex);
                _errorMessage.Enqueue(ex.Message);
                _isError = true;
            }
        }

        public string GetLastError()
        {
            if (_errorMessage.TryDequeue(out _result))
                return _result;
            return string.Empty;
        }


        public bool InitializeAsync()
        {
            throw new NotImplementedException();
        }

        public List<byte[]> SyncReceive()
        {
            throw new NotImplementedException();
        }

        public string DeviceId
        {
            get;
            set;
        }

        public string DeviceAddress
        {
            get
            {
                return deviceAddress;
            }

            set
            {
                deviceAddress = value;
            }
        }

        public string Cid { get; set; }
    }
}
