﻿//-----------------------------------------------------------------------
// <copyright file="StateObject.cs" company="L&T">
//     Copyright (c) L&T. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace EthernetSSLPlugin
{
    /// <summary>
    /// State object
    /// </summary>
    internal class StateObject
    {
        /// <summary>
        /// buffer size
        /// </summary>
        private int bufferSize = 0;

        /// <summary>
        /// Byte array buffer
        /// </summary>
        private byte[] buffer = null;

        /// <summary>
        /// Gets or sets Buffer size
        /// </summary>
        public int BufferSize
        {
            get
            {
                return this.bufferSize;
            }

            set
            {
                this.bufferSize = value;
                this.buffer = new byte[this.bufferSize];
            }
        }

        /// <summary>
        /// Gets array Buffer 
        /// </summary>
        public byte[] Buffer
        {
            get { return this.buffer; }
        }
    }
}
