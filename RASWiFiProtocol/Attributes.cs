﻿// ***********************************************************************
// Assembly         : Aprilaire.HostController.BusinessDTO
// Author           : 20021497
// Created          : 08-01-2013
//
// Last Modified By : 20021497
// Last Modified On : 08-09-2013
// ***********************************************************************
// <copyright file="Attributes.cs" company="Aprilaire">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace RASWiFiProtocol.RASProtocol
{
    /// <summary>
    /// Enumeration Attributes
    /// </summary>
    public enum Attributes
    {
        /// <summary>
        /// The HVAC variables
        /// </summary>
        [Description("HVAC Variables", 1)]
        HVACVariables = 1,

        /// <summary>
        /// The scale
        /// </summary>
        [Description("Scale", 2)]
        Scale = 2,

        /// <summary>
        /// The air cleaning installer variables
        /// </summary>
        [Description("Air Cleaning Installer Variables", 3)]
        AirCleaningInstallerVariables = 3,

        /// <summary>
        /// The humidity control installer variables
        /// </summary>
        [Description("Humidity Control Installer Variables", 4)]
        HumidityControlInstallerVariables = 4,

        /// <summary>
        /// The fresh air installer variables
        /// </summary>
        [Description("Fresh Air Installer Variables", 5)]
        FreshAirInstallerVariables = 5,

        /// <summary>
        /// The reset
        /// </summary>
        [Description("Reset", 6)]
        Reset = 6,

        /// <summary>
        /// The factory fresh
        /// </summary>
        [Description("Factory Fresh", 7)]
        FactoryFresh = 7
    }
}
