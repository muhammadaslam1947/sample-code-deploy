﻿
// ***********************************************************************
// Assembly         : Aprilaire.HostController.Business
// Author           : 20016420
// Created          : 08-16-2013
//
// Last Modified By : 20016420
// Last Modified On : 08-30-2013
// ***********************************************************************
// <copyright file="DataUpdateBusinessLogic.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using LnT.UbiqSens.PluginInterfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
//using Aprilaire.HostController.BusinessDTO;
//using Aprilaire.HostController.Common.Globals;
//using Aprilaire.HostController.Common.Services;
//using Aprilaire.HostController.Common.Utility;

namespace RASWiFiProtocol.RASProtocol
{
    /// <summary>
    /// Class DataUpdateBusinessLogic
    /// </summary>
    public class DataUpdateBusinessLogic
    {
        #region Declerations

        /// <summary>
        /// The obj data update business logic
        /// </summary>
        //private DataUpdateBusinessLogic objDataUpdateBusinessLogic;

        /// <summary>
        /// Dictionary to hold the read only attributes
        /// </summary>
        private Dictionary<string, bool> AttributeReadOnlyDictionary;

        /// <summary>
        /// Dictionary to hold the read only attributes
        /// </summary>
        private Dictionary<string, int> AttributeReadOnlyDataByte;

        /// <summary>
        /// Integer Variable to hold the Id of previously edited Command Control ID
        /// </summary>
        public int intPrevioslyEditedCommandControlID { get; set; }

        /// <summary>
        /// Integer Variable to hold the current Command Control ID being edited
        /// </summary>
        public int intCurrentCommandControl { get; set; }

        /// <summary>
        /// String value to hold the previously node selected in the particular usercontrol
        /// </summary>
        public string strPreviousNodeName { get; set; }

        /// <summary>
        /// boolean flag set to suspend the ongoing thread for the ack to be sent.
        /// </summary>
        public bool blnAckSendingFlag;

        /// <summary>
        /// Gets or sets a value indicating whether simulation mode.
        /// </summary>
        public static bool IsSimulationMode { get; set; }

        /// <summary>
        /// Byte array to hold the wake up command
        /// </summary>
        public byte[] byteWakeUpCommand;

        /// <summary>
        /// boolean value to check if it is read only
        /// </summary>
        public bool isReadOnly = false;

        /// <summary>
        /// String value to hold the Micro Port Name in Bridge Mode(Used to open the port on click of Start Monitoring and Stop Monitoring)
        /// </summary>
        public string strMicroPortName { get; set; }

        /// <summary>
        /// String value to hold the Radio Port Name in Bridge Mode(Used to open the port on click of Start Monitoring and Stop Monitoring)
        /// </summary>
        public string strRadioPortName { get; set; }

        /// <summary>
        /// tring value to hold the bool value of Import
        /// </summary>
        public bool bIsImport { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string strAttribute { get; set; }

        //Logger Object
        public ILogger ObjLog = null;


        #endregion

        #region Constructor

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <returns>DataUpdateBusinessLogic.</returns>
        //public static DataUpdateBusinessLogic GetInstance()
        //{
        //    if (objDataUpdateBusinessLogic == null)
        //    {
        //        objDataUpdateBusinessLogic = new DataUpdateBusinessLogic();
        //        return objDataUpdateBusinessLogic;
        //    }
        //    else
        //    {
        //        return objDataUpdateBusinessLogic;
        //    }
        //}

        /// <summary>
        /// Prevents a default instance of the <see cref="DataUpdateBusinessLogic" /> class from being created.
        /// </summary>
        public DataUpdateBusinessLogic(ILogger objLogger)
        {
            try
            {
                this.ObjLog = objLogger;
                AttributeReadOnlyDictionary = new Dictionary<string, bool>();
                AttributeReadOnlyDataByte = new Dictionary<string, int>();
                BuildAttributeReadOnlyDictionary();
                BuildAttributeReadOnlyDataByte();
                BuildWakeUpCommand();
                blnAckSendingFlag = false;
            }
            catch (Exception ex)
            {
                this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Exception Occurred at DataUpdateBusinessLogic Constructor @Ex: ", ex);
                //Log(ex);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        private void BuildAttributeReadOnlyDataByte()
        {
            // AttributeReadOnlyDataByte.Add("Functional Domain Value", Number Of Bytes to read)
            AttributeReadOnlyDataByte.Add("PermanentMessages", 1);//Messaging
                                                                  // AttributeReadOnlyDataByte.Add("ButtonBacklight Calibration", 2);//BackLight
                                                                  //AttributeReadOnlyDataByte.Add("ButtonBacklightColor", 1);//BackLight
            AttributeReadOnlyDataByte.Add("ScheduleDay", 1);//Scheduling
            AttributeReadOnlyDataByte.Add("SupportModules", 1);//Scheduling


        }


        /// <summary>
        /// Method called to construct the wake up command
        /// </summary>
        private void BuildWakeUpCommand()
        {
            byteWakeUpCommand = new byte[46];
            byte byteValueWakeUpcommand = 0xc0;
            for (int i = 0; i <= 45; i++)
            {
                byteWakeUpCommand[i] = byteValueWakeUpcommand;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Checks the attribute read only.
        /// </summary>
        /// <param name="Key">The key.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
        //public bool CheckAttributeReadOnly(string Key)
        //{
        //    bool bReturnValue = !AttributeReadOnlyDictionary.ContainsKey(Key);
        //    isReadOnly = !bReturnValue;
        //    return bReturnValue;
        //}

        ///// <summary>
        ///// Returns the command DTO.
        ///// </summary>
        ///// <param name="CommandID">The command ID.</param>
        ///// <returns>CommandDTO.</returns>
        //public CommandDTO ReturnCommandDTO(int CommandID)
        //{
        //    return HostControllerDTO.GetInstance().Commands.FirstOrDefault(t => t.CommandID == CommandID);
        //}

        /// <summary>
        /// Sets the data byte.
        /// </summary>
        /// <param name="CommandID">The command ID.</param>
        /// <param name="data">The data.</param>
        //public void SetDataByte(int CommandID, byte[] data)
        //{
        //    foreach (CommandDTO command in HostControllerDTO.GetInstance().Commands)
        //    {
        //        if (command.CommandID == CommandID)
        //        {
        //            command.Payload.Data = data;

        //            break;
        //        }
        //    }


        //}

        /// <summary>
        /// Forms the raw hex value.
        /// </summary>
        /// <param name="objCommandDTO">The obj command DTO.</param>
        /// <returns>System.Byte[][].</returns>
        public byte[] FormRawHexValue(CommandDTO objCommandDTO)
        {
            //if (objCommandDTO.Payload.Action == 1)
            // if (objCommandDTO.Payload.Action == 1 &&  objCommandDTO.PacketRawValue!=null && objCommandDTO.PacketRawValue[3]!=1)
            if (objCommandDTO.Payload.Action == 1 && objCommandDTO.PacketRawValue != null && objCommandDTO.PacketRawValue[4] != 1) //write and the attribute is a read command
            {
                if (isReadOnly)
                    objCommandDTO.Payload.Data = new Byte[] { 0, 1, 2, 3, 4, 5 }; //hardcoded dummy value.
            }
            else if (objCommandDTO.Payload.Action == 2)
            {
                if (AttributeReadOnlyDataByte.ContainsKey(strAttribute))
                {
                    if (objCommandDTO.Payload.Data != null)
                    {
                        int intValue = AttributeReadOnlyDataByte[strAttribute];
                        byte[] arrtemp = new byte[intValue];
                        Array.Copy(objCommandDTO.Payload.Data, arrtemp, intValue);
                        objCommandDTO.Payload.Data = new byte[arrtemp.Length];
                        Array.Copy(arrtemp, objCommandDTO.Payload.Data, arrtemp.Length);
                    }
                }
                else
                {
                    objCommandDTO.Payload.Data = null;
                }
            }

            //Intial length of payload will be 3 as it is ACTION,FUNCTIONAL DOMAIN & ATTRIBUTE
            int nPayloadPacketLength = 3;
            if (null != objCommandDTO.Payload.Data)
                nPayloadPacketLength = 3 + objCommandDTO.Payload.Data.Length;// andding the data length

            byte Revision = objCommandDTO.Revision;//  byte Revision = 1; revision
                                                   // byte sequence = 0;

            //Incrementing the sequence number

            //By VDN after confirmed with Sujay
            #region Logic is worng
            //if (objCommandDTO.Sequence == 128)
            //{
            //    if (Global.SequenceNumber != 255)
            //    {
            //        sequence = ++Global.SequenceNumber;
            //    }
            //    else
            //    {
            //        Global.SequenceNumber = 127;
            //        sequence = ++Global.SequenceNumber;
            //    }
            //}
            //else
            //{
            //    sequence = objCommandDTO.Sequence;
            //} 
            #endregion

            // //sequence = Global.SequenceNumber;

            //// objCommandDTO.Sequence = sequence; // sequence 
            // byte count = Convert.ToByte(nPayloadPacketLength);//Count of the total packet

            // int nTotalPacketLength = 3 + nPayloadPacketLength + 1 + 1;           
            // byte[] packetRawHexValue = new byte[nTotalPacketLength]; //forming the entire packet length
            // packetRawHexValue[0] = Revision; //Revision
            // packetRawHexValue[1] = objCommandDTO.Sequence; //Sequence
            ////// packetRawHexValue[1] = sequence; //Sequence    earlier in hostcontrol its like that
            // packetRawHexValue[2] = count; //Count
            // packetRawHexValue[3] = objCommandDTO.Payload.Action; //Action
            // packetRawHexValue[4] = objCommandDTO.Payload.FunctionalDomain; //Functional Domain
            // packetRawHexValue[5] = objCommandDTO.Payload.Attribute; //Attribute

            //as per protocol 14
            byte count = Convert.ToByte(nPayloadPacketLength);//Count of the total packet
            Int16 intTotalLength = Convert.ToInt16(count);
            int nTotalPacketLength = 4 + nPayloadPacketLength + 1 + 1;
            byte[] packetRawHexValue = new byte[nTotalPacketLength]; //forming the entire packet length
            packetRawHexValue[0] = objCommandDTO.Revision; // Revision    packetRawHexValue[0] = Revision;
            packetRawHexValue[1] = objCommandDTO.Sequence; //Sequence
            //// packetRawHexValue[1] = sequence; //Sequence    earlier in hostcontrol its like that
            Array.Copy(BitConverter.GetBytes(intTotalLength).Reverse().ToArray(), 0, packetRawHexValue, 2, BitConverter.GetBytes(intTotalLength).Length);
            packetRawHexValue[4] = objCommandDTO.Payload.Action; //Action
            packetRawHexValue[5] = objCommandDTO.Payload.FunctionalDomain; //Functional Domain
            packetRawHexValue[6] = objCommandDTO.Payload.Attribute; //Attribute

            //ADDING the payload
            if (null != objCommandDTO.Payload.Data)
            {
                int j = 7;  //int j = 6;
                foreach (byte dataByte in objCommandDTO.Payload.Data)
                {

                    if (null != objCommandDTO.Payload.Data)
                    {
                        packetRawHexValue[j] = dataByte; //Payload
                    }
                    else
                    {
                        break;
                    }
                    j++;
                }
            }

            //Calculating the CRC Value
            byte CRC = LRC.CalculateCRC(packetRawHexValue);
            packetRawHexValue[packetRawHexValue.Length - 2] = CRC; //CRC
            packetRawHexValue[packetRawHexValue.Length - 1] = 0xc0;//STOP Bit manually adding it
            objCommandDTO.PacketRawValue = packetRawHexValue;
            return packetRawHexValue;
        }

        public byte[] FormRASRawHexValue(CommandDTO objCommandDTO)
        {
            //if (objCommandDTO.Payload.Action == 1)
            // if (objCommandDTO.Payload.Action == 1 && objCommandDTO.PacketRawValue != null && objCommandDTO.PacketRawValue[3] != 1)
            if (objCommandDTO.Payload.Action == 1 && objCommandDTO.PacketRawValue != null && objCommandDTO.PacketRawValue[4] != 1) //write and the attribute is a read command
            {
                if (isReadOnly)
                    objCommandDTO.Payload.Data = new Byte[] { 0, 1, 2, 3, 4, 5 }; //hardcoded dummy value.
            }
            else if (objCommandDTO.Payload.Action == 2)
            {
                if (AttributeReadOnlyDataByte.ContainsKey(strAttribute))
                {
                    if (objCommandDTO.Payload.Data != null)
                    {
                        int intValue = AttributeReadOnlyDataByte[strAttribute];
                        byte[] arrtemp = new byte[intValue];
                        Array.Copy(objCommandDTO.Payload.Data, arrtemp, intValue);
                        objCommandDTO.Payload.Data = new byte[arrtemp.Length];
                        Array.Copy(arrtemp, objCommandDTO.Payload.Data, arrtemp.Length);
                    }
                }
                else
                {
                    objCommandDTO.Payload.Data = null;
                }
            }

            //Intial length of payload will be 3 as it is ACTION,FUNCTIONAL DOMAIN & ATTRIBUTE
            int nPayloadPacketLength = 3;
            if (null != objCommandDTO.Payload.Data)
                nPayloadPacketLength = 3 + objCommandDTO.Payload.Data.Length;// andding the data length

            byte Revision = 1;//revision


            //as per protocol 14
            byte count = Convert.ToByte(nPayloadPacketLength);//Count of the total packet
            Int16 intTotalLength = Convert.ToInt16(count);
            int nTotalPacketLength = 4 + nPayloadPacketLength + 1;
            byte[] packetRawHexValue = new byte[nTotalPacketLength]; //forming the entire packet length
            packetRawHexValue[0] = objCommandDTO.Revision; //Revision  packetRawHexValue[0] = Revision;
            packetRawHexValue[1] = objCommandDTO.Sequence; //Sequence
            //// packetRawHexValue[1] = sequence; //Sequence    earlier in hostcontrol its like that
            Array.Copy(BitConverter.GetBytes(intTotalLength).Reverse().ToArray(), 0, packetRawHexValue, 2, BitConverter.GetBytes(intTotalLength).Length);
            packetRawHexValue[4] = objCommandDTO.Payload.Action; //Action
            packetRawHexValue[5] = objCommandDTO.Payload.FunctionalDomain; //Functional Domain
            packetRawHexValue[6] = objCommandDTO.Payload.Attribute; //Attribute

            //ADDING the payload
            if (null != objCommandDTO.Payload.Data)
            {
                int j = 7;  // int j = 6;  as per protocol 14 it starts from 7th position
                foreach (byte dataByte in objCommandDTO.Payload.Data)
                {
                    if (null != objCommandDTO.Payload.Data)
                    {
                        packetRawHexValue[j] = dataByte; //Payload
                    }
                    else
                    {
                        break;
                    }
                    j++;
                }
            }

            //Calculating the CRC Value
            byte CRC = LRC.CalculateRASCRC(packetRawHexValue);
            packetRawHexValue[packetRawHexValue.Length - 1] = CRC; //CRC
                                                                   // packetRawHexValue[packetRawHexValue.Length - 1] = 0xc0;//STOP Bit manually adding it
            objCommandDTO.PacketRawValue = packetRawHexValue;
            return packetRawHexValue;
        }


        #endregion

        #region Private Methods

        /// <summary>
        /// Builds the attribute read only dictionary.
        /// </summary>
        private void BuildAttributeReadOnlyDictionary()
        {
            try
            {
                AttributeReadOnlyDictionary.Add("Select", false);
                AttributeReadOnlyDictionary.Add("HVAC Variables", false);
                AttributeReadOnlyDictionary.Add("Air Cleaning Installer Variables", false);
                AttributeReadOnlyDictionary.Add("Humidity variables", false);
                AttributeReadOnlyDictionary.Add("Fresh Air Installer Variables", false);
                AttributeReadOnlyDictionary.Add("HVAC/IAQ Available", false);
                AttributeReadOnlyDictionary.Add("Errors", false);
                AttributeReadOnlyDictionary.Add("Power Status", false);
                AttributeReadOnlyDictionary.Add("Sensor Values", false);
                AttributeReadOnlyDictionary.Add("COS", false);
                AttributeReadOnlyDictionary.Add("Revision", false);
                AttributeReadOnlyDictionary.Add("Installer Mode", false);
                AttributeReadOnlyDictionary.Add("HVAC Status", false);
                AttributeReadOnlyDictionary.Add("IAQ Status", false);
                AttributeReadOnlyDictionary.Add("Manufacturing Test", false);
                AttributeReadOnlyDictionary.Add("Humidity Control Installer Variables", false);
                AttributeReadOnlyDictionary.Add("Powerup", false);
                AttributeReadOnlyDictionary.Add("Heat Pump Fault", false);



            }
            catch (Exception ex)
            {
                this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Exception Occurred at BuildAttributeReadOnlyDictionary() Method @Ex: ", ex);
                //Log(ex);
            }
        }



        #endregion
    }
}
