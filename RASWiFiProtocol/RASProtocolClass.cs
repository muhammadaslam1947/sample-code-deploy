﻿//-----------------------------------------------------------------------
// <copyright file="RASProtocolClass.cs" company="L&T TS">
//     Copyright (c) L&T IES. All rights reserved.
// </copyright>
// <author>Moumita Sikdar</author>
//-----------------------------------------------------------------------
namespace RASWiFiProtocol.RASProtocol
{
    using LnT.UbiqSens.PluginInterfaces;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    //using Aprilaire.HostController.Business;
    //using Aprilaire.HostController.BusinessDTO;
    //using Aprilaire.HostController.Common.Globals;
    //using Aprilaire.HostController.Common.Services;

    /// <summary>
    /// RASP Protocol class
    /// </summary>
    public class RASProtocolClass
    {
        #region Public Fields

        /// <summary>
        /// The array copy
        /// </summary>
        public byte[] ArrayCopy = new byte[2000];

        /// <summary>
        /// The output text
        /// </summary>
        public StringBuilder OutputText;

        /// <summary>
        /// The break for loop
        /// </summary>
        public bool BreakForLoop = false;

        /// <summary>
        /// The string action text
        /// </summary>
        public string StrActionText = string.Empty;

        /// <summary>
        /// The string fun DOM text
        /// </summary>
        public string StrFunDomText = string.Empty;

        /// <summary>
        /// The fahrenheit range dictionary
        /// </summary>
        public Dictionary<string, bool> FahrenheitRangeDictionary = new Dictionary<string, bool>();

        /// <summary>
        /// The fahrenheit celsius scale
        /// </summary>
        public Dictionary<double, double> FahrenheitCelsiusScale = new Dictionary<double, double>();

        /// <summary>
        /// The object logger
        /// </summary>
        //// Log4NetLogger objLogger = new Log4NetLogger();
        public List<CommandConfigDTO> listControlVal;
        public List<ParamDTO> CurrentConfiguredData;
        #endregion

        //Logger Object
        public ILogger ObjLog = null;

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RASProtocolClass"/> class.
        /// </summary>
        public RASProtocolClass(ILogger objLogger)
        {
            this.ObjLog = objLogger;
            this.ObjLog?.WriteLog(LogLevel.Info, "WIFIProtocol: Logger Inititalized");
            this.FahrenheitRangeDictionary.Clear();
            this.FahrenheitCelsiusScale.Clear();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Reads the RAS protocol.
        /// </summary>
        /// <param name="hexpacket">The data packet.</param>
        /// <returns>string of meaningful data</returns>
        public string ReadRASProtocol(byte[] hexpacket)
        {
            try
            {
                //byte[] abc = new byte[hexpacket.Length + 6];
                //Array.Copy(hexpacket, abc, hexpacket.Length - 1);
                string strPacket = "0x" + BitConverter.ToString(hexpacket).Replace("-", " 0x");
                this.OutputText = new StringBuilder();

                DataPacket objDatapckt = new DataPacket();
                ////This portion of code is to display the hex value the radio/micro lable time stamp 
                this.OutputText.Append(System.Environment.NewLine);

                ////Write the font as Bold (Time Stamp)
                this.OutputText.Append(string.Format("{0:HH:mm:ss tt}", DateTime.Now));

                ////Splitting the Packet Array
                string[] strPacketArray = strPacket.Trim().Split(' ');

                this.BreakForLoop = false;

                ////Call Display English Value for each Byte of data
                for (int byteIndex = 0; byteIndex < strPacketArray.Length; byteIndex++)
                {
                    if (!this.BreakForLoop)
                    {
                        this.DisplayEnglishSwitchCase(strPacketArray, byteIndex, objDatapckt);
                    }
                    else
                    {
                        break;
                    }
                }

                return this.OutputText.ToString();
            }
            catch (Exception ex)
            {
                this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Exception Occurred at ReadRASProtocol() @Data: " + "0x" + BitConverter.ToString(hexpacket).Replace("-", " 0x"), ex);
                ////objLogger.WriteLog(LogLevel.Error, ex.Message);
                ////this.Log(ex);
                return string.Empty;
            }
        }

        /// <summary>
        /// Reads the RAS wi fi protocol.
        /// </summary>
        /// <param name="hexpacket">The data packet.</param>
        /// <returns>string containing parsed meaningful data</returns>      
        public string ReadRASWiFiProtocol(byte[] hexpacket)
        {
            try
            {
                //byte[] abc = new byte[hexpacket.Length + 6];
                //Array.Copy(hexpacket, abc, hexpacket.Length - 1);                
                string strPacket = "0x" + BitConverter.ToString(hexpacket).Replace("-", " 0x");

                this.ObjLog?.WriteLog(LogLevel.Info, "WIFIProtocol: Bytes received at ReadRASWiFiProtocol() Method with @Data: " + strPacket);

                this.OutputText = new StringBuilder();

                DataPacket objDatapckt = new DataPacket();
                ////This portion of code is to display the hex value the radio/micro lable time stamp 
                this.OutputText.Append(System.Environment.NewLine);

                ////Write the font as Bold (Time Stamp)
                this.OutputText.Append(string.Format("{0:HH:mm:ss tt}", DateTime.Now));

                ////Splitting the Packet Array
                string[] strPacketArray = strPacket.Trim().Split(' ');

                this.BreakForLoop = false;

                ////Call Display English Value for each Byte of data
                for (int byteIndex = 0; byteIndex < strPacketArray.Length; byteIndex++)
                {
                    if (!this.BreakForLoop)
                    {
                        this.DisplayEnglishSwitchCase(strPacketArray, byteIndex, objDatapckt);
                    }
                    else
                    {
                        break;
                    }
                }

                if (objDatapckt.Action != MessageConstants.ReadRequest)
                {
                    this.ParseWIFIProtocol(objDatapckt);
                }
                else
                {
                    objDatapckt.DataString = string.Empty;
                }

                return objDatapckt.Action + ":" + objDatapckt.Attribute + ":" + objDatapckt.DataString + ":" + objDatapckt.Sequence.ToString() + ":" + objDatapckt.FunctionalDomain + ":" + objDatapckt.Revision;
            }
            catch (Exception ex)
            {

                this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Error Occurred at ReadRASWiFiProtocol() Method with @Data: " + BitConverter.ToString(hexpacket), ex);
                ////objLogger.WriteLog(LogLevel.Error, ex.Message);
                //// this.Log(ex);
                return string.Empty;
            }
        }

        /// <summary>
        /// Converts to RAS protocol.
        /// </summary>
        /// <param name="sequence">The sequence.</param>
        /// <param name="action">The action.</param>
        /// <param name="attribute">The attribute.</param>
        /// <param name="data">The data.</param>
        /// <returns>byte array containing data packet</returns>
        public byte[] ConvertToRASProtocol(int sequence, int revision, string action, string attribute, string data)
        {
            try
            {
                if (action == MessageConstants.AckNAck)
                {
                    byte[] ackBytes;

                    if (data != string.Empty)
                    {
                        string[] arrayString = data.Split(',');
                        if (arrayString.Length == 1)
                        {
                            ackBytes = new byte[7];
                            ackBytes[0] = (byte)revision; ////Revision =1 
                            ackBytes[1] = (byte)sequence; ////Sequence
                            ackBytes[2] = (byte)0;
                            ackBytes[3] = (byte)2; ////Count= 2 always   acion = ack and status code 
                            ackBytes[4] = (byte)6; ////ACK/NAck = 6
                            ackBytes[5] = (byte)Convert.ToInt32(arrayString[0]);
                            ackBytes[6] = LRC.CalculateRASCRC(ackBytes);
                            return ackBytes;
                        }
                        else if (arrayString.Length == 2)
                        {
                            ackBytes = new byte[8];
                            ackBytes[0] = (byte)revision; ////Revision =1 
                            ackBytes[1] = (byte)sequence; ////Sequence
                            ackBytes[2] = (byte)0;
                            ackBytes[3] = (byte)3;////Count= 3 always   acion = ack and + 2 data bytes 
                            ackBytes[4] = (byte)6; ////ACK/NAck = 6
                            ackBytes[5] = (byte)Convert.ToInt32(arrayString[0]);
                            ackBytes[6] = (byte)Convert.ToInt32(arrayString[1]);
                            ackBytes[7] = LRC.CalculateRASCRC(ackBytes);
                            return ackBytes;
                        }
                        else
                        {
                            return Encoding.ASCII.GetBytes("ACk packet is not proper");
                            // this.objLogger.WriteLog(LogLevel.Info, "Acknowlegment data packet is not proper");
                        }
                    }
                    return null;
                }
                else if (attribute == MessageConstants.FirmwareUpdatePayload)
                {
                    if (data != string.Empty)
                    {
                        string[] arrayString = data.Split('#');
                        if (arrayString.Length >= 3)
                        {
                            byte[] seq = BitConverter.GetBytes(Convert.ToInt16(arrayString[0]));
                            byte[] totalCount = BitConverter.GetBytes(Convert.ToInt16(arrayString[1]));
                            byte[] datapacket = arrayString[2].Split('-').Select(s => Convert.ToByte(s, 16)).ToArray();
                            int dataCount = datapacket.Length;
                            int intTotalPayloadCount = dataCount + 4 + 3;
                            byte[] firmwareUpgrade = new byte[dataCount + 12];   //[dataCount +11];
                            firmwareUpgrade[0] = (byte)revision; ////Revision =1 
                            firmwareUpgrade[1] = (byte)sequence; ////Sequence
                            //irmwareUpgrade[2] = (byte)(datapacket.Length - 1); ////count only data count
                            Array.Copy(BitConverter.GetBytes(Convert.ToInt16(intTotalPayloadCount)).Reverse().ToArray(), 0, firmwareUpgrade, 2, BitConverter.GetBytes(Convert.ToInt16(intTotalPayloadCount)).Length);
                            firmwareUpgrade[4] = (byte)0X01; ////action   // firmwareUpgrade[3] = (byte)0X01; ////action
                            firmwareUpgrade[5] = (byte)0X0E; ////func domain   // firmwareUpgrade[4] = (byte)0X0E; ////func domain
                            firmwareUpgrade[6] = (byte)0X02;  ////attribute  check for update attri no. FirmwareUpdatePayload  // firmwareUpgrade[5] = (byte)0X02;
                            //Array.Copy(BitConverter.GetBytes(Convert.ToInt16(arrayString[0])).Reverse().ToArray(), 0, firmwareUpgrade, 6, BitConverter.GetBytes(Convert.ToInt16(arrayString[0])).Length);
                            //Array.Copy(BitConverter.GetBytes(Convert.ToInt16(arrayString[1])).Reverse().ToArray(), 0, firmwareUpgrade, 8, BitConverter.GetBytes(Convert.ToInt16(arrayString[1])).Length);

                            Array.Copy(BitConverter.GetBytes(Convert.ToInt16(arrayString[0])).Reverse().ToArray(), 0, firmwareUpgrade, 7, BitConverter.GetBytes(Convert.ToInt16(arrayString[0])).Length);
                            Array.Copy(BitConverter.GetBytes(Convert.ToInt16(arrayString[1])).Reverse().ToArray(), 0, firmwareUpgrade, 9, BitConverter.GetBytes(Convert.ToInt16(arrayString[1])).Length);
                            int j = 11;   //   int j = 10;
                            foreach (byte dataByte in datapacket)
                            {

                                if (null != arrayString[2])
                                {
                                    firmwareUpgrade[j] = dataByte; ////Payload
                                }
                                else
                                {
                                    break;
                                }
                                j++;
                            }
                            byte CRC = LRC.CalculateRASCRC(firmwareUpgrade);
                            firmwareUpgrade[firmwareUpgrade.Length - 1] = CRC; ////CRC
                            return firmwareUpgrade;
                        }
                        else
                        {
                            return Encoding.ASCII.GetBytes("Firmware Upgrade packet is not proper");
                            ////this.objLogger.WriteLog(LogLevel.Info, "The firmware upgrade data packet is not proper");
                        }
                        //// return null;
                    }
                    return null;
                }
                else
                {
                    string funDom = string.Empty;
                    XmlDocument xmlDocument = new XmlDocument();
                    Stream stream = null;
                    if (revision != 1)
                    {
                        stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(MessageConstants.DataConfigurationRev2Xml);
                    }
                    else
                    {
                        stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(MessageConstants.DataConfigurationXml);
                    }
                    StreamReader reader = new StreamReader(stream);
                    xmlDocument.Load(reader);

                    XmlNodeList xmlFieldInfoList = null;
                    xmlFieldInfoList = xmlDocument.GetElementsByTagName(attribute);
                    XmlNode rootNode = xmlFieldInfoList[0];
                    funDom = rootNode.ParentNode.Name;
                    return (this.SetData(sequence, revision, action, funDom, attribute, data));
                }
            }
            catch (Exception exp)
            {

                this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Error Occurred at ReadRASWiFiProtocol() Method With @Data: " + data + " @Attribute: " + attribute + " @Action: " + action + " @Revision: " + revision + " @Sequence: " + sequence, exp);

                //// objLogger.WriteLog(LogLevel.Error, exp.Message);
                //// this.Log(e);
                //// return null;
                throw exp;
            }
        }


        /// <summary>
        /// Calculates the CRC.
        /// </summary>
        /// <param name="hexData">The hexadecimal data.</param>
        /// <returns>CRC as integer</returns>
        public int CalculateCRC(string hexData)
        {
            try
            {
                string[] strPacketArray = hexData.Trim().Split(' ');
                byte[] bytePacket = new byte[strPacketArray.Length + 1];
                for (int count = 0; count < strPacketArray.Length; count++)
                {
                    bytePacket[count] = (byte)int.Parse(strPacketArray[count].Replace("0x", string.Empty), System.Globalization.NumberStyles.HexNumber);
                }

                byte crc = LRC.CalculateRASCRC(bytePacket);
                ////return byteCRC;
                return (int)crc;
            }
            catch (Exception ex)
            {
                this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Error Occurred at CalculateCRC() Method With @Data: " + hexData, ex);
                // objLogger.WriteLog(LogLevel.Info, ex.Message);
                throw ex;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Parses the Wi-Fi protocol.
        /// </summary>
        /// <param name="objDatapckt">The object data packet.</param>
        private void ParseWIFIProtocol(DataPacket objDatapckt)
        {
            try
            {
                bool areEqual = this.ArrayCopy.SequenceEqual(objDatapckt.DataPacketBytes);

                if (areEqual != true && objDatapckt.DataPacketBytes.Length > 0)
                {
                    byte[] array = objDatapckt.DataPacketBytes;
                    string[] dataArray = new string[array.Length];
                    bool dataIsNullOrEmpty = string.IsNullOrEmpty(objDatapckt.Data);
                    if (!dataIsNullOrEmpty)
                    {
                        dataArray = objDatapckt.Data.Split(',');
                        // if (dataArray.Length == array.Length)
                        {
                            if (objDatapckt.Attribute == MessageConstants.ThermostatName || objDatapckt.Attribute == MessageConstants.ContractInfo || objDatapckt.Attribute == MessageConstants.TemporaryMessages || objDatapckt.Attribute == MessageConstants.MACAddress ||
                                objDatapckt.Attribute == MessageConstants.FirmwareUpdatePayload || objDatapckt.Attribute == MessageConstants.ThermostatLocationName)
                            {
                                objDatapckt.DataString = objDatapckt.Data;
                                if (objDatapckt.Attribute == MessageConstants.MACAddress && objDatapckt.DataPacketBytes.Length > 10)
                                {
                                    objDatapckt.Revision = objDatapckt.DataPacketBytes[10]; // to get the revision from 10th byte of mac adress response data 

                                }
                            }
                            else if (objDatapckt.Attribute == MessageConstants.WeatherUpdate || objDatapckt.Attribute == MessageConstants.PermanentMessages || objDatapckt.Attribute == MessageConstants.ScheduleDay
                                || objDatapckt.Attribute == MessageConstants.ScheduleHold || objDatapckt.Attribute == MessageConstants.SensorValues || objDatapckt.Attribute == MessageConstants.SupportModules || objDatapckt.Attribute == MessageConstants.WiFiConfigurationStatus
                                || objDatapckt.Attribute == MessageConstants.OutdoorTempValue || objDatapckt.Attribute == MessageConstants.ThermostatSetpointModeSettings || objDatapckt.Attribute == MessageConstants.ManufacturingTest
                                || objDatapckt.Attribute == MessageConstants.RevisionModel || objDatapckt.Attribute == MessageConstants.ControllingSensorValues || objDatapckt.Attribute == MessageConstants.ControlLocation || objDatapckt.Attribute == MessageConstants.AlertSetting)
                            {
                                for (int intByteIndex = 0; intByteIndex < array.Length; intByteIndex++)
                                {
                                    if (objDatapckt.Attribute == MessageConstants.PermanentMessages)
                                    {
                                        if (intByteIndex == 0)
                                        {
                                            objDatapckt.DataString = array[intByteIndex].ToString();
                                        }
                                        else
                                        {
                                            objDatapckt.DataString = objDatapckt.DataString + "," + dataArray[intByteIndex];
                                            if (intByteIndex == 2)
                                            {
                                                break;
                                            }
                                        }
                                    }

                                    if (objDatapckt.Attribute == MessageConstants.WiFiConfigurationStatus)
                                    {
                                        if (intByteIndex == 0)
                                        {
                                            objDatapckt.DataString = array[intByteIndex].ToString();
                                        }
                                        else if (intByteIndex == 1)
                                        {
                                            objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                        }
                                        else
                                        {
                                            objDatapckt.DataString = objDatapckt.DataString + "," + dataArray[intByteIndex];
                                            if (intByteIndex == 3)
                                            {
                                                break;
                                            }
                                        }
                                    }

                                    if (objDatapckt.Attribute == MessageConstants.ControlLocation)
                                    {
                                        if (intByteIndex == 0)
                                        {
                                            objDatapckt.DataString = array[intByteIndex].ToString();
                                        }
                                        else if (intByteIndex == 1 || intByteIndex == 2)
                                        {
                                            objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                        }
                                        else
                                        {
                                            objDatapckt.DataString = objDatapckt.DataString + "," + dataArray[intByteIndex];
                                            if (intByteIndex == 3)
                                            {
                                                break;
                                            }
                                        }
                                    }

                                    if (objDatapckt.Attribute == MessageConstants.WeatherUpdate)
                                    {
                                        if (intByteIndex == 1 || intByteIndex == 2 || intByteIndex == 5 || intByteIndex == 6 || intByteIndex == 9 || intByteIndex == 10)
                                        {
                                            if (dataArray[intByteIndex].Contains("Null"))
                                            {
                                                objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                            }
                                            else if (dataArray[intByteIndex].Contains("--"))
                                            {
                                                objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                            }
                                            else
                                            {
                                                objDatapckt.DataString = objDatapckt.DataString + "," + Regex.Split(dataArray[intByteIndex], "F")[0].Trim();
                                            }
                                        }
                                        else
                                        {
                                            if (intByteIndex == 0)
                                            {
                                                objDatapckt.DataString = array[intByteIndex].ToString();
                                            }
                                            else
                                            {
                                                objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                            }
                                        }
                                    }

                                    if (objDatapckt.Attribute == MessageConstants.ScheduleDay)
                                    {
                                        if (intByteIndex == 4 || intByteIndex == 5 || intByteIndex == 9 || intByteIndex == 10 || intByteIndex == 14 || intByteIndex == 15 || intByteIndex == 19 || intByteIndex == 20)
                                        {
                                            if (dataArray[intByteIndex].Contains("Null"))
                                                objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                            else
                                                objDatapckt.DataString = objDatapckt.DataString + "," + Regex.Split(dataArray[intByteIndex], "F")[0].Trim();
                                        }
                                        else
                                        {
                                            if (intByteIndex == 0)
                                            {
                                                objDatapckt.DataString = array[intByteIndex].ToString();
                                            }
                                            else
                                            {
                                                objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                            }
                                        }
                                    }

                                    if (objDatapckt.Attribute == MessageConstants.ScheduleHold)
                                    {
                                        if (intByteIndex == 2 || intByteIndex == 3)
                                        {
                                            if (dataArray[intByteIndex].Contains("Null"))
                                                objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                            else
                                                objDatapckt.DataString = objDatapckt.DataString + "," + Regex.Split(dataArray[intByteIndex], "F")[0].Trim();
                                        }
                                        else
                                        {
                                            if (intByteIndex == 0)
                                            {
                                                objDatapckt.DataString = array[intByteIndex].ToString();
                                            }
                                            else
                                            {
                                                objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                            }
                                        }
                                    }

                                    if (objDatapckt.Attribute == MessageConstants.SensorValues)
                                    {
                                        if (intByteIndex == 1 || intByteIndex == 3 || intByteIndex == 5 || intByteIndex == 9 || intByteIndex == 11 || intByteIndex == 13)
                                        {
                                            if (dataArray[intByteIndex].Contains("Null"))
                                                objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                            else
                                                objDatapckt.DataString = objDatapckt.DataString + "," + Regex.Split(dataArray[intByteIndex], "F")[0].Trim();
                                        }
                                        else
                                        {
                                            if (intByteIndex == 0)
                                            {
                                                objDatapckt.DataString = array[intByteIndex].ToString();
                                            }
                                            else
                                            {
                                                objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                            }
                                        }
                                    }

                                    if (objDatapckt.Attribute == MessageConstants.ControllingSensorValues)
                                    {
                                        if (intByteIndex == 1 || intByteIndex == 3)
                                        {
                                            if (dataArray[intByteIndex].Contains("Null"))
                                                objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                            else
                                                objDatapckt.DataString = objDatapckt.DataString + "," + Regex.Split(dataArray[intByteIndex], "F")[0].Trim();
                                        }
                                        else
                                        {
                                            if (intByteIndex == 0)
                                            {
                                                objDatapckt.DataString = array[intByteIndex].ToString();
                                            }
                                            else
                                            {
                                                objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                            }
                                        }
                                    }
                                    if (objDatapckt.Attribute == MessageConstants.SupportModules)
                                    {
                                        if (intByteIndex == 3 || intByteIndex == 6)
                                        {
                                            if (dataArray[intByteIndex].Contains("Null"))
                                                objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                            else
                                                objDatapckt.DataString = objDatapckt.DataString + "," + Regex.Split(dataArray[intByteIndex], "F")[0].Trim();
                                        }
                                        else
                                        {
                                            if (intByteIndex == 0)
                                            {
                                                objDatapckt.DataString = array[intByteIndex].ToString();
                                            }
                                            else
                                            {
                                                objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                            }
                                        }
                                    }

                                    if (objDatapckt.Attribute == MessageConstants.OutdoorTempValue)
                                    {
                                        if (intByteIndex == 1)
                                        {
                                            if (dataArray[intByteIndex].Contains("Null"))
                                                objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                            else
                                                objDatapckt.DataString = objDatapckt.DataString + "," + Regex.Split(dataArray[intByteIndex], "F")[0].Trim();
                                        }
                                        else
                                        {
                                            if (intByteIndex == 0)
                                            {
                                                objDatapckt.DataString = array[intByteIndex].ToString();
                                            }
                                            else
                                            {
                                                objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                            }
                                        }
                                    }

                                    if (objDatapckt.Attribute == MessageConstants.ThermostatSetpointModeSettings)
                                    {
                                        if (intByteIndex == 2 || intByteIndex == 3)
                                        {
                                            if (dataArray[intByteIndex].Contains("Null"))
                                                objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                            else
                                                objDatapckt.DataString = objDatapckt.DataString + "," + Regex.Split(dataArray[intByteIndex], "F")[0].Trim();
                                        }
                                        else
                                        {
                                            if (intByteIndex == 0)
                                            {
                                                objDatapckt.DataString = array[intByteIndex].ToString();
                                            }
                                            else
                                            {
                                                objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                            }
                                        }
                                    }

                                    if (objDatapckt.Attribute == MessageConstants.RevisionModel)
                                    {
                                        if (intByteIndex == 0)
                                        {
                                            objDatapckt.DataString = dataArray[intByteIndex].ToString();
                                        }
                                        else if (intByteIndex == 4)
                                        {
                                            objDatapckt.DataString = objDatapckt.DataString + "," + dataArray[intByteIndex];
                                        }
                                        else
                                        {
                                            objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                        }
                                    }

                                    if (objDatapckt.Attribute == MessageConstants.AlertSetting)
                                    {
                                        //if (intByteIndex == 1 || intByteIndex == 3)
                                        //{
                                        //    if (dataArray[intByteIndex].Contains("Null"))
                                        //        objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                        //    else
                                        //        objDatapckt.DataString = objDatapckt.DataString + "," + Regex.Split(dataArray[intByteIndex], "F")[0].Trim();
                                        //}
                                        //else
                                        {
                                            if (intByteIndex == 0)
                                            {
                                                objDatapckt.DataString = array[intByteIndex].ToString();
                                            }
                                            else
                                            {
                                                objDatapckt.DataString = objDatapckt.DataString + "," + array[intByteIndex].ToString();
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for (int count = 0; count < array.Length; count++)
                                {
                                    if (count == 0)
                                    {
                                        objDatapckt.DataString = array[0].ToString();
                                    }
                                    else
                                    {
                                        objDatapckt.DataString = objDatapckt.DataString + "," + array[count];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Error Occurred at ParseWIFIProtocol() Method With @Data: " + objDatapckt.Data + " @Attribute: " + objDatapckt.Attribute + " @Action: " + objDatapckt.Attribute + " @FunctionDomain: " + objDatapckt.FunctionalDomain, ex);
                // this.objLogger.WriteLog(LogLevel.Info, "data representation eror");
                throw ex;
            }
        }

        /// <summary>
        /// Builds the fahrenheit range value dictionary.
        /// </summary>
        private void BuildFahrenheitRangeValueDictionary()
        {
            this.FahrenheitRangeDictionary.Add("HeatSetpoint", true);
            this.FahrenheitRangeDictionary.Add("CoolSetpoint", true);
            this.FahrenheitRangeDictionary.Add("OutdoorSensor", true);
            this.FahrenheitRangeDictionary.Add("WiredOutdoorTempSensorValue", true);
            this.FahrenheitRangeDictionary.Add("Day1HighTemp", true);
            this.FahrenheitRangeDictionary.Add("Day1LowTemp", true);
            this.FahrenheitRangeDictionary.Add("Day2HighTemp", true);
            this.FahrenheitRangeDictionary.Add("Day2LowTemp", true);
            this.FahrenheitRangeDictionary.Add("Day3HighTemp", true);
            this.FahrenheitRangeDictionary.Add("Day3LowTemp", true);
            this.FahrenheitRangeDictionary.Add("WiredRemoteTempSensorValue", true);
            this.FahrenheitRangeDictionary.Add("BuiltInTempSensorValue", true);
            this.FahrenheitRangeDictionary.Add("WakeHeatSetpoint", true);
            this.FahrenheitRangeDictionary.Add("WakeCoolSetpoint", true);
            this.FahrenheitRangeDictionary.Add("LeaveHeatSetpoint", true);
            this.FahrenheitRangeDictionary.Add("LeaveCoolSetpoint", true);
            this.FahrenheitRangeDictionary.Add("ReturnHeatSetpoint", true);
            this.FahrenheitRangeDictionary.Add("ReturnCoolSetpoint", true);
            this.FahrenheitRangeDictionary.Add("SleepHeatSetpoint", true);
            this.FahrenheitRangeDictionary.Add("SleepCoolSetpoint", true);
            this.FahrenheitRangeDictionary.Add("RATSensorValue", true);
            this.FahrenheitRangeDictionary.Add("LATSensorValue", true);
            this.FahrenheitRangeDictionary.Add("IndoorTempControllingSensorValue", true);
            this.FahrenheitRangeDictionary.Add("Sensor1TempValue", true);
            this.FahrenheitRangeDictionary.Add("Sensor2TempValue", true);
            this.FahrenheitRangeDictionary.Add("WirelessOutdoorTempSensorValue", true);
            this.FahrenheitRangeDictionary.Add("OutdoorTempControllingSensorValue", true);
            //for alerts settings accept Fahrenheit from service
            //this.FahrenheitRangeDictionary.Add("HighIndoorTemperatureLimitSetting", true);
            //this.FahrenheitRangeDictionary.Add("LowIndoorTemperatureLimitSetting", true);

            this.FahrenheitRangeDictionary.Add("Value", true);
        }

        /// <summary>
        /// Builds the fahrenheit celsius scale.
        /// </summary>
        private void BuildFahrenheitCelsiusScale()
        {
            this.FahrenheitCelsiusScale.Add(-40, -40);
            this.FahrenheitCelsiusScale.Add(-39, -39.5);
            this.FahrenheitCelsiusScale.Add(-38, -39);
            this.FahrenheitCelsiusScale.Add(-37, -38.5);
            this.FahrenheitCelsiusScale.Add(-36, -38);
            this.FahrenheitCelsiusScale.Add(-36.1, -37.5);
            this.FahrenheitCelsiusScale.Add(-35, -37);
            this.FahrenheitCelsiusScale.Add(-34, -36.5);
            this.FahrenheitCelsiusScale.Add(-33, -36);
            this.FahrenheitCelsiusScale.Add(-32, -35.5);
            this.FahrenheitCelsiusScale.Add(-31, -35);
            this.FahrenheitCelsiusScale.Add(-30, -34.5);
            this.FahrenheitCelsiusScale.Add(-29, -34);
            this.FahrenheitCelsiusScale.Add(-28, -33.5);
            this.FahrenheitCelsiusScale.Add(-27, -33);
            this.FahrenheitCelsiusScale.Add(-27.1, -32.5);
            this.FahrenheitCelsiusScale.Add(-26, -32);
            this.FahrenheitCelsiusScale.Add(-25, -31.5);
            this.FahrenheitCelsiusScale.Add(-24, -31);
            this.FahrenheitCelsiusScale.Add(-23, -30.5);
            this.FahrenheitCelsiusScale.Add(-22, -30);
            this.FahrenheitCelsiusScale.Add(-21, -29.5);
            this.FahrenheitCelsiusScale.Add(-20, -29);
            this.FahrenheitCelsiusScale.Add(-19, -28.5);
            this.FahrenheitCelsiusScale.Add(-18, -28);
            this.FahrenheitCelsiusScale.Add(-18.1, -27.5);
            this.FahrenheitCelsiusScale.Add(-17, -27);
            this.FahrenheitCelsiusScale.Add(-16, -26.5);
            this.FahrenheitCelsiusScale.Add(-15, -26);
            this.FahrenheitCelsiusScale.Add(-14, -25.5);
            this.FahrenheitCelsiusScale.Add(-13, -25);
            this.FahrenheitCelsiusScale.Add(-12, -24.5);
            this.FahrenheitCelsiusScale.Add(-11, -24);
            this.FahrenheitCelsiusScale.Add(-10, -23.5);
            this.FahrenheitCelsiusScale.Add(-9, -23);
            this.FahrenheitCelsiusScale.Add(-9.1, -22.5);
            this.FahrenheitCelsiusScale.Add(-8, -22);
            this.FahrenheitCelsiusScale.Add(-7, -21.5);
            this.FahrenheitCelsiusScale.Add(-6, -21);
            this.FahrenheitCelsiusScale.Add(-5, -20.5);
            this.FahrenheitCelsiusScale.Add(-4, -20);
            this.FahrenheitCelsiusScale.Add(-3, -19.5);
            this.FahrenheitCelsiusScale.Add(-2, -19);
            this.FahrenheitCelsiusScale.Add(-1, -18.5);
            this.FahrenheitCelsiusScale.Add(0, -18);
            this.FahrenheitCelsiusScale.Add(1, -17.5);
            this.FahrenheitCelsiusScale.Add(1.1, -17);
            this.FahrenheitCelsiusScale.Add(2, -16.5);
            this.FahrenheitCelsiusScale.Add(3, -16);
            this.FahrenheitCelsiusScale.Add(4, -15.5);
            this.FahrenheitCelsiusScale.Add(5, -15);
            this.FahrenheitCelsiusScale.Add(6, -14.5);
            this.FahrenheitCelsiusScale.Add(7, -14);
            this.FahrenheitCelsiusScale.Add(8, -13.5);
            this.FahrenheitCelsiusScale.Add(9, -13);
            this.FahrenheitCelsiusScale.Add(10, -12.5);
            this.FahrenheitCelsiusScale.Add(10.1, -12);
            this.FahrenheitCelsiusScale.Add(11, -11.5);
            this.FahrenheitCelsiusScale.Add(12, -11);
            this.FahrenheitCelsiusScale.Add(13, -10.5);
            this.FahrenheitCelsiusScale.Add(14, -10);
            this.FahrenheitCelsiusScale.Add(15, -9.5);
            this.FahrenheitCelsiusScale.Add(16, -9);
            this.FahrenheitCelsiusScale.Add(17, -8.5);
            this.FahrenheitCelsiusScale.Add(18, -8);
            this.FahrenheitCelsiusScale.Add(19, -7.5);
            this.FahrenheitCelsiusScale.Add(19.1, -7);
            this.FahrenheitCelsiusScale.Add(20, -6.5);
            this.FahrenheitCelsiusScale.Add(21, -6);
            this.FahrenheitCelsiusScale.Add(22, -5.5);
            this.FahrenheitCelsiusScale.Add(23, -5);
            this.FahrenheitCelsiusScale.Add(24, -4.5);
            this.FahrenheitCelsiusScale.Add(25, -4);
            this.FahrenheitCelsiusScale.Add(26, -3.5);
            this.FahrenheitCelsiusScale.Add(27, -3);
            this.FahrenheitCelsiusScale.Add(28, -2.5);
            this.FahrenheitCelsiusScale.Add(28.1, -2);
            this.FahrenheitCelsiusScale.Add(29, -1.5);
            this.FahrenheitCelsiusScale.Add(30, -1);
            this.FahrenheitCelsiusScale.Add(31, -0.5);
            this.FahrenheitCelsiusScale.Add(32, 0);
            this.FahrenheitCelsiusScale.Add(33, 0.5);
            this.FahrenheitCelsiusScale.Add(34, 1);
            this.FahrenheitCelsiusScale.Add(35, 1.5);
            this.FahrenheitCelsiusScale.Add(36, 2);
            this.FahrenheitCelsiusScale.Add(37, 2.5);
            this.FahrenheitCelsiusScale.Add(37.1, 3);
            this.FahrenheitCelsiusScale.Add(38, 3.5);
            this.FahrenheitCelsiusScale.Add(39, 4);
            this.FahrenheitCelsiusScale.Add(40, 4.5);
            this.FahrenheitCelsiusScale.Add(41, 5);
            this.FahrenheitCelsiusScale.Add(42, 5.5);
            this.FahrenheitCelsiusScale.Add(43, 6);
            this.FahrenheitCelsiusScale.Add(44, 6.5);
            this.FahrenheitCelsiusScale.Add(45, 7);
            this.FahrenheitCelsiusScale.Add(46, 7.5);
            this.FahrenheitCelsiusScale.Add(46.1, 8);
            this.FahrenheitCelsiusScale.Add(47, 8.5);
            this.FahrenheitCelsiusScale.Add(48, 9);
            this.FahrenheitCelsiusScale.Add(49, 9.5);
            this.FahrenheitCelsiusScale.Add(50, 10);
            this.FahrenheitCelsiusScale.Add(51, 10.5);
            this.FahrenheitCelsiusScale.Add(52, 11);
            this.FahrenheitCelsiusScale.Add(53, 11.5);
            this.FahrenheitCelsiusScale.Add(54, 12);
            this.FahrenheitCelsiusScale.Add(55, 12.5);
            this.FahrenheitCelsiusScale.Add(55.1, 13);
            this.FahrenheitCelsiusScale.Add(56, 13.5);
            this.FahrenheitCelsiusScale.Add(57, 14);
            this.FahrenheitCelsiusScale.Add(58, 14.5);
            this.FahrenheitCelsiusScale.Add(59, 15);
            this.FahrenheitCelsiusScale.Add(60, 15.5);
            this.FahrenheitCelsiusScale.Add(61, 16);
            this.FahrenheitCelsiusScale.Add(62, 16.5);
            this.FahrenheitCelsiusScale.Add(63, 17);
            this.FahrenheitCelsiusScale.Add(64, 17.5);
            this.FahrenheitCelsiusScale.Add(64.1, 18);
            this.FahrenheitCelsiusScale.Add(65, 18.5);
            this.FahrenheitCelsiusScale.Add(66, 19);
            this.FahrenheitCelsiusScale.Add(67, 19.5);
            this.FahrenheitCelsiusScale.Add(68, 20);
            this.FahrenheitCelsiusScale.Add(69, 20.5);
            this.FahrenheitCelsiusScale.Add(70, 21);
            this.FahrenheitCelsiusScale.Add(71, 21.5);
            this.FahrenheitCelsiusScale.Add(72, 22);
            this.FahrenheitCelsiusScale.Add(73, 22.5);
            this.FahrenheitCelsiusScale.Add(73.1, 23);
            this.FahrenheitCelsiusScale.Add(74, 23.5);
            this.FahrenheitCelsiusScale.Add(75, 24);
            this.FahrenheitCelsiusScale.Add(76, 24.5);
            this.FahrenheitCelsiusScale.Add(77, 25);
            this.FahrenheitCelsiusScale.Add(78, 25.5);
            this.FahrenheitCelsiusScale.Add(79, 26);
            this.FahrenheitCelsiusScale.Add(80, 26.5);
            this.FahrenheitCelsiusScale.Add(81, 27);
            this.FahrenheitCelsiusScale.Add(82, 27.5);
            this.FahrenheitCelsiusScale.Add(82.1, 28);
            this.FahrenheitCelsiusScale.Add(83, 28.5);
            this.FahrenheitCelsiusScale.Add(84, 29);
            this.FahrenheitCelsiusScale.Add(85, 29.5);
            this.FahrenheitCelsiusScale.Add(86, 30);
            this.FahrenheitCelsiusScale.Add(87, 30.5);
            this.FahrenheitCelsiusScale.Add(88, 31);
            this.FahrenheitCelsiusScale.Add(89, 31.5);
            this.FahrenheitCelsiusScale.Add(90, 32);
            this.FahrenheitCelsiusScale.Add(91, 32.5);
            this.FahrenheitCelsiusScale.Add(91.1, 33);
            this.FahrenheitCelsiusScale.Add(92, 33.5);
            this.FahrenheitCelsiusScale.Add(93, 34);
            this.FahrenheitCelsiusScale.Add(94, 34.5);
            this.FahrenheitCelsiusScale.Add(95, 35);
            this.FahrenheitCelsiusScale.Add(96, 35.5);
            this.FahrenheitCelsiusScale.Add(97, 36);
            this.FahrenheitCelsiusScale.Add(98, 36.5);
            this.FahrenheitCelsiusScale.Add(99, 37);
            this.FahrenheitCelsiusScale.Add(100, 37.5);
            this.FahrenheitCelsiusScale.Add(100.1, 38);
            this.FahrenheitCelsiusScale.Add(101, 38.5);
            this.FahrenheitCelsiusScale.Add(102, 39);
            this.FahrenheitCelsiusScale.Add(103, 39.5);
            this.FahrenheitCelsiusScale.Add(104, 40);
            this.FahrenheitCelsiusScale.Add(105, 40.5);
            this.FahrenheitCelsiusScale.Add(106, 41);
            this.FahrenheitCelsiusScale.Add(107, 41.5);
            this.FahrenheitCelsiusScale.Add(108, 42);
            this.FahrenheitCelsiusScale.Add(109, 42.5);
            this.FahrenheitCelsiusScale.Add(109.1, 43);
            this.FahrenheitCelsiusScale.Add(110, 43.5);
            this.FahrenheitCelsiusScale.Add(111, 44);
            this.FahrenheitCelsiusScale.Add(112, 44.5);
            this.FahrenheitCelsiusScale.Add(113, 45);
            this.FahrenheitCelsiusScale.Add(114, 45.5);
            this.FahrenheitCelsiusScale.Add(115, 46);
            this.FahrenheitCelsiusScale.Add(116, 46.5);
            this.FahrenheitCelsiusScale.Add(117, 47);
            this.FahrenheitCelsiusScale.Add(118, 47.5);
            this.FahrenheitCelsiusScale.Add(118.1, 48);
            this.FahrenheitCelsiusScale.Add(119, 48.5);
            this.FahrenheitCelsiusScale.Add(120, 49);
            this.FahrenheitCelsiusScale.Add(121, 49.5);
            this.FahrenheitCelsiusScale.Add(122, 50);
            this.FahrenheitCelsiusScale.Add(123, 50.5);
            this.FahrenheitCelsiusScale.Add(124, 51);
            this.FahrenheitCelsiusScale.Add(125, 51.5);
            this.FahrenheitCelsiusScale.Add(126, 52);
            this.FahrenheitCelsiusScale.Add(127, 52.5);
            this.FahrenheitCelsiusScale.Add(127.1, 53);
            this.FahrenheitCelsiusScale.Add(128, 53.5);
            this.FahrenheitCelsiusScale.Add(129, 54);
            this.FahrenheitCelsiusScale.Add(130, 54.5);
            this.FahrenheitCelsiusScale.Add(131, 55);
        }

        /// <summary>
        /// Displays the english switch case.
        /// </summary>
        /// <param name="strPacketArray">The string packet array.</param>
        /// <param name="byteIndex">Index of the byte.</param>
        /// <param name="objDataPacket">The object data packet.</param>
        /// <returns>object of the class data packet</returns>
        private void DisplayEnglishSwitchCase(string[] strPacketArray, int byteIndex, DataPacket objDataPacket)
        {
            try
            {
                this.FahrenheitRangeDictionary.Clear();
                this.FahrenheitCelsiusScale.Clear();
                List<string> strDataPacketLeftSideVariables = new List<string>();
                string[] strArrayRightSideVariables = new string[0];
                int intPackedDataIndex = 0;

                switch (byteIndex)
                {
                    case 0: ////Revision
                        this.OutputText.Append(MessageConstants.RevisionWithSpace);
                        this.OutputText.Append(" " + strPacketArray[byteIndex]);
                        objDataPacket.Revision = int.Parse(strPacketArray[byteIndex].Replace("0x", string.Empty), System.Globalization.NumberStyles.HexNumber);
                        break;
                    case 1: ////Sequence
                        this.OutputText.Append(MessageConstants.SequenceWithSpace);
                        this.OutputText.Append(" " + strPacketArray[byteIndex] + " ");
                        objDataPacket.Sequence = int.Parse(strPacketArray[byteIndex].Replace("0x", string.Empty), System.Globalization.NumberStyles.HexNumber);
                        break;
                    case 2: ////Count
                        //this.OutputText.Append(MessageConstants.CountWithSpace);
                        //int value = int.Parse(strPacketArray[byteIndex].Replace("0x", string.Empty), System.Globalization.NumberStyles.HexNumber);
                        //this.OutputText.Append(" " + value + " ");
                        break;
                    case 3:
                        this.OutputText.Append(MessageConstants.CountWithSpace);
                        byte[] bytCount = new byte[2];
                        bytCount[0] = (byte)int.Parse(strPacketArray[2].Replace("0x", string.Empty), System.Globalization.NumberStyles.HexNumber);
                        bytCount[1] = (byte)int.Parse(strPacketArray[3].Replace("0x", string.Empty), System.Globalization.NumberStyles.HexNumber);
                        Int16 count = BitConverter.ToInt16(bytCount.Reverse().ToArray(), 0);
                        this.OutputText.Append(" " + count + " ");
                        break;

                    case 99: ////Ack Handling
                        if (objDataPacket.Attribute != MessageConstants.FirmwareUpdatePayload)
                        {
                            if (objDataPacket.Attribute == MessageConstants.ContractInfo)
                                break;

                            this.OutputText.Append(MessageConstants.ACK);
                            //   int decAct = int.Parse(strPacketArray[3].Replace("0x", string.Empty), System.Globalization.NumberStyles.HexNumber);
                            int decAct = int.Parse(strPacketArray[4].Replace("0x", string.Empty), System.Globalization.NumberStyles.HexNumber);
                            this.StrActionText = ((Actions)decAct).ToString();
                            objDataPacket.Action = this.StrActionText;
                            objDataPacket.FunctionalDomain = MessageConstants.NotApplicableValue;
                            objDataPacket.Attribute = MessageConstants.NotApplicableValue;
                            if (strPacketArray.Length == 7)   //   if (strPacketArray.Length == 6)
                            {
                                this.CheckForStatusCode(strPacketArray, true);
                                //objDataPacket.DataString = int.Parse(strPacketArray[4].Replace("0x", string.Empty), System.Globalization.NumberStyles.HexNumber).ToString();
                                //this.OutputText.Append("  "+ Description.GetDescription((Status)int.Parse(strPacketArray[4].Replace("0x", string.Empty), System.Globalization.NumberStyles.HexNumber)));
                                objDataPacket.DataString = int.Parse(strPacketArray[5].Replace("0x", string.Empty), System.Globalization.NumberStyles.HexNumber).ToString();
                                this.OutputText.Append("  " + Description.GetDescription((Status)int.Parse(strPacketArray[5].Replace("0x", string.Empty), System.Globalization.NumberStyles.HexNumber)));
                            }
                            else
                            {
                                objDataPacket.Attribute = MessageConstants.FirmwareUpdate;
                                byte[] frmwareAck = new byte[2];
                                //frmwareAck[1] = (byte)int.Parse(strPacketArray[4].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                //frmwareAck[0] = (byte)int.Parse(strPacketArray[5].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                frmwareAck[1] = (byte)int.Parse(strPacketArray[5].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                frmwareAck[0] = (byte)int.Parse(strPacketArray[6].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                Int16 seq = BitConverter.ToInt16(frmwareAck, 0);
                                objDataPacket.DataString = seq.ToString();
                                this.OutputText.Append(seq.ToString() + "  " + MessageConstants.CRC.Trim());

                            }
                            this.BreakForLoop = true;
                            break;
                        }
                        else
                            break;

                    default: //// Payload
                        if (byteIndex == 4)   ///  if (byteIndex == 3)
                        {
                            //// Action 
                            if (strPacketArray[byteIndex].Equals("0x06"))
                            {
                                ////Check if it an ACK
                                byteIndex = 99; ////call case 99
                                this.DisplayEnglishSwitchCase(strPacketArray, byteIndex, objDataPacket);
                            }
                            else
                            {
                                ////Extract the Action
                                int dec = int.Parse(strPacketArray[byteIndex].Replace("0x", string.Empty), System.Globalization.NumberStyles.HexNumber);

                                this.StrActionText = Description.GetDescription((Actions)dec).ToString();   ////changed it for cos setiing in getdataconfig
                                // strActionText = ((Actions)dec).ToString();
                                objDataPacket.Action = this.StrActionText;
                                this.OutputText.Append(!this.StrActionText.Equals(string.Empty) ? "  " + this.StrActionText + "  " : MessageConstants.Action);
                            }
                        }
                        else if (byteIndex == 5) ////Functional Domain  //   else if (byteIndex == 4) 
                        {
                            int dec = int.Parse(strPacketArray[byteIndex].Replace("0x", string.Empty), System.Globalization.NumberStyles.HexNumber);
                            this.StrFunDomText = ((FunctionalDomain)dec).ToString();
                            objDataPacket.FunctionalDomain = StrFunDomText;
                            this.OutputText.Append(!StrFunDomText.Equals(string.Empty) ? "  " + StrFunDomText + "  " : MessageConstants.FunctionalDomain);
                        }
                        else if (byteIndex == 6) ////Attribute      else if (byteIndex == 5)
                        {
                            //// Attribute
                            strDataPacketLeftSideVariables.Clear();
                            intPackedDataIndex = 0;
                            string[] temparr = new string[2000];
                            int intCounter = 0;

                            ////Get the value 
                            int dec = int.Parse(strPacketArray[byteIndex - 1].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            int attri = int.Parse(strPacketArray[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            ////Get the Attribute from the ENUM
                            //// string strAttribute = FindAttribute(Description.GetDescription((FunctionalDomain)dec), int.Parse(strPacketArray[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                            string strAttribute = this.FindAttribute(((FunctionalDomain)dec).ToString(), attri);
                            objDataPacket.Attribute = strAttribute;
                            ////Display on the UI
                            this.OutputText.Append(!strAttribute.Equals(string.Empty) ? "  " + strAttribute + "  " : MessageConstants.Attribute);
                            if (strAttribute == MessageConstants.FirmwareUpdatePayload)
                            {
                                //for (int i = 6; i < strPacketArray.Length - 1; i++) 
                                for (int i = 7; i < strPacketArray.Length - 1; i++)
                                {
                                    temparr[intCounter] = strPacketArray[i];
                                    intCounter = intCounter + 1;
                                }

                                Array.Resize(ref temparr, intCounter);
                                DataParsingLogic.strAction = StrActionText;
                                for (int i = 0; i < intCounter; i++)
                                {
                                    objDataPacket.DataPacketBytes[i] = (byte)Convert.ToSByte(temparr[i], 16);
                                }

                                Array.Resize(ref objDataPacket.DataPacketBytes, intCounter);

                                objDataPacket.ParameterCount = temparr.Length;
                                string output = string.Empty;
                                foreach (var element in temparr)
                                {
                                    if (output == string.Empty)
                                    {
                                        output = output + element.Split('x')[1];
                                    }
                                    else
                                    {
                                        output = output + "-" + element.Split('x')[1];
                                    }
                                }

                                objDataPacket.Data = output;
                                ////Print DATA and CRC
                                this.OutputText.Append("   DATA   ");
                                this.OutputText.Append(Environment.NewLine + output + Environment.NewLine);
                            }
                            else if (objDataPacket.Action != MessageConstants.ReadRequest)
                            {
                                ////Extracting only the DATA part for parsing
                                //   for (int i = 6; i < strPacketArray.Length - 1; i++) 
                                for (int i = 7; i < strPacketArray.Length - 1; i++)  ////strPacketArray.Length-2 when OXCO was there now removed for wifi
                                {
                                    temparr[intCounter] = strPacketArray[i];
                                    intCounter = intCounter + 1;
                                }

                                Array.Resize(ref temparr, intCounter);
                                DataParsingLogic.strAction = StrActionText;
                                for (int i = 0; i < intCounter; i++)
                                {
                                    objDataPacket.DataPacketBytes[i] = (byte)Convert.ToSByte(temparr[i], 16);
                                }

                                Array.Resize(ref objDataPacket.DataPacketBytes, intCounter);
                                ////strDataPacketLeftSideVariables contain both the left side part and the right side variables
                                ////seperated by a '=' symbol
                                strDataPacketLeftSideVariables = this.GetDataConfig(objDataPacket.Revision, StrFunDomText, temparr, strAttribute);
                                objDataPacket.ParameterCount = strDataPacketLeftSideVariables.Count;
                                strArrayRightSideVariables = new string[strDataPacketLeftSideVariables.Count];

                                ////seperating the both and filling them in the two arrays respectively
                                ////this is done because the right side variables should be in bold.
                                for (int i = 0; i < strDataPacketLeftSideVariables.Count; i++)
                                {
                                    string[] temp = strDataPacketLeftSideVariables[i].Split('=');
                                    if (temp.Length >= 2)
                                        strArrayRightSideVariables[i] = temp[1];
                                    if ((objDataPacket.Data == null))  //  || (objDataPacket.Data.Length == 0))  to get proper contractor name info 
                                    {
                                        objDataPacket.Data = temp[1];
                                    }
                                    else
                                    {
                                        objDataPacket.Data = objDataPacket.Data + "," + temp[1];
                                    }

                                    strDataPacketLeftSideVariables[i] = temp[0];
                                }

                                ////Print DATA and CRC
                                this.OutputText.Append("   DATA   ");

                                if (intPackedDataIndex < strDataPacketLeftSideVariables.Count)
                                {
                                    while (intPackedDataIndex < strDataPacketLeftSideVariables.Count)
                                    {
                                        strDataPacketLeftSideVariables[intPackedDataIndex] = strDataPacketLeftSideVariables[intPackedDataIndex].PadRight(55 - (strDataPacketLeftSideVariables[intPackedDataIndex].Length));
                                        this.OutputText.Append(Environment.NewLine + strDataPacketLeftSideVariables[intPackedDataIndex]);
                                        strArrayRightSideVariables[intPackedDataIndex] = System.Text.RegularExpressions.Regex.Replace(strArrayRightSideVariables[intPackedDataIndex], @"\s+", " ");
                                        this.OutputText.Append("\t" + strArrayRightSideVariables[intPackedDataIndex].Replace('\t', ' ').Trim());
                                        intPackedDataIndex++;
                                        if (objDataPacket.Action == "Read Request" && (objDataPacket.Attribute == MessageConstants.ScheduleDay || objDataPacket.Attribute == MessageConstants.PermanentMessages || objDataPacket.Attribute == MessageConstants.SupportModules))
                                            break;
                                    }
                                }
                            }

                        }
                        else
                        {
                            if (byteIndex == strPacketArray.Length - 1)
                            {
                                ////CRC  as last 0xC0 has been removed so it is forming Length-1 earlier it was length-2
                                this.OutputText.Append(Environment.NewLine);
                                int crc = int.Parse(strPacketArray[byteIndex].Replace("0x", string.Empty), System.Globalization.NumberStyles.HexNumber);
                                this.OutputText.Append(Environment.NewLine + "CRC" + "\t" + "\t" + "\t" + crc);
                            }
                        }

                        break;
                }

                ////return objDataPacket;
            }
            catch (Exception ex)
            {
                this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Error Occurred at DisplayEnglishSwitchCase() Method With @DataString: " + objDataPacket.DataString + " @Action: " + objDataPacket.Action + " @FuncationDomain: " + objDataPacket.FunctionalDomain + " @Data: " + objDataPacket.Data + " @Revision: " + objDataPacket.Revision, ex);
                // this.objLogger.WriteLog(LogLevel.Info, "Error in displayhex method  " + e.Message);
                //// this.Log(e);
                //throw ex;
                ////return objDataPacket;
            }
        }

        /// <summary>
        /// Checks for status code.
        /// </summary>
        /// <param name="strPacketArray">The string packet array.</param>
        /// <param name="isEnglish">if set to <c>true</c> [b is english].</param>
        private void CheckForStatusCode(string[] strPacketArray, bool isEnglish)
        {
            try
            {
                if (!isEnglish)
                {
                    //for (int i = 4; i < strPacketArray.Length; i++)
                    for (int i = 5; i < strPacketArray.Length; i++)
                    {
                        if (strPacketArray[i] == MessageConstants.Zero0x00)
                        {
                            this.OutputText.Append(" " + strPacketArray[i]);
                        }
                        else if (i == 5) // else if (i == 4)
                        {
                            this.OutputText.Append(" " + strPacketArray[i]);
                        }
                        else
                        {
                            if (strPacketArray[i] != MessageConstants.END0x00)
                            {
                                this.OutputText.Append(" " + strPacketArray[i]);
                            }
                        }
                    }
                }
                else
                {
                    // for (int i = 4; i < strPacketArray.Length; i++)
                    for (int i = 5; i < strPacketArray.Length; i++)
                    {
                        if (strPacketArray[i] == MessageConstants.Zero0x00)
                        {
                            int dec = int.Parse(strPacketArray[i].Replace("0x", string.Empty), System.Globalization.NumberStyles.HexNumber);
                            string text = Description.GetDescription((Status)dec);
                            this.OutputText.Append(text);
                        }
                        else if (i == 5) //  else if (i == 4)
                        {
                            int dec = int.Parse(strPacketArray[i].Replace("0x", string.Empty), System.Globalization.NumberStyles.HexNumber);
                            string text = Description.GetDescription((Status)dec);
                            this.OutputText.Append(text);
                        }
                        else
                        {
                            if (strPacketArray[i] != MessageConstants.END0x00)
                            {
                                this.OutputText.Append("  " + MessageConstants.CRC.Trim());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Error Occurred at CheckForStatusCode() Method With @isEnglish: " + isEnglish + " @Data: " + string.Join("", strPacketArray), ex);
                // this.objLogger.WriteLog(LogLevel.Info, "Acknowledment description missing" + ex.Message);
                throw ex;
                ////this.Log(ex);
            }
        }

        /// <summary>
        /// Finds the attribute.
        /// </summary>
        /// <param name="strFunctionalDomain">The string functional domain.</param>
        /// <param name="decValue">The decimal value.</param>
        /// <returns>string attribute</returns>
        private string FindAttribute(string strFunctionalDomain, int decValue)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(MessageConstants.UIConfigurationXml);
                StreamReader reader = new StreamReader(stream);
                xmlDoc.Load(reader);

                XmlNodeList xmlFieldInfoList = null;
                StringBuilder sb = new StringBuilder();
                string nodeName = strFunctionalDomain;
                if (!strFunctionalDomain.Equals(""))
                {
                    xmlFieldInfoList = xmlDoc.GetElementsByTagName(nodeName);
                    XmlNode rootNode = xmlFieldInfoList[0];

                    if (rootNode != null)
                    {
                        if (rootNode.HasChildNodes)
                        {
                            foreach (XmlNode childnode in rootNode.ChildNodes)
                            {
                                int nodeValue = (int)Convert.ToSByte(childnode.InnerText, 16);
                                if (nodeValue == decValue)
                                {
                                    sb.Append(childnode.Name.ToString());
                                    break;
                                }
                            }
                        }
                    }
                }

                return sb.ToString();
            }
            catch (Exception ex)
            {

                this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Error Occurred at FindAttribute() Method With @strFunctionalDomain: " + strFunctionalDomain + " @decValue: " + decValue, ex);

                //this.objLogger.WriteLog(LogLevel.Info, ex.Message);
                ////this.Log(ex);
                ///return string.Empty;
                throw ex;
            }
        }

        /// <summary>
        /// Gets the data config.
        /// </summary>
        /// <param name="strFunctionalDomain">The string functional domain.</param>
        /// <param name="strDataPacket">The string data packet.</param>
        /// <param name="strAttribute">The string attribute.</param>
        /// <returns>List of string having data configuration</returns>
        private List<string> GetDataConfig(int revision, string strFunctionalDomain, string[] strDataPacket, string strAttribute)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                Stream stream = null;

                if (revision != 1)
                {
                    stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(MessageConstants.DataConfigurationRev2Xml);
                }
                else
                {
                    stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(MessageConstants.DataConfigurationXml);
                }
                StreamReader reader = new StreamReader(stream);
                xmlDoc.Load(reader);

                XmlNodeList xmlFieldInfoList = null;
                DataParsingLogic obj = new DataParsingLogic(ObjLog);

                List<string> lststrPacketData = new List<string>();

                xmlFieldInfoList = xmlDoc.GetElementsByTagName(strFunctionalDomain.Replace(" ", string.Empty));
                XmlNode rootNode = xmlFieldInfoList[0];

                if (rootNode != null)
                {
                    switch (strFunctionalDomain)
                    {
                        case MessageConstants.Weather:
                            if (strAttribute.Equals(MessageConstants.WeatherUpdate))
                            {
                                obj.ParseThreeDayForecast(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }
                            else
                            {
                                obj.ParseData(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }

                            break;
                        case MessageConstants.Scheduling:
                            if (strAttribute.Equals(MessageConstants.ScheduleHold))
                            {
                                obj.ParseProgramHold(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }
                            else if (strAttribute.Equals(MessageConstants.ScheduleDay))
                            {
                                obj.ParseScheduleDay(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }
                            else
                            {
                                obj.ParseData(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }

                            break;

                        case MessageConstants.Sensors:
                            if (strAttribute.Equals(MessageConstants.SensorValues))
                            {
                                obj.ParseSensorValuesV7(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }
                            else if (strAttribute.Equals(MessageConstants.OutdoorTempValue))
                            {
                                obj.ParseOutdoorTempSensor(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }
                            else if (strAttribute.Equals(MessageConstants.SupportModules))
                            {
                                obj.ParseSupportModulesV8(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }
                            else if (strAttribute.Equals(MessageConstants.ControllingSensorValues))
                            {
                                obj.ParseControllingSensorValuesV7(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }
                            else
                            {
                                obj.ParseData(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }

                            break;

                        case MessageConstants.Control:
                            if (strAttribute.Equals(MessageConstants.ThermostatSetpointModeSettings))
                            {
                                obj.ParseHVACSettings(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }
                            else
                            {
                                obj.ParseData(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }

                            break;

                        case MessageConstants.Messaging:
                            {
                                obj.ParseMessaging(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }

                            break;

                        case MessageConstants.DebugCommands:
                            if (strAttribute.Equals(MessageConstants.SetManufacturingTest) || strAttribute.Equals(MessageConstants.ManufacturingTest))
                            {
                                obj.ParseManufacturingTestV11(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }
                            else if (strAttribute.Equals(MessageConstants.WiFiConfigurationStatus))
                            {
                                obj.ParseWiFiConfigurationStatus(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }
                            else if (strAttribute.Equals(MessageConstants.WriteSensorValues))
                            {
                                obj.ParseWriteSensorValues(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }
                            else
                            {
                                obj.ParseData(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }

                            break;

                        case MessageConstants.SetUp:
                            if (strAttribute.Equals(MessageConstants.ContractInfo))
                            {
                                obj.ParseContractorInformation(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }
                            else
                            {
                                obj.ParseData(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }

                            break;

                        case MessageConstants.Alerts:
                            if (strAttribute.Equals(MessageConstants.AlertSetting))
                            {
                                obj.ParseAlertsSettingsV16(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }
                            else
                            {
                                obj.ParseData(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }

                            break;

                        case MessageConstants.Identification:
                            if (strAttribute.Equals(MessageConstants.RevisionModel))
                            {
                                obj.ParseRevisionModelV11(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }
                            else if (strAttribute.Equals(MessageConstants.ThermostatLocationName))
                            {
                                obj.ParseThermostatLocationName(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }
                            else if (strAttribute.Equals(MessageConstants.MACAddress))
                            {
                                if (strDataPacket.Length == 8)
                                {
                                    obj.ParseMACAddress(strAttribute, strDataPacket, rootNode, lststrPacketData);
                                }
                                else if (strDataPacket.Length == 14)
                                {
                                    obj.ParseMACAddressRev("MACAddressRev", strDataPacket, rootNode, lststrPacketData);
                                }
                            }
                            else if (strAttribute.Equals(MessageConstants.IPAddress))
                            {
                                obj.ParseIPAddressV5(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }
                            else if (strAttribute.Equals(MessageConstants.ControlLocation))
                            {
                                obj.ParseControlLocationV8(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }
                            else
                            {
                                obj.ParseData(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            }

                            break;
                        default:
                            obj.ParseData(strAttribute, strDataPacket, rootNode, lststrPacketData);
                            break;
                    }
                }

                return lststrPacketData;
            }
            catch (Exception ex)
            {
                this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Error Occurred at GetDataConfig() Method With @strFunctionalDomain: " + strFunctionalDomain + " @Attribute: " + strAttribute + " @Revision: " + revision + " @PacketData: " + string.Join(",", strDataPacket), ex);
                // this.objLogger.WriteLog(LogLevel.Info, e.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Finds the action in bytes.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="cmdDTO">The command  object.</param>
        private void FindActionInBytes(string action, CommandDTO cmdDTO)
        {
            try
            {
                ////to load action bytes value from Action Class (int)Actions.AckNAck
                for (int count = 1; count <= 6; count++)
                {
                    string a = ((Actions)count).ToString();
                    if (((Actions)count).ToString() == action)
                    {
                        cmdDTO.Payload.Action = (byte)count;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Error Occurred at FindActionInBytes() Method With @action: " + action + " @CommandID: " + cmdDTO.CommandID + " @CRC: " + cmdDTO.CRC + " @Payload: " + cmdDTO.Payload, ex);

                // this.objLogger.WriteLog(LogLevel.Info, "Action is not found in bytes  " + e.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Finds the functional domain in bytes.
        /// </summary>
        /// <param name="funDomain">The fun domain.</param>
        /// <param name="cmdDTO">The command object.</param>
        private void FindFunctionalDomainInBytes(string funDomain, CommandDTO cmdDTO)
        {
            try
            {
                for (int count = 1; count <= 15; count++)
                {
                    if (((FunctionalDomain)count).ToString() == funDomain)
                    {
                        cmdDTO.Payload.FunctionalDomain = (byte)count;
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Error Occurred at FindFunctionalDomainInBytes() Method With @funDomain: " + funDomain + " @CommandID: " + cmdDTO.CommandID + " @CRC: " + cmdDTO.CRC + " @Payload: " + cmdDTO.Payload, e);
                // this.objLogger.WriteLog(LogLevel.Info, "functional domain is not found in bytes  " + e.Message);
                throw e;
            }
        }

        /// <summary>
        /// Finds the attribute in bytes.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="attribute">The attribute.</param>
        /// <param name="cmd">The command.</param>
        private void FindAttributeInBytes(string action, string attribute, CommandDTO cmd)
        {
            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(MessageConstants.UIConfigurationXml);
                StreamReader reader = new StreamReader(stream);
                xmlDocument.Load(reader);

                XmlNodeList xmlFieldInfoList = null;
                string nodeName = string.Empty;
                nodeName = attribute;
                nodeName = nodeName.Replace(" ", string.Empty);

                xmlFieldInfoList = xmlDocument.GetElementsByTagName(nodeName);
                XmlNode rootNode = xmlFieldInfoList[0];

                if (rootNode != null)
                {
                    string value = rootNode.InnerText;
                    cmd.Payload.Attribute = (byte)Convert.ToSByte(value, 16);
                }
            }
            catch (Exception e)
            {
                this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Error Occurred at FindAttributeInBytes() Method with @action: " + action + " @attribute: " + attribute + " @CommandID: " + cmd.CommandID + " @CRC: " + cmd.CRC + " @Payload: " + cmd.Payload, e);
                ////this.objLogger.WriteLog(LogLevel.Info, "Attribute is not found in bytes   " + e.Message);
                throw e;
            }
        }

        /// <summary>
        /// assign data bytes based on configuration
        /// </summary>
        /// <param name="cmdDTO">The command .</param>
        private void Datapacket(CommandDTO cmdDTO)
        {
            try
            {
                this.FahrenheitRangeDictionary.Clear();
                this.FahrenheitCelsiusScale.Clear();
                this.BuildFahrenheitRangeValueDictionary();
                this.BuildFahrenheitCelsiusScale();
                byte[] bytNewdata = new byte[300];
                int totalCount = 0;
                double[] temparray = new double[300];

                ////looping through item of the Global List
                foreach (var item in listControlVal)
                {
                    int intCounter = 0;
                    ////looping through param values of the param value 
                    foreach (var pa in item.param)
                    {
                        temparray[totalCount] = pa.Value;

                        if (pa.ControlID == 1)
                        {
                            byte[] tempByteArray = null;
                            byte[] tempCopyByteArray = null;
                            //// MAC ID 
                            if (cmdDTO.Payload.Attribute == 2 && cmdDTO.Payload.FunctionalDomain == 8)
                            {
                                try
                                {
                                    if (pa.CharValue == null && pa.Value != 0)
                                    {
                                        byte dataValue = (byte)Convert.ToInt32(pa.Value.ToString(), 16);
                                        ////as per req in case of IPADD (pa.Value.ToString(), 8)
                                        bytNewdata[intCounter] = dataValue;
                                        intCounter++;
                                        totalCount++;
                                    }
                                    else
                                    {
                                        byte dataValue = (byte)Convert.ToInt32(pa.CharValue, 16);
                                        bytNewdata[intCounter] = dataValue;
                                        intCounter++;
                                        totalCount++;
                                    }
                                }
                                catch (Exception)
                                {
                                    byte dataValue = (byte)Convert.ToInt32(pa.CharValue, 16);
                                    bytNewdata[intCounter] = dataValue;
                                    intCounter++;
                                    totalCount++;
                                }
                            }
                            else
                            {
                                ////Checking if the type of data input is "Char"
                                //// && (!pa.strParamName.Contains("1st two digits")) && (!pa.strParamName.Contains("2nd two digits")) && (!pa.strParamName.Contains("3rd two digits")) && (!pa.strParamName.Contains("4th two digits")) && (!pa.strParamName.Contains("5th two digits")) && (!pa.strParamName.Contains("6th two digits"))
                                //// if (!pa.strParamName.Contains(MessageConstants.Char))
                                if ((!pa.strParamName.Contains(MessageConstants.Name)) && (!pa.strParamName.Contains(MessageConstants.Phone))
                                    && (!pa.strParamName.Contains(MessageConstants.Email)) && (!pa.strParamName.Contains(MessageConstants.Web))
                                    && (!pa.strParamName.Contains(MessageConstants.Message)) && (!pa.strParamName.Contains(MessageConstants.SSID))
                                    && (!pa.strParamName.Contains(MessageConstants.Password)) && (!pa.strParamName.Contains(MessageConstants.ConnectionNetwork)) && (!pa.strParamName.Contains(MessageConstants.Zipcode)))
                                {
                                    if (this.FahrenheitRangeDictionary.ContainsKey(pa.strParamName.Replace(" ", "")))
                                    {
                                        if (pa.strParamName.Contains("Heat Setpoint"))
                                        {
                                            DataParsingLogic.HeatSetpointRawValue = (byte)pa.Value;
                                        }

                                        if (pa.strParamName.Contains("Cool Setpoint"))
                                        {
                                            DataParsingLogic.coolSetpointRawvalue = (byte)pa.Value;
                                        }

                                        if (pa.strParamName.Equals(MessageConstants.Value))
                                        {
                                            ////check if it is Value of backlight intensity then convert the input value to the specified format
                                            ////if the value is negative then 7th bit is set to 1 and the value is stored in the next 6 bits
                                            ////if the value is positive then 7th bit is set to 0 and the value is stored in the next 6 bits
                                            byte dataValue = (byte)pa.Value;
                                            bytNewdata[intCounter] = dataValue;
                                        }
                                        else
                                        {
                                            ////The value is first converted to degrees from Fahrenheit
                                            ////if the value is negative then 7th bit is set to 1.If the value has a decimal part byteIndex.e 16.5 (.5)
                                            //// then the 5th bit is set to 1 if not to 0 and the value is stored in the next 5 bits
                                            if (pa.Value != 0.0)
                                            {
                                                if ((pa.strParamName.Equals("Day 1 High Temp") || pa.strParamName.Equals("Day 1 Low Temp") || pa.strParamName.Equals("Day 2 High Temp") || pa.strParamName.Equals("Day 2 Low Temp") || pa.strParamName.Equals("Day 3 High Temp") || pa.strParamName.Equals("Day 3 Low Temp")) && pa.Value == 255)
                                                {
                                                    bytNewdata[intCounter] = 255;
                                                }
                                                else
                                                {
                                                    byte dataValue = this.GetTemperature(this.ConvertFahrenheitToCelsius(pa.Value));
                                                    bytNewdata[intCounter] = dataValue;
                                                }
                                            }
                                            else
                                            {
                                                bytNewdata[intCounter] = (byte)pa.Value;
                                            }
                                        }
                                    }
                                    else if (pa.strParamName.Trim().Equals("Hardware Revision"))
                                    {
                                        if (null == pa.CharValue || string.Empty == pa.CharValue)
                                        {
                                            if (pa.strParamName.Equals(""))
                                            {
                                                tempCopyByteArray = new byte[] { 0 };
                                            }
                                            else
                                            {
                                                bytNewdata[intCounter] = (byte)pa.Value;
                                            }

                                        }
                                        else
                                        {
                                            tempByteArray = Encoding.ASCII.GetBytes(pa.CharValue);
                                            tempCopyByteArray = new byte[Encoding.ASCII.GetBytes(pa.CharValue).Length];
                                            Array.Copy(tempByteArray, tempCopyByteArray, tempByteArray.Length);
                                            ////increase the total count which is required to resize the array.
                                            for (int i = 0; i < tempCopyByteArray.Length; i++)
                                            {
                                                bytNewdata[intCounter] = tempCopyByteArray[i];
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (pa.Value > 999)
                                        {
                                            pa.Value = Convert.ToDouble(Convert.ToInt32(pa.Value).ToString().Substring(2));
                                        }

                                        ////Store the value as it is if it doesnt match any of the cases.
                                        bytNewdata[intCounter] = (byte)pa.Value;
                                    }

                                    intCounter++;
                                    totalCount++;
                                }
                                else
                                {
                                    ////if it of character type eg"Permanent message" we have to append it with 
                                    ////null characters after each word and two null characters if it is empty
                                    if (null == pa.CharValue || string.Empty == pa.CharValue)
                                    {
                                        if (pa.strParamName.Equals("Char1-2"))
                                        {
                                            tempCopyByteArray = new byte[] { 0 };
                                        }
                                        else
                                        {
                                            tempCopyByteArray = new byte[] { 0, 0 };
                                        }
                                    }
                                    else
                                    {
                                        ////Get the value and append it with null in the end.
                                        tempByteArray = Encoding.ASCII.GetBytes(pa.CharValue);
                                        tempCopyByteArray = new byte[Encoding.ASCII.GetBytes(pa.CharValue).Length + 1];
                                        Array.Copy(tempByteArray, tempCopyByteArray, tempByteArray.Length);
                                        tempCopyByteArray[tempByteArray.Length] = 0;
                                    }

                                    if ((!pa.strParamName.Contains(MessageConstants.Name)) && (!pa.strParamName.Contains(MessageConstants.Phone)) && (!pa.strParamName.Contains(MessageConstants.Email)) && (!pa.strParamName.Contains(MessageConstants.Web)) && (!pa.strParamName.Contains(MessageConstants.Zipcode)) && (!pa.strParamName.Contains(MessageConstants.ControlName)))
                                    {  ////increase the total count which is required to resize the array.
                                        for (int i = 0; i < tempCopyByteArray.Length; i++)
                                        {
                                            bytNewdata[intCounter] = tempCopyByteArray[i];
                                            intCounter++;
                                            totalCount++;
                                        }
                                    }
                                    else if ((pa.strParamName.Equals(MessageConstants.Name)) || (pa.strParamName.Contains(MessageConstants.Email)) || (pa.strParamName.Contains(MessageConstants.Web)))
                                    {
                                        //0 ,0 issue Contractor info
                                        if (tempByteArray == null)
                                        {
                                            ////Get the value and append it with null in the end.
                                            tempByteArray = Encoding.ASCII.GetBytes(pa.CharValue);
                                            tempCopyByteArray = new byte[Encoding.ASCII.GetBytes(pa.CharValue).Length + 1];
                                            Array.Copy(tempByteArray, tempCopyByteArray, tempByteArray.Length);
                                            tempCopyByteArray[tempByteArray.Length] = 0;
                                        }

                                        tempCopyByteArray = new byte[71];
                                        Array.Copy(tempByteArray, tempCopyByteArray, tempByteArray.Length);
                                        tempCopyByteArray[tempByteArray.Length] = 0;

                                        for (int i = 0; i < tempCopyByteArray.Length; i++)
                                        {
                                            bytNewdata[intCounter] = tempCopyByteArray[i];
                                            intCounter++;
                                            totalCount++;
                                        }
                                    }
                                    else if (pa.strParamName.Contains(MessageConstants.Phone))    //added it wAs missed as phone is always 11 
                                    {

                                        //0 ,0 issue Contractor info
                                        if (tempByteArray == null)
                                        {
                                            ////Get the value and append it with null in the end.
                                            tempByteArray = Encoding.ASCII.GetBytes(pa.CharValue);
                                            tempCopyByteArray = new byte[Encoding.ASCII.GetBytes(pa.CharValue).Length + 1];
                                            Array.Copy(tempByteArray, tempCopyByteArray, tempByteArray.Length);
                                            tempCopyByteArray[tempByteArray.Length] = 0;
                                        }
                                        tempCopyByteArray = new byte[11];
                                        Array.Copy(tempByteArray, tempCopyByteArray, tempByteArray.Length);
                                        tempCopyByteArray[tempByteArray.Length] = 0;

                                        for (int i = 0; i < tempCopyByteArray.Length; i++)
                                        {
                                            bytNewdata[intCounter] = tempCopyByteArray[i];
                                            intCounter++;
                                            totalCount++;
                                        }
                                    }
                                    else if (pa.strParamName.Contains(MessageConstants.Zipcode))
                                    {
                                        //null  "","" Zipcode n control name
                                        if (tempByteArray == null)
                                        {
                                            ////Get the value and append it with null in the end.
                                            tempByteArray = Encoding.ASCII.GetBytes(pa.CharValue);
                                            tempCopyByteArray = new byte[Encoding.ASCII.GetBytes(pa.CharValue).Length + 1];
                                            Array.Copy(tempByteArray, tempCopyByteArray, tempByteArray.Length);
                                            tempCopyByteArray[tempByteArray.Length] = 0;
                                        }

                                        tempCopyByteArray = new byte[8];
                                        Array.Copy(tempByteArray, tempCopyByteArray, tempByteArray.Length);
                                        tempCopyByteArray[tempByteArray.Length] = 0;

                                        for (int i = 0; i < tempCopyByteArray.Length; i++)
                                        {
                                            bytNewdata[intCounter] = tempCopyByteArray[i];
                                            intCounter++;
                                            totalCount++;
                                        }
                                    }
                                    else if (pa.strParamName.Contains(MessageConstants.ControlName))
                                    {
                                        //null  "","" Zipcode n control name
                                        if (tempByteArray == null)
                                        {
                                            ////Get the value and append it with null in the end.
                                            tempByteArray = Encoding.ASCII.GetBytes(pa.CharValue);
                                            tempCopyByteArray = new byte[Encoding.ASCII.GetBytes(pa.CharValue).Length + 1];
                                            Array.Copy(tempByteArray, tempCopyByteArray, tempByteArray.Length);
                                            tempCopyByteArray[tempByteArray.Length] = 0;
                                        }

                                        tempCopyByteArray = new byte[16];
                                        Array.Copy(tempByteArray, tempCopyByteArray, tempByteArray.Length);
                                        tempCopyByteArray[tempByteArray.Length] = 0;

                                        for (int i = 0; i < tempCopyByteArray.Length; i++)
                                        {
                                            bytNewdata[intCounter] = tempCopyByteArray[i];
                                            intCounter++;
                                            totalCount++;
                                        }

                                    }
                                    ////increase the total count which is required to resize the array.
                                    //for (int i = 0; i < tempCopyByteArray.Length; i++)
                                    //{
                                    //    bytNewdata[intCounter] = tempCopyByteArray[i];
                                    //    intCounter++;
                                    //    totalCount++;
                                    //}
                                }
                            }
                        }
                    }
                }

                ////resize the array going to DTO
                Array.Resize(ref bytNewdata, totalCount);
                Array.Resize(ref temparray, totalCount);
                cmdDTO.Payload.Data = bytNewdata;
                ////setting the value to playload. 
                CurrentConfiguredData.Clear();
                listControlVal.Clear();
            }
            catch (Exception e)
            {
                this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Error Occurred at DataPacket() Method with @CommandID: " + cmdDTO.CommandID + " @CRC: " + cmdDTO.CRC + " @Payload: " + cmdDTO.Payload, e);
                throw e;
            }
        }

        /// <summary>
        /// Gets the temperature.
        /// </summary>
        /// <param name="celciusValue">The database celsius value.</param>
        /// <returns>byte temperature</returns>
        private byte GetTemperature(double celciusValue)
        {
            try
            {
                double returnCelciusTemp = celciusValue * 10;
                byte tempReturnValue = 0;
                ////neg or pos
                tempReturnValue = returnCelciusTemp < 0 ? (byte)(tempReturnValue ^ 0x80) : (byte)0; ////0x80 8th bit value 128 in decimal

                ////
                //// Now construct the return value (see Return description above)
                //// Here we start using the Celsius table as we have the final Index
                ////
                returnCelciusTemp = returnCelciusTemp / 10;
                returnCelciusTemp = Math.Abs(returnCelciusTemp);
                returnCelciusTemp = Math.Round(returnCelciusTemp, 1, MidpointRounding.ToEven);
                byte tempValue = (byte)returnCelciusTemp;
                tempReturnValue = (byte)(tempReturnValue ^ tempValue);

                ////
                //// Same logic follows for getting the 0.5 indication (keep in mind
                //// modulo also gives you sign, so get absolute again)
                ////
                returnCelciusTemp = (returnCelciusTemp % 1) * 10;
                returnCelciusTemp = Math.Round(returnCelciusTemp, 1);

                tempReturnValue = returnCelciusTemp >= 5 ? (byte)(tempReturnValue ^ 0x40) : tempReturnValue; ////0x40 7th bit value 64 in decimal

                return tempReturnValue;
            }
            catch (Exception e)
            {
                this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Error Occurred at GetTemperature() Method with @CelciusValue: " + celciusValue, e);
                ////objLogger.WriteLog(LogLevel.Info, e.Message);
                throw e;
            }
        }

        /// <summary>
        /// Converts the fahrenheit to celsius.
        /// </summary>
        /// <param name="f">The f.</param>
        /// <returns>double value celsius</returns>
        private double ConvertFahrenheitToCelsius(double f)
        {
            try
            {
                if (this.FahrenheitCelsiusScale.ContainsKey(f))
                {
                    return this.FahrenheitCelsiusScale[f];
                }

                return 0;
            }
            catch (Exception e)
            {
                this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Error Occurred at ConvertFahrenheitToCelsius() Method with @fahrenheit: " + f, e);
                // objLogger.WriteLog(LogLevel.Info, e.Message);
                throw e;
            }
        }

        /// <summary>
        /// Sets the data.
        /// </summary>
        /// <param name="sequence">The sequence.</param>
        /// <param name="action">The action.</param>
        /// <param name="funDomain">The fun domain.</param>
        /// <param name="attribute">The attribute.</param>
        /// <param name="data">The data.</param>
        /// <returns>byte array with the command data bytes</returns>
        private byte[] SetData(int sequence, int revision, string action, string funDomain, string attribute, string data)
        {
            try
            {


                int dataPacketId = 1;
                CommandDTO cmdDTO = new CommandDTO(1);
                DataUpdateBusinessLogic dataUpdate = new DataUpdateBusinessLogic(ObjLog);
                listControlVal = new List<CommandConfigDTO>();
                CurrentConfiguredData = new List<ParamDTO>();
                cmdDTO.Payload.Data = null;
                cmdDTO.PacketRawValue = null;
                cmdDTO.Sequence = (byte)sequence;
                cmdDTO.Revision = (byte)revision;
                cmdDTO.Payload.Action = 0;
                cmdDTO.Payload.FunctionalDomain = 0;
                cmdDTO.Payload.Attribute = 0;
                dataUpdate.strAttribute = attribute;

                this.FindActionInBytes(action, cmdDTO);

                this.FindFunctionalDomainInBytes(funDomain, cmdDTO);

                this.FindAttributeInBytes(action, attribute, cmdDTO);

                if (action == MessageConstants.Write || action == MessageConstants.ReadResponse || action == MessageConstants.COS || action == MessageConstants.COSSettings)
                {
                    ////data packet formation part
                    if (!string.IsNullOrEmpty(data))
                    {
                        string[] dataSplitted = data.Split(',');
                        int dataLength = dataSplitted.Length;

                        ParamDTO parameter = new ParamDTO(dataPacketId);
                        //ParamDTO ft = new ParamDTO(dataPacketId);
                        ////p.strParamName = attribute;
                        //// p.Value = 1;
                        //List<Aprilaire.HostController.BusinessDTO.ParamDTO> prmDto = new List<Aprilaire.HostController.BusinessDTO.ParamDTO>();
                        CommandConfigDTO objCommandConfigDTOVal = new CommandConfigDTO();
                        objCommandConfigDTOVal.ControlID = dataPacketId;
                        listControlVal.Add(objCommandConfigDTOVal);

                        XmlDocument xmlDocument = new XmlDocument();
                        Stream stream = null;
                        using (revision == 1 ? stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(MessageConstants.DataConfigurationXml) : stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(MessageConstants.DataConfigurationRev2Xml))
                        {
                            StreamReader reader = new StreamReader(stream);
                            xmlDocument.Load(reader);
                            ////  xmlDocument.Load(".\\XML\\" + MessageConstants.DataConfigurationXml);
                            XmlNodeList xmlFieldInfoList = null;
                            string nodeName = string.Empty;

                            if (!action.Equals(MessageConstants.COSSettings))
                            {
                                nodeName = attribute;
                                nodeName = nodeName.Replace(" ", string.Empty);
                            }
                            else
                            {
                                nodeName = MessageConstants.Cos;
                            }

                            xmlFieldInfoList = xmlDocument.GetElementsByTagName(nodeName);
                            XmlNode rootNode = xmlFieldInfoList[0];

                            if (rootNode != null)
                            {
                                int paraCount = 0;

                                ////Looping throught each node 
                                foreach (XmlNode fieldInfo in rootNode.ChildNodes)
                                {

                                    parameter = new ParamDTO(dataPacketId) { strParamName = fieldInfo.Attributes[0].Value, intParameterID = paraCount };

                                    objCommandConfigDTOVal.param.Add(parameter);

                                    CurrentConfiguredData.Add(parameter);

                                }

                                if (dataLength == objCommandConfigDTOVal.param.Count)
                                {
                                    for (int i = 0; i < dataLength; i++)
                                    {
                                        try
                                        {
                                            if (objCommandConfigDTOVal.param[i].strParamName == MessageConstants.Phone || objCommandConfigDTOVal.param[i].strParamName == MessageConstants.Zipcode || objCommandConfigDTOVal.param[i].strParamName == MessageConstants.ControlName)
                                            {
                                                objCommandConfigDTOVal.param[i].CharValue = dataSplitted[i];
                                            }
                                            else
                                            {
                                                objCommandConfigDTOVal.param[i].Value = Convert.ToDouble(dataSplitted[i]);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Error Occurred at SetData() Method Inside For Loop with @dataLength: " + dataLength + " @ParameterCount: " + objCommandConfigDTOVal.param.Count + " @Sequence: " + sequence + " @Revision: " + revision + " @Data: " + data + " @FunctionalDomain: " + funDomain + " @Attribute: " + attribute, ex);
                                            objCommandConfigDTOVal.param[i].CharValue = dataSplitted[i];
                                        }
                                    }
                                }
                                else
                                    return Encoding.ASCII.GetBytes("Length of data is not matching with xml " + data + " Child Node:" + rootNode.ChildNodes.Count + "Data Lenth:" + dataLength + "Param Length:" + objCommandConfigDTOVal.param.Count);
                            }
                            else
                                return Encoding.ASCII.GetBytes("root node is null");

                            if (dataLength == objCommandConfigDTOVal.param.Count)
                                this.Datapacket(cmdDTO);
                            ////else
                            //// this.objLogger.WriteLog(LogLevel.Info, "The data length is either less or greater than expected ");
                            // prmDto.Clear();
                            objCommandConfigDTOVal.param.Clear();
                            xmlFieldInfoList = null;
                            reader = null;
                            rootNode = null;
                        }
                    }
                    else
                    {
                        return Encoding.ASCII.GetBytes("Data is null or empty");
                        // this.objLogger.WriteLog(LogLevel.Info, "Data part is null or empty for Write/Read response packet");
                    }
                }
                else if (action == MessageConstants.ReadRequest && (attribute == MessageConstants.PermanentMessages || attribute == MessageConstants.ScheduleDay || attribute == MessageConstants.SupportModules))
                {
                    if (data != string.Empty)
                    {
                        string[] dataSplitted = data.Split(',');
                        byte[] bytNewdata = new byte[1];
                        double db = Convert.ToDouble(dataSplitted[0]);
                        bytNewdata[0] = (byte)db;
                        cmdDTO.Payload.Data = bytNewdata;
                    }
                }

                byte[] arr = dataUpdate.FormRASRawHexValue(cmdDTO);
                cmdDTO.Payload = null;
                cmdDTO = null;
                dataUpdate = null;
                ////HostControllerDTO.GetInstance().Commands = null;
                ////HostControllerDTO.ClearInstance();
                string strRawPacketValue = "0x" + BitConverter.ToString(arr).Replace("-", " 0x");
                ////return strRawPacketValue;   // changed earlier it was ox0..format
                return arr;
            }
            catch (Exception ex)
            {
                this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Error Occurred at SetData() Method with  @Sequence: " + sequence + " @Revision: " + revision + " @Data: " + data + " @FunctionalDomain: " + funDomain + " @Attribute: " + attribute, ex);
                // objLogger.WriteLog(LogLevel.Error, ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Logs the specified ex.
        /// </summary>
        /// <param name="ex">The ex.</param>
        private void Log(Exception ex)
        {
            if (ex.StackTrace != null)
            {
                //Logging.ErrorEx(this, ex.Message, ex);
            }
        }
        #endregion

        /// <summary>
        /// constant class for string
        /// </summary>
        public static class MessageConstants
        {
            /// <summary>
            /// The zero0x00
            /// </summary>
            public const string Zero0x00 = "0x00";

            public const string strFahrenheitAndDegreeTemparature = "={0} F Or {1} C";

            /// <summary>
            /// The en D0X00
            /// </summary>
            public const string END0x00 = "0xC0";
            /// <summary>
            /// The cos settings
            /// </summary>
            public const string COSSettings = "COSSetting";

            /// <summary>
            /// The write
            /// </summary>
            public const string Write = "Write";

            /// <summary>
            /// The read request
            /// </summary>
            public const string ReadRequest = "ReadRequest";

            /// <summary>
            /// The COS
            /// </summary>
            public const string COS = "COS";

            /// <summary>
            /// The cos
            /// </summary>
            public const string Cos = "Cos";

            /// <summary>
            /// The phone
            /// </summary>
            public const string Phone = "Phone";

            /// <summary>
            /// The read response
            /// </summary>
            public const string ReadResponse = "ReadResponse";

            /// <summary>
            /// The thermostat name
            /// </summary>
            public const string ThermostatName = "ThermostatName";

            /// <summary>
            /// The thermostat location name
            /// </summary>
            public const string ThermostatLocationName = "ThermostatLocationName";

            /// <summary>
            /// The contract information
            /// </summary>
            public const string ContractInfo = "ContractorInformation";

            /// <summary>
            /// The temporary messages
            /// </summary>
            public const string TemporaryMessages = "TemporaryMessages";

            /// <summary>
            /// The mac address
            /// </summary>
            public const string MACAddress = "MACAddress";

            /// <summary>
            /// The zipcode
            /// </summary>
            public const string Zipcode = "Zipcode";


            /// <summary>
            /// The firmware update payload
            /// </summary>
            public const string FirmwareUpdatePayload = "FirmwareUpdatePayload";

            /// <summary>
            /// The firmware update
            /// </summary>
            public const string FirmwareUpdate = "FirmwareUpdate";

            /// <summary>
            /// The three day forecast
            /// </summary>
            public const string ThreeDayForecast = "ThreeDayForecast";

            /// <summary>
            /// The weather update
            /// </summary>
            public const string WeatherUpdate = "WeatherUpdate";

            /// <summary>
            /// The permanent messages
            /// </summary>
            public const string PermanentMessages = "PermanentMessages";

            /// <summary>
            /// The schedule day
            /// </summary>
            public const string ScheduleDay = "ScheduleDay";

            /// <summary>
            /// The controlling sensor values
            /// </summary>
            public const string ControllingSensorValues = "ControllingSensorStatusValue";

            /// <summary>
            /// The schedule hold
            /// </summary>
            public const string ScheduleHold = "ScheduleHold";

            /// <summary>
            /// The sensor values
            /// </summary>
            public const string SensorValues = "SensorValues";

            /// <summary>
            /// The support modules
            /// </summary>
            public const string SupportModules = "SupportModules";

            /// <summary>
            /// The outdoor temporary value
            /// </summary>
            public const string OutdoorTempValue = "WrittenOutdoorTempValue";

            /// <summary>
            /// The thermostat set point mode settings
            /// </summary>
            public const string ThermostatSetpointModeSettings = "ThermostatSetpointModeSettings";

            /// <summary>
            /// The revision model
            /// </summary>
            public const string RevisionModel = "RevisionModel";

            /// <summary>
            /// The manufacturing test
            /// </summary>
            public const string ManufacturingTest = "ManufacturingTest";

            /// <summary>
            /// The weather
            /// </summary>
            public const string Weather = "Weather";

            /// <summary>
            /// The scheduling
            /// </summary>
            public const string Scheduling = "Scheduling";

            /// <summary>
            /// The sensors
            /// </summary>
            public const string Sensors = "Sensors";

            /// <summary>
            /// The control
            /// </summary>
            public const string Control = "Control";

            /// <summary>
            /// The messaging
            /// </summary>
            public const string Messaging = "Messaging";

            /// <summary>
            /// The debug commands
            /// </summary>
            public const string DebugCommands = "DebugCommands";

            /// <summary>
            /// The set manufacturing test
            /// </summary>
            public const string SetManufacturingTest = "SetManufacturingTest";

            /// <summary>
            /// The wi fi configuration status
            /// </summary>
            public const string WiFiConfigurationStatus = "WiFiConfigurationStatus";

            /// <summary>
            /// The write sensor values
            /// </summary>
            public const string WriteSensorValues = "WriteSensorValues";

            /// <summary>
            /// The set up
            /// </summary>
            public const string SetUp = "SetUp";

            /// <summary>
            /// The alert setting
            /// </summary>
            public const string AlertSetting = "AlertsSettings";

            /// <summary>
            /// The alerts
            /// </summary>
            public const string Alerts = "Alerts";

            /// <summary>
            /// The identification
            /// </summary>
            public const string Identification = "Identification";

            /// <summary>
            /// The IP address
            /// </summary>
            public const string IPAddress = "IPAddress";

            /// <summary>
            /// The control location
            /// </summary>
            public const string ControlLocation = "ControlLocation";

            /// <summary>
            /// The action
            /// </summary>
            public const string Action = "Action";

            /// <summary>
            /// The functional domain
            /// </summary>
            public const string FunctionalDomain = "Functional Domain";

            /// <summary>
            /// The email
            /// </summary>
            public const string Email = "Email";

            /// <summary>
            /// The web
            /// </summary>
            public const string Web = "Web";

            /// <summary>
            /// The password
            /// </summary>
            public const string Password = "Password";

            /// <summary>
            /// The SSID
            /// </summary>
            public const string SSID = "SSID";

            /// <summary>
            /// The connection network
            /// </summary>
            public const string ConnectionNetwork = "Connection Network";

            /// <summary>
            /// The attribute
            /// </summary>
            public const string Attribute = "Attribute";

            /// <summary>
            /// The revision with space
            /// </summary>
            public const string RevisionWithSpace = "  Revision  ";

            /// <summary>
            /// The sequence with space
            /// </summary>
            public const string SequenceWithSpace = "   Sequence  ";

            /// <summary>
            /// The count with space
            /// </summary>
            public const string CountWithSpace = "  Count ";

            /// <summary>
            /// The acknowledgement and negative acknowledgement
            /// </summary>
            public const string AckNAck = "AckNAck";

            /// <summary>
            /// The acknowledgement
            /// </summary>
            public const string ACK = "  ACK  ";

            /// <summary>
            /// The CRC
            /// </summary>
            public const string CRC = "  CRC  ";

            /// <summary>
            /// The name
            /// </summary>
            public const string Name = "Name";

            /// <summary>
            /// The display name
            /// </summary>
            public const string DisplayName = "DisplayName";

            /// <summary>
            /// The byte value
            /// </summary>
            public const string ByteValue = "ByteValue";

            /// <summary>
            /// The selected index
            /// </summary>
            public const string SelectedIndex = "SelectedIndex";

            /// <summary>
            /// The selected value
            /// </summary>
            public const string SelectedValue = "SelectedValue";

            /// <summary>
            /// The value
            /// </summary>
            public const string Value = "Value";

            /// <summary>
            /// The message
            /// </summary>
            public const string Message = "Message Line";

            /// <summary>
            /// The data configuration XML
            /// </summary>
            public const string DataConfigurationXml = "RASWiFiProtocol.DataConfiguration.xml";

            /// <summary>
            /// The data configuration rev2 XML
            /// </summary>
            public const string DataConfigurationRev2Xml = "RASWiFiProtocol.DataConfigurationRev2.xml";

            /// <summary>
            /// The UI configuration XML
            /// </summary>
            public const string UIConfigurationXml = "RASWiFiProtocol.UIConfiguration.xml";

            /// <summary>
            /// The error
            /// </summary>
            public const string Error = "Error";

            /// <summary>
            /// The wake up
            /// </summary>
            public const string WakeUp = "WAKE UP";

            /// <summary>
            /// The character
            /// </summary>
            public const string Char = "Char";

            /// <summary>
            /// The not applicable value
            /// </summary>
            public const string NotApplicableValue = "NA";

            /// <summary>
            /// The control name
            /// </summary>
            public const string ControlName = "Control Name";

            public const string strNull = "=Null";

            //CheckClickedValue
            public const string strConstIdle = " Idle";
            public const string strConstClicked = " Clicked";
            public const string strConstHold = " Hold";
            public const string strConstReleased = " Released";
        }
    }
}
