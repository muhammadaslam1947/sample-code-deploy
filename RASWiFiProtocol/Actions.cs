﻿// ***********************************************************************
// Assembly         : Aprilaire.HostController.BusinessDTO
// Author           : 20021497
// Created          : 08-01-2013
//
// Last Modified By : 20021497
// Last Modified On : 08-01-2013
// ***********************************************************************
// <copyright file="Actions.cs" company="Aprilaire">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace RASWiFiProtocol.RASProtocol
{
    #region Enums
    /// <summary>
    /// Enumeration of Actions
    /// </summary>
    public enum Actions
    {
        /// <summary>
        /// The write
        /// </summary>
        [Description("Write", 1)]
        Write = 1,

        /// <summary>
        /// The read request
        /// </summary>
        [Description("Read Request", 2)]
        ReadRequest = 2,

        /// <summary>
        /// The read response
        /// </summary>
        [Description("Read Response", 3)]
        ReadResponse = 3,

        /// <summary>
        /// The COS setting
        /// </summary>
        [Description("COS Setting", 4)]
        COSSetting = 4,

        /// <summary>
        /// The COS
        /// </summary>
        [Description("COS", 5)]
        COS = 5,

        /// <summary>
        /// The acknowledgement and negative acknowledgment.
        /// </summary>
        [Description("Ack/NAck", 6)]
        AckNAck = 6
    }
    #endregion Enums
}