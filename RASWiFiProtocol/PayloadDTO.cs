﻿// ***********************************************************************
// Assembly         : Aprilaire.HostController.BusinessDTO
// Author           : 20021497
// Created          : 08-01-2013
//
// Last Modified By : 20021497
// Last Modified On : 08-09-2013
// ***********************************************************************
// <copyright file="PayloadDTO.cs" company="Aprilaire">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
namespace RASWiFiProtocol.RASProtocol
{
    /// <summary>
    /// Class PayloadDTO
    /// </summary>
    public class PayloadDTO
    {
        public EventHandler OnDTOUpdatedOfAttribute;
        public EventHandler OnDTOUpdatedOfActionFunc; 
        public PayloadDTO()
        {
            Action = 0;
            FunctionalDomain = 0;
            Attribute = 0;
        }

        /// <summary>
        /// Byte to hold the action value
        /// </summary>
        private byte action = 0;

        /// <summary>
        /// Byte to hold the functional Domain value
        /// </summary>
        private byte functionalDomain = 0;

        /// <summary>
        /// Byte to hold the attribute value
        /// </summary>
        private byte attribute = 0;
        /// <summary>
        /// Gets or sets the action.
        /// </summary>
        /// <value>The action.</value>
        public byte Action
        {
            get { return action; }
            set
            {
                if (action != value)
                {
                    action = value;
                    if (OnDTOUpdatedOfActionFunc != null)
                    {
                        OnDTOUpdatedOfActionFunc("Action", new EventArgs());
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the functional domain.
        /// </summary>
        /// <value>The functional domain.</value>
        public byte FunctionalDomain
        {
            get { return functionalDomain; }
            set
            {
                if (functionalDomain != value)
                {
                    functionalDomain = value;
                    if (OnDTOUpdatedOfActionFunc != null)
                    {
                        OnDTOUpdatedOfActionFunc("FunctionalDomain", new EventArgs());
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        /// <value>The attribute.</value>
        public byte Attribute
        {
            get { return attribute; }
            set
            {
                if (attribute != value)
                {
                    attribute = value;
                    if (OnDTOUpdatedOfAttribute != null && attribute!=0)
                    {
                        OnDTOUpdatedOfAttribute("Attribute", new EventArgs());
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the Data.
        /// </summary>
        /// <value>The data.</value>
        public byte[] Data { get; set; }
    }
}
