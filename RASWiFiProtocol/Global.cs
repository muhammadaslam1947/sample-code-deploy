﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
//using Aprilaire.HostController.BusinessDTO;

namespace RASWiFiProtocol.RASProtocol
{
    public static class Global
    {
        public static Dictionary<string, bool> fahrenheitRangeDictionary;
        public static Dictionary<double, double> FahrenheitCelsiusScale;
        //public static string strDataValue = string.Empty;
        //public static List<CommandConfigDTO> listControlVal = new List<CommandConfigDTO>();
        //public static List<ParamDTO> CurrentConfiguredData = new List<ParamDTO>();
        // public static string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Dev\";
        //public static string path = @"C:\Program Files\Dev\";
        //public static  volatile byte SequenceNumber = 0;
        //public static bool IsTestConfigurationClicked = false;
        //static bool programFilePath = Environment.Is64BitOperatingSystem;

        public static void BuildFahrenheitRangeValueDictionary()
        {
            fahrenheitRangeDictionary = new Dictionary<string, bool>();
            fahrenheitRangeDictionary.Add("HeatSetpoint", true);
            fahrenheitRangeDictionary.Add("CoolSetpoint", true);
            fahrenheitRangeDictionary.Add("OutdoorSensorValue", true);
            fahrenheitRangeDictionary.Add("WiredOutdoorSensorValue", true);
            fahrenheitRangeDictionary.Add("Day1HighTemp", true);
            fahrenheitRangeDictionary.Add("Day1LowTemp", true);
            fahrenheitRangeDictionary.Add("Day2HighTemp", true);
            fahrenheitRangeDictionary.Add("Day2LowTemp", true);
            fahrenheitRangeDictionary.Add("Day3HighTemp", true);
            fahrenheitRangeDictionary.Add("Day3LowTemp", true);
            fahrenheitRangeDictionary.Add("WiredRemoteSensorValue", true);
            fahrenheitRangeDictionary.Add("BuiltInSensorValue", true);
            fahrenheitRangeDictionary.Add("WakeHeatSetpoint", true);
            fahrenheitRangeDictionary.Add("WakeCoolSetpoint", true);
            fahrenheitRangeDictionary.Add("LeaveHeatSetpoint", true);
            fahrenheitRangeDictionary.Add("LeaveCoolSetpoint", true);
            fahrenheitRangeDictionary.Add("ReturnHeatSetpoint", true);
            fahrenheitRangeDictionary.Add("ReturnCoolSetpoint", true);
            fahrenheitRangeDictionary.Add("SleepHeatSetpoint", true);
            fahrenheitRangeDictionary.Add("SleepCoolSetpoint", true);
            fahrenheitRangeDictionary.Add("RATSensorValue", true);
            fahrenheitRangeDictionary.Add("LATSensorValue", true);                    
            fahrenheitRangeDictionary.Add("Value", true);
        }

        public static void BuildFahrenheitCelsiusScale()
        {
            FahrenheitCelsiusScale = new Dictionary<double, double>();
            FahrenheitCelsiusScale.Add(-40, -40);
            FahrenheitCelsiusScale.Add(-39, -39.5);
            FahrenheitCelsiusScale.Add(-38, -39);
            FahrenheitCelsiusScale.Add(-37, -38.5);
            FahrenheitCelsiusScale.Add(-36, -38);
            FahrenheitCelsiusScale.Add(-36.1, -37.5);
            FahrenheitCelsiusScale.Add(-35, -37);
            FahrenheitCelsiusScale.Add(-34, -36.5);
            FahrenheitCelsiusScale.Add(-33, -36);
            FahrenheitCelsiusScale.Add(-32, -35.5);
            FahrenheitCelsiusScale.Add(-31, -35);
            FahrenheitCelsiusScale.Add(-30, -34.5);
            FahrenheitCelsiusScale.Add(-29, -34);
            FahrenheitCelsiusScale.Add(-28, -33.5);
            FahrenheitCelsiusScale.Add(-27, -33);
            FahrenheitCelsiusScale.Add(-27.1, -32.5);
            FahrenheitCelsiusScale.Add(-26, -32);
            FahrenheitCelsiusScale.Add(-25, -31.5);
            FahrenheitCelsiusScale.Add(-24, -31);
            FahrenheitCelsiusScale.Add(-23, -30.5);
            FahrenheitCelsiusScale.Add(-22, -30);
            FahrenheitCelsiusScale.Add(-21, -29.5);
            FahrenheitCelsiusScale.Add(-20, -29);
            FahrenheitCelsiusScale.Add(-19, -28.5);
            FahrenheitCelsiusScale.Add(-18, -28);
            FahrenheitCelsiusScale.Add(-18.1, -27.5);
            FahrenheitCelsiusScale.Add(-17, -27);
            FahrenheitCelsiusScale.Add(-16, -26.5);
            FahrenheitCelsiusScale.Add(-15, -26);
            FahrenheitCelsiusScale.Add(-14, -25.5);
            FahrenheitCelsiusScale.Add(-13, -25);
            FahrenheitCelsiusScale.Add(-12, -24.5);
            FahrenheitCelsiusScale.Add(-11, -24);
            FahrenheitCelsiusScale.Add(-10, -23.5);
            FahrenheitCelsiusScale.Add(-9, -23);
            FahrenheitCelsiusScale.Add(-9.1, -22.5);
            FahrenheitCelsiusScale.Add(-8, -22);
            FahrenheitCelsiusScale.Add(-7, -21.5);
            FahrenheitCelsiusScale.Add(-6, -21);
            FahrenheitCelsiusScale.Add(-5, -20.5);
            FahrenheitCelsiusScale.Add(-4, -20);
            FahrenheitCelsiusScale.Add(-3, -19.5);
            FahrenheitCelsiusScale.Add(-2, -19);
            FahrenheitCelsiusScale.Add(-1, -18.5);
            FahrenheitCelsiusScale.Add(0, -18);
            FahrenheitCelsiusScale.Add(1, -17.5);
            FahrenheitCelsiusScale.Add(1.1, -17);
            FahrenheitCelsiusScale.Add(2, -16.5);
            FahrenheitCelsiusScale.Add(3, -16);
            FahrenheitCelsiusScale.Add(4, -15.5);
            FahrenheitCelsiusScale.Add(5, -15);
            FahrenheitCelsiusScale.Add(6, -14.5);
            FahrenheitCelsiusScale.Add(7, -14);
            FahrenheitCelsiusScale.Add(8, -13.5);
            FahrenheitCelsiusScale.Add(9, -13);
            FahrenheitCelsiusScale.Add(10, -12.5);
            FahrenheitCelsiusScale.Add(10.1, -12);
            FahrenheitCelsiusScale.Add(11, -11.5);
            FahrenheitCelsiusScale.Add(12, -11);
            FahrenheitCelsiusScale.Add(13, -10.5);
            FahrenheitCelsiusScale.Add(14, -10);
            FahrenheitCelsiusScale.Add(15, -9.5);
            FahrenheitCelsiusScale.Add(16, -9);
            FahrenheitCelsiusScale.Add(17, -8.5);
            FahrenheitCelsiusScale.Add(18, -8);
            FahrenheitCelsiusScale.Add(19, -7.5);
            FahrenheitCelsiusScale.Add(19.1, -7);
            FahrenheitCelsiusScale.Add(20, -6.5);
            FahrenheitCelsiusScale.Add(21, -6);
            FahrenheitCelsiusScale.Add(22, -5.5);
            FahrenheitCelsiusScale.Add(23, -5);
            FahrenheitCelsiusScale.Add(24, -4.5);
            FahrenheitCelsiusScale.Add(25, -4);
            FahrenheitCelsiusScale.Add(26, -3.5);
            FahrenheitCelsiusScale.Add(27, -3);
            FahrenheitCelsiusScale.Add(28, -2.5);
            FahrenheitCelsiusScale.Add(28.1, -2);
            FahrenheitCelsiusScale.Add(29, -1.5);
            FahrenheitCelsiusScale.Add(30, -1);
            FahrenheitCelsiusScale.Add(31, -0.5);
            FahrenheitCelsiusScale.Add(32, 0);
            FahrenheitCelsiusScale.Add(33, 0.5);
            FahrenheitCelsiusScale.Add(34, 1);
            FahrenheitCelsiusScale.Add(35, 1.5);
            FahrenheitCelsiusScale.Add(36, 2);
            FahrenheitCelsiusScale.Add(37, 2.5);
            FahrenheitCelsiusScale.Add(37.1, 3);
            FahrenheitCelsiusScale.Add(38, 3.5);
            FahrenheitCelsiusScale.Add(39, 4);
            FahrenheitCelsiusScale.Add(40, 4.5);
            FahrenheitCelsiusScale.Add(41, 5);
            FahrenheitCelsiusScale.Add(42, 5.5);
            FahrenheitCelsiusScale.Add(43, 6);
            FahrenheitCelsiusScale.Add(44, 6.5);
            FahrenheitCelsiusScale.Add(45, 7);
            FahrenheitCelsiusScale.Add(46, 7.5);
            FahrenheitCelsiusScale.Add(46.1, 8);
            FahrenheitCelsiusScale.Add(47, 8.5);
            FahrenheitCelsiusScale.Add(48, 9);
            FahrenheitCelsiusScale.Add(49, 9.5);
            FahrenheitCelsiusScale.Add(50, 10);
            FahrenheitCelsiusScale.Add(51, 10.5);
            FahrenheitCelsiusScale.Add(52, 11);
            FahrenheitCelsiusScale.Add(53, 11.5);
            FahrenheitCelsiusScale.Add(54, 12);
            FahrenheitCelsiusScale.Add(55, 12.5);
            FahrenheitCelsiusScale.Add(55.1, 13);
            FahrenheitCelsiusScale.Add(56, 13.5);
            FahrenheitCelsiusScale.Add(57, 14);
            FahrenheitCelsiusScale.Add(58, 14.5);
            FahrenheitCelsiusScale.Add(59, 15);
            FahrenheitCelsiusScale.Add(60, 15.5);
            FahrenheitCelsiusScale.Add(61, 16);
            FahrenheitCelsiusScale.Add(62, 16.5);
            FahrenheitCelsiusScale.Add(63, 17);
            FahrenheitCelsiusScale.Add(64, 17.5);
            FahrenheitCelsiusScale.Add(64.1, 18);
            FahrenheitCelsiusScale.Add(65, 18.5);
            FahrenheitCelsiusScale.Add(66, 19);
            FahrenheitCelsiusScale.Add(67, 19.5);
            FahrenheitCelsiusScale.Add(68, 20);
            FahrenheitCelsiusScale.Add(69, 20.5);
            FahrenheitCelsiusScale.Add(70, 21);
            FahrenheitCelsiusScale.Add(71, 21.5);
            FahrenheitCelsiusScale.Add(72, 22);
            FahrenheitCelsiusScale.Add(73, 22.5);
            FahrenheitCelsiusScale.Add(73.1, 23);
            FahrenheitCelsiusScale.Add(74, 23.5);
            FahrenheitCelsiusScale.Add(75, 24);
            FahrenheitCelsiusScale.Add(76, 24.5);
            FahrenheitCelsiusScale.Add(77, 25);
            FahrenheitCelsiusScale.Add(78, 25.5);
            FahrenheitCelsiusScale.Add(79, 26);
            FahrenheitCelsiusScale.Add(80, 26.5);
            FahrenheitCelsiusScale.Add(81, 27);
            FahrenheitCelsiusScale.Add(82, 27.5);
            FahrenheitCelsiusScale.Add(82.1, 28);
            FahrenheitCelsiusScale.Add(83, 28.5);
            FahrenheitCelsiusScale.Add(84, 29);
            FahrenheitCelsiusScale.Add(85, 29.5);
            FahrenheitCelsiusScale.Add(86, 30);
            FahrenheitCelsiusScale.Add(87, 30.5);
            FahrenheitCelsiusScale.Add(88, 31);
            FahrenheitCelsiusScale.Add(89, 31.5);
            FahrenheitCelsiusScale.Add(90, 32);
            FahrenheitCelsiusScale.Add(91, 32.5);
            FahrenheitCelsiusScale.Add(91.1, 33);
            FahrenheitCelsiusScale.Add(92, 33.5);
            FahrenheitCelsiusScale.Add(93, 34);
            FahrenheitCelsiusScale.Add(94, 34.5);
            FahrenheitCelsiusScale.Add(95, 35);
            FahrenheitCelsiusScale.Add(96, 35.5);
            FahrenheitCelsiusScale.Add(97, 36);
            FahrenheitCelsiusScale.Add(98, 36.5);
            FahrenheitCelsiusScale.Add(99, 37);
            FahrenheitCelsiusScale.Add(100, 37.5);
            FahrenheitCelsiusScale.Add(100.1, 38);
            FahrenheitCelsiusScale.Add(101, 38.5);
            FahrenheitCelsiusScale.Add(102, 39);
            FahrenheitCelsiusScale.Add(103, 39.5);
            FahrenheitCelsiusScale.Add(104, 40);
            FahrenheitCelsiusScale.Add(105, 40.5);
            FahrenheitCelsiusScale.Add(106, 41);
            FahrenheitCelsiusScale.Add(107, 41.5);
            FahrenheitCelsiusScale.Add(108, 42);
            FahrenheitCelsiusScale.Add(109, 42.5);
            FahrenheitCelsiusScale.Add(109.1, 43);
            FahrenheitCelsiusScale.Add(110, 43.5);
            FahrenheitCelsiusScale.Add(111, 44);
            FahrenheitCelsiusScale.Add(112, 44.5);
            FahrenheitCelsiusScale.Add(113, 45);
            FahrenheitCelsiusScale.Add(114, 45.5);
            FahrenheitCelsiusScale.Add(115, 46);
            FahrenheitCelsiusScale.Add(116, 46.5);
            FahrenheitCelsiusScale.Add(117, 47);
            FahrenheitCelsiusScale.Add(118, 47.5);
            FahrenheitCelsiusScale.Add(118.1, 48);
            FahrenheitCelsiusScale.Add(119, 48.5);
            FahrenheitCelsiusScale.Add(120, 49);
            FahrenheitCelsiusScale.Add(121, 49.5);
            FahrenheitCelsiusScale.Add(122, 50);
            FahrenheitCelsiusScale.Add(123, 50.5);
            FahrenheitCelsiusScale.Add(124, 51);
            FahrenheitCelsiusScale.Add(125, 51.5);
            FahrenheitCelsiusScale.Add(126, 52);
            FahrenheitCelsiusScale.Add(127, 52.5);
            FahrenheitCelsiusScale.Add(127.1, 53);
            FahrenheitCelsiusScale.Add(128, 53.5);
            FahrenheitCelsiusScale.Add(129, 54);
            FahrenheitCelsiusScale.Add(130, 54.5);
            FahrenheitCelsiusScale.Add(131, 55);


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strAttributeName"></param>
        /// <returns></returns>
        public static bool CheckForAttributeNeedingFahrenheitRange(string strAttributeName)
        {
            return fahrenheitRangeDictionary.ContainsKey(strAttributeName);
        }
    }
}
