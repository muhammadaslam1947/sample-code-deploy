﻿// ***********************************************************************
// <copyright file="Helpers.cs" company="Aprilaire">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
// Assembly         : Aprilaire.HostController.Common
// Author           : 20021497
// Created          : 08-09-2013
//
// Last Modified By : 20021497
// Last Modified On : 08-09-2013
// ***********************************************************************
namespace RASWiFiProtocol.RASProtocol
{
    using System;
    using System.Reflection;

    /// <summary>
    /// Class Description
    /// </summary>
    public class Description : Attribute
    {
        #region Member variables
        /// <summary>
        /// The text
        /// </summary>
        private string stringValue;

        /// <summary>
        /// The id
        /// </summary>
        private int identity;

        #endregion Member variables

        #region Constructors & Finalizers
        /// <summary>
        /// Initializes a new instance of the <see cref="Description" /> class.
        /// </summary>
        /// <param name="stringValue">The text.</param>
        /// <param name="identity">The id.</param>
        public Description(string stringValue, int identity)
        {
            this.stringValue = stringValue;
            this.identity = identity;
        }

        #endregion Constructors & Finalizers

        #region Public Methods
        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <param name="element">The enumeration element.</param>
        /// <returns>The enumeration value.</returns>
        public static string GetDescription(Enum element)
        {
            Type type = element.GetType();
            string strReturnValue = string.Empty;
            MemberInfo[] memInfo = type.GetMember(element.ToString());
            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attributes = memInfo[0].GetCustomAttributes(typeof(Description), false);
                if (attributes != null && attributes.Length > 0)
                {
                    strReturnValue = ((Description)attributes[0]).stringValue;
                }
                else
                {
                    strReturnValue = string.Empty;
                }
            }
            else
            {
                strReturnValue = string.Empty;
            }

            return strReturnValue;

        }

        /// <summary>
        /// Gets the ID.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="enumerationValue">The enumeration value.</param>
        /// <returns>The enumeration identity.</returns>
        public static int GetID(Type type, string enumerationValue)
        {
            MemberInfo[] memInfo = type.GetMembers();
            for (int j = 0; j < memInfo.Length; j++)
            {
                object[] attributes = memInfo[j].GetCustomAttributes(typeof(Description), false);
                for (int i = 0; i < attributes.Length; i++)
                {
                    if (((Description)attributes[i]).stringValue.Equals(enumerationValue))
                    {
                        return ((Description)attributes[i]).identity;
                    }
                }
            }

            return -1;
        }

        #endregion Methods
    }
}
