﻿

namespace RASWiFiProtocol.RASProtocol
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Collections;
    //using Aprilaire.HostController.Common.Globals;
    //using Aprilaire.HostController.Common.Constants;
    //using Aprilaire.HostController;
    using System.Text.RegularExpressions;
    using LnT.UbiqSens.PluginInterfaces;
    public class DataParsingLogic
    {
        Dictionary<string, bool> fahrenheitRangeDictionary = new Dictionary<string, bool>();
        Dictionary<double, double> FahrenheitCelsiusScale = new Dictionary<double, double>();
        //Logger Object
        public ILogger ObjLog = null;


        public DataParsingLogic(ILogger objLogger)
        {
            this.ObjLog = objLogger;
            BuildFahrenheitRangeValueDictionary();
            BuildFahrenheitCelsiusScale();
        }
        private void BuildFahrenheitRangeValueDictionary()
        {
            fahrenheitRangeDictionary.Add("HeatSetpoint", true);
            fahrenheitRangeDictionary.Add("CoolSetpoint", true);
            fahrenheitRangeDictionary.Add("OutdoorSensorValue", true);
            fahrenheitRangeDictionary.Add("WiredOutdoorSensorValue", true);
            fahrenheitRangeDictionary.Add("Day1HighTemp", true);
            fahrenheitRangeDictionary.Add("Day1LowTemp", true);
            fahrenheitRangeDictionary.Add("Day2HighTemp", true);
            fahrenheitRangeDictionary.Add("Day2LowTemp", true);
            fahrenheitRangeDictionary.Add("Day3HighTemp", true);
            fahrenheitRangeDictionary.Add("Day3LowTemp", true);
            fahrenheitRangeDictionary.Add("WiredRemoteSensorValue", true);
            fahrenheitRangeDictionary.Add("BuiltInSensorValue", true);
            fahrenheitRangeDictionary.Add("WakeHeatSetpoint", true);
            fahrenheitRangeDictionary.Add("WakeCoolSetpoint", true);
            fahrenheitRangeDictionary.Add("LeaveHeatSetpoint", true);
            fahrenheitRangeDictionary.Add("LeaveCoolSetpoint", true);
            fahrenheitRangeDictionary.Add("ReturnHeatSetpoint", true);
            fahrenheitRangeDictionary.Add("ReturnCoolSetpoint", true);
            fahrenheitRangeDictionary.Add("SleepHeatSetpoint", true);
            fahrenheitRangeDictionary.Add("SleepCoolSetpoint", true);
            fahrenheitRangeDictionary.Add("RATSensorValue", true);
            fahrenheitRangeDictionary.Add("LATSensorValue", true);
            fahrenheitRangeDictionary.Add("Value", true);
        }

        private void BuildFahrenheitCelsiusScale()
        {

            FahrenheitCelsiusScale.Add(-40, -40);
            FahrenheitCelsiusScale.Add(-39, -39.5);
            FahrenheitCelsiusScale.Add(-38, -39);
            FahrenheitCelsiusScale.Add(-37, -38.5);
            FahrenheitCelsiusScale.Add(-36, -38);
            FahrenheitCelsiusScale.Add(-36.1, -37.5);
            FahrenheitCelsiusScale.Add(-35, -37);
            FahrenheitCelsiusScale.Add(-34, -36.5);
            FahrenheitCelsiusScale.Add(-33, -36);
            FahrenheitCelsiusScale.Add(-32, -35.5);
            FahrenheitCelsiusScale.Add(-31, -35);
            FahrenheitCelsiusScale.Add(-30, -34.5);
            FahrenheitCelsiusScale.Add(-29, -34);
            FahrenheitCelsiusScale.Add(-28, -33.5);
            FahrenheitCelsiusScale.Add(-27, -33);
            FahrenheitCelsiusScale.Add(-27.1, -32.5);
            FahrenheitCelsiusScale.Add(-26, -32);
            FahrenheitCelsiusScale.Add(-25, -31.5);
            FahrenheitCelsiusScale.Add(-24, -31);
            FahrenheitCelsiusScale.Add(-23, -30.5);
            FahrenheitCelsiusScale.Add(-22, -30);
            FahrenheitCelsiusScale.Add(-21, -29.5);
            FahrenheitCelsiusScale.Add(-20, -29);
            FahrenheitCelsiusScale.Add(-19, -28.5);
            FahrenheitCelsiusScale.Add(-18, -28);
            FahrenheitCelsiusScale.Add(-18.1, -27.5);
            FahrenheitCelsiusScale.Add(-17, -27);
            FahrenheitCelsiusScale.Add(-16, -26.5);
            FahrenheitCelsiusScale.Add(-15, -26);
            FahrenheitCelsiusScale.Add(-14, -25.5);
            FahrenheitCelsiusScale.Add(-13, -25);
            FahrenheitCelsiusScale.Add(-12, -24.5);
            FahrenheitCelsiusScale.Add(-11, -24);
            FahrenheitCelsiusScale.Add(-10, -23.5);
            FahrenheitCelsiusScale.Add(-9, -23);
            FahrenheitCelsiusScale.Add(-9.1, -22.5);
            FahrenheitCelsiusScale.Add(-8, -22);
            FahrenheitCelsiusScale.Add(-7, -21.5);
            FahrenheitCelsiusScale.Add(-6, -21);
            FahrenheitCelsiusScale.Add(-5, -20.5);
            FahrenheitCelsiusScale.Add(-4, -20);
            FahrenheitCelsiusScale.Add(-3, -19.5);
            FahrenheitCelsiusScale.Add(-2, -19);
            FahrenheitCelsiusScale.Add(-1, -18.5);
            FahrenheitCelsiusScale.Add(0, -18);
            FahrenheitCelsiusScale.Add(1, -17.5);
            FahrenheitCelsiusScale.Add(1.1, -17);
            FahrenheitCelsiusScale.Add(2, -16.5);
            FahrenheitCelsiusScale.Add(3, -16);
            FahrenheitCelsiusScale.Add(4, -15.5);
            FahrenheitCelsiusScale.Add(5, -15);
            FahrenheitCelsiusScale.Add(6, -14.5);
            FahrenheitCelsiusScale.Add(7, -14);
            FahrenheitCelsiusScale.Add(8, -13.5);
            FahrenheitCelsiusScale.Add(9, -13);
            FahrenheitCelsiusScale.Add(10, -12.5);
            FahrenheitCelsiusScale.Add(10.1, -12);
            FahrenheitCelsiusScale.Add(11, -11.5);
            FahrenheitCelsiusScale.Add(12, -11);
            FahrenheitCelsiusScale.Add(13, -10.5);
            FahrenheitCelsiusScale.Add(14, -10);
            FahrenheitCelsiusScale.Add(15, -9.5);
            FahrenheitCelsiusScale.Add(16, -9);
            FahrenheitCelsiusScale.Add(17, -8.5);
            FahrenheitCelsiusScale.Add(18, -8);
            FahrenheitCelsiusScale.Add(19, -7.5);
            FahrenheitCelsiusScale.Add(19.1, -7);
            FahrenheitCelsiusScale.Add(20, -6.5);
            FahrenheitCelsiusScale.Add(21, -6);
            FahrenheitCelsiusScale.Add(22, -5.5);
            FahrenheitCelsiusScale.Add(23, -5);
            FahrenheitCelsiusScale.Add(24, -4.5);
            FahrenheitCelsiusScale.Add(25, -4);
            FahrenheitCelsiusScale.Add(26, -3.5);
            FahrenheitCelsiusScale.Add(27, -3);
            FahrenheitCelsiusScale.Add(28, -2.5);
            FahrenheitCelsiusScale.Add(28.1, -2);
            FahrenheitCelsiusScale.Add(29, -1.5);
            FahrenheitCelsiusScale.Add(30, -1);
            FahrenheitCelsiusScale.Add(31, -0.5);
            FahrenheitCelsiusScale.Add(32, 0);
            FahrenheitCelsiusScale.Add(33, 0.5);
            FahrenheitCelsiusScale.Add(34, 1);
            FahrenheitCelsiusScale.Add(35, 1.5);
            FahrenheitCelsiusScale.Add(36, 2);
            FahrenheitCelsiusScale.Add(37, 2.5);
            FahrenheitCelsiusScale.Add(37.1, 3);
            FahrenheitCelsiusScale.Add(38, 3.5);
            FahrenheitCelsiusScale.Add(39, 4);
            FahrenheitCelsiusScale.Add(40, 4.5);
            FahrenheitCelsiusScale.Add(41, 5);
            FahrenheitCelsiusScale.Add(42, 5.5);
            FahrenheitCelsiusScale.Add(43, 6);
            FahrenheitCelsiusScale.Add(44, 6.5);
            FahrenheitCelsiusScale.Add(45, 7);
            FahrenheitCelsiusScale.Add(46, 7.5);
            FahrenheitCelsiusScale.Add(46.1, 8);
            FahrenheitCelsiusScale.Add(47, 8.5);
            FahrenheitCelsiusScale.Add(48, 9);
            FahrenheitCelsiusScale.Add(49, 9.5);
            FahrenheitCelsiusScale.Add(50, 10);
            FahrenheitCelsiusScale.Add(51, 10.5);
            FahrenheitCelsiusScale.Add(52, 11);
            FahrenheitCelsiusScale.Add(53, 11.5);
            FahrenheitCelsiusScale.Add(54, 12);
            FahrenheitCelsiusScale.Add(55, 12.5);
            FahrenheitCelsiusScale.Add(55.1, 13);
            FahrenheitCelsiusScale.Add(56, 13.5);
            FahrenheitCelsiusScale.Add(57, 14);
            FahrenheitCelsiusScale.Add(58, 14.5);
            FahrenheitCelsiusScale.Add(59, 15);
            FahrenheitCelsiusScale.Add(60, 15.5);
            FahrenheitCelsiusScale.Add(61, 16);
            FahrenheitCelsiusScale.Add(62, 16.5);
            FahrenheitCelsiusScale.Add(63, 17);
            FahrenheitCelsiusScale.Add(64, 17.5);
            FahrenheitCelsiusScale.Add(64.1, 18);
            FahrenheitCelsiusScale.Add(65, 18.5);
            FahrenheitCelsiusScale.Add(66, 19);
            FahrenheitCelsiusScale.Add(67, 19.5);
            FahrenheitCelsiusScale.Add(68, 20);
            FahrenheitCelsiusScale.Add(69, 20.5);
            FahrenheitCelsiusScale.Add(70, 21);
            FahrenheitCelsiusScale.Add(71, 21.5);
            FahrenheitCelsiusScale.Add(72, 22);
            FahrenheitCelsiusScale.Add(73, 22.5);
            FahrenheitCelsiusScale.Add(73.1, 23);
            FahrenheitCelsiusScale.Add(74, 23.5);
            FahrenheitCelsiusScale.Add(75, 24);
            FahrenheitCelsiusScale.Add(76, 24.5);
            FahrenheitCelsiusScale.Add(77, 25);
            FahrenheitCelsiusScale.Add(78, 25.5);
            FahrenheitCelsiusScale.Add(79, 26);
            FahrenheitCelsiusScale.Add(80, 26.5);
            FahrenheitCelsiusScale.Add(81, 27);
            FahrenheitCelsiusScale.Add(82, 27.5);
            FahrenheitCelsiusScale.Add(82.1, 28);
            FahrenheitCelsiusScale.Add(83, 28.5);
            FahrenheitCelsiusScale.Add(84, 29);
            FahrenheitCelsiusScale.Add(85, 29.5);
            FahrenheitCelsiusScale.Add(86, 30);
            FahrenheitCelsiusScale.Add(87, 30.5);
            FahrenheitCelsiusScale.Add(88, 31);
            FahrenheitCelsiusScale.Add(89, 31.5);
            FahrenheitCelsiusScale.Add(90, 32);
            FahrenheitCelsiusScale.Add(91, 32.5);
            FahrenheitCelsiusScale.Add(91.1, 33);
            FahrenheitCelsiusScale.Add(92, 33.5);
            FahrenheitCelsiusScale.Add(93, 34);
            FahrenheitCelsiusScale.Add(94, 34.5);
            FahrenheitCelsiusScale.Add(95, 35);
            FahrenheitCelsiusScale.Add(96, 35.5);
            FahrenheitCelsiusScale.Add(97, 36);
            FahrenheitCelsiusScale.Add(98, 36.5);
            FahrenheitCelsiusScale.Add(99, 37);
            FahrenheitCelsiusScale.Add(100, 37.5);
            FahrenheitCelsiusScale.Add(100.1, 38);
            FahrenheitCelsiusScale.Add(101, 38.5);
            FahrenheitCelsiusScale.Add(102, 39);
            FahrenheitCelsiusScale.Add(103, 39.5);
            FahrenheitCelsiusScale.Add(104, 40);
            FahrenheitCelsiusScale.Add(105, 40.5);
            FahrenheitCelsiusScale.Add(106, 41);
            FahrenheitCelsiusScale.Add(107, 41.5);
            FahrenheitCelsiusScale.Add(108, 42);
            FahrenheitCelsiusScale.Add(109, 42.5);
            FahrenheitCelsiusScale.Add(109.1, 43);
            FahrenheitCelsiusScale.Add(110, 43.5);
            FahrenheitCelsiusScale.Add(111, 44);
            FahrenheitCelsiusScale.Add(112, 44.5);
            FahrenheitCelsiusScale.Add(113, 45);
            FahrenheitCelsiusScale.Add(114, 45.5);
            FahrenheitCelsiusScale.Add(115, 46);
            FahrenheitCelsiusScale.Add(116, 46.5);
            FahrenheitCelsiusScale.Add(117, 47);
            FahrenheitCelsiusScale.Add(118, 47.5);
            FahrenheitCelsiusScale.Add(118.1, 48);
            FahrenheitCelsiusScale.Add(119, 48.5);
            FahrenheitCelsiusScale.Add(120, 49);
            FahrenheitCelsiusScale.Add(121, 49.5);
            FahrenheitCelsiusScale.Add(122, 50);
            FahrenheitCelsiusScale.Add(123, 50.5);
            FahrenheitCelsiusScale.Add(124, 51);
            FahrenheitCelsiusScale.Add(125, 51.5);
            FahrenheitCelsiusScale.Add(126, 52);
            FahrenheitCelsiusScale.Add(127, 52.5);
            FahrenheitCelsiusScale.Add(127.1, 53);
            FahrenheitCelsiusScale.Add(128, 53.5);
            FahrenheitCelsiusScale.Add(129, 54);
            FahrenheitCelsiusScale.Add(130, 54.5);
            FahrenheitCelsiusScale.Add(131, 55);


        }

        #region Decleration
        /// <summary>
        /// 
        /// </summary>
        int intByteValue = 0;

        /// <summary>
        /// 
        /// </summary>
        int intIndexOfChildNode = 0;

        /// <summary>
        /// 
        /// </summary>
        byte byte0 = 0x00;

        /// <summary>
        /// 
        /// </summary>
        byte byte1 = 0x00;

        /// <summary>
        /// 
        /// </summary>
        double dbVal = 0;

        /// <summary>
        /// 
        /// </summary>
        string strTemperature = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public static string strAction = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public static bool clearclicked = false;

        /// <summary>
        /// 
        /// </summary>
        public static bool logclicked = false;

        /// <summary>
        /// 
        /// </summary>
        public static bool advanceclicked = false;

        /// <summary>
        /// 
        /// </summary>
        public static bool sendallClicked = false;

        //Hemanth added the code.
        public static byte HeatSetpointRawValue = 0;

        public static byte coolSetpointRawvalue = 0;

        public static string temparatureValue = string.Empty;
        #endregion

        #region Manufacturing Test
        /// <summary>
        /// Method to parse Manufacturing Test Data
        /// </summary>
        /// <param name="strAttribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        /// <returns></returns>
        public List<string> ParseManufacturingTest(string strAttribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            lststrPacketData.Clear();
            string returnValue = string.Empty;

            double val = 0;


            switch (strAttribute)
            {
                case "SetManufacturingTest":
                    int i = int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    if (strAction.Equals("COS Setting"))
                    {
                        DisplayCOSSetting(xmlNode, lststrPacketData, i);
                    }
                    else
                    {
                        if (null != xmlNode.ChildNodes[i] && !(i >= xmlNode.ChildNodes[0].ChildNodes[0].ChildNodes.Count))
                            lststrPacketData.Add(xmlNode.ChildNodes[0].Name + "=" + xmlNode.ChildNodes[0].ChildNodes[0].ChildNodes[i].InnerText.ToString());
                        else
                            lststrPacketData.Add(xmlNode.ChildNodes[0].Name + "=Unknown");
                    }

                    break;

                case "ManufacturingTest":
                    if (strAction.Equals("COS Setting"))
                    {
                        DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                    }
                    else
                    {
                        for (int byteIndex = 0; byteIndex < strDataPacket.Length; byteIndex++)
                        {
                            switch (byteIndex)
                            {

                                case 0:
                                    string buttonstatus = string.Empty;
                                    byte b = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    //IAQ button
                                    bool bit0 = (b & (1 << 0)) != 0;
                                    bool bit1 = (b & (1 << 1)) != 0;

                                    buttonstatus = CheckClickedValue(bit1, bit0);
                                    lststrPacketData.Add("IAQ" + " =" + buttonstatus);

                                    //Up  Button
                                    bool bit2 = (b & (1 << 2)) != 0;
                                    bool bit3 = (b & (1 << 3)) != 0;
                                    buttonstatus = CheckClickedValue(bit3, bit2);
                                    lststrPacketData.Add("UP" + " =" + buttonstatus);

                                    //Down Button
                                    bool bit4 = (b & (1 << 4)) != 0;
                                    bool bit5 = (b & (1 << 5)) != 0;
                                    buttonstatus = CheckClickedValue(bit5, bit4);
                                    lststrPacketData.Add("DOWN" + " =" + buttonstatus);

                                    //SCENE/USER2 Button
                                    bool bit6 = (b & (1 << 6)) != 0;
                                    bool bit7 = (b & (1 << 7)) != 0;
                                    buttonstatus = CheckClickedValue(bit7, bit6);
                                    lststrPacketData.Add("USER2" + "=" + buttonstatus);
                                    break;

                                case 1:
                                    byte b2 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    string buttonstatus2 = string.Empty;
                                    //HOLD  button
                                    bool bit00 = (b2 & (1 << 0)) != 0;
                                    bool bit11 = (b2 & (1 << 1)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit11, bit00);
                                    lststrPacketData.Add("HOLD" + "=" + buttonstatus2);

                                    //MODE   Button
                                    bool bit22 = (b2 & (1 << 2)) != 0;
                                    bool bit33 = (b2 & (1 << 3)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit33, bit22);
                                    lststrPacketData.Add("MODE" + "=" + buttonstatus2);

                                    //FAN  Button
                                    bool bit44 = (b2 & (1 << 4)) != 0;

                                    bool bit55 = (b2 & (1 << 5)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit55, bit44);
                                    lststrPacketData.Add("FAN" + "=" + buttonstatus2);
                                    break;

                                //HP/HC switch
                                case 2:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[1] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[1].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[1].Attributes[1].Value + "=Unknown");
                                    break;

                                    lststrPacketData.Add(xmlNode.ChildNodes[1].Name + " =" + returnValue);
                                    break;

                                //L Fault
                                case 3:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[2] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[2].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[2].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[2].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[2].Attributes[1].Value + "=Unknown");
                                    break;

                                    lststrPacketData.Add(xmlNode.ChildNodes[1].Name + "=" + returnValue);
                                    break;

                                //Built in thermistor temperature
                                case 4:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 5:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[3].Attributes[1].Value + "=" + val + " F");
                                    break;

                                //Wired ODT
                                case 6:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 7:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[4].Attributes[1].Value + "=" + val + "");
                                    break;

                                //Remote temperature
                                case 8:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 9:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte0, byte1);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[5].Attributes[1].Value + "=" + val + "");
                                    break;

                                //Built in Humidity Sensor
                                case 10:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 11:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 10;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[6].Attributes[1].Value + "=" + val + " % RH");
                                    break;

                                //AC Status
                                case 12:
                                    int i3 = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[7] && !(i3 >= xmlNode.ChildNodes[1].ChildNodes[7].ChildNodes.Count))
                                        returnValue = "   " + xmlNode.ChildNodes[1].ChildNodes[7].ChildNodes[i3].InnerText.ToString();
                                    else
                                        returnValue = "  Unknown";


                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[7].Attributes[1].Value + "=" + returnValue);
                                    break;

                                //Battery 1 Status
                                case 13:
                                    int i4 = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[8] && !(i4 >= xmlNode.ChildNodes[1].ChildNodes[8].ChildNodes.Count))
                                        returnValue = " " + xmlNode.ChildNodes[1].ChildNodes[8].ChildNodes[i4].InnerText.ToString();
                                    else
                                        returnValue = "   Unknown";

                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[8].Attributes[1].Value + "=" + returnValue);
                                    break;

                                // Battery1 Voltage
                                case 14:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 15:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[9].Attributes[1].Value + "=" + val + " V");
                                    break;

                                //Battery 2 Status
                                case 16:
                                    int i5 = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[10] && !(i5 >= xmlNode.ChildNodes[1].ChildNodes[10].ChildNodes.Count))
                                        returnValue = "  " + xmlNode.ChildNodes[1].ChildNodes[10].ChildNodes[i5].InnerText.ToString();
                                    else
                                        returnValue = "  Unknown";

                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[10].Attributes[1].Value + "=" + returnValue);
                                    break;

                                //Battery2 Voltage
                                case 17:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 18:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[11].Attributes[1].Value + "=" + val + " V");
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    break;

                case "ManufacturingRevisionWrite":
                    if (strAction.Equals("COS Setting"))
                    {
                        DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                    }
                    else
                    {
                        int ij = int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                        byte[] temp2 = new byte[] { (byte)ij };
                        char[] temp = ASCIIEncoding.ASCII.GetChars(temp2);
                        if (ij < 65)
                        {
                            lststrPacketData.Add("Revision =" + (double.Parse(temp[0].ToString()) / (double)10.0));
                        }
                        else
                        {
                            lststrPacketData.Add("Revision =" + temp[0].ToString());
                        }
                    }
                    break;

                case "RadioTestStatus":
                    for (int j = 0; j < strDataPacket.Length; j++)
                    {
                        j = int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                        if (strAction.Equals("COS Setting"))
                        {
                            DisplayCOSSetting(xmlNode, lststrPacketData, j);
                        }
                        else
                        {
                            if (null != xmlNode.ChildNodes[j] && !(j >= xmlNode.ChildNodes[0].ChildNodes[0].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[3].Name + "=" + xmlNode.ChildNodes[3].ChildNodes[0].ChildNodes[j].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[0].Name + "=Unknown");
                        }
                    }

                    break;

                case "LEDCalibrationStatus":

                    for (int k = 0; k < strDataPacket.Length; k++)
                    {

                        k = int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                        if (strAction.Equals("COS Setting"))
                        {
                            DisplayCOSSetting(xmlNode, lststrPacketData, k);
                        }
                        else
                        {
                            if (null != xmlNode.ChildNodes[k] && !(k >= xmlNode.ChildNodes[0].ChildNodes[0].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[4].Name + "=" + xmlNode.ChildNodes[4].ChildNodes[0].ChildNodes[k].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[0].Name + "=Unknown");
                        }
                    }

                    break;

                default:
                    returnValue = string.Empty;
                    break;
            }

            return lststrPacketData;
        }

        #endregion

        #region New Manufacturing Test v2.0
        /// <summary>
        /// Method to parse Manufacturing Test Data
        /// </summary>
        /// <param name="strAttribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        /// <returns></returns>
        public List<string> ParseNewManufacturingTest(string strAttribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            lststrPacketData.Clear();
            string returnValue = string.Empty;

            double val = 0;


            switch (strAttribute)
            {
                case "SetManufacturingTest":
                    int i = int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    if (strAction.Equals("COS Setting"))
                    {
                        DisplayCOSSetting(xmlNode, lststrPacketData, i);
                    }
                    else
                    {
                        if (null != xmlNode.ChildNodes[i] && !(i >= xmlNode.ChildNodes[0].ChildNodes[0].ChildNodes.Count))
                            lststrPacketData.Add(xmlNode.ChildNodes[0].Name + "=" + xmlNode.ChildNodes[0].ChildNodes[0].ChildNodes[i].InnerText.ToString());
                        else
                            lststrPacketData.Add(xmlNode.ChildNodes[0].Name + "=Unknown");
                    }

                    break;

                case "ManufacturingTest":
                    if (strAction.Equals("COS Setting"))
                    {
                        DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                    }
                    else
                    {
                        for (int byteIndex = 0; byteIndex < strDataPacket.Length; byteIndex++)
                        {
                            switch (byteIndex)
                            {

                                case 0:
                                    string buttonstatus = string.Empty;
                                    byte b = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    //IAQ button
                                    bool bit0 = (b & (1 << 0)) != 0;
                                    bool bit1 = (b & (1 << 1)) != 0;

                                    buttonstatus = CheckClickedValue(bit1, bit0);
                                    lststrPacketData.Add("IAQ" + " =" + buttonstatus);

                                    //Up  Button
                                    bool bit2 = (b & (1 << 2)) != 0;
                                    bool bit3 = (b & (1 << 3)) != 0;
                                    buttonstatus = CheckClickedValue(bit3, bit2);
                                    lststrPacketData.Add("UP" + " =" + buttonstatus);

                                    //Down Button
                                    bool bit4 = (b & (1 << 4)) != 0;
                                    bool bit5 = (b & (1 << 5)) != 0;
                                    buttonstatus = CheckClickedValue(bit5, bit4);
                                    lststrPacketData.Add("DOWN" + " =" + buttonstatus);

                                    //SCENE/USER2 Button
                                    bool bit6 = (b & (1 << 6)) != 0;
                                    bool bit7 = (b & (1 << 7)) != 0;
                                    buttonstatus = CheckClickedValue(bit7, bit6);
                                    lststrPacketData.Add("USER2" + "=" + buttonstatus);
                                    break;

                                case 1:
                                    byte b2 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    string buttonstatus2 = string.Empty;
                                    //HOLD  button
                                    bool bit00 = (b2 & (1 << 0)) != 0;
                                    bool bit11 = (b2 & (1 << 1)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit11, bit00);
                                    lststrPacketData.Add("HOLD" + "=" + buttonstatus2);

                                    //MODE   Button
                                    bool bit22 = (b2 & (1 << 2)) != 0;
                                    bool bit33 = (b2 & (1 << 3)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit33, bit22);
                                    lststrPacketData.Add("MODE" + "=" + buttonstatus2);

                                    //FAN  Button
                                    bool bit44 = (b2 & (1 << 4)) != 0;

                                    bool bit55 = (b2 & (1 << 5)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit55, bit44);
                                    lststrPacketData.Add("FAN" + "=" + buttonstatus2);
                                    break;

                                //AC Status
                                case 2:
                                    int i3 = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[7] && !(i3 >= xmlNode.ChildNodes[1].ChildNodes[7].ChildNodes.Count))
                                        returnValue = "   " + xmlNode.ChildNodes[1].ChildNodes[7].ChildNodes[i3].InnerText.ToString();
                                    else
                                        returnValue = "  Unknown";


                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[7].Attributes[1].Value + "=" + returnValue);
                                    break;

                                //Battery 1 Status
                                case 3:
                                    int i4 = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[8] && !(i4 >= xmlNode.ChildNodes[1].ChildNodes[8].ChildNodes.Count))
                                        returnValue = " " + xmlNode.ChildNodes[1].ChildNodes[8].ChildNodes[i4].InnerText.ToString();
                                    else
                                        returnValue = "   Unknown";

                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[8].Attributes[1].Value + "=" + returnValue);
                                    break;

                                // Battery1 Voltage
                                case 5:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 6:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[9].Attributes[1].Value + "=" + val + " V");
                                    break;

                                //Built in thermistor temperature
                                case 9:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 10:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[3].Attributes[1].Value + "=" + val + " F");
                                    break;

                                //Remote temperature
                                case 11:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 12:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte0, byte1);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[5].Attributes[1].Value + "=" + val + "");
                                    break;


                                //Built in Humidity Sensor
                                case 13:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 14:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 10;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[6].Attributes[1].Value + "=" + val + " % RH");
                                    break;


                                //HP/HC switch
                                case 15:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[1] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[1].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[1].Attributes[1].Value + "=Unknown");
                                    break;

                                    lststrPacketData.Add(xmlNode.ChildNodes[1].Name + " =" + returnValue);
                                    break;

                                //Wired ODT
                                case 16:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 17:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[4].Attributes[1].Value + "=" + val + "");
                                    break;

                                //L Fault
                                case 18:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[2] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[2].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[2].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[2].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[2].Attributes[1].Value + "=Unknown");
                                    break;

                                    lststrPacketData.Add(xmlNode.ChildNodes[1].Name + "=" + returnValue);
                                    break;


                                default:
                                    break;
                            }
                        }
                    }
                    break;

                default:
                    returnValue = string.Empty;
                    break;
            }

            return lststrPacketData;
        }

        #endregion

        #region Manufacturing Test 4.0 debug
        /// <summary>
        /// Method to parse Manufacturing Test Data
        /// </summary>
        /// <param name="strAttribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        /// <returns></returns>

        public List<string> ParseManufacturingTestV4(string strAttribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            lststrPacketData.Clear();
            string returnValue = string.Empty;

            double val = 0;


            switch (strAttribute)
            {
                case "SetManufacturingTest":
                    int i = int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    if (strAction.Equals("COS Setting"))
                    {
                        DisplayCOSSetting(xmlNode, lststrPacketData, i);
                    }
                    else
                    {
                        if (null != xmlNode.ChildNodes[i] && !(i >= xmlNode.ChildNodes[0].ChildNodes[0].ChildNodes.Count))
                            lststrPacketData.Add(xmlNode.ChildNodes[0].Name + "=" + xmlNode.ChildNodes[0].ChildNodes[0].ChildNodes[i].InnerText.ToString());
                        else
                            lststrPacketData.Add(xmlNode.ChildNodes[0].Name + "=Unknown");
                    }

                    break;

                case "ManufacturingTest":
                    if (strAction.Equals("COS Setting"))
                    {
                        DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                    }
                    else
                    {
                        for (int byteIndex = 0; byteIndex < strDataPacket.Length; byteIndex++)
                        {
                            switch (byteIndex)
                            {

                                case 0:
                                    string buttonstatus = string.Empty;
                                    byte b = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    //IAQ button
                                    bool bit0 = (b & (1 << 0)) != 0;
                                    bool bit1 = (b & (1 << 1)) != 0;

                                    buttonstatus = CheckClickedValue(bit1, bit0);
                                    lststrPacketData.Add("IAQ" + " =" + buttonstatus);

                                    //Up  Button
                                    bool bit2 = (b & (1 << 2)) != 0;
                                    bool bit3 = (b & (1 << 3)) != 0;
                                    buttonstatus = CheckClickedValue(bit3, bit2);
                                    lststrPacketData.Add("UP" + " =" + buttonstatus);

                                    //Down Button
                                    bool bit4 = (b & (1 << 4)) != 0;
                                    bool bit5 = (b & (1 << 5)) != 0;
                                    buttonstatus = CheckClickedValue(bit5, bit4);
                                    lststrPacketData.Add("DOWN" + " =" + buttonstatus);

                                    //SCENE/USER2 Button
                                    bool bit6 = (b & (1 << 6)) != 0;
                                    bool bit7 = (b & (1 << 7)) != 0;
                                    buttonstatus = CheckClickedValue(bit7, bit6);
                                    lststrPacketData.Add("USER2" + "=" + buttonstatus);
                                    break;

                                case 1:
                                    byte b2 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    string buttonstatus2 = string.Empty;
                                    //HOLD  button
                                    bool bit00 = (b2 & (1 << 0)) != 0;
                                    bool bit11 = (b2 & (1 << 1)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit11, bit00);
                                    lststrPacketData.Add("HOLD" + "=" + buttonstatus2);

                                    //MODE   Button
                                    bool bit22 = (b2 & (1 << 2)) != 0;
                                    bool bit33 = (b2 & (1 << 3)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit33, bit22);
                                    lststrPacketData.Add("MODE" + "=" + buttonstatus2);

                                    //FAN  Button
                                    bool bit44 = (b2 & (1 << 4)) != 0;

                                    bool bit55 = (b2 & (1 << 5)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit55, bit44);
                                    lststrPacketData.Add("FAN" + "=" + buttonstatus2);
                                    break;

                                //AC Status
                                case 2:
                                    int i3 = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[1] && !(i3 >= xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes.Count))
                                        returnValue = "   " + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[i3].InnerText.ToString();
                                    else
                                        returnValue = "  Unknown";


                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[1].Attributes[1].Value + "=" + returnValue);
                                    break;

                                //Built in thermistor temperature
                                case 9:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 10:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[2].Attributes[1].Value + "=" + val + " F");
                                    break;

                                //Remote temperature
                                case 11:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 12:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte0, byte1);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[3].Attributes[1].Value + "=" + val + "");
                                    break;


                                //Built in Humidity Sensor
                                case 13:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 14:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 10;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[4].Attributes[1].Value + "=" + val + " % RH");
                                    break;


                                //HP/HC switch
                                case 15:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[5] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[5].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[5].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[5].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[5].Attributes[1].Value + "=Unknown");
                                    break;

                                //Wired ODT
                                case 16:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 17:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[6].Attributes[1].Value + "=" + val + "");
                                    break;

                                //L Fault
                                case 18:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[7] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[7].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[7].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[7].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[7].Attributes[1].Value + "=Unknown");
                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                    break;

                default:
                    returnValue = string.Empty;
                    break;
            }

            return lststrPacketData;
        }

        #endregion

        #region Manufacturing Test 5.0
        /// <summary>
        /// Method to parse Manufacturing Test Data
        /// </summary>
        /// <param name="strAttribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        /// <returns></returns>
        public List<string> ParseManufacturingTestV5(string strAttribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            lststrPacketData.Clear();
            string returnValue = string.Empty;

            double val = 0;


            switch (strAttribute)
            {
                case "SetManufacturingTest":
                    int i = int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    if (strAction.Equals("COS Setting"))
                    {
                        DisplayCOSSetting(xmlNode, lststrPacketData, i);
                    }
                    else
                    {
                        if (null != xmlNode.ChildNodes[i] && !(i >= xmlNode.ChildNodes[0].ChildNodes[0].ChildNodes.Count))
                            lststrPacketData.Add(xmlNode.ChildNodes[0].Name + "=" + xmlNode.ChildNodes[0].ChildNodes[0].ChildNodes[i].InnerText.ToString());
                        else
                            lststrPacketData.Add(xmlNode.ChildNodes[0].Name + "=Unknown");
                    }

                    break;

                case "ManufacturingTest":
                    if (strAction.Equals("COS Setting"))
                    {
                        DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                    }
                    else
                    {
                        for (int byteIndex = 0; byteIndex < strDataPacket.Length; byteIndex++)
                        {
                            switch (byteIndex)
                            {

                                case 0:
                                    string buttonstatus = string.Empty;
                                    byte b = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    //IAQ button
                                    bool bit0 = (b & (1 << 0)) != 0;
                                    bool bit1 = (b & (1 << 1)) != 0;

                                    buttonstatus = CheckClickedValue(bit1, bit0);
                                    lststrPacketData.Add("IAQ" + " =" + buttonstatus);

                                    //Up  Button
                                    bool bit2 = (b & (1 << 2)) != 0;
                                    bool bit3 = (b & (1 << 3)) != 0;
                                    buttonstatus = CheckClickedValue(bit3, bit2);
                                    lststrPacketData.Add("UP" + " =" + buttonstatus);

                                    //Down Button
                                    bool bit4 = (b & (1 << 4)) != 0;
                                    bool bit5 = (b & (1 << 5)) != 0;
                                    buttonstatus = CheckClickedValue(bit5, bit4);
                                    lststrPacketData.Add("DOWN" + " =" + buttonstatus);

                                    //SCENE/USER2 Button
                                    bool bit6 = (b & (1 << 6)) != 0;
                                    bool bit7 = (b & (1 << 7)) != 0;
                                    buttonstatus = CheckClickedValue(bit7, bit6);
                                    lststrPacketData.Add("USER2" + "=" + buttonstatus);
                                    break;

                                case 1:
                                    byte b2 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    string buttonstatus2 = string.Empty;
                                    //HOLD  button
                                    bool bit00 = (b2 & (1 << 0)) != 0;
                                    bool bit11 = (b2 & (1 << 1)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit11, bit00);
                                    lststrPacketData.Add("HOLD" + "=" + buttonstatus2);

                                    //MODE   Button
                                    bool bit22 = (b2 & (1 << 2)) != 0;
                                    bool bit33 = (b2 & (1 << 3)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit33, bit22);
                                    lststrPacketData.Add("MODE" + "=" + buttonstatus2);

                                    //FAN  Button
                                    bool bit44 = (b2 & (1 << 4)) != 0;

                                    bool bit55 = (b2 & (1 << 5)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit55, bit44);
                                    lststrPacketData.Add("FAN" + "=" + buttonstatus2);
                                    break;

                                //AC Status
                                case 2:
                                    intIndexOfChildNode = returnIndexOfItem(strAttribute, xmlNode);
                                    int i3 = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[1] && !(i3 >= xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes.Count))
                                        returnValue = "   " + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[i3].InnerText.ToString();
                                    else
                                        returnValue = "  Unknown";

                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[1].Attributes[1].Value + "=" + returnValue);
                                    break;

                                //Built in thermistor temperature
                                case 3:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 4:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[2].Attributes[1].Value + "=" + val + " F");
                                    break;

                                //Remote temperature
                                case 5:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 6:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte0, byte1);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[3].Attributes[1].Value + "=" + val + "");
                                    break;

                                //Built in Humidity Sensor
                                case 7:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 8:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 10;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[4].Attributes[1].Value + "=" + val + " % RH");
                                    break;

                                //HP/HC switch
                                case 9:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[5] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[5].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[5].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[5].Attributes[1].Value + "=Unknown");
                                    break;

                                //Wired ODT
                                case 10:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 11:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[6].Attributes[1].Value + "=" + val + "");
                                    break;

                                //L Fault
                                case 12:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[7] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[7].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[7].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[2].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[7].Attributes[1].Value + "=Unknown");
                                    break;
                            }
                        }
                    }
                    break;
                default:
                    returnValue = string.Empty;
                    break;
            }

            return lststrPacketData;
        }

        #endregion

        #region Manufacturing Test 9.0
        /// <summary>
        /// Method to parse Manufacturing Test Data
        /// </summary>
        /// <param name="strAttribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        /// <returns></returns>
        public List<string> ParseManufacturingTestV9(string strAttribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            lststrPacketData.Clear();
            string returnValue = string.Empty;

            double val = 0;


            switch (strAttribute)
            {
                case "SetManufacturingTest":
                    int i = int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    if (strAction.Equals("COS Setting"))
                    {
                        DisplayCOSSetting(xmlNode, lststrPacketData, i);
                    }
                    else
                    {
                        if (null != xmlNode.ChildNodes[i] && !(i >= xmlNode.ChildNodes[0].ChildNodes[0].ChildNodes.Count))
                            lststrPacketData.Add(xmlNode.ChildNodes[0].Name + "=" + xmlNode.ChildNodes[0].ChildNodes[0].ChildNodes[i].InnerText.ToString());
                        else
                            lststrPacketData.Add(xmlNode.ChildNodes[0].Name + "=Unknown");
                    }

                    break;

                case "ManufacturingTest":
                    if (strAction.Equals("COS Setting"))
                    {
                        DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                    }
                    else
                    {
                        for (int byteIndex = 0; byteIndex < strDataPacket.Length; byteIndex++)
                        {
                            switch (byteIndex)
                            {

                                case 0:
                                    string buttonstatus = string.Empty;
                                    byte b = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    //FAN button
                                    bool bit0 = (b & (1 << 0)) != 0;
                                    bool bit1 = (b & (1 << 1)) != 0;

                                    buttonstatus = CheckClickedValue(bit1, bit0);
                                    lststrPacketData.Add("FAN" + " =" + buttonstatus);

                                    //Mode  Button
                                    bool bit2 = (b & (1 << 2)) != 0;
                                    bool bit3 = (b & (1 << 3)) != 0;
                                    buttonstatus = CheckClickedValue(bit3, bit2);
                                    lststrPacketData.Add("MODE" + " =" + buttonstatus);

                                    //Set Schedule Button
                                    bool bit4 = (b & (1 << 4)) != 0;
                                    bool bit5 = (b & (1 << 5)) != 0;
                                    buttonstatus = CheckClickedValue(bit5, bit4);
                                    lststrPacketData.Add("Set Schedule" + " =" + buttonstatus);

                                    //CLean Screen Button
                                    bool bit6 = (b & (1 << 6)) != 0;
                                    bool bit7 = (b & (1 << 7)) != 0;
                                    buttonstatus = CheckClickedValue(bit7, bit6);
                                    lststrPacketData.Add("Clean Screen" + "=" + buttonstatus);
                                    break;

                                case 1:
                                    byte b2 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    string buttonstatus2 = string.Empty;
                                    //Air Cleaning  button
                                    bool bit00 = (b2 & (1 << 0)) != 0;
                                    bool bit11 = (b2 & (1 << 1)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit11, bit00);
                                    lststrPacketData.Add("Air Cleaning" + "=" + buttonstatus2);

                                    //Humidity Control   Button
                                    bool bit22 = (b2 & (1 << 2)) != 0;
                                    bool bit33 = (b2 & (1 << 3)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit33, bit22);
                                    lststrPacketData.Add("Humidity Control" + "=" + buttonstatus2);

                                    //Fresh Air  Button
                                    bool bit44 = (b2 & (1 << 4)) != 0;

                                    bool bit55 = (b2 & (1 << 5)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit55, bit44);
                                    lststrPacketData.Add("Fresh Air" + "=" + buttonstatus2);

                                    //Set Vaccation Screen Button
                                    bool bit66 = (b2 & (1 << 6)) != 0;
                                    bool bit77 = (b2 & (1 << 7)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit77, bit66);
                                    lststrPacketData.Add("Set Vaccation" + "=" + buttonstatus2);
                                    break;

                                case 2:
                                    byte b3 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    string buttonstatus3 = string.Empty;
                                    //Set CLK/Date  button
                                    bool bit000 = (b3 & (1 << 0)) != 0;
                                    bool bit111 = (b3 & (1 << 1)) != 0;
                                    buttonstatus3 = CheckClickedValue(bit111, bit000);
                                    lststrPacketData.Add("Set CLK/Date" + "=" + buttonstatus3);

                                    //Cancel Hold   Button
                                    bool bit222 = (b3 & (1 << 2)) != 0;
                                    bool bit333 = (b3 & (1 << 3)) != 0;
                                    buttonstatus3 = CheckClickedValue(bit333, bit222);
                                    lststrPacketData.Add("Cancel Hold" + "=" + buttonstatus3);

                                    //Heat UP  Button
                                    bool bit444 = (b3 & (1 << 4)) != 0;

                                    bool bit555 = (b3 & (1 << 5)) != 0;
                                    buttonstatus3 = CheckClickedValue(bit555, bit444);
                                    lststrPacketData.Add("Heat UP" + "=" + buttonstatus3);

                                    //Heat Down Button
                                    bool bit666 = (b3 & (1 << 6)) != 0;
                                    bool bit777 = (b3 & (1 << 7)) != 0;
                                    buttonstatus3 = CheckClickedValue(bit777, bit666);
                                    lststrPacketData.Add("Heat Down" + "=" + buttonstatus3);
                                    break;

                                case 3:
                                    byte b4 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    string buttonstatus4 = string.Empty;
                                    //Cool UP  button
                                    bool bit0000 = (b4 & (1 << 0)) != 0;
                                    bool bit1111 = (b4 & (1 << 1)) != 0;
                                    buttonstatus4 = CheckClickedValue(bit1111, bit0000);
                                    lststrPacketData.Add("Cool UP" + "=" + buttonstatus4);

                                    //Cool Down Button
                                    bool bit2222 = (b4 & (1 << 2)) != 0;
                                    bool bit3333 = (b4 & (1 << 3)) != 0;
                                    buttonstatus4 = CheckClickedValue(bit3333, bit2222);
                                    lststrPacketData.Add("Cool Down" + "=" + buttonstatus4);

                                    //Copy/Blast  Button
                                    bool bit4444 = (b4 & (1 << 4)) != 0;

                                    bool bit5555 = (b4 & (1 << 5)) != 0;
                                    buttonstatus4 = CheckClickedValue(bit5555, bit4444);
                                    lststrPacketData.Add("Copy/Blast" + "=" + buttonstatus4);

                                    //Install Setup Button
                                    bool bit6666 = (b4 & (1 << 6)) != 0;
                                    bool bit7777 = (b4 & (1 << 7)) != 0;
                                    buttonstatus4 = CheckClickedValue(bit7777, bit6666);
                                    lststrPacketData.Add("Install Setup" + "=" + buttonstatus4);
                                    break;

                                case 4:
                                    byte b5 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    string buttonstatus5 = string.Empty;
                                    //NAV UP  button
                                    bool bit00000 = (b5 & (1 << 0)) != 0;
                                    bool bit11111 = (b5 & (1 << 1)) != 0;
                                    buttonstatus5 = CheckClickedValue(bit11111, bit00000);
                                    lststrPacketData.Add("NAV UP" + "=" + buttonstatus5);

                                    //NAV Down Button
                                    bool bit22222 = (b5 & (1 << 2)) != 0;
                                    bool bit33333 = (b5 & (1 << 3)) != 0;
                                    buttonstatus5 = CheckClickedValue(bit33333, bit22222);
                                    lststrPacketData.Add("NAV Down" + "=" + buttonstatus5);

                                    //BACK Button
                                    bool bit44444 = (b5 & (1 << 4)) != 0;
                                    bool bit55555 = (b5 & (1 << 5)) != 0;
                                    buttonstatus5 = CheckClickedValue(bit55555, bit44444);
                                    lststrPacketData.Add("BACK" + "=" + buttonstatus5);

                                    //Next Button
                                    bool bit66666 = (b5 & (1 << 6)) != 0;
                                    bool bit77777 = (b5 & (1 << 7)) != 0;
                                    buttonstatus5 = CheckClickedValue(bit77777, bit66666);
                                    lststrPacketData.Add("NEXT" + "=" + buttonstatus5);
                                    break;


                                case 5:
                                    byte b6 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    string buttonstatus6 = string.Empty;
                                    //Menu/Done  button
                                    bool bit000000 = (b6 & (1 << 0)) != 0;
                                    bool bit111111 = (b6 & (1 << 1)) != 0;
                                    buttonstatus6 = CheckClickedValue(bit111111, bit000000);
                                    lststrPacketData.Add("Menu/Done" + "=" + buttonstatus6);

                                    //Reserved
                                    bool bit222222 = (b6 & (1 << 2)) != 0;
                                    bool bit333333 = (b6 & (1 << 3)) != 0;
                                    buttonstatus6 = CheckClickedValue(bit333333, bit222222);
                                    lststrPacketData.Add("Reserved" + "=" + buttonstatus6);

                                    //Reserved
                                    bool bit444444 = (b6 & (1 << 4)) != 0;
                                    bool bit555555 = (b6 & (1 << 5)) != 0;
                                    buttonstatus6 = CheckClickedValue(bit555555, bit444444);
                                    lststrPacketData.Add("Reserved" + "=" + buttonstatus6);

                                    //Reserved
                                    bool bit666666 = (b6 & (1 << 6)) != 0;
                                    bool bit777777 = (b6 & (1 << 7)) != 0;
                                    buttonstatus6 = CheckClickedValue(bit777777, bit666666);
                                    lststrPacketData.Add("Reserved" + "=" + buttonstatus6);
                                    break;

                                //AC Status
                                case 6:
                                    intIndexOfChildNode = returnIndexOfItem(strAttribute, xmlNode);
                                    int i3 = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[1] && !(i3 >= xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes.Count))
                                        returnValue = "   " + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[i3].InnerText.ToString();
                                    else
                                        returnValue = "  Unknown";

                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[1].Attributes[1].Value + "=" + returnValue);
                                    break;

                                //Built in thermistor temperature
                                case 7:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 8:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[2].Attributes[1].Value + "=" + val + " F");
                                    break;

                                //Remote temperature
                                case 9:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 10:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte0, byte1);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[3].Attributes[1].Value + "=" + val + "");
                                    break;

                                //Built in Humidity Sensor
                                case 11:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 12:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 10;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[4].Attributes[1].Value + "=" + val + " % RH");
                                    break;

                                //HP/HC switch
                                case 13:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[5] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[5].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[5].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[5].Attributes[1].Value + "=Unknown");
                                    break;

                                //Wired ODT
                                case 14:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 15:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[6].Attributes[1].Value + "=" + val + "");
                                    break;

                                //L Fault
                                case 16:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[7] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[7].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[7].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[7].Attributes[1].Value + "=Unknown");
                                    break;
                                //WiFi 
                                case 17:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[8] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[8].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[8].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[8].Attributes[1].Value + "=Unknown");
                                    break;
                                //ECM 
                                case 18:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[9] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[9].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[9].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[9].Attributes[1].Value + "=Unknown");
                                    break;
                                //Support Module 
                                case 19:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[10] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[10].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[10].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[10].Attributes[1].Value + "=Unknown");
                                    break;
                                //Touchscreen 
                                case 20:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[11] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[11].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[11].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[11].Attributes[1].Value + "=Unknown");
                                    break;
                            }
                        }
                    }
                    break;
                default:
                    returnValue = string.Empty;
                    break;
            }

            return lststrPacketData;
        }

        #endregion

        #region Manufacturing Test 11.0
        /// <summary>
        /// Method to parse Manufacturing Test Data
        /// </summary>
        /// <param name="strAttribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        /// <returns></returns>
        public List<string> ParseManufacturingTestV11(string strAttribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            lststrPacketData.Clear();
            string returnValue = string.Empty;

            double val = 0;


            switch (strAttribute)
            {
                case "SetManufacturingTest":
                    int i = int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    if (strAction.Equals("COS Setting"))
                    {
                        DisplayCOSSetting(xmlNode, lststrPacketData, i);
                    }
                    else
                    {
                        if (null != xmlNode.ChildNodes[i] && !(i >= xmlNode.ChildNodes[0].ChildNodes[0].ChildNodes.Count))
                            lststrPacketData.Add(xmlNode.ChildNodes[0].Name + "=" + xmlNode.ChildNodes[0].ChildNodes[0].ChildNodes[i].InnerText.ToString());
                        else
                            lststrPacketData.Add(xmlNode.ChildNodes[0].Name + "=Unknown");
                    }

                    break;

                case "ManufacturingTest":
                    if (strAction.Equals("COS Setting"))
                    {
                        DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                    }
                    else
                    {
                        for (int byteIndex = 0; byteIndex < strDataPacket.Length; byteIndex++)
                        {
                            switch (byteIndex)
                            {

                                case 0:
                                    string buttonstatus = string.Empty;
                                    byte b = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    //FAN button
                                    bool bit0 = (b & (1 << 0)) != 0;
                                    bool bit1 = (b & (1 << 1)) != 0;

                                    buttonstatus = CheckClickedValue(bit1, bit0);
                                    lststrPacketData.Add("FAN" + " =" + buttonstatus);

                                    //Mode  Button
                                    bool bit2 = (b & (1 << 2)) != 0;
                                    bool bit3 = (b & (1 << 3)) != 0;
                                    buttonstatus = CheckClickedValue(bit3, bit2);
                                    lststrPacketData.Add("MODE" + " =" + buttonstatus);

                                    //Set Schedule Button
                                    bool bit4 = (b & (1 << 4)) != 0;
                                    bool bit5 = (b & (1 << 5)) != 0;
                                    buttonstatus = CheckClickedValue(bit5, bit4);
                                    lststrPacketData.Add("Cool UP" + " =" + buttonstatus);

                                    //CLean Screen Button
                                    bool bit6 = (b & (1 << 6)) != 0;
                                    bool bit7 = (b & (1 << 7)) != 0;
                                    buttonstatus = CheckClickedValue(bit7, bit6);
                                    lststrPacketData.Add("Cool Down" + "=" + buttonstatus);
                                    break;

                                case 1:
                                    byte b2 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    string buttonstatus2 = string.Empty;
                                    //Air Cleaning  button
                                    bool bit00 = (b2 & (1 << 0)) != 0;
                                    bool bit11 = (b2 & (1 << 1)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit11, bit00);
                                    lststrPacketData.Add("Heat UP" + "=" + buttonstatus2);

                                    //Humidity Control   Button
                                    bool bit22 = (b2 & (1 << 2)) != 0;
                                    bool bit33 = (b2 & (1 << 3)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit33, bit22);
                                    lststrPacketData.Add("Heat Down" + "=" + buttonstatus2);

                                    //Fresh Air  Button
                                    bool bit44 = (b2 & (1 << 4)) != 0;

                                    bool bit55 = (b2 & (1 << 5)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit55, bit44);
                                    lststrPacketData.Add("Cancel Hold" + "=" + buttonstatus2);

                                    //Set Vaccation Screen Button
                                    bool bit66 = (b2 & (1 << 6)) != 0;
                                    bool bit77 = (b2 & (1 << 7)) != 0;
                                    buttonstatus2 = CheckClickedValue(bit77, bit66);
                                    lststrPacketData.Add("Menu/Done" + "=" + buttonstatus2);
                                    break;

                                case 2:
                                    byte b3 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    string buttonstatus3 = string.Empty;
                                    //Set CLK/Date  button
                                    bool bit000 = (b3 & (1 << 0)) != 0;
                                    bool bit111 = (b3 & (1 << 1)) != 0;
                                    buttonstatus3 = CheckClickedValue(bit111, bit000);
                                    lststrPacketData.Add("Air Cleaning" + "=" + buttonstatus3);

                                    //Cancel Hold   Button
                                    bool bit222 = (b3 & (1 << 2)) != 0;
                                    bool bit333 = (b3 & (1 << 3)) != 0;
                                    buttonstatus3 = CheckClickedValue(bit333, bit222);
                                    lststrPacketData.Add("Humidity Control" + "=" + buttonstatus3);

                                    //Heat UP  Button
                                    bool bit444 = (b3 & (1 << 4)) != 0;

                                    bool bit555 = (b3 & (1 << 5)) != 0;
                                    buttonstatus3 = CheckClickedValue(bit555, bit444);
                                    lststrPacketData.Add("Fresh Air" + "=" + buttonstatus3);

                                    //Heat Down Button
                                    bool bit666 = (b3 & (1 << 6)) != 0;
                                    bool bit777 = (b3 & (1 << 7)) != 0;
                                    buttonstatus3 = CheckClickedValue(bit777, bit666);
                                    lststrPacketData.Add("NAV UP" + "=" + buttonstatus3);
                                    break;

                                case 3:
                                    byte b4 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    string buttonstatus4 = string.Empty;
                                    //Cool UP  button
                                    bool bit0000 = (b4 & (1 << 0)) != 0;
                                    bool bit1111 = (b4 & (1 << 1)) != 0;
                                    buttonstatus4 = CheckClickedValue(bit1111, bit0000);
                                    lststrPacketData.Add("NAV Down" + "=" + buttonstatus4);

                                    //Cool Down Button
                                    bool bit2222 = (b4 & (1 << 2)) != 0;
                                    bool bit3333 = (b4 & (1 << 3)) != 0;
                                    buttonstatus4 = CheckClickedValue(bit3333, bit2222);
                                    lststrPacketData.Add("Copy/Blast" + "=" + buttonstatus4);

                                    //Copy/Blast  Button
                                    bool bit4444 = (b4 & (1 << 4)) != 0;

                                    bool bit5555 = (b4 & (1 << 5)) != 0;
                                    buttonstatus4 = CheckClickedValue(bit5555, bit4444);
                                    lststrPacketData.Add("Reserved" + "=" + buttonstatus4);

                                    //Install Setup Button
                                    bool bit6666 = (b4 & (1 << 6)) != 0;
                                    bool bit7777 = (b4 & (1 << 7)) != 0;
                                    buttonstatus4 = CheckClickedValue(bit7777, bit6666);
                                    lststrPacketData.Add("Reserved" + "=" + buttonstatus4);
                                    break;

                                case 4:
                                    byte b5 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    string buttonstatus5 = string.Empty;
                                    //NAV UP  button
                                    bool bit00000 = (b5 & (1 << 0)) != 0;
                                    bool bit11111 = (b5 & (1 << 1)) != 0;
                                    buttonstatus5 = CheckClickedValue(bit11111, bit00000);
                                    lststrPacketData.Add("Set CLK/Date" + "=" + buttonstatus5);

                                    //NAV Down Button
                                    bool bit22222 = (b5 & (1 << 2)) != 0;
                                    bool bit33333 = (b5 & (1 << 3)) != 0;
                                    buttonstatus5 = CheckClickedValue(bit33333, bit22222);
                                    lststrPacketData.Add("Set Vaccation" + "=" + buttonstatus5);

                                    //BACK Button
                                    bool bit44444 = (b5 & (1 << 4)) != 0;
                                    bool bit55555 = (b5 & (1 << 5)) != 0;
                                    buttonstatus5 = CheckClickedValue(bit55555, bit44444);
                                    lststrPacketData.Add("Next" + "=" + buttonstatus5);

                                    //Next Button
                                    bool bit66666 = (b5 & (1 << 6)) != 0;
                                    bool bit77777 = (b5 & (1 << 7)) != 0;
                                    buttonstatus5 = CheckClickedValue(bit77777, bit66666);
                                    lststrPacketData.Add("Install Setup" + "=" + buttonstatus5);
                                    break;


                                case 5:
                                    byte b6 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    string buttonstatus6 = string.Empty;
                                    //Menu/Done  button
                                    bool bit000000 = (b6 & (1 << 0)) != 0;
                                    bool bit111111 = (b6 & (1 << 1)) != 0;
                                    buttonstatus6 = CheckClickedValue(bit111111, bit000000);
                                    lststrPacketData.Add("Back" + "=" + buttonstatus6);

                                    //Reserved
                                    bool bit222222 = (b6 & (1 << 2)) != 0;
                                    bool bit333333 = (b6 & (1 << 3)) != 0;
                                    buttonstatus6 = CheckClickedValue(bit333333, bit222222);
                                    lststrPacketData.Add("Clean Screen" + "=" + buttonstatus6);

                                    //Reserved
                                    bool bit444444 = (b6 & (1 << 4)) != 0;
                                    bool bit555555 = (b6 & (1 << 5)) != 0;
                                    buttonstatus6 = CheckClickedValue(bit555555, bit444444);
                                    lststrPacketData.Add("Set Schedule" + "=" + buttonstatus6);

                                    //Reserved
                                    bool bit666666 = (b6 & (1 << 6)) != 0;
                                    bool bit777777 = (b6 & (1 << 7)) != 0;
                                    buttonstatus6 = CheckClickedValue(bit777777, bit666666);
                                    lststrPacketData.Add("Reserved" + "=" + buttonstatus6);
                                    break;

                                //AC Status
                                case 6:
                                    intIndexOfChildNode = returnIndexOfItem(strAttribute, xmlNode);
                                    int i3 = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[1] && !(i3 >= xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes.Count))
                                        returnValue = "   " + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[i3].InnerText.ToString();
                                    else
                                        returnValue = "  Unknown";

                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[1].Attributes[1].Value + "=" + returnValue);
                                    break;

                                //Built in thermistor temperature
                                case 7:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 8:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[2].Attributes[1].Value + "=" + val + " F");
                                    break;

                                //Remote temperature
                                case 9:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 10:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte0, byte1);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[3].Attributes[1].Value + "=" + val + "");
                                    break;

                                //Built in Humidity Sensor
                                case 11:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 12:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 10;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[4].Attributes[1].Value + "=" + val + " % RH");
                                    break;

                                //HP/HC switch
                                case 13:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[5] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[5].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[5].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[5].Attributes[1].Value + "=Unknown");
                                    break;

                                //Wired ODT
                                case 14:
                                    byte0 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    break;
                                case 15:
                                    byte1 = (byte)int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    val = CombineTwobytesAndReturnValue(byte1, byte0);
                                    val = val / 100;
                                    lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[6].Attributes[1].Value + "=" + val + "");
                                    break;

                                //L Fault
                                case 16:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[7] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[7].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[7].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[7].Attributes[1].Value + "=Unknown");
                                    break;
                                //WiFi 
                                case 17:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[8] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[8].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[8].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[8].Attributes[1].Value + "=Unknown");
                                    break;
                                //ECM 
                                case 18:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[9] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[9].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[9].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[9].Attributes[1].Value + "=Unknown");
                                    break;
                                //Support Module 
                                case 19:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[10] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[10].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[10].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[10].Attributes[1].Value + "=Unknown");
                                    break;
                                //Touchscreen 
                                case 20:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[11] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[11].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[11].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[11].Attributes[1].Value + "=Unknown");
                                    break;
                                //Flash memory Test
                                case 21:
                                    intByteValue = int.Parse(strDataPacket[byteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    if (null != xmlNode.ChildNodes[1].ChildNodes[12] && !(intByteValue >= xmlNode.ChildNodes[1].ChildNodes[12].ChildNodes.Count))
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[12].Attributes[1].Value + "=" + xmlNode.ChildNodes[1].ChildNodes[1].ChildNodes[intByteValue].InnerText.ToString());
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[1].ChildNodes[12].Attributes[1].Value + "=Unknown");
                                    break;
                            }
                        }
                    }
                    break;
                default:
                    returnValue = string.Empty;
                    break;
            }

            return lststrPacketData;
        }

        #endregion


        #region Write Sensor Values 1.02

        /// <summary>
        /// Parses the write sensor values.
        /// </summary>
        /// <param name="Attribute">The attribute.</param>
        /// <param name="strDataPacket">The string data packet.</param>
        /// <param name="xmlNode">The XML node.</param>
        /// <param name="lststrPacketData">The LSTSTR packet data.</param>
        public void ParseWriteSensorValues(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            double val = 0;

            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            if (null != xmlNode.ChildNodes[9].ChildNodes[0] && !(intByteValue >= xmlNode.ChildNodes[9].ChildNodes[0].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[9].ChildNodes[0].Attributes[1].Value + "=" + xmlNode.ChildNodes[9].ChildNodes[0].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[9].ChildNodes[0].Attributes[1].Value + "=Unknown");
                            break;
                        //Write Sensor Value
                        case 1:
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            break;
                        case 2:
                            byte1 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            string hexValue = strDataPacket[1].Replace("0x", "") + strDataPacket[2].Replace("0x", "");
                            val = int.Parse(hexValue, System.Globalization.NumberStyles.HexNumber);
                            if (val > 2999)
                            {
                                hexValue = "FFFFFFFFFFFF" + strDataPacket[1].Replace("0x", "") + strDataPacket[2].Replace("0x", "");
                                val = long.Parse(hexValue, System.Globalization.NumberStyles.HexNumber);

                            }
                            // val = CombineTwobytesAndReturnValue(byte1, byte0);
                            val = val / 10;
                            lststrPacketData.Add(xmlNode.ChildNodes[9].ChildNodes[1].Attributes[1].Value + "=" + val);
                            break;
                    }
                }
            }

        }

        #endregion


        #region Power Status

        /// <summary>
        /// Method to parse Power Status Data
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParsePowerStatusData(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {
                        //24VAC
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Battery 1 Status
                        case 1:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Battery 1 Voltage
                        case 2:
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            break;
                        case 3:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte1 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            dbVal = CombineTwobytesAndReturnValue(byte1, byte0);
                            dbVal = dbVal / 100;
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[2].Attributes[1].Value + "=" + dbVal);
                            break;

                        //Battery 2
                        case 4:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[3] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[3].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[3].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[3].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[3].Attributes[1].Value + "=Unknown");
                            break;

                        // Battery 2 Voltage
                        case 5:
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            break;
                        case 6:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte1 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            dbVal = CombineTwobytesAndReturnValue(byte1, byte0);
                            dbVal = dbVal / 100;
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[4].Attributes[1].Value + "=" + dbVal);
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        #endregion

        #region Program Hold schdule hold 5.0

        /// <summary>
        /// Parse the program Hold values
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseProgramHold(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    //Return the index of the node using the attribute
                    intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                    if (intByteIndex == 2 || intByteIndex == 3)
                    {
                        byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                        //hemanth updated the 32F conversion by adding the method ReturnCelsiusAndFahrenheitValueForMicro(byte0)
                        //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);

                        //if (byte0 == 0 && HeatSetpointRawValue != 32.0 && coolSetpointRawvalue != 32.0)
                        if (byte0 == 0)
                        {
                            strTemperature = "= Null";
                            //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                            //if (strTemperature == "= NA")
                            //    strTemperature = MessageConstants.strNull;
                        }
                        else
                        {
                            strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                        }
                        lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                        continue;
                    }

                    //Add the name and value to the list. 
                    if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                    {
                        string TValue = string.Empty;

                        #region code added for min and max attributes


                        for (int x = 0; x < xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count; x++)
                        {

                            if (xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes.Count != 0)
                            {
                                short min = Convert.ToInt16(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes["min"].Value.ToString());
                                short max = Convert.ToInt16(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes["max"].Value.ToString());
                                int ind = 0;

                                if (intByteValue >= min && intByteValue <= max
                                    && !(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText == "Heat Setpoint"
                                        || xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText == "Cool Setpoint"))
                                {
                                    TValue = xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText.ToString();
                                    bool IsPrecent = TValue.IndexOf('%') > 0;
                                    ind = TValue.IndexOf('(');
                                    if (ind > 0)
                                        TValue = TValue.Substring(0, ind);
                                    TValue = string.Format("{0} ({1}{2})", TValue.Trim(), intByteValue, IsPrecent ? "%" : string.Empty); //TValue  + " : "+intByteValue;
                                    break;
                                }
                            }
                        }
                        if (TValue != string.Empty)
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + TValue);

                        #endregion

                        if (TValue == string.Empty && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                        {
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString().Trim());
                        }
                        else if (TValue == string.Empty)
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    }
                    else
                    {
                        lststrPacketData.Add("=Unknown");
                    }


                }
            }
        }
        #endregion

        #region schedule Hold
        /// <summary>
        /// Parse the schedule Hold values
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseSchduleHold(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    //Return the index of the node using the attribute
                    intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                    if (intByteIndex == 3 || intByteIndex == 4)
                    {
                        byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                        //hemanth updated the 32F conversion by adding the method ReturnCelsiusAndFahrenheitValueForMicro(byte0)
                        //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);

                        //if (byte0 == 0 && HeatSetpointRawValue != 32.0 && coolSetpointRawvalue != 32.0)
                        //if (byte0 == 0)
                        //{
                        //    strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                        //    if (strTemperature == "= NA")
                        //        strTemperature = MessageConstants.strNull;
                        //}
                        //else
                        {
                            strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                        }
                        lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                        continue;
                    }

                    //Add the name and value to the list. 
                    if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                    {
                        string TValue = string.Empty;

                        #region code added for min and max attributes


                        for (int x = 0; x < xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count; x++)
                        {

                            if (xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes.Count != 0)
                            {
                                short min = Convert.ToInt16(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes["min"].Value.ToString());
                                short max = Convert.ToInt16(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes["max"].Value.ToString());
                                int ind = 0;

                                if (intByteValue >= min && intByteValue <= max
                                    && !(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText == "Heat Setpoint"
                                        || xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText == "Cool Setpoint"))
                                {
                                    TValue = xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText.ToString();
                                    bool IsPrecent = TValue.IndexOf('%') > 0;
                                    ind = TValue.IndexOf('(');
                                    if (ind > 0)
                                        TValue = TValue.Substring(0, ind);
                                    TValue = string.Format("{0} ({1}{2})", TValue.Trim(), intByteValue, IsPrecent ? "%" : string.Empty); //TValue  + " : "+intByteValue;
                                    break;
                                }
                            }
                        }
                        if (TValue != string.Empty)
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + TValue);

                        #endregion

                        if (TValue == string.Empty && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                        {
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString().Trim());
                        }
                        else if (TValue == string.Empty)
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    }
                    else
                    {
                        lststrPacketData.Add("=Unknown");
                    }


                }
            }
        }
        #endregion

        #region Schedule Day
        /// <summary>
        /// Parse the Schedule Day values
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseScheduleDay(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    #region Sujay code commented...
                    //switch (intByteIndex)
                    //{
                    //    //Hold
                    //    case 0:
                    //        intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //        if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                    //        else
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    //        break;

                    //    //Fan
                    //    case 1:
                    //        intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //        if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                    //        else
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    //        break;

                    //    //Heat Setpoint
                    //    case 2:
                    //        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //        byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //        strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                    //        lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                    //        break;

                    //    // Cool Setpoint
                    //    case 3:
                    //        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //        byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //        strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                    //        lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                    //        break;

                    //    // End Minute 
                    //    case 4:
                    //        intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //        if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                    //        else
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    //        break;

                    //    //End Hour
                    //    case 5:
                    //        intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //        if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                    //        else
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    //        break;

                    //    //End Date
                    //    case 6:
                    //        intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //        if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                    //        else
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    //        break;

                    //    //End Month
                    //    case 7:
                    //        intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //        if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                    //        else
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    //        break;

                    //    //year
                    //    case 8:
                    //        intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //        if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                    //        else
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    //        break;

                    //    default:
                    //        break;
                    //}

                    #endregion


                    //Return the index of the node using the attribute
                    intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                    if (intByteIndex == 4 || intByteIndex == 5 || intByteIndex == 9 || intByteIndex == 10 || intByteIndex == 14 || intByteIndex == 15 || intByteIndex == 19 || intByteIndex == 20)
                    {
                        byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                        //hemanth updated the 32F conversion by adding the method ReturnCelsiusAndFahrenheitValueForMicro(byte0)
                        strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);

                        //if (byte0 == 0 && HeatSetpointRawValue != 32.0 && coolSetpointRawvalue != 32.0)
                        //if (byte0 == 0)
                        //{
                        //    //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                        //    //if (strTemperature == "= NA")
                        //    //    strTemperature = MessageConstants.strNull;
                        //    strTemperature = "= Null";
                        //}
                        //else
                        {
                            strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                        }
                        lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                        continue;
                    }

                    //Add the name and value to the list. 
                    if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                    {
                        string TValue = string.Empty;

                        #region code added for min and max attributes


                        for (int x = 0; x < xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count; x++)
                        {

                            if (xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes.Count != 0)
                            {
                                short min = Convert.ToInt16(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes["min"].Value.ToString());
                                short max = Convert.ToInt16(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes["max"].Value.ToString());
                                int ind = 0;

                                if (intByteValue >= min && intByteValue <= max
                                    && !(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText == "Heat Setpoint"
                                        || xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText == "Cool Setpoint"))
                                {
                                    TValue = xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText.ToString();
                                    bool IsPrecent = TValue.IndexOf('%') > 0;
                                    ind = TValue.IndexOf('(');
                                    if (ind > 0)
                                        TValue = TValue.Substring(0, ind);
                                    TValue = string.Format("{0} ({1}{2})", TValue.Trim(), intByteValue, IsPrecent ? "%" : string.Empty); //TValue  + " : "+intByteValue;
                                    break;
                                }
                            }
                        }
                        if (TValue != string.Empty)
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + TValue);

                        #endregion

                        if (TValue == string.Empty && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                        {
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString().Trim());
                        }
                        else if (TValue == string.Empty)
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    }
                    else
                    {
                        lststrPacketData.Add("=Unknown");
                    }


                }
            }
        }
        #endregion

        #region Sensor Values 7.0
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseSensorValuesV7(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {
                        //Built in Sensor Status
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Built In Sensor Value
                        case 1:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            // hemanth
                            //  strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);   
                            //if (byte0 == 0)
                            //    strTemperature = "= Null";
                            //else
                            strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //Wired Remote Sensor  Status
                        case 2:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            //xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[0].InnerText.ToString()
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Wired Remote Sensor value
                        case 3:

                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            if (lststrPacketData.Contains("Wired Remote Temp Sensor Status=Not Installed"))
                                strTemperature = "= NA";
                            else
                                // hemanth
                                //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                //if (byte0 == 0 || byte0 == 21)
                                if (byte0 == 21)
                                strTemperature = "= Null";
                            else
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);

                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);

                            break;

                        //Outdoor Sensor status
                        case 4:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");

                            break;

                        //Outdoor Sensor value
                        case 5:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            if (lststrPacketData.Contains("Wired Outdoor Temp Sensor Status=Not Installed"))
                                strTemperature = "= NA";
                            else
                                // hemanth
                                //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                //if (byte0 == 0)
                                //    strTemperature = "= Null";
                                //else
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);

                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //Humidity Sensor Status
                        case 6:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Humidity Sensor Value
                        case 7:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Returning Air Sensor Status
                        case 8:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //RAT Sensor Value
                        case 9:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            if (lststrPacketData.Contains("RAT Sensor Status=Not Installed"))
                                strTemperature = "= NA";
                            else
                                //  strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);  
                                //if (byte0 == 0)
                                //    strTemperature = "= Null";
                                //else
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //Leaving Air Sensor Status
                        case 10:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //LAT Sensor Value
                        case 11:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);


                            if (lststrPacketData.Contains("LAT Sensor Status=Not Installed"))
                                strTemperature = "= NA";
                            else
                                //  strTemperature = ReturnCelsiusAndFahrenheitValue(byte0); 
                                //if (byte0 == 0)
                                //    strTemperature = "= Null";
                                //else
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //Wireless Outdoor Sensor status
                        case 12:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");

                            break;

                        //Wireless Outdoor Sensor value
                        case 13:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            if (lststrPacketData.Contains("Wireless Outdoor Temp Sensor Status=Not Installed"))
                                strTemperature = "= NA";
                            else
                                // hemanth
                                //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                //if (byte0 == 0)
                                //    strTemperature = "= Null";
                                //else
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);

                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //Wireless Humidity Sensor Status
                        case 14:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Wireless Humidity Sensor Value
                        case 15:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        default:
                            break;
                    }
                }
            }
        }

        #endregion

        #region Controlling Sensor Values 7.0
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseControllingSensorValuesV7(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {
                        //Indoor Temp Controlling  Sensor Status
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Indoor Temp Controlling  Sensor Value
                        case 1:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            // hemanth
                            //  strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);    
                            //if (byte0 == 0)
                            //    strTemperature = "= Null";
                            //else
                            strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //Outdoor Temp Controlling  Sensor Status
                        case 2:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            //xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[0].InnerText.ToString()
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Outdoor Temp Controlling  Sensor Value
                        case 3:

                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            if (lststrPacketData.Contains("Wired Remote Temperature Sensor Status=Not Installed"))
                                strTemperature = "= NA";
                            else
                                // hemanth
                                //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                //if (byte0 == 0)
                                //    strTemperature = "= Null";
                                //else
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);

                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);

                            break;

                        //Indoor Humidity Controlling Sensor Status
                        case 4:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Indoor Humidity Controlling Sensor Value
                        case 5:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Outdoor Humidity Controlling Sensor Status
                        case 6:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;


                        //Outdoor Humidity Controlling Sensor Value
                        case 7:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;


                        default:
                            break;
                    }
                }
            }
        }

        #endregion

        #region Sensor Values 5.0
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseSensorValuesV5(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {
                        //Built in Sensor Status
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Built In Sensor Value
                        case 1:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            // hemanth
                            //  strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);                           
                            strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //Wired Remote Sensor  Status
                        case 2:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            //xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[0].InnerText.ToString()
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Wired Remote Sensor value
                        case 3:

                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            if (lststrPacketData.Contains("Wired Remote Sensor Status=Not Installed"))
                                strTemperature = "= NA";
                            else
                                // hemanth
                                //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);

                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);

                            break;

                        //Outdoor Sensor status
                        case 4:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");

                            break;

                        //Outdoor Sensor value
                        case 5:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            if (lststrPacketData.Contains("Outdoor Sensor Status=Not Installed"))
                                strTemperature = "= NA";
                            else
                                // hemanth
                                //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);

                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //Humidity Sensor Status
                        case 6:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Humidity Sensor Value
                        case 7:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Returning Air Sensor Status
                        case 8:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //RAT Sensor Value
                        case 9:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            //  strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);                           
                            strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //Leaving Air Sensor Status
                        case 10:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //LAT Sensor Value
                        case 11:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            //  strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);                           
                            strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //IndoorControllingSensorValue
                        case 12:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            //  strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);                           
                            strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        default:
                            break;

                    }
                }
            }
        }

        #endregion

        #region Sensor Values 4.0
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseSensorValuesV4(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {
                        //Built in Sensor Status
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Built In Sensor Value
                        case 1:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            // hemanth
                            //  strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);                           
                            strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //Wired Remote Sensor  Status
                        case 2:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            //xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[0].InnerText.ToString()
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Wired Remote Sensor value
                        case 3:

                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            if (lststrPacketData.Contains("Wired Remote Sensor Status=Not Installed"))
                                strTemperature = "= NA";
                            else
                                // hemanth
                                //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);

                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);

                            break;

                        //Outdoor Sensor status
                        case 4:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");

                            break;

                        //Outdoor Sensor value
                        case 5:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            if (lststrPacketData.Contains("Outdoor Sensor Status=Not Installed"))
                                strTemperature = "= NA";
                            else
                                // hemanth
                                //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);

                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //Humidity Sensor Status
                        case 6:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Humidity Sensor Value
                        case 7:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //RAT Sensor Value
                        case 8:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            //  strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);                           
                            strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //LAT Sensor Value
                        case 9:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            //  strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);                           
                            strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;
                        default:
                            break;

                    }
                }
            }
        }

        #endregion

        #region Sensor Values 3.0
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseSensorValues(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {
                        //Built in Sensor Status
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Built In Sensor Value
                        case 1:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            // hemanth
                            //  strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);                           
                            strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //Wired Outdoor Sensor  Status
                        case 2:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            //xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[0].InnerText.ToString()
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Wired Outdoor Sensor value
                        case 3:

                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            if (lststrPacketData.Contains("Outdoor Sensor Status=Not Installed"))
                                strTemperature = "= NA";
                            else
                                // hemanth
                                //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);

                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);

                            break;

                        //Remote Sensor status
                        case 4:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");

                            break;

                        //Remote Sensor value
                        case 5:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            if (lststrPacketData.Contains("Remote Sensor Status=Not Installed"))
                                strTemperature = "= NA";
                            else
                                // hemanth
                                //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);

                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //Humidity Sensor Status
                        case 6:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Humidity Sensor Value
                        case 7:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;
                    }
                }
            }
        }

        #endregion

        #region New Sensor Values 2.0
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseNewSensorValues(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {

                        //Built In Sensor Value
                        case 0:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            // hemanth
                            //  strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);                           
                            strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;


                        //Remote Sensor value

                        case 1:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            if (lststrPacketData.Contains("Remote Sensor Status=Not Installed"))
                                strTemperature = "= NA";
                            else
                                // hemanth
                                //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);

                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //Wired Outdoor Sensor value
                        case 2:

                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            if (lststrPacketData.Contains("Outdoor Sensor Status=Not Installed"))
                                strTemperature = "= NA";
                            else
                                // hemanth
                                //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);

                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);

                            break;

                        //Humidity Sensor Value
                        case 3:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;
                    }
                }
            }
        }

        #endregion

        #region Support Modules
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseSupportModules(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {

                        //Sensor Address
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;


                        //Temp Sensor 1

                        case 1:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            if (lststrPacketData.Contains("Remote Sensor Status=Not Installed"))
                                strTemperature = "= NA";
                            else
                                // hemanth
                                //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);

                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //Temp Sensor 2
                        case 2:

                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            if (lststrPacketData.Contains("Outdoor Sensor Status=Not Installed"))
                                strTemperature = "= NA";
                            else
                                // hemanth
                                //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);

                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);

                            break;

                        //Humidity Sensor Value
                        case 3:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;
                    }
                }
            }
        }

        #endregion

        #region Support Modules 5.0

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseSupportModulesV5(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {

                        //Module Address
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Module Status
                        case 1:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Sensor Mode
                        case 2:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Temp Sensor 1

                        case 3:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            if (lststrPacketData.Contains("Remote Sensor Status=Not Installed"))
                                strTemperature = "= NA";
                            else
                                // hemanth
                                //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);

                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //Temp Sensor 2
                        case 4:

                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            if (lststrPacketData.Contains("Outdoor Sensor Status=Not Installed"))
                                strTemperature = "= NA";
                            else
                                // hemanth
                                //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);

                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);

                            break;

                        // Sensor Value RH
                        case 5:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;
                    }
                }
            }
        }

        #endregion

        #region Support Modules 8.0

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseSupportModulesV8(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {

                        //Module Address
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Sensor 1 Status
                        case 1:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Sensor 1  Mode
                        case 2:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        // Sensor 1 Temp

                        case 3:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            // hemanth
                            //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                            //if (byte0 == 0 || byte0 == 21)
                            if (byte0 == 21)
                                strTemperature = "= Null";
                            else
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);

                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //Sensor 2 Status
                        case 4:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Sensor 2  Mode
                        case 5:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;


                        // Sensor 2 Temp
                        case 6:

                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);

                            // hemanth
                            //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                            if (byte0 == 21)
                                strTemperature = "= Null";
                            else
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            // if (int.Parse(Regex.Split(strTemperature.Split('=')[1], "F")[0].Trim()) < 105 && int.Parse(Regex.Split(strTemperature.Split('=')[1], "F")[0].Trim()) > 32)
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            //else
                            //hi
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Reserved");

                            break;

                        // Sensor 2 Value RH
                        case 7:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;
                    }
                }
            }
        }

        #endregion

        #region C4OutdoorTempSensor
        /// <summary>
        /// Parse C4OutdoorTempSensor
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseC4OutdoorTempSensor(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {
                        //Outdoor Sensor Status
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Outdoor SensorValue
                        case 1:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            // hemanth
                            //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                            strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;
                    }
                }
            }
        }
        #endregion

        #region OutdoorTempSensor
        /// <summary>
        /// Parse C4OutdoorTempSensor
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseOutdoorTempSensor(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {
                        // Outdoor  Sensor Value
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;
                        //Outdoor SensorValue
                        case 1:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            // hemanth
                            //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                            //if (byte0 == 0)
                            //    strTemperature = "= Null";
                            //else
                            strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;
                    }
                }
            }
        }
        #endregion

        #region Messages
        /// <summary>
        /// Parsing Messages
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseMessaging(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            if (strAction.Equals("COS Setting"))
            {
                DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
            }
            else
            {
                int index = 0;
                if (Attribute.Equals("PermanentMessages"))
                {
                    index = 1;
                    lststrPacketData.Add("Message Number =" + int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }


                string strMessageLine1 = string.Empty;
                string strMessageLine2 = string.Empty;
                bool tempFlag = true;
                for (int i = index; i < strDataPacket.Length; i++)
                {
                    if (!strDataPacket[i].Equals("0x00"))
                    {
                        int ij = int.Parse(strDataPacket[i].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                        byte[] temp2 = new byte[] { (byte)ij };
                        char[] temp1 = ASCIIEncoding.ASCII.GetChars(temp2);
                        if (tempFlag)
                            strMessageLine1 = strMessageLine1 + temp1[0];
                        else
                            strMessageLine2 = strMessageLine2 + temp1[0];
                    }
                    else
                    {
                        tempFlag = false;
                    }
                }

                lststrPacketData.Add("Message Line 1 =" + strMessageLine1);
                lststrPacketData.Add("Message Line 2 =" + strMessageLine2);
            }
        }
        #endregion

        #region ContractorInformation
        /// <summary>
        /// Parsing ContractorInformation
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseContractorInformation(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            if (strAction.Equals("COS Setting"))
            {
                DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
            }
            else
            {
                int index = 0;
                int count = 0;
                string strName = string.Empty;
                string strPhone = string.Empty;
                string strEmail = string.Empty;
                string strWeb = string.Empty;
                bool tempFlag = true;

                for (int i = index; i < strDataPacket.Length; i++)
                {
                    if (!strDataPacket[i].Equals("0x00"))
                    {
                        int ij = int.Parse(strDataPacket[i].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                        byte[] temp2 = new byte[] { (byte)ij };
                        char[] temp1 = ASCIIEncoding.ASCII.GetChars(temp2);
                        if (tempFlag)
                        {
                            strName = strName + temp1[0];
                        }
                        else if (!tempFlag && count == 1)
                        {
                            strPhone = strPhone + temp1[0];
                        }
                        else if (!tempFlag && count == 2)
                        {
                            strEmail = strEmail + temp1[0];
                        }
                        else if (!tempFlag && count == 3)
                        {
                            strWeb = strWeb + temp1[0];
                        }
                    }
                    else
                    {
                        tempFlag = false;

                        if (i == 70) //if (i == 30)
                        {
                            count++;
                        }
                        else if (i == 81)   // else if (i == 41)
                        {
                            count++;
                        }
                        else if (i == 152)   //   else if (i == 72)
                        {
                            count++;
                        }
                    }
                }

                lststrPacketData.Add("Name =" + strName);
                lststrPacketData.Add("Phone =" + strPhone);
                lststrPacketData.Add("Email =" + strEmail);
                lststrPacketData.Add("Web =" + strWeb);
                strName = null;
                strPhone = null;
                strEmail = null;
                strWeb = null;

            }
        }
        #endregion

        #region HVAC settings
        /// <summary>
        /// HVACSettings
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseHVACSettings(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {
                        //Mode
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Fan
                        case 1:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        //Heat Setpoint
                        case 2:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            // hemanth
                            //if (byte0 == 0 && HeatSetpointRawValue != 32.0)
                            if (byte0 == 0)
                            {
                                //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                //if (strTemperature == "= NA")
                                //    strTemperature = MessageConstants.strNull;
                                strTemperature = "= Null";
                            }
                            else
                            {
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            }
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //cool setpoint
                        case 3:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            // hemanth
                            //if (byte0 == 0 && coolSetpointRawvalue != 32.0)
                            if (byte0 == 0)
                            {
                                //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                //if (strTemperature == "= NA")
                                //    strTemperature = MessageConstants.strNull;
                                strTemperature = "= Null";
                            }
                            else
                            {
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            }
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        #endregion

        #region Current Event
        int Index = 0;
        /// <summary>
        /// CurrentEvnetdata
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseScheduleCurrentEvnetdata(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {
                        //Heat set point
                        case 0:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            //hemanth updated the 32F conversion by adding the method ReturnCelsiusAndFahrenheitValueForMicro(byte0)
                            // if (byte0 == 0 && HeatSetpointRawValue!=32.0)
                            if (byte0 == 0)
                            {
                                strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                if (strTemperature == "= NA")
                                    strTemperature = RASProtocolClass.MessageConstants.strNull;
                            }
                            else
                            {
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            }
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //cool set point
                        case 1:
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            //hemanth updated the 32F conversion by adding the method ReturnCelsiusAndFahrenheitValueForMicro(byte0)
                            //if (byte0 == 0 && coolSetpointRawvalue!=32.0)
                            if (byte0 == 0)
                            {
                                strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                if (strTemperature == "= NA")
                                    strTemperature = RASProtocolClass.MessageConstants.strNull;
                            }
                            else
                            {
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            }
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            break;

                        //Fan
                        case 2:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Reserved");
                            break;

                        //Message Line 1 and Line 2
                        case 3:

                            string strMessageLine1 = string.Empty;
                            string strMessageLine2 = string.Empty;
                            bool tempFlag = true;
                            for (int i = 3; i < strDataPacket.Length; i++)
                            {
                                if (!strDataPacket[i].Equals("0x00"))
                                {
                                    int ij = int.Parse(strDataPacket[i].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    byte[] temp2 = new byte[] { (byte)ij };
                                    char[] temp1 = ASCIIEncoding.ASCII.GetChars(temp2);
                                    if (tempFlag)
                                        strMessageLine1 = strMessageLine1 + temp1[0];
                                    else
                                        strMessageLine2 = strMessageLine2 + temp1[0];
                                }
                                else
                                {
                                    tempFlag = false;
                                }
                            }
                            lststrPacketData.Add("Current Event Message Line 1 =" + strMessageLine1);
                            lststrPacketData.Add("Current Event Message Line 2 =" + strMessageLine2);
                            break;

                        default:
                            break;

                    }
                }

            }

        }
        #endregion

        #region Next Event
        /// <summary>
        /// ScheduleNextEvnet
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseScheduleNextEvnetdata(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    #region Sujay Code commented...
                    //switch (intByteIndex)
                    //{
                    //    //Second 
                    //    case 0:
                    //        intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //        if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                    //        else
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    //        break;


                    //    //Minute
                    //    case 1:
                    //        intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //        if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                    //        else
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    //        break;

                    //    //Hour
                    //    case 2:
                    //        intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //        if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                    //        else
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    //        break;

                    //    //Day
                    //    case 3:
                    //        intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //        if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                    //        else
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    //        break;

                    //    //Date
                    //    case 4:
                    //        intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //        if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                    //        else
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    //        break;

                    //    //Month   
                    //    case 5:
                    //        intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //        if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                    //        else
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    //        break;

                    //    //year
                    //    case 6:
                    //        intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //        if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                    //        else
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    //        break;

                    //    //Heat set point
                    //    case 7:
                    //        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //        byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //        strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                    //        lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                    //        break;
                    //    //cool set point 
                    //    case 8:
                    //        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //        byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //        strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                    //        lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                    //        break;

                    //    //Fan
                    //    case 9:
                    //        intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //        if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                    //        else
                    //            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    //        break;

                    //    //Message Line 1 and 2
                    //    case 10:
                    //        string strMessageLine1 = string.Empty;
                    //        string strMessageLine2 = string.Empty;
                    //        bool tempFlag = true;
                    //        for (int i = 10; i < strDataPacket.Length; i++)
                    //        {
                    //            if (!strDataPacket[i].Equals("0x00"))
                    //            {
                    //                int ij = int.Parse(strDataPacket[i].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //                byte[] temp2 = new byte[] { (byte)ij };
                    //                char[] temp1 = ASCIIEncoding.ASCII.GetChars(temp2);
                    //                if (tempFlag)
                    //                    strMessageLine1 = strMessageLine1 + temp1[0];
                    //                else
                    //                    strMessageLine2 = strMessageLine2 + temp1[0];
                    //            }
                    //            else
                    //            {
                    //                tempFlag = false;
                    //            }
                    //        }
                    //        lststrPacketData.Add("Next Event Message Line 1 =" + strMessageLine1);
                    //        lststrPacketData.Add("Next Event Message Line 2 =" + strMessageLine2);
                    //        break;

                    //    default:
                    //        break;
                    //}

                    #endregion

                    if (intByteIndex < 10)
                    {

                        //Return the index of the node using the attribute
                        intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);


                        if (intByteIndex == 7 || intByteIndex == 8)
                        {
                            byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            //hemanth updated the 32F conversion by adding the method ReturnCelsiusAndFahrenheitValueForMicro(byte0)
                            // if (byte0 == 0 && HeatSetpointRawValue!=32.0 && coolSetpointRawvalue != 32.0)
                            if (byte0 == 0)
                            {
                                strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                                if (strTemperature == "= NA")
                                    strTemperature = RASProtocolClass.MessageConstants.strNull;
                            }
                            else
                            {
                                strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                            }
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                            continue;
                        }



                        //if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                        //    lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                        //else
                        //    lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");

                        //Add the name and value to the list. 
                        if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                        {
                            string TValue = string.Empty;

                            #region code added for min and max attributes

                            for (int x = 0; x < xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count; x++)
                            {

                                if (xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes.Count != 0)
                                {
                                    short min = Convert.ToInt16(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes["min"].Value.ToString());
                                    short max = Convert.ToInt16(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes["max"].Value.ToString());
                                    int ind = 0;

                                    if (intByteValue >= min && intByteValue <= max
                                     && !(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText == "Heat Setpoint"
                                         || xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText == "Cool Setpoint"))
                                    {
                                        TValue = xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText.ToString();
                                        bool IsPrecent = TValue.IndexOf('%') > 0;
                                        ind = TValue.IndexOf('(');
                                        if (ind > 0)
                                            TValue = TValue.Substring(0, ind);
                                        TValue = string.Format("{0} ({1}{2})", TValue.Trim(), intByteValue, IsPrecent ? "%" : string.Empty); //TValue  + " : "+intByteValue;
                                        break;
                                    }
                                }
                            }
                            if (TValue != string.Empty)
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + TValue);

                            #endregion

                            if (TValue == string.Empty && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                            {
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString().Trim());
                            }
                            else if (TValue == string.Empty)
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                        }


                        else
                        {
                            lststrPacketData.Add("=Unknown");
                        }
                    }
                    else
                    {
                        string strMessageLine1 = string.Empty;
                        string strMessageLine2 = string.Empty;
                        bool tempFlag = true;
                        for (int i = 10; i < strDataPacket.Length; i++)
                        {
                            if (!strDataPacket[i].Equals("0x00"))
                            {
                                int ij = int.Parse(strDataPacket[i].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                byte[] temp2 = new byte[] { (byte)ij };
                                char[] temp1 = ASCIIEncoding.ASCII.GetChars(temp2);
                                if (tempFlag)
                                    strMessageLine1 = strMessageLine1 + temp1[0];
                                else
                                    strMessageLine2 = strMessageLine2 + temp1[0];
                            }
                            else
                            {
                                tempFlag = false;
                            }
                        }
                        lststrPacketData.Add("Next Event Message Line 1 =" + strMessageLine1);
                        lststrPacketData.Add("Next Event Message Line 2 =" + strMessageLine2);
                        break;
                    }

                }
            }
        }
        #endregion

        #region ButtonBacklightColor
        /// <summary>
        ///  ParseButtonBacklightColor
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseButtonBacklightColor(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        case 1:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue);
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        case 2:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue);
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;
                        case 3:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue);
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        default:
                            break;
                    }
                }
            }
        }
        #endregion

        #region ButtonBacklightIntensity
        /// <summary>
        /// Parse Button Backlight Intensity
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseButtonBacklightIntensity(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue + " %");
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;
                    }
                }
            }
        }
        #endregion

        #region ButtonBacklightCalibration
        /// <summary>
        /// parse Back Light
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseButtonBacklightCalibration(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;

                        case 1:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            break;
                        case 2:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            sbyte shortByte = (sbyte)intByteValue;
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + shortByte);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        #endregion

        #region Revision
        /// <summary>
        /// Parse Revision
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseRevision(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {
                        //Major Firmware Revision
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue);
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;


                        //Minor Firmware Revision
                        case 1:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue);
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;

                        //Protocol Major Revision
                        case 2:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue + " ");
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;
                        //Protocol minor Revision
                        case 3:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue + " ");
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;
                        //Hardware Revision
                        case 4:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue + " ");
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;
                    }
                }
            }
        }
        #endregion

        #region RevisionModel
        /// <summary>
        /// Parse Revision
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseRevisionModel(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {
                        //Major Firmware Revision
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue);
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;


                        //Minor Firmware Revision
                        case 1:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue);
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;

                        //Protocol Major Revision
                        case 2:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue + " ");
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;
                        //Protocol minor Revision
                        case 3:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue + " ");
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;
                        //Hardware Revision
                        case 4:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue + " ");
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;
                        //Model Number
                        case 5:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString().Trim());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;
                    }
                }
            }
        }
        #endregion

        #region RevisionModel 7.0
        /// <summary>
        /// Parse Revision
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseRevisionModelV7(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {
                        //Major Firmware Revision
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue);
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;


                        //Minor Firmware Revision
                        case 1:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue);
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;

                        //Protocol Major Revision
                        case 2:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue + " ");
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;

                        //Hardware Revision
                        case 3:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue + " ");
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;
                        //Model Number
                        case 4:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString().Trim());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;
                    }
                }
            }
        }
        #endregion

        #region RevisionModel 11.0
        /// <summary>
        /// Parse Revision
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseRevisionModelV11(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    switch (intByteIndex)
                    {
                        //Hardware Revision
                        case 0:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                            if (intByteValue >= 0 && intByteValue <= 9)
                            {
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue);
                            }
                            else
                            {
                                string strHardwareRevision = string.Empty;
                                if (!strDataPacket[0].Equals("0x00"))
                                {
                                    int ij = int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                    byte[] temp2 = new byte[] { (byte)ij };
                                    char[] temp1 = ASCIIEncoding.ASCII.GetChars(temp2);
                                    strHardwareRevision = strHardwareRevision + temp1[0];
                                    lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + strHardwareRevision);

                                }
                                else
                                    lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            }
                            break;

                        //Major Firmware Revision
                        case 1:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue);
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;

                        //Minor Firmware Revision
                        case 2:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue + " ");
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;

                        //Protocol Revision
                        case 3:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue + " ");
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;
                        //Model Number
                        case 4:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString().Trim());
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;
                        //Gainspan Major Firmware Revision
                        case 5:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue);
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;

                        //Gainspan Minor Revision
                        case 6:
                            intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                            if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue + " ");
                            else
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                            break;
                    }
                }
            }
        }
        #endregion


        #region 3 day Forecast
        /// <summary>
        /// Parse the 3 day Forecast values
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseThreeDayForecast(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            // 0x01 0x00 0x0F 0x01 0x0D 0x01 0x01 0xDA 0xDA 0x01 0x01 0xDA 0xDA 0x01 0x01 0xDA 0xDA 0x01 0x74 0xC0
            // string[] array = new string[] { "0x01", "0xDA", "0xDA", "0x01", "0x01", "0xDA", "0xDA", "0x01","0x01", "0xDA", "0xDA", "0x01"};
            //strDataPacket = array;
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    //Return the index of the node using the attribute
                    intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                    if (intByteIndex == 1 || intByteIndex == 2 || intByteIndex == 5 || intByteIndex == 6 || intByteIndex == 9 || intByteIndex == 10)
                    {
                        byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                        //hemanth updated the 32F conversion by adding the method ReturnCelsiusAndFahrenheitValueForMicro(byte0)
                        //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);

                        //if (byte0 == 0 && HeatSetpointRawValue != 32.0 && coolSetpointRawvalue != 32.0)
                        //if (byte0 == 0)
                        //{
                        //    strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                        //    if (strTemperature == "= NA")
                        //        strTemperature = MessageConstants.strNull;
                        //}
                        //else
                        //if (byte0 == 0)
                        //    strTemperature = "= Null";
                        //else
                        if (byte0 == 255)
                            strTemperature = "= --";
                        else
                        {
                            strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                        }
                        lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                        continue;
                    }

                    //Add the name and value to the list. 
                    if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                    {
                        string TValue = string.Empty;

                        #region code added for min and max attributes


                        for (int x = 0; x < xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count; x++)
                        {

                            if (xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes.Count != 0)
                            {
                                short min = Convert.ToInt16(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes["min"].Value.ToString());
                                short max = Convert.ToInt16(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes["max"].Value.ToString());
                                int ind = 0;

                                if (intByteValue >= min && intByteValue <= max
                                    && !(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText == "Heat Setpoint"
                                        || xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText == "Cool Setpoint"))
                                {
                                    TValue = xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText.ToString();
                                    bool IsPrecent = TValue.IndexOf('%') > 0;
                                    ind = TValue.IndexOf('(');
                                    if (ind > 0)
                                        TValue = TValue.Substring(0, ind);
                                    TValue = string.Format("{0} ({1}{2})", TValue.Trim(), intByteValue, IsPrecent ? "%" : string.Empty); //TValue  + " : "+intByteValue;
                                    break;
                                }
                            }
                        }
                        if (TValue != string.Empty)
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + TValue);

                        #endregion

                        if (TValue == string.Empty && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                        {
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString().Trim());
                        }
                        else if (TValue == string.Empty)
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    }
                    else
                    {
                        lststrPacketData.Add("=Unknown");
                    }


                }
            }
        }
        #endregion

        #region Alerts Settings
        /// <summary>
        /// Parse the 3 day Forecast values
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseAlertsSettings(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            // 0x01 0x00 0x0F 0x01 0x0D 0x01 0x01 0xDA 0xDA 0x01 0x01 0xDA 0xDA 0x01 0x01 0xDA 0xDA 0x01 0x74 0xC0
            // string[] array = new string[] { "0x01", "0xDA", "0xDA", "0x01", "0x01", "0xDA", "0xDA", "0x01","0x01", "0xDA", "0xDA", "0x01"};
            //strDataPacket = array;
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    //Return the index of the node using the attribute
                    intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                    if (intByteIndex == 1 || intByteIndex == 3)
                    {
                        byte0 = (byte)int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                        //hemanth updated the 32F conversion by adding the method ReturnCelsiusAndFahrenheitValueForMicro(byte0)
                        //strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);

                        //if (byte0 == 0 && HeatSetpointRawValue != 32.0 && coolSetpointRawvalue != 32.0)
                        //if (byte0 == 0)
                        //{
                        //    strTemperature = ReturnCelsiusAndFahrenheitValue(byte0);
                        //    if (strTemperature == "= NA")
                        //        strTemperature = MessageConstants.strNull;
                        //}
                        //else
                        //if (byte0 == 0)
                        //    strTemperature = "= Null";
                        //else
                        {
                            strTemperature = ReturnCelsiusAndFahrenheitValueForMicro(byte0);
                        }
                        lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + strTemperature);
                        continue;
                    }

                    //Add the name and value to the list. 
                    if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                    {
                        string TValue = string.Empty;

                        #region code added for min and max attributes


                        for (int x = 0; x < xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count; x++)
                        {

                            if (xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes.Count != 0)
                            {
                                short min = Convert.ToInt16(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes["min"].Value.ToString());
                                short max = Convert.ToInt16(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes["max"].Value.ToString());
                                int ind = 0;

                                if (intByteValue >= min && intByteValue <= max
                                    && !(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText == "Heat Setpoint"
                                        || xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText == "Cool Setpoint"))
                                {
                                    TValue = xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText.ToString();
                                    bool IsPrecent = TValue.IndexOf('%') > 0;
                                    ind = TValue.IndexOf('(');
                                    if (ind > 0)
                                        TValue = TValue.Substring(0, ind);
                                    TValue = string.Format("{0} ({1}{2})", TValue.Trim(), intByteValue, IsPrecent ? "%" : string.Empty); //TValue  + " : "+intByteValue;
                                    break;
                                }
                            }
                        }
                        if (TValue != string.Empty)
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + TValue);

                        #endregion

                        if (TValue == string.Empty && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                        {
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString().Trim());
                        }
                        else if (TValue == string.Empty)
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    }
                    else
                    {
                        lststrPacketData.Add("=Unknown");
                    }


                }
            }
        }
        #endregion

        #region Alerts Settings 16.0
        /// <summary>
        /// Parse the 3 day Forecast values
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseAlertsSettingsV16(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            // 0x01 0x00 0x0F 0x01 0x0D 0x01 0x01 0xDA 0xDA 0x01 0x01 0xDA 0xDA 0x01 0x01 0xDA 0xDA 0x01 0x74 0xC0
            // string[] array = new string[] { "0x01", "0xDA", "0xDA", "0x01", "0x01", "0xDA", "0xDA", "0x01","0x01", "0xDA", "0xDA", "0x01"};
            //strDataPacket = array;
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    //Return the index of the node using the attribute
                    intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                    //Add the name and value to the list. 
                    if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                    {
                        string TValue = string.Empty;

                        #region code added for min and max attributes


                        for (int x = 0; x < xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count; x++)
                        {

                            if (xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes.Count != 0)
                            {
                                short min = Convert.ToInt16(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes["min"].Value.ToString());
                                short max = Convert.ToInt16(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].Attributes["max"].Value.ToString());
                                int ind = 0;

                                if (intByteValue >= min && intByteValue <= max
                                    && !(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText == "Heat Setpoint"
                                        || xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText == "Cool Setpoint"))
                                {
                                    TValue = xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[x].InnerText.ToString();
                                    bool IsPrecent = TValue.IndexOf('%') > 0;
                                    ind = TValue.IndexOf('(');
                                    if (ind > 0)
                                        TValue = TValue.Substring(0, ind);
                                    TValue = string.Format("{0} ({1}{2})", TValue.Trim(), intByteValue, IsPrecent ? "%" : string.Empty); //TValue  + " : "+intByteValue;
                                    break;
                                }
                            }
                        }
                        if (TValue != string.Empty)
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + TValue);

                        #endregion

                        if (TValue == string.Empty && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                        {
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString().Trim());
                        }
                        else if (TValue == string.Empty)
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    }
                    else
                    {
                        lststrPacketData.Add("=Unknown");
                    }


                }
            }
        }
        #endregion

        #region MACAddRev

        /// <summary>
        /// Parses the mac address rev.
        /// </summary>
        /// <param name="Attribute">The attribute.</param>
        /// <param name="strDataPacket">The string data packet.</param>
        /// <param name="xmlNode">The XML node.</param>
        /// <param name="lststrPacketData">The LSTSTR packet data.</param>
        public void ParseMACAddressRev(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {

                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    if (intByteIndex < 7)
                    {
                        //Return the index of the node using the attribute             
                        string hexeValue = strDataPacket[intByteIndex].Replace("0x", "");
                        // intByteValue= Convert.ToInt32(intByteValue.ToString(), 8);
                        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                        //Add the name and value to the list. 
                        if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                        {
                            try
                            {
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + hexeValue.ToString());
                            }
                            catch (Exception ex)
                            {
                                this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Exception Occurred at ParseMACAddressRev() Method InSide For Loop @Attribute: " + Attribute + " @strDataPacket: " + string.Join(",", strDataPacket) + " @lststrPacketData: " + string.Join(",", lststrPacketData) + " @intByteIndex: " + intByteIndex, ex);
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                            }
                        }
                        else
                        {
                            lststrPacketData.Add("=Unknown");
                        }
                    }
                    else
                    {
                        switch (intByteIndex)
                        {
                            //Hardware Revision
                            case 7:
                                intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                                if (intByteValue >= 0 && intByteValue <= 9)
                                {
                                    lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue);
                                }
                                else
                                {
                                    string strHardwareRevision = string.Empty;
                                    if (!strDataPacket[intByteIndex].Equals("0x00"))
                                    {
                                        int ij = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                        byte[] temp2 = new byte[] { (byte)ij };
                                        char[] temp1 = ASCIIEncoding.ASCII.GetChars(temp2);
                                        strHardwareRevision = strHardwareRevision + temp1[0];
                                        lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + strHardwareRevision);

                                    }
                                    else
                                        lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");

                                }
                                break;

                            //Major Firmware Revision
                            case 8:
                                intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                                if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                    lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue);
                                else
                                    lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                                break;

                            //Minor Firmware Revision
                            case 9:
                                intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                                if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                    lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue + " ");
                                else
                                    lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                                break;

                            //Protocol Revision
                            case 10:
                                intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                                if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                    lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue + " ");
                                else
                                    lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                                break;
                            //Model Number
                            case 11:
                                intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                                if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                    lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString().Trim());
                                else
                                    lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                                break;
                            //Gainspan Major Firmware Revision
                            case 12:
                                intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                                if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                    lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue);
                                else
                                    lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                                break;

                            //Gainspan Minor Revision
                            case 13:
                                intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                                intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                                if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                                    lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue + " ");
                                else
                                    lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                                break;
                        }
                    }
                }
            }
        }

        #endregion

        #region MACAddress
        /// <summary>
        /// Parse the MACAddress values
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseMACAddress(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {

            //  string[] array = new string[] { "0x0C", "0x17", "0x0A", "0x2E", "0x4A", "0x20"};
            //   string[] array = new string[] { "0x0E", "0x1B", "0x0C", "0x2E", "0x70", "0x28"};

            //  strDataPacket = array;

            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    //moumita
                    #region MACID converted to oct
                    //Return the index of the node using the attribute             
                    // intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //intByteValue= Convert.ToInt32(intByteValue.ToString(), 8);
                    //intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    #endregion

                    //Return the index of the node using the attribute             
                    string hexeValue = strDataPacket[intByteIndex].Replace("0x", "");
                    // intByteValue= Convert.ToInt32(intByteValue.ToString(), 8);
                    intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                    //Add the name and value to the list. 
                    if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                    {
                        try
                        {
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + hexeValue.ToString());
                        }

                        catch (Exception ex)
                        {
                            this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Exception Occurred at ParseMACAddress() Method InSide For Loop @Attribute: " + Attribute + " @strDataPacket: " + string.Join(",", strDataPacket) + " @lststrPacketData: " + string.Join(",", lststrPacketData) + " @intByteIndex: " + intByteIndex, ex);
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                        }
                    }
                    else
                    {
                        lststrPacketData.Add("=Unknown");
                    }
                }
            }
        }
        #endregion

        #region IPAddress
        /// <summary>
        /// Parse the IPAddress values
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseIPAddress(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {

            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    //Return the index of the node using the attribute             
                    intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //intByteValue = Convert.ToInt32(intByteValue.ToString(), 8);
                    intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                    //Add the name and value to the list. 
                    if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                    {
                        try
                        {
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue.ToString());
                        }

                        catch (Exception ex)
                        {
                            this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Exception Occurred at ParseIPAddress() Method InSide For Loop @Attribute: " + Attribute + " @strDataPacket: " + string.Join(",", strDataPacket) + " @lststrPacketData: " + string.Join(",", lststrPacketData) + " @intByteIndex: " + intByteIndex, ex);
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                        }
                    }
                    else
                    {
                        lststrPacketData.Add("=Unknown");
                    }


                }
            }
        }
        #endregion

        #region IPAddress 5.0
        /// <summary>
        /// Parse the IPAddress values
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseIPAddressV5(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {

            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    //Return the index of the node using the attribute             
                    intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //intByteValue = Convert.ToInt32(intByteValue.ToString(), 8);
                    intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    if (intByteIndex == 0)
                    {
                        if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex] && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes.Count))
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString());
                        else
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                    }
                    //Add the name and value to the list. 
                    else if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                    {
                        try
                        {
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue.ToString());
                        }

                        catch (Exception ex)
                        {
                            this.ObjLog?.WriteLog(LogLevel.Error, "WIFIProtocol: Exception Occurred at ParseIPAddressV5() Method InSide For Loop @Attribute: " + Attribute + " @strDataPacket: " + string.Join(",", strDataPacket) + " @lststrPacketData: " + string.Join(",", lststrPacketData) + " @intByteIndex: " + intByteIndex, ex);
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=Unknown");
                        }
                    }
                    else
                    {
                        lststrPacketData.Add("=Unknown");
                    }


                }
            }
        }
        #endregion

        #region ControlLocation
        /// <summary>
        /// Parse the Control Location values
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseControlLocation(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {

            for (int intByteIndex = 0; intByteIndex < strDataPacket.Length; intByteIndex++)
            {
                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                }
                else
                {
                    //Return the index of the node using the attribute
                    intByteValue = int.Parse(strDataPacket[intByteIndex].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);

                    if (intByteIndex == 6 || intByteIndex == 7)
                    {
                        if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex])
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].ChildNodes[intByteValue].InnerText.ToString().Trim());
                        else
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=DATA");
                    }
                    else
                    {
                        // intByteValue = Encoding.ASCII.GetBytes(intByteValue.ToString())[0]; // ASCII to HEx  then DEC but direct ascii to dec convertion here
                        intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                        lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[intByteIndex].Attributes[1].Value + "=" + intByteValue.ToString());
                    }
                }
            }
        }
        #endregion

        #region Thermostat Name
        /// <summary>
        /// Parsing Thermostat name
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseControlName(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            if (strAction.Equals("COS Setting"))
            {
                DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
            }
            else
            {
                int index = 0;
                string strMessageLine1 = string.Empty;
                string strMessageLine2 = string.Empty;
                bool tempFlag = true;
                for (int i = index; i < strDataPacket.Length; i++)
                {
                    if (!strDataPacket[i].Equals("0x00"))
                    {
                        int ij = int.Parse(strDataPacket[i].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                        byte[] temp2 = new byte[] { (byte)ij };
                        char[] temp1 = ASCIIEncoding.ASCII.GetChars(temp2);
                        if (tempFlag)
                            strMessageLine1 = strMessageLine1 + temp1[0];
                        else
                            break;
                    }
                    else
                    {
                        tempFlag = false;
                    }
                }

                lststrPacketData.Add("Control Name =" + strMessageLine1);

            }
        }
        #endregion

        #region ThermostatLocationName 1.02

        /// <summary>
        /// Parses the name of the thermostat location.
        /// </summary>
        /// <param name="Attribute">The attribute.</param>
        /// <param name="strDataPacket">The string data packet.</param>
        /// <param name="xmlNode">The XML node.</param>
        /// <param name="lststrPacketData">The LSTSTR packet data.</param>
        public void ParseThermostatLocationName(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            if (strAction.Equals("COS Setting"))
            {
                DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
            }
            else
            {
                if (strDataPacket.Length > 0)
                {
                    int index = 0;
                    //intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //for (index = 0; index < 3; index++)
                    //{
                    //    intByteValue = int.Parse(strDataPacket[index].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    //    lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[index].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[index].ChildNodes[intByteValue].InnerText.ToString().Trim());
                    //}

                    string strZipcode = string.Empty;

                    bool tempFlag = true;
                    for (int i = index; i < 8; i++)
                    {
                        if (!strDataPacket[i].Equals("0x00"))
                        {
                            int ij = int.Parse(strDataPacket[i].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            byte[] temp2 = new byte[] { (byte)ij };
                            char[] temp1 = ASCIIEncoding.ASCII.GetChars(temp2);
                            if (tempFlag)
                                strZipcode = strZipcode + temp1[0];
                        }
                        else
                        {
                            tempFlag = false;
                        }
                    }

                    lststrPacketData.Add("ZipCode =" + strZipcode);
                    strZipcode = null;

                    index = 8;
                    string strMessageLine1 = string.Empty;
                    bool tempFlag1 = true;
                    for (int i = index; i < strDataPacket.Length; i++)
                    {
                        if (!strDataPacket[i].Equals("0x00"))
                        {
                            int ij = int.Parse(strDataPacket[i].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                            byte[] temp2 = new byte[] { (byte)ij };
                            char[] temp1 = ASCIIEncoding.ASCII.GetChars(temp2);
                            if (tempFlag1)
                                strMessageLine1 = strMessageLine1 + temp1[0];
                        }
                        else
                        {
                            tempFlag1 = false;
                        }
                    }

                    lststrPacketData.Add("Control Name =" + strMessageLine1);
                    strMessageLine1 = null;
                }
            }

        }
        #endregion

        #region ControlLocation 8.0
        /// <summary>
        /// Parse the Control Location values
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseControlLocationV8(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            if (strAction.Equals("COS Setting"))
            {
                DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
            }
            else
            {
                int index = 0;
                intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                for (index = 0; index < 3; index++)
                {
                    intByteValue = int.Parse(strDataPacket[index].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                    lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[index].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[index].ChildNodes[intByteValue].InnerText.ToString().Trim());
                }

                string strZipcode = string.Empty;

                bool tempFlag = true;
                for (int i = index; i < strDataPacket.Length; i++)
                {
                    if (!strDataPacket[i].Equals("0x00"))
                    {
                        int ij = int.Parse(strDataPacket[i].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                        byte[] temp2 = new byte[] { (byte)ij };
                        char[] temp1 = ASCIIEncoding.ASCII.GetChars(temp2);
                        if (tempFlag)
                            strZipcode = strZipcode + temp1[0];
                    }
                    else
                    {
                        tempFlag = false;
                    }
                }

                lststrPacketData.Add("ZipCode =" + strZipcode);

            }

        }
        #endregion

        #region WiFi Network Status
        /// <summary>
        /// Parsing WiFi Network Status
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseWiFiNetworkStatus(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            if (strAction.Equals("COS Setting"))
            {
                DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
            }
            else
            {
                int index = 0;

                lststrPacketData.Add("Connection Status =" + int.Parse(strDataPacket[index].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                index = 1;
                string strMessageLine1 = string.Empty;
                bool tempFlag = true;
                for (int i = index; i < strDataPacket.Length; i++)
                {
                    if (!strDataPacket[i].Equals("0x00"))
                    {
                        int ij = int.Parse(strDataPacket[i].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                        byte[] temp2 = new byte[] { (byte)ij };
                        char[] temp1 = ASCIIEncoding.ASCII.GetChars(temp2);
                        if (tempFlag)
                            strMessageLine1 = strMessageLine1 + temp1[0];
                        else
                            break;
                    }
                    else
                    {
                        tempFlag = false;
                    }
                }

                lststrPacketData.Add("Connected Network =" + strMessageLine1);
            }
        }
        #endregion

        #region WiFi Network Cofiguration
        /// <summary>
        /// Parsing WiFi Network Configuration
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseWiFiConfiguration(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            if (strAction.Equals("COS Setting"))
            {
                DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
            }
            else
            {
                int index = 0;
                string strMessageLine1 = string.Empty;
                string strMessageLine2 = string.Empty;
                bool tempFlag = true;
                bool flag = true;
                for (int i = index; i < strDataPacket.Length; i++)
                {
                    if (!strDataPacket[i].Equals("0x00"))
                    {
                        int ij = int.Parse(strDataPacket[i].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                        byte[] temp2 = new byte[] { (byte)ij };
                        char[] temp1 = ASCIIEncoding.ASCII.GetChars(temp2);
                        if (tempFlag)
                            strMessageLine1 = strMessageLine1 + temp1[0];
                        else
                        {
                            if (flag)
                            {
                                lststrPacketData.Add("SSID =" + strMessageLine1);
                                lststrPacketData.Add("Encryption Type =" + int.Parse(strDataPacket[i].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
                                flag = false;
                            }
                            else
                            {
                                strMessageLine2 = strMessageLine2 + temp1[0];
                            }
                        }
                    }
                    else
                    {
                        tempFlag = false;
                    }
                }

                lststrPacketData.Add("Password =" + strMessageLine2);
            }

        }
        #endregion

        #region WiFi Configuration Status 8.0
        /// <summary>
        /// Parsing WiFi Network Status
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseWiFiConfigurationStatus(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            if (strAction.Equals("COS Setting"))
            {
                DisplayCOSSetting(xmlNode, lststrPacketData, int.Parse(strDataPacket[0].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber));
            }
            else
            {
                intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                int index = 0;
                intByteValue = int.Parse(strDataPacket[index].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                lststrPacketData.Add("Connection Status =" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[index].ChildNodes[intByteValue].InnerText.ToString().Trim());
                index++;
                intByteValue = int.Parse(strDataPacket[index].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                lststrPacketData.Add("Encryption Type =" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[index].ChildNodes[intByteValue].InnerText.ToString().Trim());
                index++;
                string strSSID = string.Empty;
                string strPassword = string.Empty;
                bool tempFlag = true;
                for (int i = index; i < strDataPacket.Length; i++)
                {
                    if (!strDataPacket[i].Equals("0x00"))
                    {
                        int ij = int.Parse(strDataPacket[i].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                        byte[] temp2 = new byte[] { (byte)ij };
                        char[] temp1 = ASCIIEncoding.ASCII.GetChars(temp2);
                        if (tempFlag)
                            strSSID = strSSID + temp1[0];
                        else
                            strPassword = strPassword + temp1[0];
                    }
                    else
                    {
                        tempFlag = false;
                    }
                }

                lststrPacketData.Add("SSID =" + strSSID);
                lststrPacketData.Add("Password =" + strPassword);
            }
        }
        #endregion

        #region Generic Parse
        /// <summary>
        /// Generic Method to handle the parsing of data of 80% of the Attributes
        /// </summary>
        /// <param name="Attribute"></param>
        /// <param name="strDataPacket"></param>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        public void ParseData(string Attribute, string[] strDataPacket, XmlNode xmlNode, List<string> lststrPacketData)
        {
            for (int i = 0; i < strDataPacket.Length; i++)
            {
                //get the value from the byte
                intByteValue = int.Parse(strDataPacket[i].Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);


                if (strAction.Equals("COS Setting"))
                {
                    DisplayCOSSetting(xmlNode, lststrPacketData, intByteValue);
                }
                else
                {
                    //Return the index of the node using the attribute
                    intIndexOfChildNode = returnIndexOfItem(Attribute, xmlNode);
                    //Add the name and value to the list. 
                    if (null != xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[i])
                    {
                        string TValue = string.Empty;

                        #region code added for min and max attributes

                        if (intByteValue >= 0)
                        {
                            for (int x = 0; x < xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[i].ChildNodes.Count; x++)
                            {

                                if (xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[i].ChildNodes[x].Attributes.Count != 0)
                                {
                                    short min = Convert.ToInt16(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[i].ChildNodes[x].Attributes["min"].Value.ToString());
                                    short max = Convert.ToInt16(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[i].ChildNodes[x].Attributes["max"].Value.ToString());
                                    int ind = 0;

                                    if (intByteValue >= min && intByteValue <= max)
                                    {
                                        TValue = xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[i].ChildNodes[x].InnerText.ToString();
                                        bool IsPrecent = TValue.IndexOf('%') > 0;
                                        ind = TValue.IndexOf('(');
                                        if (ind > 0)
                                            TValue = TValue.Substring(0, ind);
                                        TValue = string.Format("{0} ({1}{2})", TValue.Trim(), intByteValue, IsPrecent ? "%" : string.Empty); //TValue  + " : "+intByteValue;
                                        break;
                                    }
                                }
                            }
                            if (TValue != string.Empty && !TValue.Contains("Others"))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[i].Attributes[1].Value + "=" + TValue);
                            else if (TValue != string.Empty && TValue.Contains("Others"))
                                lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[i].Attributes[1].Value + "=Unknown");
                        }

                        #endregion

                        if (TValue == string.Empty && !(intByteValue >= xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[i].ChildNodes.Count))
                        {
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[i].Attributes[1].Value + "=" + xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[i].ChildNodes[intByteValue].InnerText.ToString().Trim());
                        }
                        else if (TValue == string.Empty)
                            lststrPacketData.Add(xmlNode.ChildNodes[intIndexOfChildNode].ChildNodes[i].Attributes[1].Value + "=Unknown");
                    }
                    else
                    {
                        //Commented by Hemanth  -- if the Next event ttransition occurs then, we are not suppoose to parse the data as there will be no data packets
                        //if we parse, then Unknown will be displayed.
                        if (Attribute != "NextEventTransition")
                        {
                            lststrPacketData.Add("Unknown"); //"DATA=DATA");
                        }
                    }
                }
            }
        }
        #endregion

        #region Cos Settings
        /// <summary>
        /// Cos Settings
        /// </summary>
        /// <param name="xmlNode"></param>
        /// <param name="lststrPacketData"></param>
        /// <param name="intByteValue"></param>
        private void DisplayCOSSetting(XmlNode xmlNode, List<string> lststrPacketData, int intByteValue)
        {
            if (intByteValue == 0)
                lststrPacketData.Add("COS Setting=Remove Subscription");
            else if (intByteValue == 1)
                lststrPacketData.Add("COS Setting=Subscribe");
            else
                lststrPacketData.Add("COS Setting=Unknown");
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Returns the Celsius and Fahrenheit Value converting it from the format 
        /// 
        /// bits 7: Heat Setpoint Sign
        /*0: Positive (Use positive when sending a null value)
        1: Negative
        bits 6: Heat Setpoint ½ Celsius Indicator
        0: “.0” Celsius (Use “.0” when sending a null value)
        1: “.5” Celsius 
        bits 5-0: Heat Setpoint in Celsius (refer to appendix 1 for C / F conversion)
        0: Null
        1-3: Reserved 
        4-32: Heat Setpoint 
        32-63: Reserved*/
        /// </summary>
        /// <param name="byteTemperature"></param>
        /// <returns></returns>
        private string ReturnCelsiusAndFahrenheitValue(byte byteTemperature)
        {

            if (byteTemperature != 0)
            {
                //Converting the Byte to a bitarray
                BitArray bits = new BitArray(new byte[] { byteTemperature });
                string strPosOrNeg = string.Empty;
                string strDecimal = string.Empty;
                string strDegTemp = string.Empty;

                //checking if the bit 7 is is 1 if so its negative else its a positive number
                if (bits[7].Equals(true))
                    strPosOrNeg = "-";

                //checking if the bit 6 is is 1 if so it has a decimal value else its a whole number
                if (bits[6].Equals(true))
                    strDecimal = ".5";

                //setting the th and 6th bit to false so that we can exctract the data from 5th-0th bit
                bits.Set(7, false);
                bits.Set(6, false);

                byte[] bytes = new byte[1];
                bits.CopyTo(bytes, 0);

                //send the value to the FahrenheitCelsiusScale dictionary to get the value.
                strDegTemp = strPosOrNeg + bytes[0].ToString() + strDecimal;
                var Fahrenheit = FahrenheitCelsiusScale.FirstOrDefault(x => x.Value == double.Parse(strDegTemp)).Key;
                temparatureValue = Fahrenheit.ToString();
                //return "=" + Fahrenheit + " F" + " Or " + strDegTemp + " C";
                return string.Format(RASWiFiProtocol.RASProtocol.RASProtocolClass.MessageConstants.strFahrenheitAndDegreeTemparature, Fahrenheit, strDegTemp);
            }
            else
            {
                return RASWiFiProtocol.RASProtocol.RASProtocolClass.MessageConstants.NotApplicableValue; //"= Null";
            }
        }


        /// <summary>
        /// Input Celsius and converts to FahrenheitValue For Micro Response.
        /// </summary>
        /// <param name="byteTemperature"></param>
        /// <returns></returns>
        private string ReturnCelsiusAndFahrenheitValueForMicro(byte byteTemperature)
        {

            //Converting the Byte to a bitarray
            BitArray bits = new BitArray(new byte[] { byteTemperature });
            string strPosOrNeg = string.Empty;
            string strDecimal = string.Empty;
            string strDegTemp = string.Empty;

            //checking if the bit 7 is is 1 if so its negative else its a positive number
            if (bits[7].Equals(true))
                strPosOrNeg = "-";

            //checking if the bit 6 is is 1 if so it has a decimal value else its a whole number
            if (bits[6].Equals(true))
                strDecimal = ".5";

            //setting the th and 6th bit to false so that we can exctract the data from 5th-0th bit
            bits.Set(7, false);
            bits.Set(6, false);

            byte[] bytes = new byte[1];
            bits.CopyTo(bytes, 0);

            //send the value to the FahrenheitCelsiusScale dictionary to get the value.
            strDegTemp = strPosOrNeg + bytes[0].ToString() + strDecimal;
            var Fahrenheit = FahrenheitCelsiusScale.FirstOrDefault(x => x.Value == double.Parse(strDegTemp)).Key;
            temparatureValue = Fahrenheit.ToString();
            //return "=" + Fahrenheit + " F" + " Or " + strDegTemp + " C";
            return string.Format(RASWiFiProtocol.RASProtocol.RASProtocolClass.MessageConstants.strFahrenheitAndDegreeTemparature, Fahrenheit, strDegTemp);

        }
        /// <summary>
        /// Method to return the index of the child node 
        /// </summary>
        /// <param name="strName"></param>
        /// <param name="xmlNode"></param>
        /// <returns></returns>
        private int returnIndexOfItem(string strName, XmlNode xmlNode)
        {
            int intReturnValue = 0;
            for (int i = 0; i < xmlNode.ChildNodes.Count; i++)
            {
                if (xmlNode.ChildNodes.Item(i).Name.Equals(strName))
                {
                    return intReturnValue = i;
                    break;
                }
            }
            return intReturnValue;
        }

        /// <summary>
        /// Converting 2 bytes of data to 1 
        /// </summary>
        /// <param name="byte0"></param>
        /// <param name="byte1"></param>
        /// <returns></returns>
        private int CombineTwobytesAndReturnValue(int byte0, int byte1)
        {
            byte[] array = new byte[] { 0x00, 0x00, (byte)byte0, (byte)byte1 };
            int valueInDecimal = 0;
            if (BitConverter.IsLittleEndian)
                Array.Reverse(array); //need the bytes in the reverse order 
            return valueInDecimal = BitConverter.ToInt32(array, 0);
        }

        /// <summary>
        /// Set Manucaturing test Button code on a byte.
        /// </summary>
        /// <param name="boolMsb"></param>
        /// <param name="boolLsb"></param>
        /// <returns></returns>
        private string CheckClickedValue(bool boolMsb, bool boolLsb)
        {
            if (boolMsb == false && boolLsb == false)
                return RASProtocolClass.MessageConstants.strConstIdle;
            else if (boolMsb == false && boolLsb == true)
                return RASProtocolClass.MessageConstants.strConstClicked;
            else if (boolMsb == true && boolLsb == false)
                return RASProtocolClass.MessageConstants.strConstHold;
            else if (boolMsb == true && boolLsb == true)
                return RASProtocolClass.MessageConstants.strConstReleased;

            return string.Empty;
        }

        #region Old method commented...
        //private string CheckClickedValue(bool boolMsb, bool boolLsb)
        //{
        //    string returnval = string.Empty;
        //    if (boolMsb == false && boolLsb == false)
        //        returnval = " Idle";
        //    else if (boolMsb == false && boolLsb == true)
        //        returnval = " Clicked";
        //    else if (boolMsb == true && boolLsb == false)
        //        returnval = " Hold";
        //    else if (boolMsb == true && boolLsb == true)
        //        returnval = " Released";

        //    return returnval;
        //} 
        #endregion

        #endregion
    }
}
