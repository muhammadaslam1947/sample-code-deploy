﻿
// ***********************************************************************
// Assembly         : RASWiFiProtocol.RASProtocol
// Author           : 20050101
// Created          : 04-20-2014
//
// Last Modified By : 20050101
// Last Modified On : 07-04-2014
// ***********************************************************************
// <copyright file="DataPacket.cs" company="">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace RASWiFiProtocol.RASProtocol
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class DataPacket
    {

        #region Public Fields

        /// <summary>
        /// The data packet bytes
        /// </summary>
        public byte[] DataPacketBytes = new byte[2000];

        #endregion

        #region Private Fields

        /// <summary>
        /// The action
        /// </summary>
        private string action;

        /// <summary>
        /// The functional domain
        /// </summary>
        private string functionalDomain;

        /// <summary>
        /// The attribute
        /// </summary>
        private string attribute;

        /// <summary>
        /// The sequence
        /// </summary>
        private int sequence;

        /// <summary>
        /// The parameter count
        /// </summary>
        private int parameterCount;

        /// <summary>
        /// The data string
        /// </summary>
        private string dataString;

        /// <summary>
        /// The data
        /// </summary>
        private string data;

        /// <summary>
        /// The revision
        /// </summary>
        private int revision;

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the action.
        /// </summary>
        /// <value>
        /// The action.
        /// </value>
        public string Action
        {
            get { return action; }
            set { action = value; }
        }

        /// <summary>
        /// Gets or sets the functional domain.
        /// </summary>
        /// <value>
        /// The functional domain.
        /// </value>
        public string FunctionalDomain
        {
            get { return functionalDomain; }
            set { functionalDomain = value; }
        }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        /// <value>
        /// The attribute.
        /// </value>
        public string Attribute
        {
            get { return attribute; }
            set { attribute = value; }
        }

        /// <summary>
        /// Gets or sets the sequence.
        /// </summary>
        /// <value>
        /// The sequence.
        /// </value>
        public int Sequence
        {
            get { return sequence; }
            set { sequence = value; }
        }
        /// <summary>
        /// Gets or sets the parameter count.
        /// </summary>
        /// <value>
        /// The parameter count.
        /// </value>
        public int ParameterCount
        {
            get { return parameterCount; }
            set { parameterCount = value; }
        }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        public string Data
        {
            get { return data; }
            set { data = value; }
        }

        /// <summary>
        /// Gets or sets the data string.
        /// </summary>
        /// <value>
        /// The data string.
        /// </value>
        public string DataString
        {
            get { return dataString; }
            set { dataString = value; }
        }

        /// <summary>
        /// Gets or sets the revision.
        /// </summary>
        /// <value>
        /// The revision.
        /// </value>
        public int Revision
        {
            get { return revision; }
            set { revision = value; }
        }

        #endregion

    }
}
