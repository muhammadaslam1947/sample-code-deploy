﻿// ***********************************************************************
// Assembly         : Aprilaire.HostController.BusinessDTO
// Author           : 20021497
// Created          : 08-09-2013
//
// Last Modified By : 20021497
// Last Modified On : 08-09-2013
// ***********************************************************************
// <copyright file="Status.cs" company="Aprilaire">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace RASWiFiProtocol.RASProtocol
{
    #region Enums
    /// <summary>
    /// Enumerator GenericStatus
    /// </summary>
    public enum Status
    {
        /// <summary>
        /// The success
        /// </summary>
        [Description("Generic Success", 0)]
        Success = 0,

        /// <summary>
        /// The generic error
        /// </summary>
        [Description("Generic Error", 1)]
        GenericError = 1,

        /// <summary>
        /// The bad CRC
        /// </summary>
        [Description("Bad CRC", 2)]
        BadCRC = 2,

        /// <summary>
        /// The device busy
        /// </summary>
        [Description("Buffer Full or Device Busy", 3)]
        DeviceBusy = 3,

        /// <summary>
        /// The unsupported protocol
        /// </summary>
        [Description("Unsupported protocol Revision", 4)]
        UnsupportedProtocol = 4,

        /// <summary>
        /// The unknown action
        /// </summary>
        [Description("Unknown Action", 5)]
        UnknownAction = 5,

        /// <summary>
        /// The unknown functional domain
        /// </summary>
        [Description("Unknown Functional Domain", 6)]
        UnknownFunctionalDomain = 6,

        /// <summary>
        /// The unknown attribute
        /// </summary>
        [Description("Unknown Attribute", 7)]
        UnknownAttribute = 7,

        /// <summary>
        /// The test mode
        /// </summary>
        [Description("Thermostat cannot accept writes in current application mode (includes error and test modes).", 8)]
        TestMode = 8,

        /// <summary>
        /// The test mode
        /// </summary>
        [Description("Timed out waiting for response.", 9)]
        TimeOut = 9,

        /// <summary>
        /// The test mode
        /// </summary>
        [Description("Unsupported Model.", 10)]
        UnsupportedModel = 10,

        /// <summary>
        /// The value out of range
        /// </summary>
        [Description("Value out of range", 16)] // protocol document its 0x10 so when converted to dec it gives 16
        ValueOutOfRange = 16,

        /// <summary>
        /// The attribute read only
        /// </summary>
        [Description("Attribute Read Only", 17)]
        AttributeReadOnly = 17,


        /// <summary>
        /// The not writeable
        /// </summary>
        [Description("Attribute not writeable in current configuration.", 18)]
        NotWriteable = 18,

        /// <summary>
        /// The not writeable
        /// </summary>
        [Description("Incorrect number or data bytes for the defined attribute in the payload", 19)]
        Incorrectnumber = 19,

        /// <summary>
        /// The not readable
        /// </summary>
        [Description("Attribute not readable (write only)", 32)]
        NotReadable = 32,

        /// <summary>
        /// The not available
        /// </summary>
        [Description("Attribute not available – try later", 33)]
        NotAvailable = 33,

        // <summary>
        /// The not writeable
        /// </summary>
        [Description("Incorrect number or data bytes for the defined attribute in the payload", 34)]
        NotCorrectnumber = 34,

        /// <summary>
        /// The does not support COS subscription
        /// </summary>
        [Description("Attribute does not support COS subscription.  Poll only.", 64)]
        DoesNotSupportCOSSubscription = 64,

        /* commented by hemanth as per the spec. ///// <summary> */
           //The unsupported configuration
           //</summary>
          [Description("Unsupported COS Configuration", 65)]
          UnsupportedConfiguration = 65,

        /// <summary>
        /// The unknown configuration
        /// </summary>
        [Description("Unknown COS Configuration", 66)]
        UnknownConfiguration = 66,

        ////Hemanth added
        ///// <summary>
        ///// Incorrect number or data bytes
        ///// </summary>
        //[Description("Incorrect number or data bytes for the defined attribute in the payload", 66)]
        //Incorrectnumberofdatabytes = 66,


        ///// <summary>
        ///// The COS variable unsubscribed
        ///// </summary>
        //[Description("COS Variable Un subscribed – remove subscription please.", 128)]
        //COSVariableUnsubscribed = 128,

        /// <summary>
        /// The remap to new table
        /// </summary>
        [Description("Future proofing", 255)]
        RemapToNewTable = 255
    }

    //public enum Status
    //{
    //    /// <summary>
    //    /// The success
    //    /// </summary>
    //    [Description("Generic Success", 0)]
    //    Success = 0,

    //    /// <summary>
    //    /// The generic error
    //    /// </summary>
    //    [Description("Generic Error", 1)]
    //    GenericError = 1,

    //    /// <summary>
    //    /// The bad CRC
    //    /// </summary>
    //    [Description("Bad CRC", 2)]
    //    BadCRC = 2,

    //    /// <summary>
    //    /// The device busy
    //    /// </summary>
    //    [Description("Buffer Full or Device Busy", 3)]
    //    DeviceBusy = 3,

    //    /// <summary>
    //    /// The unsupported protocol
    //    /// </summary>
    //    [Description("Unsupported protocol Revision", 4)]
    //    UnsupportedProtocol = 4,

    //    /// <summary>
    //    /// The unknown action
    //    /// </summary>
    //    [Description("Unknown Action", 5)]
    //    UnknownAction = 5,

    //    /// <summary>
    //    /// The unknown functional domain
    //    /// </summary>
    //    [Description("Unknown Functional Domain", 6)]
    //    UnknownFunctionalDomain = 6,

    //    /// <summary>
    //    /// The unknown attribute
    //    /// </summary>
    //    [Description("Unknown Attribute", 7)]
    //    UnknownAttribute = 7,

    //    /// <summary>
    //    /// The test mode
    //    /// </summary>
    //    [Description("Thermostat currently in manufacturing test mode or installer test/set-up.", 8)]
    //    TestMode = 8,

    //    /// <summary>
    //    /// The test mode
    //    /// </summary>
    //    [Description("Timed out waiting for response.", 9)]
    //    TimeOut = 9,

    //    /// <summary>
    //    /// The value out of range
    //    /// </summary>
    //    [Description("Value out of range", 10)] // protocol document its 0x10 so when converted to dec it gives 16
    //    ValueOutOfRange = 10,

    //    /// <summary>
    //    /// The attribute read only
    //    /// </summary>
    //    [Description("Attribute Read Only", 11)]
    //    AttributeReadOnly = 11,


    //    /// <summary>
    //    /// The not writeable
    //    /// </summary>
    //    [Description("Attribute not writeable in current configuration.", 12)]
    //    NotWriteable = 12,

    //    /// <summary>
    //    /// The not writeable
    //    /// </summary>
    //    [Description("Incorrect number or data bytes for the defined attribute in the payload", 13)]
    //    Incorrectnumber = 13,

    //    /// <summary>
    //    /// The not readable
    //    /// </summary>
    //    [Description("Attribute not readable (write only)", 20)]
    //    NotReadable = 20,

    //    /// <summary>
    //    /// The not available
    //    /// </summary>
    //    [Description("Attribute not available – try later", 21)]
    //    NotAvailable = 21,

    //    // <summary>
    //    /// The not writeable
    //    /// </summary>
    //    [Description("Incorrect number or data bytes for the defined attribute in the payload", 22)]
    //    NotCorrectnumber = 22,

    //    /// <summary>
    //    /// The does not support COS subscription
    //    /// </summary>
    //    [Description("Attribute does not support COS subscription.  Poll only.", 40)]
    //    DoesNotSupportCOSSubscription = 40,

    //    /// <summary>
    //    /// The unsupported configuration
    //    /// </summary>
    //    [Description("Unsupported COS Configuration", 41)]
    //    UnsupportedConfiguration = 41,

    //    /// <summary>
    //    /// The unknown configuration
    //    /// </summary>
    //    [Description("Unknown COS Configuration", 42)]
    //    UnknownConfiguration = 42,
       
    //    /// <summary>
    //    /// The remap to new table
    //    /// </summary>
    //    [Description("Future proofing", 255)]
    //    RemapToNewTable = 255


    //}
    #endregion
}
