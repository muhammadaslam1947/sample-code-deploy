﻿// ***********************************************************************
// Assembly         : Aprilaire.HostController.BusinessDTO
// Author           : 20021497
// Created          : 08-01-2013
//
// Last Modified By : 20021497
// Last Modified On : 08-01-2013
// ***********************************************************************
// <copyright file="CommandDTO.cs" company="Aprilaire">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace RASWiFiProtocol.RASProtocol
{
    /// <summary>
    /// Class CommandDTO
    /// </summary>
    public class CommandDTO
    {
        public CommandDTO(int i)
        {
            CommandID = i;
            Count = 0;
            Payload = new PayloadDTO();
            CRC = 0;
            PacketRealLanguage = string.Empty;
            Sequence = 0;// 128;
            Revision = 1;
            strDataValueForExport=string.Empty;
        }
        #region Properties
        /// <summary>
        /// Gets or sets the command ID.
        /// </summary>
        /// <value>The command ID.</value>
        public int CommandID { get; set; }

        /// <summary>
        /// Gets or sets the sequence
        /// </summary>
        /// <value>The sequence.</value>
        public byte Sequence { get; set; }

        /// <summary>
        /// Gets or sets the revision.
        /// </summary>
        /// <value>
        /// The revision.
        /// </value>
        public byte Revision { get; set; }

        /// <summary>
        /// Gets or sets the count.
        /// </summary>
        /// <value>The count.</value>
        public byte Count { get; set; }

        /// <summary>
        /// Gets or sets the payload.
        /// </summary>
        /// <value>The payload.</value>
        public PayloadDTO Payload { get; set; }

        /// <summary>
        /// Gets or sets the CRC.
        /// </summary>
        /// <value>The CRC.</value>
        public byte CRC { get; set; }

        /// <summary>
        /// Gets or sets the packet raw value.
        /// </summary>
        /// <value>The packet raw value.</value>
        public byte[] PacketRawValue { get; set; }

        /// <summary>
        /// Gets or sets the packet real language.
        /// </summary>
        /// <value>The packet real language.</value>
        public string PacketRealLanguage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="CommandDTO"/> is override.
        /// </summary>
        /// <value><c>true</c> if override; otherwise, <c>false</c>.</value>
        public bool Override { get; set; }

        /// <summary>
        /// Gets or sets a value of comma seperated string for export
        /// </summary>
        public string strDataValueForExport  {get;set;}

        #endregion Properties
    }
}
