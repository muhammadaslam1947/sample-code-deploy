﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
//using Aprilaire.HostController.BusinessDTO.EventArguments;

namespace RASWiFiProtocol.RASProtocol
{
    public class CommandConfigDTO
    {
        private int controlId = 0;

        public int ControlID
        {
            get
            {
                return controlId;
            }
            set
            {
                controlId = value;
            }
        }

        public List<ParamDTO> param = new List<ParamDTO>();
    }

    public class ParamDTO
    {
        public ParamDTO(int controlId)
        {
            ControlID = controlId;
        }
        /// <summary>
        /// Event Raised when index gets changed
        /// </summary>
        public event EventHandler DataReconfiguredIndexChanged;

        /// <summary>
        /// Event Raised when value gets changed
        /// </summary>
        public event EventHandler DataReconfiguredValueChanged;

        /// <summary>
        /// Interger object to hold the selected Index
        /// </summary>
        private int intSelectedIndex = 0;

        /// <summary>
        /// Double object to hold the value entered
        /// </summary>
        private double doubleValue = 0;

        /// <summary>
        /// Interger object to hold the parameter ID
        /// </summary>
        public int intParameterID { get; set; }

        /// <summary>
        /// string object to hold the parameter name
        /// </summary>
        public string strParamName { get; set; }

        /// <summary>
        /// Integer object to hold the control ID
        /// </summary>
        private int intControlId = 0;

        /// <summary>
        /// Gets and sets the ControlID
        /// </summary>
        public int ControlID
        {
            get
            {
                return intControlId;
            }
            set
            {
                intControlId = value;
            }
        }

        /// <summary>
        /// Gets and sets the CharValue
        /// </summary>
        private string strCharValue;

        /// <summary>
        /// Gets and sets the SelectedIndex
        /// </summary>
        public int SelectedIndex
        {
            get
            {
                return intSelectedIndex;
            }
            set
            {
                if (this.intSelectedIndex != value)
                {
                    this.intSelectedIndex = value;

                    //if (DataReconfiguredIndexChanged != null)
                    //{
                    //    DataReconfiguredIndexChanged(this, new CommandReConfiguredEventArgs() { ControlID = this.ControlID, ParameterID = this.intParameterID, NewIndex = SelectedIndex, NewValue = this.Value });
                    //}
                }
            }
        }

        /// <summary>
        /// Gets and sets the Value
        /// </summary>
        public double Value
        {
            get
            {
                return doubleValue;
            }

            set
            {
                if (this.doubleValue != value)
                {
                    this.doubleValue = value;

                    //if (DataReconfiguredValueChanged != null)
                    //{
                    //    DataReconfiguredValueChanged(this, new CommandReConfiguredEventArgs() { ControlID = this.ControlID, ParameterID = this.intParameterID, NewIndex = SelectedIndex, NewValue = this.Value });
                    //}
                }
            }
        }

        /// <summary>
        /// Gets and sets the CharValue
        /// </summary>
        public string CharValue
        {
            get
            {
                return strCharValue;
            }

            set
            {
                if (this.strCharValue != value)
                {
                    this.strCharValue = value;

                    //if (DataReconfiguredValueChanged != null)
                    //{
                    //    DataReconfiguredValueChanged(this, new CommandReConfiguredEventArgs() { ControlID = this.ControlID, ParameterID = this.intParameterID, NewIndex = SelectedIndex, strValue = this.strCharValue });
                    //}
                }
            }
        }
    }
}
