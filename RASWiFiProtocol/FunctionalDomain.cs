﻿// ***********************************************************************
// Assembly         : Aprilaire.HostController.BusinessDTO
// Author           : 20021497
// Created          : 08-01-2013
//
// Last Modified By : 20021497
// Last Modified On : 08-01-2013
// ***********************************************************************
// <copyright file="FunctionalDomain.cs" company="Aprilaire">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace RASWiFiProtocol.RASProtocol
{
    /// <summary>
    /// Enumeration of FunctionalDomain
    /// </summary>
    public enum FunctionalDomain
    {
        /// <summary>
        /// The setup
        /// </summary>
        [Description("SetUp", 1)]
        SetUp = 1,

        /// <summary>
        /// The control
        /// </summary>
        [Description("Control", 2)]
        Control = 2,

        /// <summary>
        /// The scheduling
        /// </summary>
        [Description("Scheduling", 3)]
        Scheduling = 3,

        /// <summary>
        /// The alarms
        /// </summary>
        [Description("Alerts", 4)]
        Alerts = 4,

        /// <summary>
        /// The sensors
        /// </summary>
        [Description("Sensors", 5)]
        Sensors = 5,

        /// <summary>
        /// The lockout
        /// </summary>
        [Description("Lockout", 6)]
        Lockout = 6,

        /// <summary>
        /// The status
        /// </summary>
        [Description("Status", 7)]
        Status = 7,

        /// <summary>
        /// The Identification
        /// </summary>
        [Description("Identification", 8)]
        Identification = 8,
        ///// <summary>
        ///// The messaging
        ///// </summary>
        [Description("Messaging", 9)]
        Messaging = 9,

        /// <summary>
        /// The Display
        /// </summary>
        [Description("Display ", 10)]
        Display = 10,

          /// <summary>
        /// The Weather
        /// </summary>
        [Description("Weather", 13)]
        Weather = 13,
        
        /// <summary>
        /// The Firware Update
        /// </summary>
        [Description("FirmwareUpdate", 14)]
        FirmwareUpdate = 14,

        /// <summary>
        /// The Debug Commands
        /// </summary>
        [Description("DebugCommands", 15)]
        DebugCommands = 15

    }
}
