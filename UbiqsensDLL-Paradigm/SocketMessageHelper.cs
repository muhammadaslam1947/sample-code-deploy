﻿namespace RemoteAccessProtocol
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using LnT.UbiqSens.PluginInterfaces;

    /// <summary>The socket message helper.</summary>
    public class SocketMessageHelper
    {
        #region Constants

        /// <summary>The rev.</summary>
        private static readonly int RevByteCount;

        /// <summary>The seq.</summary>
        private static readonly int SeqByteCount;

        /// <summary>The cnt.</summary>
        private static readonly int CntByteCount;

        /// <summary>The rsv.</summary>
        private static readonly int RsvByteCount;

        private static int[] CRCData;

        #endregion
        
        /// <summary>Gets or sets the device address.</summary>
        public string DeviceAddress { get; set; }

        /// <summary>Gets or sets the logger.</summary>
        public ILogger Logger { get; set; }

        /// <summary>Initializes static members of the <see cref="SocketMessageHelper"/> class.</summary>
        static SocketMessageHelper()
        {
            RevByteCount = 1;
            SeqByteCount = 1;
            CntByteCount = 2;
            RsvByteCount = 1;
            CRCData = new[]
              {
                  0, 49, 98, 83, 196, 245, 166, 151, 185, 136, 219, 234, 125, 76, 31,
                  46, 67, 114, 33, 16, 135, 182, 229, 212, 250, 203, 152, 169, 62, 15,
                  92, 109, 134, 183, 228, 213, 66, 115, 32, 17, 63, 14, 93, 108, 251,
                  202, 153, 168, 197, 244, 167, 150, 1, 48, 99, 82, 124, 77, 30, 47,
                  184, 137, 218, 235, 61, 12, 95, 110, 249, 200, 155, 170, 132, 181,
                  230, 215, 64, 113, 34, 19, 126, 79, 28, 45, 186, 139, 216, 233, 199,
                  246, 165, 148, 3, 50, 97, 80, 187, 138, 217, 232, 127, 78, 29, 44, 2,
                  51, 96, 81, 198, 247, 164, 149, 248, 201, 154, 171, 60, 13, 94, 111,
                  65, 112, 35, 18, 133, 180, 231, 214, 122, 75, 24, 41, 190, 143, 220,
                  237, 195, 242, 161, 144, 7, 54, 101, 84, 57, 8, 91, 106, 253, 204,
                  159, 174, 128, 177, 226, 211, 68, 117, 38, 23, 252, 205, 158, 175, 56,
                  9, 90, 107, 69, 116, 39, 22, 129, 176, 227, 210, 191, 142, 221, 236,
                  123, 74, 25, 40, 6, 55, 100, 85, 194, 243, 160, 145, 71, 118, 37, 20,
                  131, 178, 225, 208, 254, 207, 156, 173, 58, 11, 88, 105, 4, 53, 102,
                  87, 192, 241, 162, 147, 189, 140, 223, 238, 121, 72, 27, 42, 193, 240,
                  163, 146, 5, 52, 103, 86, 120, 73, 26, 43, 188, 141, 222, 239, 130,
                  179, 224, 209, 70, 119, 36, 21, 59, 10, 89, 104, 255, 206, 157, 172
              };
        }

        /// <summary>Initializes a new instance of the <see cref="SocketMessageHelper"/> class.</summary>
        /// <param name="logger">The logger.</param>
        /// <param name="deviceAddress">The device address.</param>
        public SocketMessageHelper(ILogger logger, string deviceAddress)
        {
            this.Logger = logger;
            this.DeviceAddress = deviceAddress;
            
            this.Logger.WriteLog(
                LogLevel.Info,
                $"SocketMessageHelper: Initialized @DeviceAddress: {this.DeviceAddress} ");
        }

        /// <summary>The get message.</summary>
        /// <param name="stream">The stream.</param>
        /// <param name="unreadBytes">The unread Buffer.</param>
        /// <returns>The <see cref="byte[]"/>.</returns>
        public List<byte[]> GetMessages(byte[] stream, out byte[] unreadBytes)
        {
            var index = 0;
            var payloadPaddingLength = RevByteCount + SeqByteCount + CntByteCount + RsvByteCount;

            unreadBytes = new byte[] { };
            var messages = new List<byte[]>();

            try
            {
                this.Logger.WriteLog(LogLevel.Info, $"SocketMessageHelper: Checking if stream has Cnt bytes for message #{messages.Count + 1}   @DeviceAddress: {this.DeviceAddress}  ");

                // if stream is long enough to retrieve Cnt Bytes
                while (stream.Length - index >= RevByteCount + SeqByteCount + CntByteCount)
                {
                    var countByte1 = stream[index + RevByteCount + SeqByteCount];
                    var countByte2 = stream[index + RevByteCount + SeqByteCount + 1];

                    this.Logger.WriteLog(LogLevel.Info, $"SocketMessageHelper: Cnt Bytes: {countByte1}-{countByte2}    @DeviceAddress: {this.DeviceAddress}  ");

                    var payloadLength = BitConverter.ToInt16(new byte[] { countByte2, countByte1 }, 0);

                    this.Logger.WriteLog(LogLevel.Info, $"SocketMessageHelper: Calculated payload length: {payloadLength}   @DeviceAddress: {this.DeviceAddress}  ");


                    // if stream has next complete message
                    if (stream.Length - (index + payloadPaddingLength) >= payloadLength)
                    {
                        var messageLength = payloadPaddingLength + payloadLength;
                        this.Logger.WriteLog(LogLevel.Info, $"SocketMessageHelper: Packet has a complete message of {messageLength} bytes.   @DeviceAddress: {this.DeviceAddress}  ");

                        var message = stream.Skip(index).Take(messageLength).ToArray();

                        this.Logger.WriteLog(LogLevel.Info, $"SocketMessageHelper: Parsed message: @Message:{BitConverter.ToString(message)} for message #{messages.Count + 1}   @DeviceAddress: {this.DeviceAddress}  ");

                        // next payload starting point
                        index += messageLength;
                        this.Logger.WriteLog(LogLevel.Info, $"SocketMessageHelper: Moved index to {index} for next message   @DeviceAddress: {this.DeviceAddress}  ");

                        if (this.CheckCrc(message))
                        {
                            this.Logger.WriteLog(LogLevel.Info, $"SocketMessageHelper: Crc matches successfully. Adding message #{messages.Count + 1}: @Message: {BitConverter.ToString(message)} to list   @DeviceAddress: {this.DeviceAddress}  ");
                            messages.Add(message);
                        }
                        else
                        {
                            this.Logger.WriteLog(LogLevel.Info, $"SocketMessageHelper: Crc didn't matched. Ignoring received message #{messages.Count + 1}. @Message: {BitConverter.ToString(message)}.   @DeviceAddress: {this.DeviceAddress}  ");
                        }
                    }
                    else
                    {
                        // inserting all remaining message in unread bytes buffer and moving index to end of stream
                        unreadBytes = stream.Skip(index).ToArray();

                        this.Logger.WriteLog(
                            LogLevel.Info,
                            $"SocketMessageHelper: Incomplete message found. @Message: {BitConverter.ToString(unreadBytes)} @Stream: {BitConverter.ToString(stream)}   @DeviceAddress: {this.DeviceAddress}  ");

                        index = stream.Length;
                    }
                }

                // if stream has remaining bytes
                if (index != stream.Length)
                {
                    unreadBytes = stream.Skip(index).ToArray();

                    this.Logger.WriteLog(
                        LogLevel.Info,
                        $"SocketMessageHelper: Incomplete message found. @Message: {BitConverter.ToString(unreadBytes)} @Stream: {BitConverter.ToString(stream)}   @DeviceAddress: {this.DeviceAddress}  ");
                }
            }
            catch (Exception e)
            {
                this.Logger.WriteLog(LogLevel.Error, $"SocketMessageHelper: GetMessages() threw exception. @Stream: {BitConverter.ToString(stream)} {e.Message}, stack trace: {e.StackTrace}   @DeviceAddress: {this.DeviceAddress}  ", e);
            }

            this.Logger.WriteLog(LogLevel.Info, $"SocketMessageHelper: Read {messages.Count} messages from @Stream: {BitConverter.ToString(stream)}   @DeviceAddress: {this.DeviceAddress}  ");

            return messages;
        }

        /// <summary>The check crc.</summary>
        /// <param name="message">The message.</param>
        /// <returns>The <see cref="bool"/>.</returns>
        public bool CheckCrc(byte[] message)
        {
            try
            {
                byte crc = 0x00;

                for (int i = 0; i < message.Length - RsvByteCount; i++)
                {
                    byte index = (byte)(crc ^ message[i]);
                    crc = (byte)CRCData[index];
                }

                this.Logger.WriteLog(LogLevel.Info, $"SocketMessageHelper: Calculated: {crc}, Received: {message[message.Length - RsvByteCount]}, @Message: {BitConverter.ToString(message)}  @DeviceAddress {this.DeviceAddress}");

                // checking calculated CRC with message RSV
                return crc == message[message.Length - RsvByteCount];
            }
            catch (Exception e)
            {
                this.Logger.WriteLog(LogLevel.Info, $"SocketMessageHelper: CheckCrc() threw exception @Message: {BitConverter.ToString(message)} {e.Message}, stack trace: {e.StackTrace}   @DeviceAddress: {this.DeviceAddress}  ", e);
                throw;
            }
        }
    }
}
