﻿//-----------------------------------------------------------------------
// <copyright file="Ubiqsens.cs" company="L&T TS">
//     Copyright (c) L&T IES. All rights reserved.
// </copyright>
// <author>Moumita Sikdar</author>
//-----------------------------------------------------------------------
namespace RemoteAccessProtocol
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using LnT.UbiqSens.PluginInterfaces;
    using RASWiFiProtocol.RASProtocol;
    using System.Net;

    using LnT.UbiqSens.Helpers;

    /// <summary>
    /// Protocol Container class inherited from I gateway Protocol interface
    /// </summary>
    public class ProtocolContainer : IGatewayProtocol
    {
        #region Public Feilds

        /// <summary>
        /// The colon
        /// </summary>
        public const char Colon = ':';

        /// <summary>
        /// The comma character
        /// </summary>
        public const char CommaChar = ',';

        /// <summary>
        /// The colon string
        /// </summary>
        public const string ColonString = ":";

        /// <summary>
        /// The dollar string
        /// </summary>
        public const string DollarString = "$";

        /// <summary>
        /// The COS
        /// </summary>
        public const string COS = "COS";

        /// <summary>
        /// The mac address
        /// </summary>
        public const string MACAddress = "MACAddress";

        /// <summary>
        /// The read reqfor mac id
        /// </summary>
        public const string ReadReqForMACID = "0:ReadRequest:";

        /// <summary>
        /// The comma
        /// </summary>
        public const string Comma = ",";

        /// <summary>
        /// The mac identifier
        /// </summary>
        public const string MACId = "MACId";

        /// <summary>
        /// The write
        /// </summary>
        public const string Write = "Write";

        /// <summary>
        /// The force connection
        /// </summary>
        public const string ForceConnection = "ForceConnection";

        /// <summary>
        /// The hvac automatic setting
        /// </summary>
        public const string HVACAutoSetting = "HVACAutoSetting";

        /// <summary>
        /// The messages
        /// </summary>
        public const string Message = "Message";


        /// <summary>
        /// The revision model
        /// </summary>
        public const string RevisionModel = "RevisionModel";

        /// <summary>
        /// The true
        /// </summary>
        public const string True = "True";

        /// <summary>
        /// The false
        /// </summary>
        public const string False = "False";

        /// <summary>
        /// The ElventhBit
        /// </summary>
        public const string ElventhBit = "ElventhBit";

        /// <summary>
        /// The Source
        /// </summary>
        public const string SOURCE = "Source";

        /// <summary>
        /// The SourcePort
        /// </summary>
        public const string SOURCEPORT = "SourcePort";

        /// <summary>
        /// The PROXY
        /// </summary>
        public const string PROXY = "PROXY";

        /// <summary>
        /// The Automation
        /// </summary>
        public const string AUTOMATION = "Automation";

        /// <summary>The unread bytes.</summary>
        public new byte[] unreadBytes = new byte[] {};

        /// <summary>The device address.</summary>
        public string DeviceAddress = string.Empty;

        /// <summary>
        /// The EndStreamProxy
        /// </summary>
        public const string ENDSTREAMPROXY = "\r\n";

        /// <summary>
        ///  UNKNOWN
        /// </summary>
        public const string UNKNOWN = "UNKNOWN";

        /// <summary>
        /// The RASProtocolClass object
        /// </summary>
        public RASProtocolClass RASPobj;

        /// <summary>
        /// The object gateway connection
        /// </summary>
        public IGatewayConnection ObjGatewayConn;

        /// <summary>
        /// The Logger Object
        /// </summary>
        public ILogger ObjLog = null;

        #endregion

        #region Public Methods
        /// <summary>
        /// Gets the command.
        /// </summary>
        /// <param name="cmdName">Name of the command.</param>
        /// <param name="value">The value.</param>
        /// <returns>a byte array containing packet data</returns>
        public byte[] GetCommand(string cmdName, string value)
        {
            try
            {
                this.RASPobj = new RASProtocolClass(this.ObjLog);
                string[] strDataArray = new string[3];
                //if(!string.IsNullOrEmpty(value))
                strDataArray = value.Split(Colon);
                if (strDataArray.Length >= 2)
                {
                    if (strDataArray[1] == Write)
                    {
                        if (strDataArray.Length != 3)
                            return Encoding.ASCII.GetBytes(value);
                        else if (string.IsNullOrEmpty(strDataArray[2]))
                            return Encoding.ASCII.GetBytes(value);
                    }
                    int intSequence = int.Parse(strDataArray[0]);
                    int intRevision = int.Parse(strDataArray[1]);
                    string strAction = strDataArray[2];
                    // if(strDataArray[2]!=null)
                    return this.RASPobj.ConvertToRASProtocol(intSequence, intRevision, strAction, cmdName, strDataArray[3]);

                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                this.ObjLog?.WriteLog(LogLevel.Error, "UBIQSENSDLL: Error Occurred at GetCommand() Method with @CommandName: " + cmdName + " @Value: " + value + " @DeviceAddress: " + this.DeviceAddress, ex);
                throw ex;
            }
        }

        /// <summary>
        /// Initializes the specified object gateway connection.
        /// </summary>
        /// <param name="objGatewayConnection">The object gateway connection.</param>
        /// <returns>boolean true or false</returns>
        public bool Initialize(IGatewayConnection objGatewayConnection, ILogger objLogger)
        {
            this.ObjGatewayConn = objGatewayConnection;
            this.ObjLog = objLogger;
            return true;
        }

        /// <summary>
        /// Parses the specified reply data list.
        /// </summary>
        /// <param name="replyDataList">The reply data list.</param>
        /// <param name="cmdName">Name of the command.</param>
        /// <returns>List of object of class parsed data </returns>
        public List<ParsedData> Parse(List<byte[]> replyDataList, string cmdName)
        {
            try
            {
                List<ParsedData> objParsedDataList = new List<ParsedData>();
                this.RASPobj = new RASProtocolClass(ObjLog);
                
                //ZipCode response is the first data packet expected from Automation board connected to 58766 port. 
                if (cmdName == AUTOMATION)
                {
                    this.ObjLog?.WriteLog(LogLevel.Info, "UBIQSENSDLL: ZipCode response from Automation board with total bytes. @FullResponse " + BitConverter.ToString(replyDataList[0]));

                    List<byte[]> receivedDataBytes = this.GetDataBytesWithoutProxy(replyDataList);

                    //Expected to contain the ZipCode response in the first element of list.
                    replyDataList[0] = receivedDataBytes[0];
                    this.ObjLog?.WriteLog(LogLevel.Info, "UBIQSENSDLL: ZipCode response from Automation board after proxy parsing if any. @DeviceAddress: " + this.DeviceAddress + " @response " + BitConverter.ToString(replyDataList[0]));

                }
                
                #region Parsing Logic
                var helper = new SocketMessageHelper(this.ObjLog, this.DeviceAddress);
                var parsedMessages = new List<byte[]>();

                this.ObjLog?.WriteLog(LogLevel.Info, $"UBIQSENSDLL: Total packets for parsing {replyDataList.Count} @DeviceAddress: {this.DeviceAddress}");

                foreach (var item in replyDataList)
                {
                    this.ObjLog?.WriteLog(LogLevel.Info, $"UBIQSENSDLL: Previous unread bytes: {BitConverter.ToString(this.unreadBytes)}, Bytes received: {BitConverter.ToString(item)}  @DeviceAddress: {this.DeviceAddress}");

                    var stream = this.unreadBytes.Concat(item).ToArray();

                    this.ObjLog?.WriteLog(LogLevel.Info, $"UBIQSENSDLL: Complete stream after concatenation @Stream: {BitConverter.ToString(stream)}, Bytes received: {BitConverter.ToString(item)}  @DeviceAddress: {this.DeviceAddress}");

                    var messages = helper.GetMessages(stream, out this.unreadBytes);

                    parsedMessages.AddRange(messages);

                    this.ObjLog?.WriteLog(LogLevel.Info, $"UBIQSENSDLL: Messages received after after parsing: {messages.Count}, incomplete message bytes: @UnreadBytes: {BitConverter.ToString(this.unreadBytes)}  @DeviceAddress: {this.DeviceAddress}");
                }

                this.ObjLog?.WriteLog(LogLevel.Info, $"UBIQSENSDLL: Total parsed messages: {parsedMessages.Count}  @DeviceAddress: {this.DeviceAddress}");

                #endregion

                string strNotificationData = string.Empty;
                ParsedData parsedDataobj = null;

                foreach (var dataBytes in parsedMessages)
                {
                    string strParsedData = this.RASPobj.ReadRASWiFiProtocol(dataBytes);
                    strParsedData = WebUtility.HtmlEncode(strParsedData);
                    string[] parsedSplittedData = strParsedData.Split(':');

                    if (parsedSplittedData.Length > 5)
                    {
                        string strAction = parsedSplittedData[0];
                        if (strAction == COS)
                        {
                            //parsedDataobj.ParsedDataType = ParsedValueType.Notification;

                            if (!string.IsNullOrEmpty(strNotificationData))
                            {
                                strNotificationData = strNotificationData + "$" + parsedSplittedData[1] + ColonString + parsedSplittedData[2] + ColonString + parsedSplittedData[3] + ColonString + parsedSplittedData[5];
                            }
                            else
                            {
                                strNotificationData = parsedSplittedData[1] + ColonString + parsedSplittedData[2] + ColonString + parsedSplittedData[3] + ColonString + parsedSplittedData[5];

                            }
                        }
                        else
                        {
                            parsedDataobj = new ParsedData();
                            parsedDataobj.ParsedDataType = ParsedValueType.Response;
                            parsedDataobj.Data = parsedSplittedData[1] + ColonString + parsedSplittedData[2] + ColonString + parsedSplittedData[3] + ColonString + parsedSplittedData[5];
                            objParsedDataList.Add(parsedDataobj);
                        }

                        // parsedDataobj.Data = parsedSplittedData[1] + ColonString + parsedSplittedData[2] + ColonString + parsedSplittedData[3];
                        // objParsedDataList.Add(parsedDataobj);
                        ////dataList.Add(RASProtocol.DataPacket.attribute + ":" + RASProtocol.DataPacket.dataString);  
                    }
                    else
                    {
                        this.ObjLog?.WriteLog(LogLevel.Warn, "UBIQSENSDLL: Data length is not proper in parsedSplittedData, @parsedSplittedDataLength" + parsedSplittedData.Length + " @strParsedData: " + strParsedData + " @DeviceAddress: " + this.DeviceAddress);
                    }
                }

                if (!string.IsNullOrEmpty(strNotificationData))
                {
                    parsedDataobj = new ParsedData();
                    parsedDataobj.ParsedDataType = ParsedValueType.Notification;
                    parsedDataobj.Data = strNotificationData;
                    objParsedDataList.Add(parsedDataobj);
                }

                return objParsedDataList;
            }
            catch (Exception ex)
            {
                this.ObjLog?.WriteLog(LogLevel.Error, "UBIQSENSDLL: Error Occurred at Parse() Method with @CommandName: " + cmdName + " @DeviceAddress: " + this.DeviceAddress, ex);
                throw ex;
            }

        }

        /// <summary>
        /// Releases this instance.
        /// </summary>
        /// <returns>returns true</returns>
        public bool Release()
        {
            return true;
        }

        /// <summary>
        /// Validates the device.
        /// </summary>
        /// <returns>dictionary having MAcId and the comma separated data</returns>
        public Dictionary<string, string> ValidateDevice()
        {
            try
            {
                string[] splittedString;
                string[] splittedMACID;
                string strMacId = string.Empty;
                string strRevModel = string.Empty;
                string strForceConnection = string.Empty;
                string hvacAutoSetting = string.Empty;

                List<ParsedData> responseString = null;

                Dictionary<string, string> validateDict = new Dictionary<string, string>();
                List<byte[]> receivedBytes = new List<byte[]>();
                //byte[] receivedBytes = null;
                int intRetryCount = 0; int intResponseReadCount = 0;
                bool blnIsRunning = true, isRetryRequired = false;

                do
                {
                    if (intRetryCount == 3)
                        break;
                    isRetryRequired = false;
                    //// form the hex packet of MACAddress with sequence=0 and revision =1 for ReadRequest
                    //this.ObjGatewayConn.Send(this.GetCommand(MACAddress, ReadReqForMACID));

                    do
                    {
                        if (intResponseReadCount == 3)
                            break;

                        Thread.Sleep(1000); /// We are assuming all the datas for mac id response will come within a sec from device.
                        receivedBytes = this.ObjGatewayConn.SyncReceive();

                        if (receivedBytes != null && receivedBytes.Count > 0)
                        {
                            this.ObjLog?.WriteLog(LogLevel.Info, "UBIQSENSDLL: Data Received from Device for MacAddress Response: " + BitConverter.ToString(receivedBytes[0]));

                            //extracting SourceIpifo  & main data part for parsing
                            List<byte[]> receivedDataBytes = GetDataBytesWithoutProxy(receivedBytes);
                            this.ObjLog?.WriteLog(LogLevel.Info, "UBIQSENSDLL: The main data part received for Mac Address Response is " + BitConverter.ToString(receivedDataBytes[0]) + " @DeviceAddress: " + this.DeviceAddress);

                            responseString = this.Parse(receivedDataBytes, MACAddress);

                            if (responseString != null && responseString.Count > 0)
                            {
                                foreach (var item in responseString)
                                {
                                    this.ObjLog?.WriteLog(LogLevel.Info, "DEBUG:UBIQSENSDLL: MacID Response - COS - After Parse is " + item.Data + " @DeviceAddress: " + this.DeviceAddress);

                                    splittedString = item.Data.Split(Colon);
                                    if (splittedString.Length >= 2)
                                    {
                                        if (splittedString[0] == MACAddress)
                                        {
                                            splittedMACID = splittedString[1].Split(CommaChar);

                                            if (splittedMACID.Length == 8 || splittedMACID.Length == 14)
                                            {
                                                for (int count = 0; count < 6; count++)
                                                {
                                                    strMacId = strMacId + splittedMACID[count];
                                                }
                                                this.ObjLog?.WriteLog(LogLevel.Info, "UBIQSENSDLL: MAC Id after parsing : " + strMacId + " @DeviceAddress: " + this.DeviceAddress);
                                                if (splittedMACID.Length == 8)
                                                {

                                                    if (Convert.ToInt32(splittedMACID[6]) == 1)
                                                    {
                                                        strForceConnection = True;
                                                    }
                                                    else
                                                    {
                                                        strForceConnection = False;
                                                    }

                                                    if (Convert.ToInt32(splittedMACID[7]) == 1)
                                                    {
                                                        hvacAutoSetting = "Automation";
                                                    }
                                                    else
                                                    {
                                                        hvacAutoSetting = "HVAC";
                                                    }
                                                }
                                                else if (splittedMACID.Length == 14)
                                                {
                                                    if (Convert.ToInt32(splittedMACID[6]) == 1)
                                                    {
                                                        hvacAutoSetting = "Automation";
                                                    }
                                                    else
                                                    {
                                                        hvacAutoSetting = "HVAC";
                                                    }

                                                    for (int count = 7; count < 14; count++)
                                                    {
                                                        if (count == 7)
                                                        {
                                                            strRevModel = splittedMACID[count];
                                                        }
                                                        else
                                                        {
                                                            strRevModel = strRevModel + Comma + splittedMACID[count];
                                                        }
                                                    }

                                                    if ((splittedMACID[11]) == "SecondaryZone1" || (splittedMACID[11]) == "SecondaryZone2")
                                                    {
                                                        string strModelIdentifier = splittedMACID[11] == "SecondaryZone2" ? "26" : "25";
                                                        strMacId += strModelIdentifier;
                                                    }

                                                }
                                                else
                                                {
                                                    this.ObjLog?.WriteLog(LogLevel.Warn, "UBIQSENSDLL: MAC Id response from thermostat invalid @MAcIdLength: " + splittedMACID.Length + " @ReceivedBytes: " + BitConverter.ToString(receivedBytes[0]) + " @DeviceAddress: " + this.DeviceAddress);
                                                }
                                            }

                                            if (!string.IsNullOrEmpty(strRevModel))
                                            {
                                                strRevModel = strRevModel + Comma + splittedString[3];
                                            }
                                            validateDict.Add(MACId, strMacId);
                                            validateDict.Add(ForceConnection, strForceConnection);
                                            validateDict.Add(HVACAutoSetting, hvacAutoSetting);
                                            validateDict.Add(RevisionModel, strRevModel);
                                            validateDict.Add(Message, string.Empty);
                                            validateDict.Add(SOURCE, this.DeviceAddress);

                                            if (splittedMACID.Length == 14)
                                            {
                                                validateDict.Add(ElventhBit, splittedMACID[11]);
                                            }
                                            else
                                            {
                                                this.ObjLog?.WriteLog(LogLevel.Warn, "UBIQSENSDLL: MAC Id response length mismatch" + " @DeviceAddress: " + this.DeviceAddress);
                                                validateDict.Add(ElventhBit, string.Empty);
                                            }

                                        }
                                        else
                                        {
                                            // Alert status data packet comes immediatly right after the MacId response for some devices that leads MacId validation failure
                                            // and forcefully closing the socket connection
                                            if (validateDict.ContainsKey(Message))  // Added the validation for non-MacId packets
                                            {
                                                if (validateDict[Message] == string.Empty)
                                                {
                                                    validateDict[Message] += item.Data;
                                                }
                                                else
                                                    validateDict[Message] += "$" + item.Data;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        validateDict.Add(MACId, string.Empty);
                                        validateDict.Add(ForceConnection, string.Empty);
                                        validateDict.Add(HVACAutoSetting, string.Empty);
                                        validateDict.Add(RevisionModel, string.Empty);
                                        validateDict.Add(Message, string.Empty);
                                        validateDict.Add(ElventhBit, "Else Condtion");
                                        validateDict.Add(SOURCE, this.DeviceAddress);

                                        isRetryRequired = true;
                                        break;
                                    }
                                }
                                if (!isRetryRequired)
                                    return validateDict;
                                else
                                    break;
                            }
                        }

                        intResponseReadCount++;
                    } while (blnIsRunning);

                    intRetryCount++;
                    intResponseReadCount = 0;
                } while (blnIsRunning);

                validateDict.Add(MACId, string.Empty);
                validateDict.Add(ForceConnection, string.Empty);
                validateDict.Add(HVACAutoSetting, string.Empty);
                validateDict.Add(RevisionModel, string.Empty);
                validateDict.Add(Message, string.Empty);
                validateDict.Add(ElventhBit, "After while");
                validateDict.Add(SOURCE, this.DeviceAddress);

                return validateDict;
                // responseString = this.Parse(receivedBytes, MACAddress);    

            }
            catch (Exception ex)
            {
                this.ObjLog?.WriteLog(LogLevel.Error, "UBIQSENSDLL: Error Occurred at ValidateDevice() Method, " + " @DeviceAddress: " + this.DeviceAddress + " @Ex: ", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Extract Proxy Ip and original data part the device.
        /// </summary>
        /// <returns>list with extracted bytes array</returns>
        List<byte[]> GetDataBytesWithoutProxy(List<byte[]> receivedBytes)
        {
            /*
             Extracting SourceIpifo - PROXY V1 parsing is handled for TCP/IPv4 protocol 
             PROXY V1 example : PROXY TCP4 172.128.32.34 182.125.32.38 65535 65535\r\n , unknown connection (short form) :
            "PROXY UNKNOWN\r\n"
            */
            try
            {
                if (receivedBytes != null && receivedBytes.Count > 0)
                {
                    int intReceivedbyteLength = receivedBytes[0].Length;
                    if (intReceivedbyteLength > PROXY.Length)
                    {
                        string completeData = Encoding.UTF8.GetString(receivedBytes.First(), 0, intReceivedbyteLength);

                        if (!string.IsNullOrEmpty(completeData) && completeData.ToUpper().StartsWith(PROXY))
                        {
                            string[] strSplittedStringArray = completeData.Split(new String[] { ENDSTREAMPROXY }, StringSplitOptions.None);

                            if (strSplittedStringArray.Length > 1)
                            {
                                string[] strProxyStringArray = strSplittedStringArray[0].Split(' ');
                                if (strProxyStringArray.Length > 1)
                                {
                                    if (strProxyStringArray.Length > 5)
                                    {
                                        this.DeviceAddress = strProxyStringArray[2] + "_" + strProxyStringArray[4];

                                    }
                                    else if (strProxyStringArray.Length > 3)
                                    {
                                        this.DeviceAddress = strProxyStringArray[2] + "_" + SOURCEPORT;
                                    }
                                    else
                                    {
                                        this.DeviceAddress = UNKNOWN;
                                    }
                                    var totalLength = strSplittedStringArray[0].Length + 2;

                                    var mainDataArray = receivedBytes.First().ToList().Skip(totalLength).ToArray();
                                    this.ObjLog?.WriteLog(LogLevel.Info, "UBIQSENSDLL: GetDataBytesWithoutProxy - @DeviceAddress: " + this.DeviceAddress + " @destinationData: " + BitConverter.ToString(mainDataArray));
                                    var recByte = new List<byte[]>();
                                    recByte.Add(mainDataArray);
                                    mainDataArray = null;
                                    strSplittedStringArray = null;
                                    strProxyStringArray = null;
                                    return recByte;
                                }
                                else
                                {
                                    this.ObjLog?.WriteLog(LogLevel.Warn, "UBIQSENSDLL: GetDataBytesWithoutProxy - Invalid Proxy header. @completeData: " + completeData);
                                }
                            }
                        }
                        else
                        {
                            this.ObjLog?.WriteLog(LogLevel.Info, "UBIQSENSDLL: GetDataBytesWithoutProxy - Missing Proxy header. @completeData: " + completeData);
                        }
                    }
                }
                else
                {
                    this.ObjLog?.WriteLog(LogLevel.Warn, "UBIQSENSDLL: GetDataBytesWithoutProxy ReceivedBytes List is empty. @receivedBytes.Count: " + receivedBytes.Count);
                }
                return receivedBytes;

            }
            catch (Exception ex)
            {
                this.ObjLog?.WriteLog(LogLevel.Error, "UBIQSENSDLL: The error occured GetDataBytesWithoutProxy method @ReceivedBytes: " + BitConverter.ToString(receivedBytes[0]) + " @DeviceAddress: " + this.DeviceAddress + " @ex: " + ex);
                throw ex;
            }
        }

        #endregion
    }
}