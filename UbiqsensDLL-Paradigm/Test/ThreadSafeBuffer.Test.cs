﻿namespace LnT.UbiqSens.Helpers.Test
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using LnT.Ubiqsens.LoggerPlugins.Log4NetLogger;

    /// <summary>The socket message helper test.</summary>
    public class ThreadSafeBufferTest
    {

        /// <summary>The test parsing logic.</summary>
        public static void EnqueuDequeueLogicTest()
        {
            var queue = new ConcurrentQueue<byte[]>();
            var messages = new List<byte[]>(){new byte[] {0x00, 0x01}, new byte[] { 0x02, 0xff } };

            messages.ForEach(x => queue.Enqueue(x));

            var messagesString = new StringBuilder();
            var buf = new List<byte[]>();

            while (queue.TryDequeue(out byte[] message))
            {
                buf.Add(message);
                messagesString.Append($" {BitConverter.ToString(message)}, ");
            }
        }
    }
}
