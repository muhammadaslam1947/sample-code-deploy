﻿//-----------------------------------------------------------------------
// <copyright file="Log4NetLogger.cs" company="L&T">
//     Copyright (c) L&T. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LnT.Ubiqsens.LoggerPlugins.Log4NetLogger
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using LnT.UbiqSens.PluginInterfaces;
    using log4net;
    using log4net.Repository.Hierarchy;
    // //**********************************************************************************************
    //// Class Name  : Log4NetLogger
    //// Purpose: Class used for creating customized error/info log files or an error/info can be registered as a log entry
    ////               
    //// Modification History: Initial build
    ////  Ver # 1.0         Date:10 April 2014    Author/Modified By:kaliprasad M    Remarks:UBIQSens uses this class to log error or info
    ////----------------------------------------------------------------------------------------------
    ////                   
    ////***********************************************************************************************

    /// <summary>
    /// Class logger
    /// </summary>
    public class Log4NetLogger : LnT.UbiqSens.PluginInterfaces.ILogger
    {
        #region private variables

        /// <summary>
        /// ILog object
        /// </summary>
        private static ILog logger = LogManager.GetLogger(typeof(Log4NetLogger));

        /// <summary>
        /// To check the initialization
        /// </summary>
        private bool isIntialized = false;

        #endregion

        #region constructor

        /////// <summary>
        /////// Initializes new instance of the Logger class 
        /////// </summary>
        ////static Log4NetLogger()
        ////{

        ////}

        #endregion constructor

        /// <summary>
        /// Initializing the logger
        /// </summary>
        /// <param name="logPath">log path</param>
        /// <param name="filename">File name</param>
        /// <param name="append">To append</param>
        /// <param name="maxFileSizeInMb">File size in MB</param>
        /// <returns>true or false</returns>
        public bool Initialise(string logPath, string filename, bool append, int maxFileSizeInMb, string allowedLogLevel)
        {
            if (logPath == null)
            {
                throw new Exception("Path cannot be empty");
            }

            if (filename == null)
            {
                throw new Exception("filename cannot be empty");
            }

            if (maxFileSizeInMb == 0)
            {
                throw new Exception("maximum File Size cannot be zero");
            }

            var loggerName = typeof(Log4NetLogger).FullName;
            Hierarchy hierarchy = (log4net.Repository.Hierarchy.Hierarchy)LogManager.GetRepository();
            var logger = (log4net.Repository.Hierarchy.Logger)log4net.LogManager.GetRepository().GetLogger(loggerName);
            var ilogger = log4net.LogManager.GetRepository().GetLogger(loggerName);
            switch (allowedLogLevel.ToLower())
            {
                case "all":
                    hierarchy.Root.Level = log4net.Core.Level.All;
                    break;
                case "info":
                    hierarchy.Root.Level = log4net.Core.Level.Info;
                    break;
                case "debug":
                    hierarchy.Root.Level = log4net.Core.Level.Debug;
                    break;
                case "error":
                    hierarchy.Root.Level = log4net.Core.Level.Error;
                    break;
                case "warn":
                    hierarchy.Root.Level = log4net.Core.Level.Warn;
                    break;
                default:
                    hierarchy.Root.Level = log4net.Core.Level.All;
                    break;
            }
            hierarchy.Configured = true;

            ////Add the default log appender if none exist
            if (logger.Appenders.Count == 0)
            {
                var directoryName = logPath;

                ////If the directory doesn't exist then create it
                if (!Directory.Exists(directoryName))
                {
                    Directory.CreateDirectory(directoryName);
                }

                var fileName = Path.Combine(directoryName, filename);

                ////Create the rolling file appender
                var appender = new log4net.Appender.RollingFileAppender
                {
                    Name = "RollingFileAppender",
                    File = fileName,
                    StaticLogFileName = true,
                    AppendToFile = append,
                    RollingStyle = log4net.Appender.RollingFileAppender.RollingMode.Size,
                    MaxSizeRollBackups = maxFileSizeInMb,
                    MaximumFileSize = maxFileSizeInMb.ToString() + "MB"
                };

                ////Configure the layout of the trace message write
                var layout = new log4net.Layout.PatternLayout()
                {
                    ConversionPattern = "%date{MM/dd/yy HH:mm:ss.fff} [%thread] %-5level - %message%newline"
                };
                appender.Layout = layout;
                layout.ActivateOptions();

                ////Let log4net configure itself based on the values provided
                appender.ActivateOptions();
                log4net.Config.BasicConfigurator.Configure(appender);
            }

            return this.isIntialized = true;
        }

        /// <summary>
        /// Write log
        /// </summary>
        /// <param name="logLevel">log level</param>
        /// <param name="log">String log message</param>
        public void WriteLog(LogLevel logLevel, string log, Exception ex)
        {
            if (!this.isIntialized)
            {
                throw new Exception("Initialization of Log4NetLogger is required");
            }

            string functionname = string.Empty;
            string classname = string.Empty;

            if (logLevel.Equals(LogLevel.Debug))
            {
                logger.Debug(log + classname + functionname);
            }
            else if (logLevel.Equals(LogLevel.Error))
            {
                logger.Error(log + classname + functionname, ex);
            }
            else if (logLevel.Equals(LogLevel.Fatal))
            {
                logger.Fatal(log + classname + functionname, ex);
            }
            else if (logLevel.Equals(LogLevel.Info))
            {
                logger.Info(log);
            }
            else if (logLevel.Equals(LogLevel.Warn))
            {
                logger.Warn(log + classname + functionname);
            }
        }
    }
}
