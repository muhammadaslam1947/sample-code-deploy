﻿//-----------------------------------------------------------------------
// <copyright file="EthernetGatewayHandler.cs" company="L&T">
//     Copyright (c) L&T. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Concurrent;
using System.Net.NetworkInformation;
using System.Text;

namespace LnT.UbiqSens.Plugins.EthernetGatewayHandler
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using LnT.UbiqSens.PluginInterfaces;

    /// <summary>
    /// Class Ethernet gateway handler
    /// </summary>
    public class EthernetGatewayHandler : IGatewayConnection
    {
        /// <summary>
        /// socket object gateway handler
        /// </summary>
        private Socket socketGatewayHandler;

        private bool isError = false;

        public ConcurrentStack<string> ErrorMessage = new ConcurrentStack<string>();

        string deviceAddress = string.Empty;

        public ILogger LogObj = null;
        /// <summary>
        /// Array of received bytes
        /// </summary>
        private List<byte[]> recBytes;

        private StateObject state = new StateObject { BufferSize = 2048 };

        /// <summary>
        /// Initializes the connection
        /// </summary>
        /// <param name="objSocket">socket object</param>
        /// <returns>true or false</returns>
        public bool Initialize(TcpClient objSocket, ILogger logObj)
        {
            if (objSocket == null)
            {
                LogObj.WriteLog(LogLevel.Error, "EthernetGateWay: CP Error in Communication Plugin Socket Object is NULL");
                return false;
            }

            try
            {
                LogObj = logObj;
                this.recBytes = new List<byte[]>();
                this.socketGatewayHandler = objSocket.Client;
                this.socketGatewayHandler.DontFragment = true;
                this.socketGatewayHandler.SendTimeout = int.MaxValue;
                this.socketGatewayHandler.ReceiveTimeout = int.MaxValue;
                deviceAddress = IPAddress.Parse(((IPEndPoint)objSocket.Client.RemoteEndPoint).Address.ToString()) + ":" + ((IPEndPoint)objSocket.Client.RemoteEndPoint).Port.ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        public bool InitializeAsync()
        {
            /**********************************AsynReceive**************************/
            try
            {
                this.socketGatewayHandler.BeginReceive(state.Buffer, 0, state.BufferSize, 0, this.ReadCallback, state);
            }
            catch (Exception ex)
            {
                LogObj.WriteLog(LogLevel.Error, "EthernetGateWay: CP Error in InitializeAsync. @Ex: " + ex.Message);
                return false;
            }
            /**********************************AsynReceive**************************/
            return true;
        }


        /// <summary>
        /// Sends the command to the device
        /// </summary>
        /// <param name="cmd">byte array</param>
        /// <returns>true or false</returns>
        public bool Send(byte[] cmd)
        {
            bool issent = false;

            if (this.socketGatewayHandler != null)
            {
                try
                {
                    this.socketGatewayHandler.Send(cmd);
                    LogObj.WriteLog(LogLevel.Info, deviceAddress + "EthernetGateWay:  CP Data Sent to Device: " + BitConverter.ToString(cmd));
                    issent = true;
                }
                catch (Exception ex)
                {
                    LogObj.WriteLog(LogLevel.Error, deviceAddress + "EthernetGateWay: CP Error in Communication Plugin -Send() Method @Cmd: " + BitConverter.ToString(cmd));
                    ErrorMessage.Push(ex.Message);
                    isError = true;
                }
            }
            else
            {
                throw new NullReferenceException();
            }

            return issent;
        }

        /// <summary>
        /// Receive the data from the device
        /// </summary>
        /// <returns>byte array</returns>
        public List<byte[]> Receive()
        {
            /**********************************AsynReceive**************************/
            var buf = this.recBytes.ToList();
            this.recBytes.Clear();
            return buf;
            /**********************************AsynReceive**************************/
        }

        /// <summary>
        /// Receive the data from the device
        /// </summary>
        /// <returns>byte array</returns>
        public List<byte[]> SyncReceive()
        {
            // Receive the response from the remote device.
            /**********************************AsynReceive**************************/
            // Data buffer for incoming data.
            var recByte = new List<byte[]>();
            byte[] bytes = new byte[1024];
            int bytesRec = this.socketGatewayHandler.Receive(bytes);
            if (bytesRec > 0)
            {
                Array.Resize(ref bytes, bytesRec);
                recByte.Add(bytes);
            }
            return recByte;
            /**********************************AsynReceive**************************/
        }

        /// <summary>
        /// disconnect the connection from the device
        /// </summary>
        /// <returns>true or false</returns>
        public bool Disconnect()
        {
            bool isDisconnected = false;
            if (this.socketGatewayHandler == null)
            {
                throw new NullReferenceException();
            }
            try
            {

                if (this.socketGatewayHandler.Connected)
                {
                    this.socketGatewayHandler.Disconnect(false);
                    this.socketGatewayHandler.Close();
                    this.socketGatewayHandler.Dispose();
                    this.socketGatewayHandler = null;
                    isDisconnected = true;
                }
            }
            catch (Exception e)
            {
                LogObj.WriteLog(LogLevel.Error, deviceAddress + "EthernetGateWay: CP Error in Communication Plugin - Disconnect() Method. @Ex: " + e.Message, e);
                ErrorMessage.Push(e.Message);

                isError = true;
            }
            return isDisconnected;
        }

        /// <summary>
        /// Is connected
        /// </summary>
        /// <returns>returns true or false</returns>
        public bool IsConnected()
        {
            if (socketGatewayHandler == null)
            {
                LogObj.WriteLog(LogLevel.Error, "EthernetGateWay: Socket  object is NULL ");
                return false;
            }

            if (!(socketGatewayHandler.Connected && !isError))
            {
                //LogObj.WriteLog(LogLevel.Error, "Socket is not Connected isError ");
                return false;
            }
            return true;
            //try
            //{
            //    if (socketGatewayHandler.Poll(0, SelectMode.SelectRead)
            //        && socketGatewayHandler.Available == 0)
            //    {
            //        LogObj.WriteLog(LogLevel.Error, "Socket is not Connected - POLL ");
            //        return false;
            //    }
            //    else
            //    {
            //        return true;
            //    }

            //}
            //catch (SocketException)
            //{
            //    LogObj.WriteLog(LogLevel.Error, "Socket exception ");
            //    return false;
            //}
        }

        /// <summary>
        /// Asynchronous read call back
        /// </summary>
        /// <param name="ar">Asynchronous result</param>
        private void ReadCallback(IAsyncResult ar)
        {
            try
            {
                var state = (StateObject)ar.AsyncState;
                if (this.socketGatewayHandler != null)
                {
                    if (this.socketGatewayHandler.Connected)
                    {
                        int bytesRead = this.socketGatewayHandler.EndReceive(ar);
                        if (bytesRead > 0)
                        {
                            byte[] rec = state.Buffer;
                            Array.Resize(ref rec, bytesRead);
                            this.recBytes.Add(rec);
                            LogObj.WriteLog(LogLevel.Info,  "EthernetGateWay: CP Data Received as Notification CP: " + BitConverter.ToString(rec) + " @DeviceAddress" + deviceAddress);
                            //if (QueueManager.GateMap.ContainsKey(DeviceId))
                            //    QueueManager.GateMap[DeviceId].Set();
                        }

                        if (socketGatewayHandler.Poll(0, SelectMode.SelectRead)
                            && socketGatewayHandler.Available == 0)
                        {
                            LogObj.WriteLog(LogLevel.Error, "EthernetGateWay: Socket is not Connected - POLL ");
                            ErrorMessage.Push("EthernetGateWay: Socket is not Connected - POLL ");
                            isError = true;
                            return;
                        }

                        var stateObj = new StateObject { BufferSize = 2048 };
                        this.socketGatewayHandler.BeginReceive(stateObj.Buffer, 0, stateObj.BufferSize, 0, this.ReadCallback, stateObj);
                    }
                }
            }
            catch (Exception ex)
            {
                LogObj.WriteLog(LogLevel.Error, deviceAddress + "EthernetGateWay:  CP Error in Communication Plugin Read Call Back, ReadCallback() Method. @Ex: " + ex.Message, ex);
                ErrorMessage.Push(ex.Message);
                isError = true;
            }
        }


        public string GetLastError()
        {
            string result = String.Empty;
            if (ErrorMessage.TryPop(out result))
                return result;
            return string.Empty;
        }

        private string deviceId;
        public string DeviceId
        {
            get
            {
                return deviceId;
            }
            set
            {
                deviceId = value;
            }
        }

        public string DeviceAddress
        {
            get
            {
                return deviceAddress;
            }

            set
            {
                deviceAddress = value;
            }
        }

        public string Cid { get; set; }
    }
}